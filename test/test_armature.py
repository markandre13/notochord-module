# -- START OF COPYRIGHT & LICENSE NOTICES -- 
#
# Chordata Motion's Notochord (python module)
# -- Core implementation of Chordata Motion's Open Source motion capture framework
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2021 Bruno Laurencich
# Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
#
# This file is part of Chordata Motion's Notochord (python module).
#
# Chordata Motion's Notochord (python module) is free software: you can 
# redistribute it and/or modify it under the terms of the 
# GNU General Public License as published by the Free Software Foundation, 
# either version 3 of the License, or (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata Motion's Notochord (python module).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

import pytest
import json
from os.path import join, dirname, realpath
from notochord.math import Matrix4, Quaternion, Vector

# ========================== TEST ARMATURE ==========================
#
# In this test file several pre-recorded armature animation are used as ground truth data 
# to assert the notochord Armature transforms.
#
# ====================================================================

@pytest.fixture
def armature():
    """Returns a basic armature
    This armature is a simple 9 bone armature. It was exported from blender with the following settings:
        - Export format: glTF embedded
        - Export selected objects only
        - +Y Up: False
        - Animation: No animation""" 
    from notochord.armature import Armature
    gltf_path = realpath(join(dirname(__file__), 'data/armature_files/armature_test.gltf'))
    return Armature("test_armature", gltf_path)

@pytest.fixture
def masha_mocap_data():
    """This armature was taken from an optical motion capture system. 
    It was exported from blender file test/data/armature_files/masha_animation_baked.blend
    with the following settings:
        - Export format: glTF embedded
        - Export selected objects only
        - +Y Up: False
        - Animation: No animation

    A Json file was extracted from the animation, it contains:
    - global_transforms Matrix exported as row list 
    - global_rotations Quaternions exported as a list 
    - local_transforms Matrix exported as row list
    """
    from notochord.armature import Armature
    gltf_path = realpath(join(dirname(__file__), 'data/armature_files/masha_avatar.gltf'))

    # Parse the JSON file
    json_path = realpath(join(dirname(__file__), 'data/armature_files/masha_transforms.json'))

    with open(json_path, 'r') as f:
        json_data = json.load(f)

    return Armature("masha_avatar", gltf_path), json_data

@pytest.fixture
def avatar_mocap_data():
    """This armature was taken from a recording with the chordata motion capture system. 
    It's a dump of a calibration sequence containing raw sensor data and KCeptor orientations.
    The blender addon version 1.2.0 and the pose calibration version 0.1.1 were used

    It was exported from blender file test/data/armature_files/avatar_animation.blend
    with the following settings:
        - Export format: glTF embedded
        - Export selected objects only
        - +Y Up: False
        - Animation: No animation

    The sensor data was recorded in CSV format, and calibration indexes were exported from the Blender addon
    The pose-calibration algorithm was executed with these inputs to obtain the pre and post calibration quaternions
    """
    from notochord.armature import Armature
    gltf_path = realpath(join(dirname(__file__), 'data/armature_files/avatar.gltf'))
    
    json_path = realpath(join(dirname(__file__), 'data/armature_files/avatar.json')) 
    pos_csv = realpath(join(dirname(__file__),'data/armature_files/avatar_rotation_pos.csv'))
    pre_csv = realpath(join(dirname(__file__),'data/armature_files/avatar_rotation_pre.csv'))
    dump_csv = realpath(join(dirname(__file__),'data/armature_files/avatar_dump.csv'))

    # Parse the JSON file
    with open(json_path, 'r') as f:
        json_transforms = json.load(f)
    # Parse the CSV files
    with open(pos_csv, 'r') as f:
        pos_data = f.readlines()
    with open(pre_csv, 'r') as f:
        pre_data = f.readlines()
    with open(dump_csv, 'r') as f:
        dump_csv = f.readlines()

    calibration_data_pos = {}
    calibration_data_pre = {}


    # Save the data in a dictionary with the bone name as the key (node_label,w,x,y,z)
    for line in pos_data[1:]:
        line = line.split(',')

        calibration_data_pos[line[0]] = Quaternion(float(line[1]), float(line[2]), float(line[3]), float(line[4]))
    for line in pre_data[1:]:
        line = line.split(',')
        calibration_data_pre[line[0]] = Quaternion(float(line[1]), float(line[2]), float(line[3]), float(line[4]))

    dump_data = {}
    for line in dump_csv[1:]:
        line = line.split(',')
        
        if line[0] not in dump_data:
            dump_data[line[0]] = {} # Timestamp - Bone name
        
        dump_data[line[0]][line[1]] = Quaternion(float(line[2]), float(line[3]), float(line[4]), float(line[5])) # Bone name - Quaternion


    return Armature("test_avatar", gltf_path), json_transforms, calibration_data_pos, calibration_data_pre, dump_data


def test_bone_creation(Bone):
    """Test Bone creation and basic properties."""

    b1 = Bone(None, "test_bone_1")

    # Assert b1.transform = identity Matrix4    
    assert b1.local_transform == Matrix4()
    assert b1.global_transform == Matrix4()

    # Transform matrix translated by (1, 2, 3)
    local_transform = Matrix4(1, 0, 0, 1,
                              0, 1, 0, 2,
                              0, 0, 1, 3,
                              0, 0, 0, 1)
    b2 = Bone(b1, "test_bone_2", local_transform)

    assert b2.local_transform == local_transform

    # Assert b2.parent == b1
    assert b2.parent == b1

def equals(m1, m2):
    for i in range(4):
        for j in range(4):
            assert m1[i, j] == pytest.approx(m2[i, j], abs=1e-5)

def to_mat4(matrix_rows):
    return Matrix4(*matrix_rows[0], *matrix_rows[1], *matrix_rows[2], *matrix_rows[3])

def test_simplified(Bone):
    """Test basic Bone transformations."""
        
    a = Bone(None , "A")
    b = Bone(a, "B")

    identity = Matrix4()

    assert a.local_transform == identity
    assert b.local_transform == identity

    assert a.global_transform == identity
    assert b.global_transform == identity

    translation = Vector(1, 2, 3)

    a.translate_local(translation)
    a.update()

    matrix_translation = Matrix4(1, 0, 0, translation.x,
                                 0, 1, 0, translation.y,
                                 0, 0, 1, translation.z,
                                 0, 0, 0, 1)

    assert a.local_transform == matrix_translation
    assert b.local_transform == identity

    assert a.global_transform == matrix_translation
    assert b.global_transform == matrix_translation

    rotation = Quaternion(0.70711, 0.70711, 0, 0)

    a.set_global_rotation(rotation)
    a.update()

    matrix_rotation = Matrix4(1, 0, 0, 0,
                            0, 0,-1, 0,
                            0, 1, 0, 0,
                            0, 0, 0, 1)
                            
    equals(a.local_transform, matrix_translation * matrix_rotation)
    assert b.local_transform == identity

    equals(a.global_transform, matrix_translation * matrix_rotation)
    equals(b.global_transform, matrix_translation * matrix_rotation)

    b.set_global_rotation(Quaternion())
    b.update()

    equals(a.global_transform, matrix_translation * matrix_rotation)
    equals(b.global_transform, matrix_translation)

    equals(b.local_transform, matrix_rotation.inverse())

    b.set_global_translation(Vector(2, 2, 3))
    b.update()

    assert b.global_translation == Vector(2, 2, 3)

#from notochord.config import Output_Sink
def test_complex(notochord_init, def_conf, armature, types):    
    """Test complex Bone transformations.
    This test uses a simplified armature created in blender and exported as .gltf.
    1. We assert the armature's bones global translation with known values.
    2. We perform several rotations to the bones and for each one we assert the armature's bones global translation
    against values taken from the blender file."""
    
    def_conf.comm.transmit = types.Output_Sink.NONE
    notochord_init.setup(def_conf)
    
    assert armature.bones["base"].global_translation == Vector(0, 0, 0)
    assert armature.bones["dorsal"].global_translation == Vector(0, 4, 0)
    assert armature.bones["head"].global_translation == Vector(0, 12, 0)

    assert armature.bones["l-upperleg"].global_translation == Vector(4, -4, 0)
    assert armature.bones["l-lowerleg"].global_translation == Vector(4, -12, 0)
    assert armature.bones["l-foot"].global_translation == Vector(4, -12, 4)

    assert armature.bones["r-upperleg"].global_translation == Vector(-4, -4, 0)
    assert armature.bones["r-lowerleg"].global_translation == Vector(-4, -12, 0)
    assert armature.bones["r-foot"].global_translation == Vector(-4, -12, 4)

    # Set dorsal's global rotation to 90 degrees around X
    armature.bones["dorsal"].set_global_rotation(Quaternion(0.70711, 0.70711, 0, 0))
    armature.update()

    assert armature.bones["base"].global_translation == Vector(0, 0, 0)
    assert armature.bones["dorsal"].global_translation == Vector(0, 4, 0)
    assert tuple(armature.bones["head"].global_translation) == pytest.approx((0, 4, 8), abs=0.001)

    # Assert head local_transform matrix is unchanged
    assert armature.bones["head"].local_transform == Matrix4(1, 0, 0, 0,
                                                             0, 1, 0, 8,     
                                                             0, 0, 1, 0,
                                                             0, 0, 0, 1)
                                                            
    # Set dorsal's global rotation to 90 degrees around X and 45 degrees around Y
    armature.bones["dorsal"].set_global_rotation(Quaternion(0.653282, 0.653282, 0.270598, -0.270598))
    armature.update()

    assert tuple(armature.bones["head"].global_translation) == pytest.approx((5.6569, 4, 5.6569), abs=0.001)

    # Set dorsal's global rotation to 90 degrees around X and 45 degrees around Y and 45 degrees around Z
    armature.bones["dorsal"].set_global_rotation(Quaternion(0.707107, 0.5, 0.5, 0))
    armature.update()

    # Assert head global position is changed according to its parent's global rotation
    assert tuple(armature.bones["head"].global_translation) == pytest.approx((4, 8, 5.657), abs=0.001)
    assert tuple(armature.bones["head"].global_rotation) == pytest.approx((0.7071, 0.5, 0.5, 0), abs=0.001)

    # Set r-upperleg's global rotation to -45 degrees around Z and 20 degrees around X
    armature.bones["r-upperleg"].set_global_rotation(Quaternion(0.909844, 0.16043, 0.066452, -0.37687))
    armature.update()

    # Assert r-lowerleg global position is changed according to its parent's global rotation
    assert tuple(armature.bones["r-lowerleg"].global_translation) == pytest.approx((-9.6569, -9.3158, -1.9348), abs=0.001)

    # Assert r-lowerleg global rotation is changed according to its parent's global rotation
    assert tuple(armature.bones["r-lowerleg"].global_rotation) == pytest.approx((0.9098, 0.1604, 0.0664, -0.3769), abs=0.001)

    # Assert r-foot global position is changed according to its parent's global rotation
    assert tuple(armature.bones["r-foot"].global_translation) == pytest.approx((-9.6568, -10.6837, 1.8240), abs=0.001)

    # Assert r-foot global rotation is changed according to its parent's global rotation
    assert tuple(armature.bones["r-foot"].global_rotation) == pytest.approx((0.9098, 0.1604, 0.0664, -0.3769), abs=0.001)

    # Set base's global rotation to 90, 20, 80 degrees around X, Y, Z
    armature.bones["base"].set_global_rotation(Quaternion(0.612372, 0.45452, 0.541675, 0.353553))
    armature.update()

    # Assert dorsal global position is changed according to its parent's global rotation
    assert tuple(armature.bones["dorsal"].global_translation) == pytest.approx((0.2375, 1.3472, 3.7587), abs=0.001)

    # Assert dorsal global rotation is changed according to its parent's global rotation
    assert tuple(armature.bones["dorsal"].global_rotation) == pytest.approx((-0.0651, 0.451, 0.866, 0.206), abs=0.001)

def test_armature_keyframes(notochord_init, def_conf,masha_mocap_data, types):
    """
    In this test ground truth data coming from an optical mocap recording is used.
    Global and local transforms for each bone were extracted and exported in Json format
    During the test the global rotation of each bone is set.
    The local resulting rotation is compared against the ground truth data.
    """
    def_conf.comm.transmit = types.Output_Sink.NONE
    notochord_init.setup(def_conf)

    masha_avatar, json_data = masha_mocap_data
    
    assert len(json_data["global_transforms"]) == len(json_data["local_transforms"])

    for i in range(len(json_data["global_transforms"])):
        for bone in json_data["global_transforms"][i]:
            #global_transform = to_mat4(json_data["global_transforms"][i][bone])
            global_rotation = Quaternion(*json_data["global_rotations"][i][bone])
            local_transform = to_mat4(json_data["local_transforms"][i][bone])

            #armature_blender.bones[bone].set_global_transform(global_transform)
            masha_avatar.bones[bone].set_global_rotation(global_rotation)
            masha_avatar.update()
            equals(masha_avatar.bones[bone].local_transform, local_transform)

def test_livestream_keyframes(notochord_init, def_conf, avatar_mocap_data, types):
    """
    In this test ground thruth data coming from a recording using the Chordata Motion capture system
    Global rotations for each bone are obtained from the raw rotation of each KCeptor++ and multiplied by
    the calibration quaternions.
    Same data is applied to an armature inside Blender and the global and local transforms are extracted and 
    exported in JSON format.
    During the test the global rotation (from calibrated KCeptor++ data) of each bone is set.
    The local resulting rotation and global translation are compared against the ground truth data.
    """   
    def_conf.comm.transmit = types.Output_Sink.NONE
    notochord_init.setup(def_conf)
    avatar, json_transforms, calibration_data_pos, calibration_data_pre, dump_data = avatar_mocap_data

    json_data = {}
    for i in range(len(json_transforms["timestamp"])):
        timestamp = json_transforms["timestamp"][i]
        json_data[timestamp] = {}
        json_data[timestamp]["global_transforms"] = json_transforms["global_transforms"][i]
        json_data[timestamp]["global_rotations"] = json_transforms["global_rotations"][i]
        json_data[timestamp]["local_transforms"] = json_transforms["local_transforms"][i]

    assert len(json_transforms["global_transforms"]) == len(json_transforms["local_transforms"])

    temp_hierarchy = ['base', 'lumbar', 'dorsal', 'neck', 'head',
                    'r-upperleg', 'r-lowerleg', 'r-foot',
                    'l-upperleg', 'l-lowerleg', 'l-foot',
                    'r-clavice', 'r-upperarm', 'r-lowerarm', 'r-hand',
                    'l-clavice', 'l-upperarm', 'l-lowerarm', 'l-hand']
    
    for bone in calibration_data_pre:
        avatar.bones[bone].set_pre(calibration_data_pre[bone])
        avatar.bones[bone].set_post(calibration_data_pos[bone])

    for timestamp in json_data:
        for bone in temp_hierarchy:
            if bone not in dump_data[str(timestamp)]:
                continue
            raw_quat = Quaternion(*dump_data[str(timestamp)][bone])
            # pre_quat = calibration_data_pre[bone]
            # pos_quat = calibration_data_pos[bone]
            global_rotation = raw_quat # pre_quat * raw_quat * pos_quat
            local_transform = to_mat4(json_data[timestamp]["local_transforms"][bone])
            global_transform = to_mat4(json_data[timestamp]["global_transforms"][bone])
            avatar.bones[bone].set_global_rotation(global_rotation)

            translation = global_transform[0, 3], global_transform[1, 3], global_transform[2, 3]
            avatar.update()
            assert tuple(translation) == pytest.approx(tuple(avatar.bones[bone].global_translation), abs=1e-3)

            equals(avatar.bones[bone].local_transform, local_transform)