# -- START OF COPYRIGHT & LICENSE NOTICES -- 
#
# Chordata Motion's Notochord (python module)
# -- Core implementation of Chordata Motion's Open Source motion capture framework
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2021 Bruno Laurencich
# Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
#
# This file is part of Chordata Motion's Notochord (python module).
#
# Chordata Motion's Notochord (python module) is free software: you can 
# redistribute it and/or modify it under the terms of the 
# GNU General Public License as published by the Free Software Foundation, 
# either version 3 of the License, or (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata Motion's Notochord (python module).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

import pytest
from random import randint, random
  
from os import path
import csv


def test_import(notochord_init):
	"""The class name should be exposed, but not the independent submodules modules"""
	with pytest.raises(ImportError): 
		from notochord.fusion import madgwick
	
	with pytest.raises(ImportError): 
		from notochord.fusion import kalman
	
	from notochord.fusion import Madgwick
	from notochord.fusion import Kalman

		
@pytest.fixture
def Madgwick():
	from notochord.fusion import Madgwick
	return Madgwick

@pytest.fixture
def Kalman():
	from notochord.fusion import Kalman
	return Kalman

def test_Madgwick_instance(Quaternion, Madgwick):
	M = Madgwick()
	assert M.beta > 0 and M.beta < 1
	assert M.sample_freq >= 50 and M.sample_freq < 300

	M.beta = 0.345
	M.sample_freq = 234

	assert M.beta == pytest.approx(0.345)
	assert M.sample_freq == 234

	assert type(M.q) == Quaternion
	assert M.q == Quaternion(0.707106828689575, 0.0, 0.0, 0.707106828689575)

	M.update(0,1,10, 20,16,80, 200, 0, 3)
	assert M.q != Quaternion(0.707106828689575, 0.0, 0.0, 0.707106828689575)


def test_Madgwick_properties(notochord_init, Madgwick):
	M = Madgwick()

	with pytest.raises(TypeError):
		M.beta = "0.1"

	with pytest.raises(TypeError):
		M.sample_freq = "50"

def test_Kalman_instance(notochord_init, Kalman):
	K = Kalman(2)
	# The Kalman calibration axis convention is rotated in relation to the one used by the notochod
	assert (0.0, 0.0, 1.0, 0) == pytest.approx(tuple(K.q))
	assert K.loopcounter == 0

	for i in range(50):
		random_values = (randint(-1000, 1000) for _ in range(9))
		K.run(*random_values)

	#print(K.q)
	assert (0.0, 0.0, 1.0, 0) != pytest.approx(tuple(K.q))

	assert K.loopcounter == 50

def test_Kalman_errors(notochord_init, Kalman, error):

	with pytest.raises(ValueError):
		Kalman(oversample_ratio = 10)

	with pytest.raises(ValueError):
		K = Kalman(sensor_axis="HOTDOG")

	K = Kalman(2)
	with pytest.raises(ValueError):
		K.set_sensor_res(2.0,0.5,0.5) #"The sensor resolution should be 0 < res < 1"

	with pytest.raises(ValueError):
		K.set_sensor_res(0.5,2.0,0.5) #"The sensor resolution should be 0 < res < 1"

	with pytest.raises(ValueError):
		K.set_sensor_res(0.5,0.5,2.0) #"The sensor resolution should be 0 < res < 1"


def test_Kalman_properties(notochord_init, Kalman):
	K = Kalman(2)
	g_res, a_res, m_res = (K.gyro_res, K.accel_res, K.gyro_res)

	K.gyro_res = g_res / 2
	assert K.gyro_res == g_res / 2

	K.mag_res = m_res / 2
	assert K.mag_res == m_res / 2

def test_Kalman_mag_calibration(notochord_init, Kalman):
	from os.path import join, realpath, dirname
	from notochord.utils import convert_LSM9DS1_ENU_degs
	import csv
	csv_file = join(dirname(realpath(__file__)),'data/fake_spheroid_data.csv')

	K = Kalman()

	assert K.m_offset == (0,0,0)
	assert K.m_matrix == [[1,0,0],[0,1,0],[0,0,1]]

	csv_lines = []
	with open(csv_file, newline='') as csvfile:
		reader = csv.reader(csvfile)
		for row in reader:
			converted = tuple(int(num) for num in row)
			csv_lines.append(converted)
			K.mag_calib(*converted)

	random_indexes = (randint(0, K.mag_buf.iMagBufferCount-1) for _ in range(10))
	mag_buf = tuple(K.mag_buf.iBs)

	for i in random_indexes:
		# print(i, mag_buf[i])
		assert mag_buf[i] in csv_lines

	assert K.m_offset != (0,0,0)
	assert K.m_matrix != [[1,0,0],[0,1,0],[0,0,1]]

def test_kc_apply_mag_calibration(Kalman, Mock_KCeptor, Matrix3):

	offset = (1,2,3)
	matrix = tuple([[1,0,0],[0,1,0],[0,0,1]])

	kalman = Kalman()

	kalman.m_offset = offset
	kalman.m_matrix = matrix

	kc = Mock_KCeptor(None, 0x1b, "mock_kc")
	kc.m_offset = offset
	kc.m_matrix = Matrix3() # Identity Matrix

	inc = 10

	values = tuple(randint(-1000, 5000) for xyz in range(3)) 
	for i in range(20):		
		kalman.set_mag(*values)
		kalman.process()
		#print("")
		if i > 3:
			assert kalman.mag_sensor.iBcAvg == pytest.approx(kc.apply_mag_calib(*values), rel=1)
		#print("kalman {}".format(kalman.avg_calib_measurement))
		#print("kc {}".format(kc.apply_mag_calib(*values)))
		values = (values[0] + randint(-inc, inc), values[1] + randint(-inc, inc), values[2] + randint(-inc, inc))
	

def test_Kalman_sensor_axis_RAW(notochord_init, Kalman):
	"""Test the sensor entries get filled in the correct order for a 'RAW' sensor_axis argument.
	The entries should follow the original order"""
	K = Kalman(8, 8, sensor_axis = 'RAW') #Kalman object with a oversample_ratio = 8
	
	N_entries = 6 #should be minor than the oversample_ratio for the purpuoses of this test
	#generate random 9 values entries (3x gyro, accel and mag)
	values = tuple(tuple(randint(-500, 500) for xyz in range(9)) for entries in range(N_entries) )
	
	for entry in values:
		K.set_gyro(*entry[:3])
		K.set_accel(*entry[3:6])
		K.set_mag(*entry[6:9])
		K.process()

	assert len(tuple(K.gyro_buffer)) == N_entries

	for i,entry in enumerate(values):
		assert entry[:3] == tuple(K.gyro_buffer)[i]
		assert entry[3:6] == tuple(K.accel_buffer)[i]
		assert entry[6:9] == tuple(K.mag_buffer)[i]

def test_Kalman_sensor_axis_LSM9DS1(notochord_init, Kalman):
	"""Test the sensor entries get filled in the correct order for a 'LSM9DS1' sensor_axis argument.
	The entries should follow get shuffled in this order:
	|  gy,  gx,   gz|
	| -ay, -ax,  -az|
	|  mx, -my,   mz|
	"""
	K = Kalman(8, 8, sensor_axis = 'LSM9DS1') #Kalman object with a oversample_ratio = 8
	
	N_entries = 6 #should be minor than the oversample_ratio for the purpuoses of this test
	#generate random 9 values entries (3x gyro, accel and mag)
	values = tuple(tuple(randint(-500, 500) for xyz in range(9)) for entries in range(N_entries) )
	
	for entry in values:
		K.set_gyro(*entry[:3])
		K.set_accel(*entry[3:6])
		K.set_mag(*entry[6:9])
		K.process()

	assert len(tuple(K.gyro_buffer)) == N_entries

	for i,entry in enumerate(values):
		assert (entry[1], entry[0], entry[2]) == tuple(K.gyro_buffer)[i]
		assert (-entry[4], -entry[3], -entry[5]) == tuple(K.accel_buffer)[i]
		assert (entry[7], -entry[6], entry[8]) == tuple(K.mag_buffer)[i]

def test_Kalman_arguments(notochord_init, Kalman):
	with pytest.raises(TypeError):
		Kalman(sensor_axis = 10)

	with pytest.raises(ValueError):
		Kalman(sensor_axis = 'Foo')


def test_Kalman_set_calib_RAW(notochord_init, Kalman):
	K = Kalman(sensor_axis = 'RAW')

	to_set_offset = tuple(randint(-500, 500) for _ in range(3))

	K.m_offset = to_set_offset

	assert pytest.approx(K.m_offset) == to_set_offset

	to_set_matrix = [[1,0,0],[0,1,0],[0,0,1]]
	for i, row in enumerate(to_set_matrix):
		for j, num in enumerate(row):
			to_set_matrix[i][j] += (random() - 0.5) * 0.01 #add random variation to matrix

	to_set_matrix = tuple(tuple(n for n in row) for row in to_set_matrix )		

	K.m_matrix = to_set_matrix

	for i, row in enumerate(K.m_matrix): 
		#compare each row since pytest.approx doesn't support nested structures
		assert row == pytest.approx(to_set_matrix[i])


def test_Kalman_set_calib_LSM9DS1(notochord_init, Kalman):
	K = Kalman(sensor_axis = 'LSM9DS1')

	to_set_offset = tuple(randint(-500, 500) for _ in range(3))

	K.m_offset = to_set_offset

	assert pytest.approx(K.m_offset) == to_set_offset

	to_set_matrix = [[1,0,0],[0,1,0],[0,0,1]]
	for i, row in enumerate(to_set_matrix):
		for j, num in enumerate(row):
			to_set_matrix[i][j] += (random() - 0.5) * 0.01 #add random variation to matrix

	to_set_matrix = tuple(tuple(n for n in row) for row in to_set_matrix )		
	#print(to_set_matrix)

	K.m_matrix = to_set_matrix

	for i, row in enumerate(K.m_matrix): 
		#compare each row since pytest.approx doesn't support nested structures
		assert row == pytest.approx(to_set_matrix[i])

def quat_dot(left, right):            
	return left.x * right.x + left.y * right.y + left.z * right.z + left.w * right.w
  
def test_Kalman_run_csv_data(Kalman, Quaternion):
	''' Run the Kalman Filter on a pre-recorded CSV file containing RAW data 
	and previous sensor fusion results.	'''

	csv_filename = "KC_DUMP_flat_on_table_mag_nord_25Hz.csv"
	csv_filepath = path.realpath(path.join(path.dirname(__file__),"data/"+csv_filename))

	K = Kalman(25, 1, sensor_axis = 'RAW', enable_mag_correct = False)
	SENSITIVITY_ACCELEROMETER_8 = 0.000244
	SENSITIVITY_MAGNETOMETER_4 = 0.00014 
	SENSITIVITY_GYROSCOPE_2000 = 0.07 
	K.set_sensor_res(SENSITIVITY_GYROSCOPE_2000, SENSITIVITY_ACCELEROMETER_8, SENSITIVITY_MAGNETOMETER_4)
	
	assert K.m_offset == (0,0,0)
	assert K.m_matrix == [[1,0,0],[0,1,0],[0,0,1]]
 
	dot_accumulate = 0
	counter = 0
	with open(csv_filepath) as csvfile:
		reader = csv.DictReader(csvfile)
		for row in reader:
			if row['node_label'] == "kc-CH_2-0":
				counter +=1
			
				recorded_result = (row['q_w'], row['q_x'], row['q_y'], row['q_z'])
				recorded_result = tuple(float(val) for val in recorded_result)				
				K.run(float(row['g_x']), float(row['g_y']), float(row['g_z']),
						float(row['a_x']), float(row['a_y']), float(row['a_z']),
						float(row['m_x']), float(row['m_y']), float(row['m_z']))

				#skip the first lines and last lines
				if counter < 500: continue
				if counter > 1500: break
				
				# The quaternion dot product can be interpreted as a similarity
				# index between two quats
				dot = abs(quat_dot(K.q, Quaternion(*recorded_result)))
				dot_accumulate += dot
    
				assert K.m_offset == (0,0,0)
				assert K.m_matrix == [[1,0,0],[0,1,0],[0,0,1]]

	dot_avg = dot_accumulate / (counter+1-500)
	#print("DOT ACCUM", dot_accumulate, "COUNTER", counter, "AVG", dot_avg)
	assert dot_avg == pytest.approx(1, rel=1e-2)
	
				