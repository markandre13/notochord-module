// -- START OF COPYRIGHT & LICENSE NOTICES -- 
//
// Chordata Motion's Notochord (python module)
// -- Core implementation of Chordata Motion's Open Source motion capture framework
//
// http://chordata.cc
// contact@chordata.cc
//
//
// Copyright 2018-2021 Bruno Laurencich
// Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
//
// This file is part of Chordata Motion's Notochord (python module).
//
// Chordata Motion's Notochord (python module) is free software: you can 
// redistribute it and/or modify it under the terms of the 
// GNU General Public License as published by the Free Software Foundation, 
// either version 3 of the License, or (at your option) any later version.
//
// Chordata client is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Chordata Motion's Notochord (python module).  
// If not, see <https://www.gnu.org/licenses/>.
//
//
// -- END OF COPYRIGHT & LICENSE NOTICES -- 

#include <Eigen/Dense>
#include <Eigen/Geometry> 
#include <iostream>
#include "_armature.h"
// from Quaternion.h:372
// Coefficients is a 4x1 Matrix in the order X Y Z W

// namespace internal {
//   template<typename _Scalar, int _Options>
//   struct traits<Map<Quaternion<_Scalar>, _Options> > : traits<Quaternion<_Scalar, (int(_Options)&Aligned)==Aligned ? AutoAlign : DontAlign> >
//   {
//     typedef Map<Matrix<_Scalar,4,1>, _Options> Coefficients;
//   };
// }

int main(int argc, char const *argv[]){

    auto q2 = Eigen::Quaternionf(0.7071068,0,0.7071068,0 );
    auto inverse = q2.inverse();

    q2 = inverse * q2;

    auto mat = q2.toRotationMatrix();


    std::cout << mat << std::endl;

    Chordata::matrix4_ptr m = std::make_shared<Chordata::Matrix4>();
    *m = Chordata::Matrix4::Identity();

    auto joint = Chordata::Joint(nullptr, "test_joint", m);

    auto transform = joint.get_local_transform();

    // Print transform matrix
    std::cout << transform << std::endl;


}