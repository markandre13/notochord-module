// -- START OF COPYRIGHT & LICENSE NOTICES -- 
//
// Chordata Motion's Notochord (python module)
// -- Core implementation of Chordata Motion's Open Source motion capture framework
//
// http://chordata.cc
// contact@chordata.cc
//
//
// Copyright 2018-2021 Bruno Laurencich
// Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
//
// This file is part of Chordata Motion's Notochord (python module).
//
// Chordata Motion's Notochord (python module) is free software: you can 
// redistribute it and/or modify it under the terms of the 
// GNU General Public License as published by the Free Software Foundation, 
// either version 3 of the License, or (at your option) any later version.
//
// Chordata client is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Chordata Motion's Notochord (python module).  
// If not, see <https://www.gnu.org/licenses/>.
//
//
// -- END OF COPYRIGHT & LICENSE NOTICES -- 

#include <iostream>

// int (*array)[5][10]
// 
// (double (&myArray)[R][C])

void get_mag_matrix_for_EEPROM(float (&Me)[3][3]){

	Me[0][0] = 10.0;	
	Me[0][1] = 20.0; 	
	Me[0][2] = 30.0; 

}

using namespace std;

int main(int argc, char const *argv[])
{
	float M[3][3];

	// float *pippo[3] = (float*[3])M;
	// pippo[0][0] = 23;


	get_mag_matrix_for_EEPROM(M);

	cout<< M[0][0] << " "
	<< M[0][1] << " "
	<< M[0][2] << " "
	<< '\n'
	<< M[1][0] << " "
	<< M[2][1] << " "
	<< M[3][2] << " "
	<< endl;

	return 0;
}