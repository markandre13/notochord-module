// -- START OF COPYRIGHT & LICENSE NOTICES -- 
//
// Chordata Motion's Notochord (python module)
// -- Core implementation of Chordata Motion's Open Source motion capture framework
//
// http://chordata.cc
// contact@chordata.cc
//
//
// Copyright 2018-2021 Bruno Laurencich
// Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
//
// This file is part of Chordata Motion's Notochord (python module).
//
// Chordata Motion's Notochord (python module) is free software: you can 
// redistribute it and/or modify it under the terms of the 
// GNU General Public License as published by the Free Software Foundation, 
// either version 3 of the License, or (at your option) any later version.
//
// Chordata client is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Chordata Motion's Notochord (python module).  
// If not, see <https://www.gnu.org/licenses/>.
//
//
// -- END OF COPYRIGHT & LICENSE NOTICES -- 

/*=========================================
=            TO PRINT decltype()          =
=========================================*/

//see 
// https://dev.krzaq.cc/post/checking-whether-a-class-has-a-member-function-with-a-given-signature/
// http://cpp.sh/94tdya

using namespace std;

#include <iostream>
#include <memory>
#include <vector>

// double* func() { double a = 12.0; return &a; }

void sfunc(std::string){}

#include <type_traits>
#include <typeinfo>
#ifndef _MSC_VER
#   include <cxxabi.h>
#endif
#include <memory>
#include <string>
#include <cstdlib>

// template <class T>
// std::string
// type_name()
// {
//     typedef typename std::remove_reference<T>::type TR;
//     std::unique_ptr<char, void(*)(void*)> own
//            (
// #ifndef _MSC_VER
//                 abi::__cxa_demangle(typeid(TR).name(), nullptr,
//                                            nullptr, nullptr),
// #else
//                 nullptr,
// #endif
//                 std::free
//            );
//     std::string r = own != nullptr ? own.get() : typeid(TR).name();
//     if (std::is_const<TR>::value)
//         r += " const";
//     if (std::is_volatile<TR>::value)
//         r += " volatile";
//     if (std::is_lvalue_reference<T>::value)
//         r += "&";
//     else if (std::is_rvalue_reference<T>::value)
//         r += "&&";
//     return r;
// }



/*=====  End of TO PRINT decltype()======*/


/*======================================
=            TEST signature            =
======================================*/


#include <type_traits>
#include <string>

struct Quaternion {
        Quaternion(float _w, float _x, float _y, float _z):
        w(_w), x(_x), y(_y), z(_z)
        {};

        void format_to(std::string) const{
            // fmt::format_to(buf, "{:4f}, {:4f}, {:4f}, {:4f}", w, x, y, z);
        }

        void size();

        float w,x,y,z;
    };

struct Derived: public Quaternion{
    Derived(): Quaternion(0,1,1,1) {};
};


template<typename T>
void static_assert_has_func(){
    static_assert(std::is_same<void (T::*)(std::string)const, decltype( &T::format_to )>::value, "No void(std::string)");

}

template<typename T>
struct has_size_method
{
private:
	typedef std::true_type yes;
	typedef std::false_type no;
 
	template<typename U> static auto test(int) -> decltype(std::declval<U>().size(std::string) == 1, yes());
 
	template<typename> static no test(...);
 
public:
 
	static constexpr bool value = std::is_same<decltype(test<T>(0)),yes>::value;
};


/*=====  End of TEST signature  ======*/


int main(void)
{

    // static_assert(std::is_same<double*(), decltype(func)>::value, "");

    // static_assert(std::is_same<void(std::string), decltype(sfunc)>::value, "No void(std::string)");

    // std::cout << type_name<decltype(&Quaternion::format_to)>() << '\n';

    // static_assert(std::is_same<void (Quaternion::*)(std::string)const, decltype( &Quaternion::format_to )>::value, "No void(std::string)");

    Quaternion q(1,2,3,4);

    static_assert_has_func<Quaternion>();

    // Derived d;

    //  static_assert_has_func<Derived>();

    return 0;

}