# -- START OF COPYRIGHT & LICENSE NOTICES -- 
#
# Chordata Motion's Notochord (python module)
# -- Core implementation of Chordata Motion's Open Source motion capture framework
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2021 Bruno Laurencich
# Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
#
# This file is part of Chordata Motion's Notochord (python module).
#
# Chordata Motion's Notochord (python module) is free software: you can 
# redistribute it and/or modify it under the terms of the 
# GNU General Public License as published by the Free Software Foundation, 
# either version 3 of the License, or (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata Motion's Notochord (python module).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

import pytest
import re
from os import path
from lxml import etree

def test_config_read_only(def_conf):
	"""Test read only props"""
	config = def_conf
	assert re.match(r"^\d+\.\d+\.\d+", config.version ) 

	with pytest.raises(AttributeError): 
		config.version = "1.367.4"

	with pytest.raises(AttributeError): 
		config.version_number = (1,367,4)

	with pytest.raises(AttributeError): 
		config.hardware_concurrency = 9

def test_default_config_xml(def_conf):
	c = def_conf
	assert c.xml.filename == path.realpath(path.join(path.dirname(__file__), "../" + 'dist/notochord/Chordata.xml'))
	assert c.xml.schema == path.realpath(path.join(path.dirname(__file__), "../" + 'dist/notochord/Chordata.xsd'))

def test_config_xml(def_conf, randstr):
	config= def_conf
	st = randstr
	test_string = "Testing" + st
	abs_test_string = path.abspath(test_string)

	config.xml.filename = test_string
	assert config.xml.filename == abs_test_string

	config.xml.schema = test_string
	assert config.xml.schema == abs_test_string
	
def test_config_xml_absolute_path(def_conf, randstr):
	config = def_conf
	abs_path = path.abspath(randstr)

	config.xml.filename = abs_path
	assert config.xml.filename == abs_path

	config.xml.schema = abs_path
	assert config.xml.schema == abs_path

def test_config_string(def_conf, randstr):
	config= def_conf
	test_string = path.abspath('.') + "Testing" + randstr
	
	config.osc.base_address = test_string
	assert config.osc.base_address == test_string

	config.osc.error_address = test_string
	assert config.osc.error_address == test_string	

	config.osc.msg_address = test_string
	assert config.osc.msg_address == test_string

	config.comm.adapter = test_string
	assert config.comm.adapter == test_string

	config.comm.ip = test_string
	assert config.comm.ip == test_string

def test_config_boolean(def_conf):
	"""Test getting and setting booleans"""
	config = def_conf 

	config.comm.use_bundles = True
	assert config.comm.use_bundles
	config.comm.use_bundles = False
	assert config.comm.use_bundles == False

	# test casting of other python types to boolean
	config.comm.use_bundles = 23
	assert config.comm.use_bundles
	config.comm.use_bundles = ""
	assert config.comm.use_bundles == False

def test_config_enums(def_conf, comm):
	"""Configuration struct communication enums testing"""
	config = def_conf
	new_sinks = comm.Output_Sink.OSC
	new_sinks |= comm.Output_Sink.FILE
	assert config.comm.msg != new_sinks
	config.comm.msg = new_sinks
	assert config.comm.msg == new_sinks

def test_config_enums_wrong_type(def_conf):
	"""Configuration struct communication enums testing when bad arguments"""
	config = def_conf
	with pytest.raises(TypeError):
		config.comm.msg =  (1 | 2) #<--passing an int

def test_config_int(def_conf, config, Accel_Scale_Options, Gyro_Scale_Options, Mag_Scale_Options, Output_Data_Rate):
	def_conf
	def_conf.comm.log_level = 12
	assert def_conf.comm.log_level == 12

	def_conf.sensor.accel_scale = Accel_Scale_Options.accel_16
	assert def_conf.sensor.accel_scale == Accel_Scale_Options.accel_16

	def_conf.sensor.gyro_scale = Gyro_Scale_Options.gyro_245
	assert def_conf.sensor.gyro_scale == Gyro_Scale_Options.gyro_245

	def_conf.sensor.mag_scale = Mag_Scale_Options.mag_16
	assert def_conf.sensor.mag_scale == Mag_Scale_Options.mag_16
	
	def_conf.sensor.output_rate = Output_Data_Rate.OUTPUT_RATE_119
	assert def_conf.sensor.output_rate == Output_Data_Rate.OUTPUT_RATE_119


def test_config_def_filename(def_conf):
	config = def_conf
	assert config.comm.filepath == path.realpath(".notochord_log/chord_log")

def test_config_def_float(def_conf):
	config = def_conf
	config.comm.send_rate = float(7.3)
	assert config.comm.send_rate == pytest.approx(7.3)
