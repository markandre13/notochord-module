import notochord

from notochord import node
from notochord import io

i2c = io.I2C("foo", False)

m = node.Mux(None, 0x73, "test_mux", i2c)

print("Finish memory test")