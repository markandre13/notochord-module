import notochord
from notochord import communicator as comm


def_conf = notochord.config.Notochord_Config()
def_conf.comm.use_bundles = False

def_conf.comm.port = 6565
def_conf.comm.transmit = comm.Output_Sink.OSC | comm.Output_Sink.STDOUT
def_conf.comm.msg = comm.Output_Sink.OSC | comm.Output_Sink.STDOUT
def_conf.comm.adapter = ''

notochord.setup(def_conf)

test_string = "#1 string"

comm.info(test_string)
print("Sent string:", test_string)


# test_string = "#2 string"
# comm.info(test_string)
# comm.flush()
# print("Sent string:", test_string)

# how_many = 12
# for i in range(how_many):
# 	to_osc = "#{} | OSC random string".format(i+1)
# 	comm.info(to_osc)
# 	print("Sent string:", to_osc)


# comm.flush()


# comm.test_transmit()
# comm.test_transmit()
# comm.dump_transmit()

comm.flush()
print("Sent test payload")