import notochord
from notochord.node import Mux, KCeptorPP, Mux_Channel
from notochord.config import Notochord_Config
from notochord.fusion import Kalman
from time import sleep

MUX_ADDR = 0x70
KC_ADDR = 0x47
KC_WHO_REG = 0x0F

notochord.init()

conf = Notochord_Config()
# conf.comm.adapter = '/dev/null'
conf.comm.log_level = 1

notochord.setup(conf)
mux = Mux(None, MUX_ADDR, "test_mux")
branch_2 = mux.get_branch(Mux_Channel.CH_2)
print("about to bang branch")


mux.bang_branch(Mux_Channel.CH_2)

print("branch banged")

kc = KCeptorPP(branch_2, KC_ADDR, "test_kc")


response = kc.setup()
print("KC setup", response)

slope = 1 

for i in range(10):
    sleep(0.05) 
    reading = kc.read()
    # f = ["{:8d}".format(val) for val in reading]
    print("read", i)
