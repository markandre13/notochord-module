from time import sleep
from tracemalloc import stop
import uuid
import notochord
import random

from notochord.config import Notochord_Config, Output_Sink
from notochord.timer import Node_Scheduler, Timer
from notochord.node import Mux, KCeptorPP
from notochord.types import Mux_Channel

notochord.init()

n = Notochord_Config()
n.comm.msg = Output_Sink.STDOUT
n.comm.port = 6565
n.comm.ip = "192.168.1.51"

#n.comm.adapter = '/dev/null'
n.comm.log_level = 2

notochord.setup(n) 

KC_ADDR = int(input("Write current address\n"), 16)

kc  = KCeptorPP(None, KC_ADDR, "test_kc")
kc.setup()
sleep(1)

KC_ADDR = int(input("Write NEW address\n"), 16)

kc.change_addr(KC_ADDR)

print("KCeptor's new address is: 0x{:0x}".format(KC_ADDR))
