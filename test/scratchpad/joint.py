import notochord, pytest
from notochord.math import Vector, Quaternion, Matrix4
from notochord.armature import Bone, Armature
from os.path import join, dirname, realpath
import json

def print_matrix(m, decimals=6):
    for i in range(4):
        for j in range(4):
            print(f"{m[i, j]:.{decimals}f}", end=" ")
        print()
    print("-" * 40)

def equals(m1, m2):
    for i in range(4):
        for j in range(4):
            if j == 3: #skip the translation column
                continue 
            if m1[i, j] != pytest.approx(m2[i, j], abs=1e-6):
                print(f"m1[{i}, {j}] = {m1[i, j]} != {m2[i, j]} = m2[{i}, {j}]")
                assert False
            #assert m1[i, j] == pytest.approx(m2[i, j], abs=0.0001)

def to_mat4(matrix_rows):
    return Matrix4(*matrix_rows[0], *matrix_rows[1], *matrix_rows[2], *matrix_rows[3])

def to_quat(quat_list):
    return Quaternion(*quat_list)

gltf_path = join(dirname(realpath(__file__)),'../data/avatar.gltf')
json_path = join(dirname(realpath(__file__)),'../data/test_armature_transforms.json')

pos_csv = join(dirname(realpath(__file__)),'../data/rotation_pos_Chordata_calib.csv')
pre_csv = join(dirname(realpath(__file__)),'../data/rotation_pre_Chordata_calib.csv')
dump_csv = join(dirname(realpath(__file__)),'../data/Chordata_calib_dump.csv')

armature = Armature("test_armature", gltf_path)

# Parse the JSON file
with open(json_path, 'r') as f:
    json_transforms = json.load(f)
# Parse the CSV files
with open(pos_csv, 'r') as f:
    pos_data = f.readlines()
with open(pre_csv, 'r') as f:
    pre_data = f.readlines()
with open(dump_csv, 'r') as f:
    dump_csv = f.readlines()

calibration_data_pos = {}
calibration_data_pre = {}

# Save the data in a dictionary with the bone name as the key (node_label,w,x,y,z)
for line in pos_data[1:]:
    line = line.split(',')

    calibration_data_pos[line[0]] = Quaternion(float(line[1]), float(line[2]), float(line[3]), float(line[4]))
for line in pre_data[1:]:
    line = line.split(',')
    calibration_data_pre[line[0]] = Quaternion(float(line[1]), float(line[2]), float(line[3]), float(line[4]))

dump_data = {}
for line in dump_csv[1:]:
    line = line.split(',')
    
    if line[0] not in dump_data:
        dump_data[line[0]] = {} # Timestamp - Bone name
    
    dump_data[line[0]][line[1]] = Quaternion(float(line[2]), float(line[3]), float(line[4]), float(line[5])) # Bone name - Quaternion

json_data = {}

for i in range(len(json_transforms["timestamp"])):
    timestamp = json_transforms["timestamp"][i]
    json_data[timestamp] = {}
    json_data[timestamp]["global_transforms"] = json_transforms["global_transforms"][i]
    json_data[timestamp]["global_rotations"] = json_transforms["global_rotations"][i]
    json_data[timestamp]["local_transforms"] = json_transforms["local_transforms"][i]

# # Pretty print json_data
# for timestamp in json_data:
#     print(timestamp)
#     for transform in json_data[timestamp]:
#         print(transform)
#         for bone in json_data[timestamp][transform]:
#             print(bone)
#             if transform != "global_rotations":
#                 print_matrix(to_mat4(json_data[timestamp][transform][bone]))
#             else:
#                 print(to_quat(json_data[timestamp][transform][bone]))
#     print("=====================")

# # Pretty print dump_data
# for timestamp in dump_data:
#     print(timestamp)
#     for bone in dump_data[timestamp]:
#         print(bone, dump_data[timestamp][bone])
#     print("=====================")

assert len(json_transforms["global_transforms"]) == len(json_transforms["local_transforms"])

temp_hierarchy = ['base', 'lumbar', 'dorsal', 'neck', 'head',
                  'r-upperleg', 'r-lowerleg', 'r-foot',
                  'l-upperleg', 'l-lowerleg', 'l-foot',
                  'r-clavice', 'r-upperarm', 'r-lowerarm', 'r-hand',
                  'l-clavice', 'l-upperarm', 'l-lowerarm', 'l-hand']

for timestamp in json_data:
    for bone in temp_hierarchy:#json_data[timestamp]["global_rotations"]:
        if bone not in dump_data[str(timestamp)]:
            print(bone, "not in dump_data")
            continue
        raw_quat = Quaternion(*dump_data[str(timestamp)][bone])
        pre_quat = calibration_data_pre[bone]
        pos_quat = calibration_data_pos[bone]
        global_rotation = pre_quat * raw_quat * pos_quat
        local_transform = to_mat4(json_data[timestamp]["local_transforms"][bone])
        global_transform = to_mat4(json_data[timestamp]["global_transforms"][bone])
        armature.bones[bone].set_global_rotation(global_rotation)
        print(timestamp)
        print(bone)
        print(pre_quat)
        print(raw_quat)
        print(pos_quat)
        print(global_rotation)
        print("Parent's")
        print("Locals")
        print_matrix(armature.bones[bone].parent.local_transform)
        print("Globals")
        print_matrix(armature.bones[bone].parent.global_transform)
        print("Original")
        print("Locals")
        print_matrix(local_transform)
        print("Globals")
        print_matrix(global_transform)
        print("Armature")
        print("Locals")
        print_matrix(armature.bones[bone].local_transform)
        print("Globals")
        print_matrix(armature.bones[bone].global_transform)

        translation = Vector(global_transform[0, 3], global_transform[1, 3], global_transform[2, 3])
        print(translation)
        print(armature.bones[bone].global_translation)
        assert tuple(translation) == pytest.approx(tuple(armature.bones[bone].global_translation), abs=1e-3)

        equals(armature.bones[bone].local_transform, local_transform)

# assert 1 == 0
# for i in range(len(json_data["global_transforms"])):
#     for bone in json_data["global_transforms"][i]:
#         #global_transform = to_mat4(json_data["global_transforms"][i][bone])
#         raw_quat = Quaternion(*json_data["global_rotations"][i][bone])
#         global_rotation = calibration_data_pre[bone] * raw_quat * calibration_data_pos[bone]
#         local_transform = to_mat4(json_data["local_transforms"][i][bone])
#         #next_local_rotation = Quaternion(json_data["local_rotations"][i+1][bone])

#         #armature.bones[bone].set_global_transform(global_transform)
#         armature.bones[bone].set_global_rotation(global_rotation)
#         print("=====================================")
#         print(global_rotation)
#         #print("Global transform for bone", bone)
#         #print_matrix(armature.bones[bone].global_transform)
#         #print_matrix(global_transform)
#         print("Local transform for bone", bone)
#         print_matrix(armature.bones[bone].local_transform)

#         print("Local transform from GLTF")
#         print_matrix(local_transform)
#         equals(armature.bones[bone].local_transform, local_transform)

# for frame in json_data["global_transforms"]:
#     for bone_name in frame:
#         global_transform = to_mat4(frame[bone_name])

#         print(bone_name)
#         print_matrix(global_transform)

#         armature.bones[bone_name].set_global_transform(global_transform)
    
#     print("=====================")

# for frame in json_data["local_rotations"]:
#     for bone_name in frame:
#         local_rotation = Quaternion(frame[bone_name])
        
#         print(local_rotation)

#     print("=====================")
    
# print(len(json_data["global_transforms"]))