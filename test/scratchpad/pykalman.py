import notochord
from notochord.fusion import Kalman
from notochord.node import Mux, KCeptor, Mux_Channel
from notochord.config import Notochord_Config
from random import randint

from os.path import join, realpath, dirname
import csv

notochord.init()
conf = Notochord_Config()
conf.comm.adapter = '/dev/null'
conf.comm.log_level = 1
notochord.setup(conf)

mux = Mux(None, 0x73, "test_mux")
branch_2 = mux.get_branch(Mux_Channel.CH_2)
mux.bang_branch(Mux_Channel.CH_2)
kc = KCeptor(branch_2, 0x02, "test_kc")

print("--------------------")

# KCeptor Kalman
kc_k= kc.kalman
K = Kalman(1, 1) #Kalman object with a oversample_ratio = 8

print("Initial KCeptor buffer")
print(tuple(kc_k.gyro_buffer))

print("Initial Kalman buffer")
print(tuple(K.gyro_buffer))

print("Initial GyroSensor iYsBuffer")
print(kc_k.gyro_sensor.iYsBuffer)

N_entries = 4 #should be minor than the oversample_ratio for the purpuoses of this test
#generate random 9 values entries (3x gyro, accel and mag)
values = tuple(tuple(randint(-500, 500) for xyz in range(9)) for entries in range(N_entries) )

print("Values length: ", len(values))

for entry in values:
    print("--------------------")
    print("iCounter: ", K.iCounter)

    kc_k.set_gyro(*entry[:3])
    kc_k.set_accel(*entry[3:6])
    kc_k.set_mag(*entry[6:9])
    kc_k.process()
    K.set_gyro(*entry[:3])
    K.set_accel(*entry[3:6])
    K.set_mag(*entry[6:9])
    K.process()
    

print("--------------------")
print("--------------------")
print("iCounter: ", K.iCounter)

print(" ")

print("Final KCeptor buffer")
print(tuple(kc_k.gyro_buffer))

print("Final Kalman buffer, length: ", len(tuple(K.gyro_buffer)))
print(tuple(K.gyro_buffer))

print("Final GyroSensor iYsBuffer")
print(K.gyro_sensor.iYsBuffer)