
import notochord
from notochord import communicator as comm
from notochord.math import Quaternion

def_conf = notochord.config.Notochord_Config()
def_conf.comm.adapter = '/dev/null'
def_conf.comm.transmit = comm.Output_Sink.WEBSOCKET
def_conf.comm.websocket_port = 7681

notochord.init()
notochord.setup(def_conf)
input()

# comm.transmit("q", Quaternion(1,0,0,1))
# comm.flush()
input()

notochord.terminate()