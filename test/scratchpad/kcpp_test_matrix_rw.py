from time import sleep
from tracemalloc import stop
import uuid
import notochord
import random

from notochord.config import Notochord_Config, Output_Sink
from notochord.timer import Node_Scheduler, Timer
from notochord.node import Mux, KCeptorPP
from notochord.types import Mux_Channel
from notochord.math import Matrix3

MUX_ADDR = 0x70

notochord.init()

n = Notochord_Config()
n.comm.msg = Output_Sink.STDOUT
n.comm.port = 6565
n.comm.ip = "192.168.1.255"

n.raw = True
n.calib = False

# inp = input("Run calibration? (y/n)")

# if inp == "y" or inp == "Y":
#     n.calib = True


n.comm.log_level = 1

notochord.setup(n) 
notochord.communicator.info("Starting Notochord Scratch. IP {}".format(n.comm.ip))

s = Node_Scheduler()
t = Timer(s)

mux = Mux(None, 0x70, "test_mux")
kcpp = KCeptorPP(None, 0x40, "test_kcpp")

mux.bang_branch(Mux_Channel.CH_1)

kcpp.m_matrix = Matrix3(4.044,5.055,6.066,7.077,8.088,9.099,1.011,2.022,3.033)
kcpp.save_data()

# sleep(1)
kcpp.setup()

# print(kcpp.m_matrix)

# inpt_str = ""

# print(notochord.status())
# notochord.start(s)
# print("Write q to stop...")

# while inpt_str != "q":
#     inpt_str = input()


# notochord.end()
