import notochord
from notochord import communicator as comm
from notochord.config import Notochord_Config
from notochord.math import Vector, Quaternion

notochord.init()

conf = Notochord_Config()
conf.comm.transmit = comm.Output_Sink.STDOUT
conf.comm.adapter = '/dev/null'
notochord.configure(conf)

v= Vector(4,5,6)
print(v)
q=Quaternion(0, 0.7, 0, -0.7)
print(q)

comm.transmit("somevec", v)
comm.transmit("somequat", q)