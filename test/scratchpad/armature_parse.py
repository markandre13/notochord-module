import notochord
from notochord.config import Notochord_Config, Output_Sink
from notochord.parse import get_xml, parse, parse_hierarchy_xml, parse_config
from notochord.timer import Node_Scheduler
import notochord.calibration as calibrator
from notochord.mock_node import Mock_Mux, Mock_KCeptor
from notochord.node import Mux_Channel
from time import sleep

notochord.init()

addr = "192.168.1.132"
port = "6565"
verbose = 1
raw = True

# n = Notochord_Config()


# Do xml_filepath = "blender_config.xml" but with absolute path using os.path.abspath
from os import path
xml_filepath = path.abspath("data/test_Chordata.xml")




# xml = get_xml(xml_filepath)
# parse(xml, n)
# n.calib = False
# n.raw = True
# n.comm.use_bundles = True
# n.comm.msg = Output_Sink.STDOUT
# n.comm.log_level = 2
# n.comm.port = 6565
# n.comm.ip = "192.168.1.132"

c = parse_config(["-c",xml_filepath,addr, port]) # parse xml config, addr and port are needed
c.xml.filename = xml_filepath
c.comm.msg |= Output_Sink.PYTHON # Do we want this as default? It is needed to show the log in /state
c.comm.error |= Output_Sink.PYTHON
c.comm.log_level = verbose
c.raw = raw


MOCK = False

inp = input("Enable Mock Sensors? (y/n)")

if inp == "y" or inp == "Y":
    MOCK = True
    c.comm.adapter = '/dev/null'

notochord.setup(c) 
notochord.communicator.info("Starting Notochord. Using armature: {}".format(c.use_armature))

s = Node_Scheduler()

if MOCK:
    mux = Mock_Mux(None, 0x73, "test_mux")
    sensors = []
    sensors_labels =    ["base", "dorsal", "neck",
                        "l-upperarm", "l-lowerarm", "l-hand",
                        "r-upperarm", "r-lowerarm", "r-hand",
                        "l-upperleg", "l-lowerleg", "l-foot",
                        "r-upperleg", "r-lowerleg", "r-foot"]

    for i in range(5):
        s.add_node(mux.get_branch(Mux_Channel(i+1)))

        for j in range(3):
            sensors.append(Mock_KCeptor(None, 0x1b, sensors_labels[i*3+j]))
            #sensors_label.append(n.osc.base_address+"/q/"+sensors[-1].label)
            s.add_node(sensors[-1])    
else:
    h = parse_hierarchy_xml(xml_filepath, s)     

inpt_str = ""

print(notochord.status())
notochord.start(s)
print("Write q to stop...")

while inpt_str != "q":
    inpt_str = input()

    if inpt_str == "d":
        df = calibrator.get_data()
        print(df)
        df.to_csv("data/test.csv")
    
    if inpt_str == "s":
        calibrator.change_status(calibrator.Calibration_Status.STATIC)

    if inpt_str == "p":
        calibrator.change_status(calibrator.Calibration_Status.CALIB_IDLE)
    
    if inpt_str == "i":
        print(calibrator.get_indexes())

    # run test
    if inpt_str == "t":
        STATUS_TIME = 1
        WAIT_TIME = 0.5
        calibrator.change_status(calibrator.Calibration_Status.STATIC)
        sleep(STATUS_TIME)        
        calibrator.change_status(calibrator.Calibration_Status.CALIB_IDLE)
        sleep(WAIT_TIME)
        calibrator.change_status(calibrator.Calibration_Status.ARMS)
        sleep(STATUS_TIME)
        calibrator.change_status(calibrator.Calibration_Status.CALIB_IDLE)
        sleep(WAIT_TIME)
        calibrator.change_status(calibrator.Calibration_Status.TRUNK)
        sleep(STATUS_TIME)
        calibrator.change_status(calibrator.Calibration_Status.CALIB_IDLE)
        sleep(WAIT_TIME)
        calibrator.change_status(calibrator.Calibration_Status.LEFT_LEG)
        sleep(STATUS_TIME)
        calibrator.change_status(calibrator.Calibration_Status.CALIB_IDLE)
        sleep(WAIT_TIME)
        calibrator.change_status(calibrator.Calibration_Status.RIGHT_LEG)
        sleep(STATUS_TIME)
        calibrator.change_status(calibrator.Calibration_Status.CALIB_IDLE)
        sleep(WAIT_TIME)

        print("Test finished")        

    if inpt_str == "c":
        try:
            print(calibrator.run_calibration())
        except Exception as e:
            print(e)

notochord.end()

