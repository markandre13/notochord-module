from time import sleep
from tracemalloc import stop
import uuid
import notochord
import random

from notochord.config import Notochord_Config, Output_Sink
from notochord.timer import Node_Scheduler, Timer
from notochord.node import Mux, KCeptor
from notochord.types import Mux_Channel

n = Notochord_Config()
n.comm.msg = Output_Sink.STDOUT
n.comm.port = 6565
n.comm.ip = "192.168.1.111"

n.raw = True

n.calib = True

#n.comm.adapter = '/dev/null'
n.comm.log_level = 1

notochord.setup(n) 
notochord.communicator.info("Starting scheduler scratch...")
# notochord.terminate()

s = Node_Scheduler()
t = Timer(s)
sensors = []

mux = Mux(None, 0x73, "test_mux")
branch = mux.get_branch(Mux_Channel.CH_2)
kc = KCeptor(branch, 0x0, "test_kc")

#s.add_node(mux)
s.add_node(branch)
s.add_node(kc)

# for i in range(6):
#     s.add_node(mux.get_branch(Mux_Channel(i+1)))

#     for i in range(3):
#         sensors.append(KCeptor(None, 0x1b, ("test_m_kc_" + str(uuid.uuid1()))))
#         s.add_node(sensors[-1])


print(notochord.status())
notochord.start(s)
input("Press enter to stop...")
notochord.end()