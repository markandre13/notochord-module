from notochord.io import I2C
from time import sleep

MUX_ADDR = 0x73
KC_ADDR = 0x6b
KC_WHO_REG = 0x0F
KC_WHO_ANS = 0b01101000
i2c = I2C()
i2c.write_simple_byte(MUX_ADDR, 0x0f)
assert i2c.read_simple_byte(MUX_ADDR) == 0x0f

kc_who_resp = i2c.read_byte(KC_ADDR, KC_WHO_REG)
assert kc_who_resp == KC_WHO_ANS
print("KC WHOAMI RESPONSE: 0x{:0x}".format(kc_who_resp))

#INIT ACEL:
CTRL_REG5_XL = 0x1F
CTRL_REG6_XL = 0x20
OUT_X_L_XL = 0x28
tempRegValue = 0;
tempRegValue |= (1<<5);
tempRegValue |= (1<<4);
tempRegValue |= (1<<3);
i2c.write_byte(KC_ADDR, CTRL_REG5_XL, tempRegValue);

tempRegValue = 0;
tempRegValue |= (2 & 0x07) << 5;
tempRegValue |= (0x2 << 3);


i2c.write_byte(KC_ADDR, CTRL_REG6_XL, tempRegValue);

while 1:
    res = i2c.read_bytes(KC_ADDR, OUT_X_L_XL, 6)
    print(res)
    sleep(0.2)