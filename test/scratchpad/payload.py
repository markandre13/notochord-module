import notochord, copp_server
from time import sleep
import uuid

from notochord import communicator as comm
from notochord.math import Quaternion
from notochord.timer import Node_Scheduler, Timer
from notochord.mock_node import Mock_Mux, Mock_KCeptor
from notochord.node import KCeptor
from notochord.types import Mux_Channel, Log_Levels

port = 4565
server = copp_server.COPP_Server( port = port )
server.receive_timeout = 0.1 # this speeds up the teardown wait

# add some COPP_Server types for convenience
server.COPP_Lazy_Packet = copp_server.COPP_Lazy_Packet
server.COPP_Common_packet = copp_server.COPP_Common_packet

server.start()


def_conf = notochord.config.Notochord_Config()

def_conf.comm.adapter = '/dev/null'
def_conf.comm.msg = comm.Output_Sink.PYTHON
to_cython = "cython random string "
notochord.setup(def_conf)



notochord.communicator.error(to_cython)
captured = comm.get_registry(False, Log_Levels.Error)

print(captured)
notochord.communicator.info(to_cython)
captured = comm.get_registry(True)

print(captured)
def_conf.comm.use_bundles = True


def_conf.comm.port = 6565
def_conf.comm.transmit = comm.Output_Sink.OSC# | comm.Output_Sink.STDOUT
print(notochord.status())
notochord.setup(def_conf)
notochord.communicator.info("some preinfo")
notochord.terminate()
print(notochord.status())
notochord.setup(def_conf)
print(notochord.status())
notochord.communicator.info("some info")
notochord.communicator.flush()




# q = Quaternion(1,2,3,4)

# print(q)

# notochord.communicator.transmit("addrA",q)

# q = Quaternion(5,6,7,8)

# notochord.communicator.transmit("addrB", q)

# comm.flush()

# print(tuple(q))

s = Node_Scheduler()
mux = Mock_Mux(None, 0x73, "test_mux")
t = Timer(s)
sensors = []

for i in range(6):
    s.add_node(mux.get_branch(Mux_Channel(i+1)))

    for i in range(3):
        sensors.append(Mock_KCeptor(None, 0x1b, ("test_m_kc_" + str(uuid.uuid1()))))
        s.add_node(sensors[-1])


print(notochord.status())
# notochord.start(s)
# sleep(5)
# notochord.end()
sleep(0.1)

print("----------------------")

# number_msg_received = 0

# for packet in server.get():		
#     print(isinstance(packet, server.COPP_Lazy_Packet))
#     for msg in packet.get():
#         print(msg)
#         if msg.typetags == ',ffff':
#             print(msg.addrpattern)
#             number_msg_received += 1

# print(number_msg_received)

kc = KCeptor(None, 0x02, "test_kc")

notochord.communicator.transmit_node(kc, Quaternion())
notochord.communicator.transmit_node(kc, Quaternion())
notochord.communicator.transmit_node(kc, Quaternion())


notochord.communicator.transmit_raw(kc)
notochord.communicator.transmit_raw(kc)
notochord.communicator.transmit_raw(kc)


notochord.communicator.flush()

server.close_and_wait()

