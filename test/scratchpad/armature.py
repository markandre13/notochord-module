import notochord
from math import pi, sin, cos
from notochord.math import Vector, Quaternion
from notochord.armature import Bone
from notochord.config import Notochord_Config, Output_Sink
from notochord.communicator import transmit, flush
from notochord.timer import Node_Scheduler, Timer
from notochord.node import Mux
from time import sleep
import copp_server

print("Starting Notochord Scratch...")

# Calculate quaternion from angle and axis
def quaternion_from_axis_angle(degrees, axis):
    radians = degrees*pi/180
    w = cos(radians/2)
    x = axis[0]*sin(radians/2)
    y = axis[1]*sin(radians/2)
    z = axis[2]*sin(radians/2)
    return Quaternion(w, x, y, z)

def transmit_all(bones):
    for bone in bones:
        transmit("/%/{}".format(bone.label), bone.global_position)
    flush()

def rotate_and_transmit(bones, angle, id = 0):
    #degrees to radians
    angle = angle*pi/180
    
    bones[id].rotateY(angle)

    transmit_all(bones)

def rotate_quaternion(bones, degrees, id = 0):
    bones[id].set_global_rotation(quaternion_from_axis_angle(degrees, (0, 1, 0)))
    bones[id].update()

    transmit_all(bones)

def set_rotation(bones, quaternion, id = 0):
    print(quaternion)
    bones[id].set_global_rotation(quaternion)
    bones[id].update()

    transmit_all(bones)



SERVER = False
PORT = 6564
IP = "127.0.0.1"

if SERVER:
    IP = "192.168.1.112"
notochord.init()

n = Notochord_Config()
n.comm.msg = Output_Sink.STDOUT
n.comm.port = PORT
n.comm.ip = IP
n.comm.use_bundles = True
n.comm.log_level = 1
if not SERVER:
    n.comm.adapter = '/dev/null'

notochord.setup(n) 
notochord.communicator.info("Starting Notochord Scratch. IP {}".format(n.comm.ip))

upper_arm = Bone(None, "upper_arm")
lower_arm = Bone(upper_arm, "lower_arm")
hand = Bone(lower_arm, "hand")
hand_tip = Bone(hand, "hand_tip")

bones = [upper_arm, lower_arm, hand, hand_tip]

upper_arm.translate(Vector(0,0,0))
lower_arm.translate(Vector(0,2,0))
hand.translate(Vector(0,2,0))
hand_tip.translate(Vector(0,0.5,0))

# rotate_and_transmit(bones, 90, 0)
# rotate_and_transmit(bones, 90, 1)
# rotate_and_transmit(bones, 90, 2)

q = Quaternion()

#rotate quaternion around Y axis
q.w = 0.7071068
q.x = 0
q.y = 0.7071068
q.z = 0

qz = Quaternion(0.7071068, 0, 0, 0.7071068)

# upper_arm.set_global_rotation(q)
# lower_arm.set_global_rotation(q)
# hand.set_global_rotation(qz)
# #hand_tip
# upper_arm.update()

transmit_all(bones)


time = 0

max_angle = 50
increment = 2
current_angle = 0
aux = current_angle

max_angle_2 = 10
increment_2 = 2
current_angle_2 = 0
aux_2 = current_angle_2

port = 6565
server = copp_server.COPP_Server( port = port )
server.receive_timeout = 0.1 # this speeds up the teardown wait

# add some COPP_Server types for convenience
server.COPP_Lazy_Packet = copp_server.COPP_Lazy_Packet
server.COPP_Common_packet = copp_server.COPP_Common_packet

# s = Node_Scheduler()
# t = Timer(s)

# mux = Mux(None, 0x70, "test_mux")
# s.scan(mux)

server.start()
# notochord.start(s)

number_msg_received = 0

while time < 1000:
    #rotate_and_transmit(bones, increment)

    #rotate_quaternion(bones, aux)
    # rotate_quaternion(bones, aux, 1)
    # rotate_quaternion(bones, aux_2, 2)

    current_angle += abs(increment)
    aux += increment

    current_angle_2 += abs(increment_2)
    aux_2 += increment_2

    sleep(0.05)
    if current_angle > max_angle:
        increment *= -1
        current_angle = -max_angle
    
    if current_angle_2 > max_angle_2:
        increment_2 *= -1
        current_angle_2 = -max_angle_2
        
    time += 0.05

    for packet in server.get():
		#Common_packet is created when the messages arrive outside bundles
        assert isinstance(packet, server.COPP_Lazy_Packet)
        for msg in packet.get():
            if msg.typetags == ',ffff':
                number_msg_received += 1
                #print("{}: {}".format(msg.subtarget, msg.payload))
                if msg.subtarget == "kc_0x40branch2":
                    set_rotation(bones, Quaternion(msg.payload), 0)
                if msg.subtarget == "kc_0x41branch2":
                    set_rotation(bones, Quaternion(msg.payload), 1)
                if msg.subtarget == "kc_0x43branch2":
                    set_rotation(bones, Quaternion(msg.payload), 2)

    # assert number_msg_received > 0 

# notochord.end()
server.close_and_wait()





