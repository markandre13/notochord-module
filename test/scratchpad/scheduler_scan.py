from time import sleep
from tracemalloc import stop
import uuid
import notochord
import random

from notochord.config import Notochord_Config, Output_Sink
from notochord.timer import Node_Scheduler
from notochord.node import Mux, KCeptorPP
from notochord.types import Mux_Channel
from notochord.error import IO_Error

notochord.init()

n = Notochord_Config()
n.comm.msg = Output_Sink.STDOUT
n.calib = False

n.comm.log_level = 1

notochord.setup(n) 
notochord.communicator.info("Starting Notochord Scratch. IP {}".format(n.comm.ip))

s = Node_Scheduler()

mux = None

for i in range(101, 118):
    try:
        mux = Mux(None, i, "mux_"+hex(i))
        break
    except IO_Error:
        continue

for i in range(1, 7):
    branch = mux.get_branch(Mux_Channel(i))
    mux.bang_branch(Mux_Channel(i))

    for j in range(64, 97):
        kc  = KCeptorPP(branch, j, "kc_"+hex(j)+"branch"+str(i))
        try:
            kc.setup()
            print("kc_"+hex(j)+"branch"+str(i)+" ^")
            sleep(0.1)
            kc.stop()
            s.add_node(kc)
        except IO_Error:
            continue
    
    s.add_node(branch)