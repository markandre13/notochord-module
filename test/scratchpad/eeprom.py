import notochord
from notochord.node import Mux, KCeptor, Mux_Channel
from notochord.config import Notochord_Config
from notochord.fusion import Kalman
from time import sleep


MUX_ADDR = 0x73
KC_ADDR = 0x1f
KC_WHO_REG = 0x0F

notochord.init()

conf = Notochord_Config()
# conf.comm.adapter = '/dev/null'
conf.comm.log_level = 1

notochord.configure(conf)
mux = Mux(None, MUX_ADDR, "test_mux")
branch_2 = mux.get_branch(Mux_Channel.CH_2)
mux.bang_branch(Mux_Channel.CH_2)

kc = KCeptor(branch_2, 0x02, "test_kc")

response = kc.setup()
print("KC setup", response)

print(kc.eeprom)
print(kc.eeprom["timestamp"])
print("{:x}".format(kc.eeprom["validation"]))

print("KC OFFSET BEFORE EEPROM:",kc.g_offset, kc.a_offset, kc.m_offset)
print("KC MATRIX BEFORE EEPROM:\n",kc.m_matrix)


kc.read_calib_from_eeprom()
print("KC OFFSET AFTER EEPROM:",kc.g_offset, kc.a_offset, kc.m_offset)
print("KC MATRIX AFTER EEPROM:\n",kc.m_matrix)

print("-------- write ---------")
kc.g_offset = (44, 55, 66)
kc.write_calib_to_eeprom()

print(kc.eeprom)
kc.read_calib_from_eeprom()

print("KC OFFSET AFTER WRITE EEPROM:",kc.g_offset, kc.a_offset, kc.m_offset)
print("KC MATRIX AFTER WRITEEEPROM:\n",kc.m_matrix)





# count = 0    
# with open("/home/pi/scratch_dump.log", "w") as f:
#     f.write("g_x,g_y,g_z,a_x,a_y,a_z,m_x,m_y,m_z\n")
#     while 1:
#         reading = kc.read_sensors(False)
#         o = ",".join((str(val) for val in reading))
#         print(count, o)
#         f.write(o+"\n")
#         sleep(0.05)
#         count +=1
#         if count > 500: break
    



