from time import sleep
from tracemalloc import stop
import uuid
import notochord
import random

from notochord.config import Notochord_Config, Output_Sink
from notochord.timer import Node_Scheduler, Timer
from notochord.node import Mux, KCeptorPP
from notochord.types import Mux_Channel

MUX_ADDR = 0x70

notochord.init()

n = Notochord_Config()
n.comm.msg = Output_Sink.STDOUT
n.comm.port = 6565
n.comm.ip = "127.0.0.1"
n.comm.use_bundles = False
n.raw = False
n.calib = False



n.comm.log_level = 1

notochord.setup(n) 
notochord.communicator.info("Starting Notochord Scratch. IP {}".format(n.comm.ip))

s = Node_Scheduler()
t = Timer(s)

kc = KCeptorPP(None, 0x40, "test_kc")
s.add_node(kc)

inpt_str = ""

print(notochord.status())
notochord.start(s)
print("Write q to stop...")

while inpt_str != "q":
    inpt_str = input()


notochord.end()
