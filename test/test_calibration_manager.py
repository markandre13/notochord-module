import pytest
from time import sleep
import uuid

def test_empty_dataframe(notochord_init, def_conf, Calibrator):
	"""Test that the default dataframe is empty, has the correct columns, and is indexes are zero."""
	def_conf.comm.port = 6565
	notochord_init.setup(def_conf)

	df = Calibrator.get_data()

	assert df.empty
	print(df.columns.tolist())
	#assert df.columns.tolist() == ["time(msec)", "node_label", "qx", "qy", "qz", "qw", "gx", "gy", "gz", "ax", "ay", "az", "mx", "my", "mz"]
	
	indexes = Calibrator.get_indexes()

	assert indexes["vert_i"] == 0
	assert indexes["vert_s"] == 0
	assert indexes["func_arms_i"] == 0
	assert indexes["func_arms_s"] == 0
	assert indexes["func_legs_l_i"] == 0
	assert indexes["func_legs_l_s"] == 0
	assert indexes["func_legs_r_i"] == 0
	assert indexes["func_legs_r_s"] == 0
	
@pytest.mark.slow
def test_calibrator(notochord_init, def_conf, Node_Scheduler, Mock_Mux, Mock_KCeptor, Mux_Channel, Calibrator):
	"""Test that the calibrator works as expected."""
	def_conf.comm.port = 6565
	def_conf.comm.use_bundles = False
	notochord_init.setup(def_conf)
	
	s = Node_Scheduler()
	mux = Mock_Mux(None, 0x73, "test_mux")
	sensors = []
	sensors_label = []

	for i in range(6):
		s.add_node(mux.get_branch(Mux_Channel(i+1)))

		for i in range(3):
			sensors.append(Mock_KCeptor(None, 0x1b, ("test_m_kc_" + str(uuid.uuid1()))))
			sensors_label.append(sensors[-1].label)
			s.add_node(sensors[-1])
	
	sensors_per_cycle = 3*6

	notochord_init.start(s)
	sleep(1)

	# Set the calibrator to static mode
	Calibrator.change_status(Calibrator.Calibration_Status.STATIC)
	sleep(0.5)
	Calibrator.change_status(Calibrator.Calibration_Status.CALIB_IDLE)
	sleep(0.1)

	df = Calibrator.get_data()

	assert not df.empty

	# Check that all the sensors are in the dataframe
	for label in sensors_label:
		assert label in df["node_label"].tolist()
		
	# Check that the indexes are correct
	aux = df.shape[0]
	indexes = Calibrator.get_indexes()
	
	assert indexes["vert_i"] == 0
	assert indexes["vert_s"] == (aux / sensors_per_cycle) - 1
	assert indexes["func_arms_i"] == 0
	assert indexes["func_arms_s"] == 0
	assert indexes["func_legs_l_i"] == 0
	assert indexes["func_legs_l_s"] == 0
	assert indexes["func_legs_r_i"] == 0
	assert indexes["func_legs_r_s"] == 0

	# Check that the indexes are correct after a second run
	Calibrator.change_status(Calibrator.Calibration_Status.STATIC)
	sleep(0.5)
	Calibrator.change_status(Calibrator.Calibration_Status.CALIB_IDLE)
	sleep(0.1)

	df = Calibrator.get_data()

	assert not df.empty
	
	indexes = Calibrator.get_indexes()

	assert indexes["vert_i"] == aux / sensors_per_cycle
	assert indexes["vert_s"] == (df.shape[0] / sensors_per_cycle) - 1

	# Set the calibrator to ARMS mode
	aux = df.shape[0]
	aux_vert_i = indexes["vert_i"]
	aux_vert_s = indexes["vert_s"]

	Calibrator.change_status(Calibrator.Calibration_Status.ARMS)
	sleep(0.5)
	Calibrator.change_status(Calibrator.Calibration_Status.CALIB_IDLE)
	sleep(0.1)

	df = Calibrator.get_data()

	assert not df.empty

	# Check that the indexes are correct	
	indexes = Calibrator.get_indexes()

	assert indexes["vert_i"] == aux_vert_i
	assert indexes["vert_s"] == aux_vert_s
	assert indexes["func_arms_i"] == aux / sensors_per_cycle
	assert indexes["func_arms_s"] == (df.shape[0] / sensors_per_cycle) - 1
	assert indexes["func_legs_l_i"] == 0
	assert indexes["func_legs_l_s"] == 0
	assert indexes["func_legs_r_i"] == 0
	assert indexes["func_legs_r_s"] == 0

	# Set the calibrator to LEFT_LEG mode
	aux = df.shape[0]
	aux_func_arms_i = indexes["func_arms_i"]
	aux_func_arms_s = indexes["func_arms_s"]

	Calibrator.change_status(Calibrator.Calibration_Status.LEFT_LEG)
	sleep(0.5)
	Calibrator.change_status(Calibrator.Calibration_Status.CALIB_IDLE)
	sleep(0.1)

	df = Calibrator.get_data()

	assert not df.empty

	# Check that the indexes are correct
	indexes = Calibrator.get_indexes()

	assert indexes["vert_i"] == aux_vert_i
	assert indexes["vert_s"] == aux_vert_s
	assert indexes["func_arms_i"] == aux_func_arms_i
	assert indexes["func_arms_s"] == aux_func_arms_s
	assert indexes["func_legs_l_i"] == aux / sensors_per_cycle
	assert indexes["func_legs_l_s"] == (df.shape[0] / sensors_per_cycle) - 1
	assert indexes["func_legs_r_i"] == 0
	assert indexes["func_legs_r_s"] == 0

	# Set the calibrator to RIGHT_LEG mode
	aux = df.shape[0]
	aux_func_legs_l_i = indexes["func_legs_l_i"]
	aux_func_legs_l_s = indexes["func_legs_l_s"]

	Calibrator.change_status(Calibrator.Calibration_Status.RIGHT_LEG)
	sleep(0.5)
	Calibrator.change_status(Calibrator.Calibration_Status.CALIB_IDLE)
	sleep(0.1)

	df = Calibrator.get_data()

	assert not df.empty

	# Check that the indexes are correct
	indexes = Calibrator.get_indexes()

	assert indexes["vert_i"] == aux_vert_i
	assert indexes["vert_s"] == aux_vert_s
	assert indexes["func_arms_i"] == aux_func_arms_i
	assert indexes["func_arms_s"] == aux_func_arms_s
	assert indexes["func_legs_l_i"] == aux_func_legs_l_i
	assert indexes["func_legs_l_s"] == aux_func_legs_l_s
	assert indexes["func_legs_r_i"] == aux / sensors_per_cycle
	assert indexes["func_legs_r_s"] == (df.shape[0] / sensors_per_cycle) - 1

	notochord_init.end()