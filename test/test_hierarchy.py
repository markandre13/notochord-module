

import pytest

def test_hierarchy_raw(notochord_init, Mux, KCeptor, error):
    mux = Mux(None, 0x73, "test_mux")
    the_branch = mux.get_branch(notochord_init.node.Mux_Channel.CH_1)
    kc1 = KCeptor(the_branch, 0x6b, "test_kc1")
    kc2 = KCeptor(kc1, 0x6c, "test_kc2")
    assert len(mux.children) == 6
    assert mux.children[0] == the_branch 
    
    assert the_branch.parent == mux
    assert the_branch.children == (kc1,)
    
    assert kc1.parent == the_branch
    assert kc1.children == (kc2,)
    assert kc2.parent == kc1
    assert kc2.children == None

    #cannot set a Kc as child of another that already has a child
    with pytest.raises(error.Hierarchy_Error):
        kc3 = KCeptor(kc1, 0x6d, "test_kc3")

    #cannot set a Mux as parent of a KC
    with pytest.raises(error.Hierarchy_Error):
        kc4 = KCeptor(mux, 0x6d, "test_kc4")


def test_Hierarchy_class(notochord_init, Mux, KCeptor, Hierarchy):
    mux = Mux(None, 0x73, "test_mux")
    the_branch = mux.get_branch(notochord_init.node.Mux_Channel.CH_1)
    kc1 = KCeptor(the_branch, 0x6b, "test_kc1")
    kc2 = KCeptor(kc1, 0x6c, "test_kc2")

    h = Hierarchy(mux)
    mux_info = h.get_node_info("test_mux")
    kc1_info = h.get_node_info("test_kc1")
    kc2_info = h.get_node_info("test_kc2")

    assert mux_info["parent_label"] == ''
    assert kc1_info["parent_label"] == 'test_mux.CH_1'
    assert kc2_info["parent_label"] == 'test_kc1'

    assert len(mux_info["children_label"]) == 6
    assert len(kc1_info["children_label"]) == 1
    assert len(kc2_info["children_label"]) == 0

    # the 'users' key is the use_count of the c++ shared_ptr
    # for each node the cython extension type and the underlying C++
    # object hold a pointer to the C++ object
    assert mux_info["users"] == 2
    assert kc1_info["users"] == 2
    assert kc1_info["users"] == 2

def test_access_nonexistent_node(notochord_init, Hierarchy, Mux):
    mux = Mux(None, 0x73, "test_mux")
    h = Hierarchy(mux)
    empty_node = h.get_node_info("nonexistent_node")
    assert empty_node["label"] == ""
    assert empty_node["parent_label"] == ""
    assert len(empty_node["children_label"]) == 0
    assert empty_node["users"] == 0