# -- START OF COPYRIGHT & LICENSE NOTICES -- 
#
# Chordata Motion's Notochord (python module)
# -- Core implementation of Chordata Motion's Open Source motion capture framework
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2021 Bruno Laurencich
# Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
#
# This file is part of Chordata Motion's Notochord (python module).
#
# Chordata Motion's Notochord (python module) is free software: you can 
# redistribute it and/or modify it under the terms of the 
# GNU General Public License as published by the Free Software Foundation, 
# either version 3 of the License, or (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata Motion's Notochord (python module).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

import pytest
import re, uuid
from time import sleep

ASYNC_SLEEP = 0.05

def test_comm_configure(notochord_init, comm, def_conf):
	logger_msg = comm.get_logger_messagge()
	logger_err = comm.get_logger_error()
	logger_tra = comm.get_logger_transmit()

	def_conf.comm.msg = comm.Output_Sink.OSC | comm.Output_Sink.FILE
	def_conf.comm.error = comm.Output_Sink.OSC
	def_conf.comm.transmit = comm.Output_Sink.STDERR | comm.Output_Sink.FILE | comm.Output_Sink.OSC
	notochord_init.setup(def_conf)
	assert logger_msg.sinks == comm.Output_Sink.OSC | comm.Output_Sink.FILE
	assert logger_err.sinks == comm.Output_Sink.OSC
	assert logger_tra.sinks == comm.Output_Sink.STDERR | comm.Output_Sink.FILE | comm.Output_Sink.OSC

	# test reconfigure
	def_conf.comm.msg = comm.Output_Sink.NONE
	notochord_init.setup(def_conf)
	assert logger_msg.sinks == comm.Output_Sink.NONE

def test_comm_no_reconfigure(notochord_init, comm, def_conf):
	logger_msg = comm.get_logger_messagge()
	logger_err = comm.get_logger_error()
	logger_tra = comm.get_logger_transmit()

	def_conf.comm.msg = comm.Output_Sink.OSC | comm.Output_Sink.FILE
	def_conf.comm.error = comm.Output_Sink.OSC
	def_conf.comm.transmit = comm.Output_Sink.STDERR | comm.Output_Sink.FILE | comm.Output_Sink.OSC
	notochord_init.setup(def_conf)
	assert logger_msg.sinks == comm.Output_Sink.OSC | comm.Output_Sink.FILE
	assert logger_err.sinks == comm.Output_Sink.OSC
	assert logger_tra.sinks == comm.Output_Sink.STDERR | comm.Output_Sink.FILE | comm.Output_Sink.OSC

	def_conf.comm.msg = comm.Output_Sink.NONE
	def_conf.comm.error = comm.Output_Sink.NONE
	def_conf.comm.transmit = comm.Output_Sink.NONE
	notochord_init.setup(def_conf, False)
	assert logger_msg.sinks == comm.Output_Sink.OSC | comm.Output_Sink.FILE
	assert logger_err.sinks == comm.Output_Sink.OSC
	assert logger_tra.sinks == comm.Output_Sink.STDERR | comm.Output_Sink.FILE | comm.Output_Sink.OSC
	
def test_comm_info(notochord_init, comm, config, capfd, def_conf, randstr):
	"""User should be able to log strings if the runtime was configured"""
	to_stdout = "out random string " + randstr
	captured = capfd.readouterr()
	assert to_stdout not in captured.out
	
	assert comm.Output_Sink.STDOUT in def_conf.comm.msg
	notochord_init.setup(def_conf)
	comm.info(to_stdout)
	
	sleep(ASYNC_SLEEP) # Wait until async logger is done
	captured = capfd.readouterr()
	assert to_stdout in captured.out
	#assert "HERE: " in captured

	#reconfigure and log again
	def_conf.comm.msg = comm.Output_Sink.STDERR
	to_stderr = "err random string " + randstr
	notochord_init.setup(def_conf)
	comm.info(to_stderr)
	sleep(ASYNC_SLEEP) # Wait until async logger is done
	captured = capfd.readouterr()
	assert to_stderr in captured.err

def test_comm_error(notochord_init, comm, config, capfd, def_conf, randstr):
	"""User should be able to log strings if the runtime was configured"""
	to_stdout = "out random string " + randstr
	captured = capfd.readouterr()
	assert to_stdout not in captured.err
	
	assert comm.Output_Sink.STDOUT in def_conf.comm.msg
	notochord_init.setup(def_conf)
	comm.error(to_stdout)
	sleep(ASYNC_SLEEP) # Wait until async logger is done
	captured = capfd.readouterr()
	assert to_stdout in captured.err

	#reconfigure and log again
	def_conf.comm.msg = comm.Output_Sink.STDERR
	to_stderr = "err random string " + randstr
	notochord_init.setup(def_conf)
	comm.error(to_stderr)
	sleep(ASYNC_SLEEP) # Wait until async logger is done
	captured = capfd.readouterr()
	assert to_stderr in captured.err

def test_comm_trace(notochord_init, comm, config, capfd, def_conf, randstr):
	"""User should be able to log strings if the runtime was configured"""
	to_stdout = "out random string " + randstr
	captured = capfd.readouterr()
	assert to_stdout not in captured.out
	
	assert comm.Output_Sink.STDOUT in def_conf.comm.msg
	def_conf.comm.log_level = 2
	notochord_init.setup(def_conf)
	comm.trace(to_stdout)
	sleep(ASYNC_SLEEP) # Wait until async logger is done
	captured = capfd.readouterr()
	assert to_stdout in captured.out

	#reconfigure and log again
	def_conf.comm.msg = comm.Output_Sink.STDERR
	to_stderr = "err random string " + randstr
	notochord_init.setup(def_conf)
	comm.trace(to_stderr)
	sleep(ASYNC_SLEEP) # Wait until async logger is done
	captured = capfd.readouterr()
	assert to_stderr in captured.err

def test_comm_debug(notochord_init, comm, config, capfd, def_conf, randstr):
	"""User should be able to log strings if the runtime was configured"""
	to_stdout = "out random string " + randstr
	captured = capfd.readouterr()
	assert to_stdout not in captured.out
	
	assert comm.Output_Sink.STDOUT in def_conf.comm.msg
	def_conf.comm.log_level = 1
	notochord_init.setup(def_conf)
	comm.debug(to_stdout)
	sleep(ASYNC_SLEEP) # Wait until async logger is done
	captured = capfd.readouterr()
	assert to_stdout in captured.out

	#reconfigure and log again
	def_conf.comm.msg = comm.Output_Sink.STDERR
	to_stderr = "err random string " + randstr
	notochord_init.setup(def_conf)
	comm.debug(to_stderr)
	sleep(ASYNC_SLEEP) # Wait until async logger is done
	captured = capfd.readouterr()
	assert to_stderr in captured.err

def test_comm_warn(notochord_init, comm, config, capfd, def_conf, randstr):
	"""User should be able to log strings if the runtime was configured"""
	to_stdout = "out random string " + randstr
	captured = capfd.readouterr()
	assert to_stdout not in captured.out
	
	assert comm.Output_Sink.STDOUT in def_conf.comm.msg
	def_conf.comm.log_level = 3
	notochord_init.setup(def_conf)
	comm.warn(to_stdout)
	sleep(ASYNC_SLEEP) # Wait until async logger is done
	captured = capfd.readouterr()
	assert to_stdout in captured.out

	#reconfigure and log again
	def_conf.comm.msg = comm.Output_Sink.STDERR
	to_stderr = "err random string " + randstr
	notochord_init.setup(def_conf)
	comm.warn(to_stderr)
	sleep(ASYNC_SLEEP) # Wait until async logger is done
	captured = capfd.readouterr()
	assert to_stderr in captured.err

def test_comm_critical(notochord_init, comm, config, capfd, def_conf, randstr):
	"""User should be able to log strings if the runtime was configured"""
	to_stdout = "out random string " + randstr
	captured = capfd.readouterr()
	assert to_stdout not in captured.out
	
	assert comm.Output_Sink.STDOUT in def_conf.comm.msg
	def_conf.comm.log_level = 4
	notochord_init.setup(def_conf)
	comm.critical(to_stdout)
	sleep(ASYNC_SLEEP) # Wait until async logger is done
	captured = capfd.readouterr()
	assert to_stdout in captured.out

	#reconfigure and log again
	def_conf.comm.msg = comm.Output_Sink.STDERR
	to_stderr = "err random string " + randstr
	notochord_init.setup(def_conf)
	comm.critical(to_stderr)
	sleep(ASYNC_SLEEP) # Wait until async logger is done
	captured = capfd.readouterr()
	assert to_stderr in captured.err

def test_comm_file(notochord_init, comm, config, def_conf, randstr, tmpdir):
	"""The comm outputs can be redirected to a file"""
	to_file = "file random string " + randstr
	assert comm.current_log_file() == ""

	def_conf.comm.msg = comm.Output_Sink.FILE
	def_conf.comm.filepath = str(tmpdir.join("templog.txt"))

	notochord_init.setup(def_conf)
	comm.info(to_file)
	comm.flush()
	sleep(ASYNC_SLEEP) # Wait until async logger is done
	#print("Current log file:", comm.current_log_file())
	from os.path import isfile 
	assert isfile(comm.current_log_file())
	with open (comm.current_log_file(), "r") as logfile:
		data=logfile.read()
		assert to_file in data


def test_comm_info_non_str(comm, capfd):
	"""When logging non-string objects a TypeError is raised"""
	to_stdout = 33.1
	with pytest.raises(TypeError):
		comm.info(to_stdout)

# def test_comm_info_noconf(comm, capfd, randstr):
# 	"""If the runtime was not configured the communicator functions
# 	should do nothing"""
# 	to_stdout = "random string " + randstr
# 	comm.info(to_stdout)
# 	sleep(ASYNC_SLEEP) # Wait until async logger is done
# 	captured = capfd.readouterr()
# 	assert to_stdout not in captured.out

def test_error_over_osc_address(notochord_init, comm, def_conf, randstr, copp_server):
	to_osc = "OSC random string " + randstr
	def_conf.comm.port = copp_server.port
	def_conf.comm.error = comm.Output_Sink.OSC
	def_conf.comm.use_bundles = False

	assert def_conf.osc.error_address == "/error"

	notochord_init.setup(def_conf)
	comm.flush()
	sleep(ASYNC_SLEEP)
	for packet in copp_server.get():
		pass #discard messages generated by setup()
	comm.error("#2 | error " + to_osc)
	comm.flush()
	sleep(ASYNC_SLEEP) # Wait until async logger is done
	sleep(0.1)
	for packet in copp_server.get():
		for msg in packet.get():
			assert msg.addrpattern == "/Chordata/error"

def test_msg_over_osc_address(notochord_init, comm, def_conf, randstr, copp_server):
	to_osc = "OSC random string " + randstr
	def_conf.comm.port = copp_server.port
	def_conf.comm.msg = comm.Output_Sink.OSC
	def_conf.comm.use_bundles = False

	assert def_conf.osc.msg_address == "/comm"

	notochord_init.setup(def_conf)
	comm.flush()
	sleep(ASYNC_SLEEP)
	for packet in copp_server.get():
		pass #discard messages generated by setup()
	comm.info("#1 | " + to_osc)
	comm.flush()
	sleep(ASYNC_SLEEP) # Wait until async logger is done
	sleep(0.1)
	for packet in copp_server.get():
		for msg in packet.get():
			assert msg.addrpattern == "/Chordata/comm"

def test_comm_osc(notochord_init, comm, def_conf, randstr, copp_server):
	"""This (integration) test launchs an OSC server which waits for a message.
	It tries to send several OSC message using comm.info() and checks the result"""
	to_osc = "OSC random string " + randstr

	def_conf.comm.port = copp_server.port
	def_conf.comm.msg = comm.Output_Sink.OSC
	def_conf.comm.use_bundles = False
	notochord_init.setup(def_conf)
	comm.flush()
	sleep(ASYNC_SLEEP)
	for packet in copp_server.get():
		pass #discard messages generated by setup()

	comm.info("#1 | " + to_osc)
	comm.info("#2 | " + to_osc)
	comm.flush()
	sleep(ASYNC_SLEEP)
	comm.info("#3 | " + to_osc)
	comm.info("#4 | " + to_osc)
	comm.flush()
	sleep(ASYNC_SLEEP) # Wait until async logger is done
	sleep(0.1)
	number_msg_received = 0
	for packet in copp_server.get():
		for msg in packet.get():
			if msg.typetags == ',s':
				number_msg_received += 1
				if not msg.payload[0].endswith(to_osc):
					pytest.fail(
						"Expected OSC message to end with `{}`, received `{}` instead".format(to_osc, msg.payload[0]))

	assert number_msg_received == 4

def test_comm_osc_many(notochord_init, comm, def_conf, randstr, copp_server):
	"""This (integration) test launchs an OSC server which waits for a message.
	It tries to send many* OSC message using comm.info() and checks the result
	*Mora than the pre-alocated quantity"""
	def_conf.comm.port = copp_server.port
	def_conf.comm.msg = comm.Output_Sink.OSC
	def_conf.comm.use_bundles = False
	notochord_init.setup(def_conf)
	comm.flush()
	sleep(ASYNC_SLEEP)
	for packet in copp_server.get():
		pass #discard messages generated by setup()
	
	how_many = 25
	for i in range(how_many):
		to_osc = "#{} | OSC random string {}".format(i,randstr)
		comm.info(to_osc)

	comm.flush()

	sleep(0.1)
	number_msg_received = 0
	for packet in copp_server.get():
		for msg in packet.get():
			number_msg_received += 1

	assert number_msg_received == how_many

def test_comm_osc_bundles(notochord_init, comm, def_conf, randstr, copp_server):
	"""This (integration) test launchs an OSC server which waits for a message.
	It tries to send two OSC message using comm.info(), with bundles ON.
	The comm messages should be send without bundles"""
	to_osc = "OSC random string " + randstr

	def_conf.comm.port = copp_server.port
	def_conf.comm.msg = comm.Output_Sink.OSC
	def_conf.comm.use_bundles = True
	notochord_init.setup(def_conf)
	comm.flush()
	sleep(ASYNC_SLEEP)
	for packet in copp_server.get():
		pass #discard messages generated by setup()
	
	comm.info("#1 | " + to_osc)
	comm.info("#2 | " + to_osc)
	comm.flush()
	
	sleep(0.1)
	number_msg_received = 0
	for packet in copp_server.get():
		#Common_packet is created when the messages arrive outside bundles
		assert isinstance(packet, copp_server.COPP_Common_packet) 
		for msg in packet.get():
			if msg.typetags == ',s':
				number_msg_received += 1
				if not msg.payload[0].endswith(to_osc):
					pytest.fail(
						"Expected OSC message to end with `{}`, received `{}` instead".format(to_osc, msg.payload[0]))

	assert number_msg_received == 2

def test_quat_osc(notochord_init, comm, def_conf, transmit_payload, copp_server):
	"""This (integration) test launchs an OSC server which waits for a message.
	It tries to send two OSC quaternions using comm.transmit(), with bundles OFF.
	Then checks the results"""

	def_conf.comm.port = copp_server.port
	def_conf.comm.msg = comm.Output_Sink.OSC
	def_conf.comm.use_bundles = False
	notochord_init.setup(def_conf)

	quats, addrs, tuple_quats = transmit_payload
	keep_addrs = addrs.copy()
	comm.transmit(addrs.pop(), quats.pop())
	comm.transmit(addrs.pop(), quats.pop())
	comm.flush()

	sleep(0.1)
	number_msg_received = 0
	for packet in copp_server.get():
		#Common_packet is created when the messages arrive outside bundles
		assert isinstance(packet, copp_server.COPP_Common_packet)
		for msg in packet.get():
			if msg.typetags == ',ffff':
				assert msg.addrpattern in keep_addrs
				assert msg.payload in tuple_quats
				number_msg_received += 1
				
	assert number_msg_received == 2


def test_quat_osc_bundles(notochord_init, comm, def_conf, transmit_payload, copp_server):
	"""This (integration) test launchs an OSC server which waits for a message.
	It tries to send two OSC quaternions using comm.transmit(), with bundles ON.
	Then checks the results"""

	def_conf.comm.port = copp_server.port
	def_conf.comm.msg = comm.Output_Sink.OSC
	def_conf.comm.use_bundles = True
	notochord_init.setup(def_conf)
	comm.flush()
	sleep(ASYNC_SLEEP)
	for packet in copp_server.get():
		pass #discard messages generated by setup()
	
	
	quats, addrs, tuple_quats = transmit_payload
	keep_addrs = addrs.copy()
	comm.transmit(addrs.pop(), quats.pop())
	comm.transmit(addrs.pop(), quats.pop())
	comm.flush()
	
	sleep(0.1)
	number_msg_received = 0
	for packet in copp_server.get():
		#Lazy_packet is created when the messages arrive in bundles
		assert isinstance(packet, copp_server.COPP_Lazy_Packet)
		for msg in packet.get():
			if msg.typetags == ',ffff':
				assert msg.addrpattern in keep_addrs
				assert msg.payload in tuple_quats
				number_msg_received += 1
				
	assert number_msg_received == 2

def test_payload_stdout(notochord_init, comm, capfd, def_conf,transmit_payload, Vector, Matrix3):
	def_conf.comm.transmit = comm.Output_Sink.STDOUT
	def_conf.comm.use_bundles = False
	notochord_init.setup(def_conf)
	quats, addrs, tuple_quats = transmit_payload
	
	quataddr = addrs.pop()
	quat = quats.pop()
	comm.transmit(quataddr, quat)
	comm.flush()
	captured = capfd.readouterr()
	pattr = r"{}.*,\W*{:.2f}.*,\W*{:.2f}.*,\W*{:.2f}.*,\W*{:.2f}.*".format(quataddr, *quat)
 
	assert re.search(pattr, captured.out)

	vecaddr = "/testvec"
	vec = Vector(1.22, 3.14, -0.87)
	comm.transmit(vecaddr, vec)
	comm.flush()
	pattr = r"{}.*,\W*{:.2f}.*,\W*{:.2f}.*,\W*{:.2f}.*".format(vecaddr, *vec)
	captured = capfd.readouterr()
	assert re.search(pattr, captured.out)

	m = Matrix3()
	with pytest.raises(NotImplementedError):
		comm.transmit("somematrix", m)

@pytest.mark.slow
def test_handlers_osc(notochord_init, def_conf, copp_server, Node_Scheduler, Mock_Mux, Mock_KCeptor, Mux_Channel):
	"""This (integration) test launchs an OSC server which waits for a message.
	It tries to send two OSC quaternions using comm.transmit(), with bundles OFF.
	Then checks the results"""

	def_conf.comm.port = copp_server.port
	def_conf.comm.use_bundles = False
	notochord_init.setup(def_conf)
	
	s = Node_Scheduler()
	mux = Mock_Mux(None, 0x73, "test_mux")
	sensors = []
	sensors_label = []

	for i in range(6):
		s.add_node(mux.get_branch(Mux_Channel(i+1)))

		for i in range(3):
			sensors.append(Mock_KCeptor(None, 0x1b, ("test_m_kc_" + str(uuid.uuid1()))))
			sensors_label.append(def_conf.osc.base_address+"/q/"+sensors[-1].label)
			s.add_node(sensors[-1])

	notochord_init.start(s)
	sleep(4)
	notochord_init.end()

	sleep(0.1)
	number_msg_received = 0
	for packet in copp_server.get():		
		#Common_packet is created when the messages arrive outside bundles
		assert isinstance(packet, copp_server.COPP_Common_packet)
		for msg in packet.get():
			if msg.typetags == ',ffff':
				assert msg.addrpattern in sensors_label
				#assert msg.payload in tuple_quats
				number_msg_received += 1
				
	assert number_msg_received > 5

@pytest.mark.slow
def test_handlers_osc_bundles(notochord_init, comm, def_conf, copp_server, Node_Scheduler, Mock_Mux, Mock_KCeptor, Mux_Channel):
	"""This (integration) test launchs an OSC server which waits for a message.
	It tries to send two OSC quaternions using comm.transmit(), with bundles ON.
	Then checks the results"""

	def_conf.comm.port = copp_server.port
	def_conf.comm.use_bundles = True
	notochord_init.setup(def_conf)

	s = Node_Scheduler()
	mux = Mock_Mux(None, 0x73, "test_mux")
	sensors = []
	sensors_label = []

	for i in range(6):
		s.add_node(mux.get_branch(Mux_Channel(i+1)))

		for j in range(3):
			sensors.append(Mock_KCeptor(None, 0x1b, ("{}-{}_test_m_kc_{}".format(i,j, str(uuid.uuid1())))))
			sensors_label.append("/%/" + sensors[-1].label)
			s.add_node(sensors[-1])

	notochord_init.start(s)
	sleep(5)
	notochord_init.end()
	
	sleep(0.1)
	number_msg_received = 0
	for packet in copp_server.get():
		#Lazy_packet is created when the messages arrive in bundles
		assert isinstance(packet, copp_server.COPP_Lazy_Packet)
		for msg in packet.get():
			if msg.typetags == ',ffff':
				assert msg.addrpattern in sensors_label
				#assert msg.payload in tuple_quats
				number_msg_received += 1
				
	assert number_msg_received > 5
