# -- START OF COPYRIGHT & LICENSE NOTICES -- 
#
# Chordata Motion's Notochord (python module)
# -- Core implementation of Chordata Motion's Open Source motion capture framework
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2021 Bruno Laurencich
# Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
#
# This file is part of Chordata Motion's Notochord (python module).
#
# Chordata Motion's Notochord (python module) is free software: you can 
# redistribute it and/or modify it under the terms of the 
# GNU General Public License as published by the Free Software Foundation, 
# either version 3 of the License, or (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata Motion's Notochord (python module).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 


def test_import(notochord_init):
	# core symbols imported globally
	assert notochord_init.init
	assert notochord_init.terminate
	
	# subpackages accesed directly
	from notochord import communicator as comm
	assert comm.info
	
	# subpackages accesed from main module
	assert notochord_init.config

def test_status(notochord_init):
	notochord_init.terminate()
	assert notochord_init.status() == 'IDLE'

	notochord_init.init()
	assert notochord_init.status() == 'STOPPED'

	# 'RUNNING' status is tested in test_timer.py

