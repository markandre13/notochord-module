# -- START OF COPYRIGHT & LICENSE NOTICES -- 
#
# Chordata Motion's Notochord (python module)
# -- Core implementation of Chordata Motion's Open Source motion capture framework
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2021 Bruno Laurencich
# Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
#
# This file is part of Chordata Motion's Notochord (python module).
#
# Chordata Motion's Notochord (python module) is free software: you can 
# redistribute it and/or modify it under the terms of the 
# GNU General Public License as published by the Free Software Foundation, 
# either version 3 of the License, or (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata Motion's Notochord (python module).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

from xmlrpc.client import Marshaller
import pytest
from random import random

def test_Math_type_mutable(Matrix3, KCeptor, error):
	""" Math types created by the user are mutable, the ones inside some notochord
	types as the KCeptor are not"""
	m = Matrix3()
	assert m.mutable
 
	m[0,0] = 0.94632
	assert m[0,0] == pytest.approx(0.94632)
	
	kc = KCeptor(None, 0x6b, "test_kc")
	assert kc.m_matrix.mutable == False
 
	with pytest.raises(error.Inmutable_Error):
		kc.m_matrix[0,0] = 0.97
 
def test_quaternion(Quaternion):
	q0 = Quaternion(1,0,0,0)
	q1 = Quaternion(1,2,3,4)
	q2 = Quaternion(1,0,0,0)

	assert tuple(q0) == (1,0,0,0)
	assert list(q1) == [1,2,3,4]

	assert q0 == q2
	assert q0 != q1

def test_quaternion_accesing(Quaternion):
	q = Quaternion(43, 2, -98, 2.34)
	
	assert q[0] == q.w
	assert q[1] == q.x
	assert q[2] == q.y
	assert q[3] == q.z
	
	with pytest.raises(IndexError):
		q[-1]

	with pytest.raises(IndexError):
		q[4]
  
def test_quaternion_setting(Quaternion):
	q = Quaternion(43, 2, -98, 2.34)
	
	q[1] = 608.25
	assert q[0] == 43
	assert q[1] == pytest.approx(608.25)
	assert q[2] == pytest.approx(-98)
	assert q[3] == pytest.approx(2.34)

	q.w = -302.47
	assert q[0] == pytest.approx(-302.47)
	q.y = 55.21
	assert q[2] == pytest.approx(55.21) 

def test_quaternion_conjugate(Quaternion):
	q = Quaternion(1,2,3,4)
	q2 = q.conjugate()

	assert q[0] == q2[0]
	assert q[1] == -q2[1]
	assert q[2] == -q2[2]
	assert q[3] == -q2[3]

	q[1] = 5
	q2 = q.conjugate()

	assert q2[1] == -5

def test_quaternion_inverse(Quaternion):
	q0 = Quaternion(1,0,1,0)
	q1 = q0.inverse()

	assert q1[0] == 0.5
	assert q1[1] == 0
	assert q1[2] == -0.5
	assert q1[3] == 0

def test_quaternion_normalize(Quaternion):
	q = Quaternion(2,0,0,0)
	q.normalize()

	assert q[0] == 1
	assert q[1] == 0
	assert q[2] == 0
	assert q[3] == 0

def test_quaternion_normalized(Quaternion):
	q0 = Quaternion(2,0,0,0)
	q1 = q0.normalized()

	assert q1[0] == 1
	assert q1[1] == 0
	assert q1[2] == 0
	assert q1[3] == 0

def test_quaternion_norm(Quaternion):
	q = Quaternion(1,2,3,4)

	assert q.norm() == pytest.approx(5.477225575)

	q[0] = 10

	assert q.norm() == pytest.approx(11.357816692)

def test_quaternion_dot(Quaternion):
	q0 = Quaternion(1,2,3,4)
	q1 = Quaternion(1,2,3,4)

	assert q0.dot(q1) == 30

def test_quaternion_isApprox(Quaternion):
	q0 = Quaternion(1,2,3,4)
	q1 = Quaternion(1,2,3,5)

	assert q0.isApprox(q1, 1) == True

	q2 = Quaternion(1,0,0,0)
	q3 = Quaternion(1,0.002, 0.002, 0.002)

	assert q2.isApprox(q3, 0.01) == True

def test_quaternion_slerp(Quaternion):
	q0 = Quaternion(0,0,0,0)
	q1 = Quaternion(1,0,0,0)
	q2 = q0.slerp(0.5, q1)

	assert q2[0] == pytest.approx(0.7071067)
	assert q2[1] == 0
	assert q2[2] == 0
	assert q2[3] == 0

def test_quaternion_operator_mul(Quaternion):
	q0 = Quaternion(2,0,0,0)
	q1 = Quaternion(3,0,0,0)
	q2 = q0 * q1

	assert q2[0] == 6

def test_quaternion_toRotationMatrix(Quaternion):
	q = Quaternion(0.7071067, 0.7071067, 0, 0)
	m = q.toRotationMatrix()

	assert pytest.approx(m[0,0]) == 1
	assert pytest.approx(m[0,1]) == 0
	assert pytest.approx(m[1,2]) == -1
	assert pytest.approx(m[2,1]) == 1

def test_quaternion_constructor(Quaternion):
	q = Quaternion()

	assert q[0] == 1
	assert q[1] == 0
	assert q[2] == 0
	assert q[3] == 0

	q1 = Quaternion(1,2,3,4)

	assert q1[0] == 1
	assert q1[1] == 2
	assert q1[2] == 3
	assert q1[3] == 4

	q2 = Quaternion((1,2,3,4))

	assert q2[0] == 1
	assert q2[1] == 2
	assert q2[2] == 3
	assert q2[3] == 4

	#Raise ValueError if constructed with incorrect arguments
	with pytest.raises(ValueError):
		q3 = Quaternion(1,2,3,4,5)

	with pytest.raises(ValueError):
		q3 = Quaternion("a")
	
	with pytest.raises(ValueError):
		q3 = Quaternion((1,2,3,4,5))
	
	with pytest.raises(ValueError):
		q3 = Quaternion(("a"))


def test_vector_constructor(Vector):
	v = Vector()

	assert v[0] == 0
	assert v[1] == 0
	assert v[2] == 0

	v1 = Vector(1,2,3)

	assert v1[0] == 1
	assert v1[1] == 2
	assert v1[2] == 3

	v2 = Vector((1,2,3))

	assert v2[0] == 1
	assert v2[1] == 2
	assert v2[2] == 3

	#Raise ValueError if constructed with incorrect arguments
	with pytest.raises(ValueError):
		v3 = Vector(1,2,3,4,5)

	with pytest.raises(ValueError):
		v3 = Vector("a")
	
	with pytest.raises(ValueError):
		v3 = Vector((1,2,3,4,5))
	
	with pytest.raises(ValueError):
		v3 = Vector(("a"))

def test_vector_accesing(Vector):
	v = Vector(43, -98, 2.34)
	
	assert v[0] == v.x
	assert v[1] == v.y
	assert v[2] == v.z
	
	with pytest.raises(IndexError):
		v[-1]

	with pytest.raises(IndexError):
		v[3]
  
def test_vector_setting(Vector):
	v = Vector(43, -98, 2.34)
	
	v[1] = 608.25
 
	assert v[0] == 43
	assert v[1] == pytest.approx(608.25)
	assert v[2] == pytest.approx(2.34)

	v.x = -302.47
	assert v[0] == pytest.approx(-302.47)
	
	
def test_matrix_accesing(Matrix3):
	vals = tuple(random() for _ in range(9))
	m = Matrix3(*vals)
	
	#test values accesor
	for i,val in enumerate(vals):
		assert m[i//3, i%3] == pytest.approx(val)
		
	#Raise TypeError if accesed with incorrect arguments
	with pytest.raises(TypeError):
		m[1]
	
	with pytest.raises(TypeError):
		m[1,1,2]
  
	with pytest.raises(TypeError):
		m["x"]
	
	#bounds checking
	with pytest.raises(IndexError):
		m[2,3]

	with pytest.raises(IndexError):
		m[-1,0]
	
	# test convertion to nested iterable
	iterable = tuple(m)
	for i,val in enumerate(vals):
		assert iterable[i//3][i%3] == pytest.approx(val)


def test_matrix_build(Matrix3):
	# build with no arguments should return an Identity matrix
	mi = Matrix3()
	mi_tuple = (1,0,0,0,1,0,0,0,1)
	for i, val in enumerate(mi_tuple):
		assert mi[i//3, i%3] == val
	
	#build matrix with a single value
	m = Matrix3(2.345)
	# test values accesor
	for i in range(9):
		assert m[i//3, i%3] == pytest.approx(2.345)
	
	# build matrix from a linear 9-value iterable
	vals = tuple(random() for _ in range(9))
	m = Matrix3(vals)
	#test values accesor
	for i,val in enumerate(vals):
		assert m[i//3, i%3] == pytest.approx(val)
  
	# Should only accept 1 or 9 positional args
	with pytest.raises(ValueError):
		Matrix3(*vals[:7])
  
	# Should only accept numeric args
	with pytest.raises(ValueError):
		Matrix3("2.34")
	with pytest.raises(ValueError):
		Matrix3((6.54,)*8 + ("2.34",))
  
  
def test_matrix_compare(Matrix3):
	vals1 = tuple(random() for _ in range(9))
	m1a = Matrix3(*vals1)
	m1b = Matrix3(*vals1)
 
	vals2 = vals1[:-1] + (random(),)
	m2 = Matrix3(*vals2)
 
	assert m1a is not m1b
	assert m1a is m1a
 
	assert m1a == m1b
	assert m1a == m1a
	assert m1a != m2

def test_matrix_inverse(Matrix3):
	mat = Matrix3()
	mat[0,0] = 2
	mat[1,1] = 2
	mat[2,2] = 2

	inverse = mat.inverse()

	assert inverse[0,0] == 0.5
	assert inverse[1,1] == 0.5
	assert inverse[2,2] == 0.5
	assert inverse[0,1] == 0

	assert mat[0,0] == 2
 
def test_vector_cross(Vector):
	v0 = Vector(1,2,3)
	v1 = Vector(4,5,6)
	v2 = v0.cross(v1)

	assert v2.x == -3
	assert v2.y == 6
	assert v2.z == -3

def test_vector_dot(Vector):
	v0 = Vector(1,2,3)
	v1 = Vector(3,4,5)

	assert v0.dot(v1) == 26

def test_vector_norm(Vector):
	v = Vector(1,2,3)

	assert v.norm() == pytest.approx(3.741657387)

	v[0] = 10

	assert v.norm() == pytest.approx(10.630145813)

def test_matrix_norm(Matrix3):
	m = Matrix3(2)

	assert m.norm() == 6

def test_vector_normalized(Vector):
	v = Vector(2,0,0)
	v = v.normalized()

	assert v[0] == 1
	assert v[1] == 0
	assert v[2] == 0

def test_matrix_determinant(Matrix3):
	m = Matrix3(1)

	assert m.determinant() == 0

def test_vector_scalar_mul(Vector):
	v = Vector(1,2,3)
	v = 2 * v

	assert v[0] == 2
	assert v[1] == 4
	assert v[2] == 6

def test_matrix_scalar_mul(Matrix3):
	m0 = Matrix3(2)
	m0 *= 2

	assert m0[0,0] == 4
	assert m0[1,1] == 4
	assert m0[2,2] == 4

	m0 = 0  * m0

	assert m0[0,0] == 0
	assert m0[1,1] == 0
	assert m0[2,2] == 0

	m1 = Matrix3(2)
	m1 *= m1

	assert m1[0,0] == 12
	assert m1[1,1] == 12
	assert m1[2,2] == 12

def test_quaternion_copy(Quaternion):
	q0 = Quaternion(1,0,0,0)
	q1 = q0.copy()

	assert q0 == q1

	q1.w = 2

	assert q0 == (1,0,0,0)
	assert q1 == (2,0,0,0)

def test_vector_copy(Vector):
	v0 = Vector(1,2,3)
	v1 = v0.copy()

	assert v0 == v1

	v1.x = 3

	assert v0 == (1,2,3)
	assert v1 == (3,2,3)

def test_matrix_copy(Matrix3):
	m0 = Matrix3(1)
	m1 = m0.copy()

	assert m0 == m1

	m1[0,0] = 5

	assert m0[0,0] == 1
	assert m1[0,0] == 5

def test_matrix4_accesing(Matrix4):
	vals = tuple(random() for _ in range(16))
	m = Matrix4(*vals)
	
	#test values accesor
	for i,val in enumerate(vals):
		assert m[i//4, i%4] == pytest.approx(val)
		
	#Raise TypeError if accesed with incorrect arguments
	with pytest.raises(TypeError):
		m[1]
	
	with pytest.raises(TypeError):
		m[1,1,2]
  
	with pytest.raises(TypeError):
		m["x"]
	
	#bounds checking
	with pytest.raises(IndexError):
		m[4,3]

	with pytest.raises(IndexError):
		m[-1,0]
	
	# test convertion to nested iterable
	iterable = tuple(m)
	for i,val in enumerate(vals):
		assert iterable[i//4][i%4] == pytest.approx(val)


def test_matrix4_build(Matrix4):
	# build with no arguments should return an Identity matrix
	mi = Matrix4()
	mi_tuple = (1,0,0,0, 0,1,0,0, 0,0,1,0, 0,0,0,1)
	for i, val in enumerate(mi_tuple):
		assert mi[i//4, i%4] == val
	
	#build matrix with a single value
	m = Matrix4(2.345)
	# test values accesor
	for i in range(16):
		assert m[i//4, i%4] == pytest.approx(2.345)
	
	# build matrix from a linear 9-value iterable
	vals = tuple(random() for _ in range(16))
	m = Matrix4(vals)
	#test values accesor
	for i,val in enumerate(vals):
		assert m[i//4, i%4] == pytest.approx(val)
  
	# Should only accept 1 or 16 positional args
	with pytest.raises(ValueError):
		Matrix4(*vals[:7])
  
	# Should only accept numeric args
	with pytest.raises(ValueError):
		Matrix4("2.34")
	with pytest.raises(ValueError):
		Matrix4((6.54,)*8 + ("2.34",))
  
  
def test_matrix_compare(Matrix4):
	vals1 = tuple(random() for _ in range(16))
	m1a = Matrix4(*vals1)
	m1b = Matrix4(*vals1)
 
	vals2 = vals1[:-1] + (random(),)
	m2 = Matrix4(*vals2)
 
	assert m1a is not m1b
	assert m1a is m1a
 
	assert m1a == m1b
	assert m1a == m1a
	assert m1a != m2
