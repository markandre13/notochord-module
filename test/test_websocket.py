import pytest
import websockets
import asyncio
import time
import re, uuid
import queue
from copp_server import COPP_Lazy_Packet, COPP_Message
from copp_server.osc4py3_buildparse.oscbuildparse import decode_packet

def test_websocket_start(notochord_init, comm, def_conf, Quaternion, error):
	logger_tra = comm.get_logger_transmit()
	logger_msg = comm.get_logger_messagge()
	logger_error = comm.get_logger_error()
	def_conf.comm.transmit = comm.Output_Sink.WEBSOCKET
	def_conf.comm.msg = comm.Output_Sink.WEBSOCKET
	def_conf.comm.error = comm.Output_Sink.WEBSOCKET
	def_conf.comm.websocket_port = 7890
	notochord_init.setup(def_conf)
	assert comm.Output_Sink.WEBSOCKET in logger_tra.sinks
	assert comm.Output_Sink.WEBSOCKET in logger_msg.sinks
	assert comm.Output_Sink.WEBSOCKET in logger_error.sinks

	# test reconfigure
	def_conf.comm.transmit = comm.Output_Sink.NONE
	def_conf.comm.msg = comm.Output_Sink.NONE
	def_conf.comm.error = comm.Output_Sink.NONE
	notochord_init.setup(def_conf)
	assert logger_tra.sinks == comm.Output_Sink.NONE
	assert logger_error.sinks == comm.Output_Sink.NONE
	assert logger_msg.sinks == comm.Output_Sink.NONE

	def_conf.comm.transmit = comm.Output_Sink.WEBSOCKET | comm.Output_Sink.OSC
	with pytest.raises(error.Logic_Error):
		notochord_init.setup(def_conf)

def test_websocket_default_config(notochord_init, def_conf):
	def_conf.comm.websocket_port = 7890
	assert def_conf.comm.websocket_port == 7890
	with pytest.raises(TypeError):
		def_conf.comm.websocket_port = 'a'

def test_websocket_msg_send(notochord_init, comm, def_conf):
	payload = ["TEST MESSAGE"]
	def_conf.comm.msg |= comm.Output_Sink.WEBSOCKET
	def_conf.comm.websocket_port = 7681
	def_conf.comm.use_bundles = False
	notochord_init.setup(def_conf)
	comm.flush()
	time.sleep(0.05)
	result = asyncio.run(send_messages_loop(comm, def_conf, "Chordata_comm", payload))
	result = asyncio.run(send_messages_loop(comm, def_conf, "Chordata_comm", payload))
	assert len(result) == 1
	r = result[0].split('\0')
	for s in r:
		s.strip('\0')
	
	print(bytes("".join(r), 'utf-8'))
	assert "".join(r) == "/Chordata/comm,s{}".format(payload[0])

def test_websocket_msg_send_multi(notochord_init, comm, def_conf):
    payload = ["TEST MESSAGE", "TEST MESSAGE 2", "TEST MESSAGE 3"]
    def_conf.comm.msg |= comm.Output_Sink.WEBSOCKET
    def_conf.comm.websocket_port = 7681
    def_conf.comm.use_bundles = False
    notochord_init.setup(def_conf)
    comm.flush()
    time.sleep(0.05)
    result = asyncio.run(send_messages_loop(comm, def_conf, "Chordata_comm", payload))
    result = asyncio.run(send_messages_loop(comm, def_conf, "Chordata_comm", payload))
    assert len(result) == len(payload)
    clean_result = []
    for msg in result:
        r = msg.split('\0')
        for s in r:
            s.strip('\0')
        clean_result.append("".join(r))
    for i in range(len(result)):
        assert clean_result[i] == "/Chordata/comm,s{}".format(payload[i])

def test_websocket_error_send(notochord_init, comm, def_conf):
	payload = ["ERROR MESSAGE"]
	def_conf.comm.error |= comm.Output_Sink.WEBSOCKET
	def_conf.comm.websocket_port = 7681
	def_conf.comm.use_bundles = False
	notochord_init.setup(def_conf)
	comm.flush()
	time.sleep(0.05)
	result = asyncio.run(send_messages_loop(comm, def_conf, "Chordata_error", payload))
	assert len(result) == 1
	r = result[0].split('\0')
	for s in r:
		s.strip('\0')
	
	print(bytes("".join(r), 'utf-8'))
	assert "".join(r) == "/Chordata/error,s{}".format(payload[0])

def test_websocket_error_send_multi(notochord_init, comm, def_conf):
    payload = ["ERROR MESSAGE", "ERROR MESSAGE 2", "ERROR MESSAGE 3"]
    def_conf.comm.error |= comm.Output_Sink.WEBSOCKET
    def_conf.comm.websocket_port = 7681
    def_conf.comm.use_bundles = False
    notochord_init.setup(def_conf)
    comm.flush()
    time.sleep(0.05)
    result = asyncio.run(send_messages_loop(comm, def_conf, "Chordata_error", payload))
    assert len(result) == len(payload)
    clean_result = []
    for msg in result:
        r = msg.split('\0')
        for s in r:
            s.strip('\0')
        clean_result.append("".join(r))
    for i in range(len(result)):
        assert clean_result[i] == "/Chordata/error,s{}".format(payload[i])

def test_websocket_send_single_quaternion(notochord_init, comm, def_conf, Quaternion):
    quat_addr = ['/Chordata/q']
    quat = [Quaternion()]
    def_conf.comm.transmit = comm.Output_Sink.WEBSOCKET
    def_conf.comm.websocket_port = 7681
    def_conf.comm.use_bundles = False
    notochord_init.setup(def_conf)
    result = asyncio.run(send_receive_loop(comm, def_conf, quat_addr, quat))
    assert len(result) == 1
    msg = decode_packet(result[0])
    number_msg_received = 0
    if msg.typetags == ',ffff':
        assert msg.addrpattern == quat_addr[0]
        assert msg.payload == quat[0]
        number_msg_received += 1
    assert number_msg_received == len(quat)

def test_websocket_send_single_vector(notochord_init, comm, def_conf, Vector):
    vec_addr = ['/Chordata/v']
    vec = [Vector()]
    def_conf.comm.transmit = comm.Output_Sink.WEBSOCKET
    def_conf.comm.websocket_port = 7681
    def_conf.comm.use_bundles = False
    notochord_init.setup(def_conf)
    result = asyncio.run(send_receive_loop(comm, def_conf, vec_addr, vec))
    assert len(result) == 1
    msg = decode_packet(result[0])
    number_msg_received = 0
    if msg.typetags == ',fff':
        assert msg.addrpattern == vec_addr[0]
        assert msg.payload == vec[0]
        number_msg_received += 1
    assert number_msg_received == len(vec)

def test_websocket_send_many_quaternion(notochord_init, comm, def_conf, Quaternion):
    quat_addr = ['/Chordata/q','/Chordata/q']
    quat = [Quaternion(), Quaternion(0.0, 1.0, 0.0, 1.0)]
    def_conf.comm.transmit = comm.Output_Sink.WEBSOCKET
    def_conf.comm.websocket_port = 7681
    def_conf.comm.use_bundles = False
    notochord_init.setup(def_conf)
    result = asyncio.run(send_receive_loop(comm, def_conf, quat_addr, quat))
    assert len(result) == len(quat)
    number_msg_received = 0
    for i in range(len(result)):
        msg = decode_packet(result[i])
        if msg.typetags == ',ffff':
            assert msg.addrpattern == quat_addr[i]
            assert msg.payload == quat[i]
            number_msg_received += 1
    assert number_msg_received == len(quat)

def test_websocket_send_many_vector(notochord_init, comm, def_conf, Vector):
    vec_addr = ['/Chordata/v', '/Chordata/v']
    vec = [Vector(), Vector(1,2,3)]
    def_conf.comm.transmit = comm.Output_Sink.WEBSOCKET
    def_conf.comm.websocket_port = 7681
    def_conf.comm.use_bundles = False
    notochord_init.setup(def_conf)
    result = asyncio.run(send_receive_loop(comm, def_conf, vec_addr, vec))
    assert len(result) == len(vec)
    number_msg_received = 0
    for i in range(len(result)):
        msg = decode_packet(result[i])
        if msg.typetags == ',fff':
            assert msg.addrpattern == vec_addr[i]
            assert msg.payload == vec[i]
            number_msg_received += 1
    assert number_msg_received == len(vec)

def test_websocket_send_bundle_quaternion(notochord_init, comm, def_conf, transmit_payload):
    quat, quat_addr, tp_quat = transmit_payload
    quat = tuple(quat)
    quat_addr = tuple(quat_addr)
    def_conf.comm.transmit = comm.Output_Sink.WEBSOCKET
    def_conf.comm.websocket_port = 7681
    def_conf.comm.use_bundles = True
    notochord_init.setup(def_conf)
    result = asyncio.run(send_receive_loop(comm, def_conf, quat_addr, quat))
    assert len(result) == 1
    pkts = COPP_Lazy_Packet(result[0])
    number_msg_received = 0
    for msg in pkts.get():
        if msg.typetags == ',ffff':
            assert msg.addrpattern == quat_addr[number_msg_received]
            assert msg.payload == quat[number_msg_received]
            number_msg_received += 1
    assert number_msg_received == len(quat)

def test_websocket_send_bundle_vector(notochord_init, comm, def_conf, transmit_payload_vector):
    vec, vec_addr, _ = transmit_payload_vector
    vec = tuple(vec)
    vec_addr = tuple(vec_addr)
    def_conf.comm.transmit = comm.Output_Sink.WEBSOCKET
    def_conf.comm.websocket_port = 7681
    def_conf.comm.use_bundles = True
    notochord_init.setup(def_conf)
    result = asyncio.run(send_receive_loop(comm, def_conf, vec_addr, vec))
    assert len(result) == 1
    pkts = COPP_Lazy_Packet(result[0])
    number_msg_received = 0
    for msg in pkts.get():
        if msg.typetags == ',fff':
            assert msg.addrpattern == vec_addr[number_msg_received]
            assert msg.payload == vec[number_msg_received]
            number_msg_received += 1
    assert number_msg_received == len(vec)

@pytest.mark.slow
def test_websocket_send_bundle_kceptor(notochord_init, comm, def_conf, Node_Scheduler, Mock_Mux, Mock_KCeptor, Mux_Channel):
    def_conf.comm.use_bundles = True
    def_conf.comm.transmit = comm.Output_Sink.WEBSOCKET
    def_conf.comm.websocket_port = 7654
    notochord_init.setup(def_conf)
    s = Node_Scheduler()
    mux = Mock_Mux(None, 0x73, "test_mux")
    sensors = []
    sensors_label = []
    for i in range(6):
        s.add_node(mux.get_branch(Mux_Channel(i+1)))

        for i in range(3):
            sensors.append(Mock_KCeptor(None, 0x1b, ("test_m_kc_" + str(uuid.uuid1()))))
            sensors_label.append("/%/" + sensors[-1].label)
            s.add_node(sensors[-1])

    bundle_max = 2
    result = asyncio.run(scheduler_send(notochord_init, def_conf, s, bundle_count=bundle_max))
    print(len(result))
    assert len(result) == bundle_max
    bundle_count = 0
    for bundle in result:
        print(bundle)
        pkts = COPP_Lazy_Packet(bundle)
        assert isinstance(pkts, COPP_Lazy_Packet)
        number_quat_received = 0
        number_raw_received = 0
        bundle_count += 1
        for msg in pkts.get():
            if msg.typetags == ',ffff':
                is_quat = True
                assert msg.addrpattern in sensors_label
                number_quat_received += 1
            if msg.typetags == ',iiiiiiiii':
                is_quat = False
                assert msg.addrpattern in sensors_label
                number_raw_received += 1
        if is_quat:
            assert number_quat_received == 18
        else:
            assert number_raw_received == 18
    assert bundle_count == bundle_max


async def scheduler_send(noto, conf, scheduler, bundle_count):
    msg = asyncio.Queue()
    # spawn two workers in parallel, and have them send
    # data to our queue
    ev = asyncio.Event()
    client_disconect = asyncio.Event()
    client_task = asyncio.create_task(
            client_recv("Chordata",
                        conf.comm.websocket_port, msg, ev, client_disconect))
    s = scheduler
    # Wait for client to connect
    await ev.wait()
    noto.start(s)
    result = list()
    msg_count = 0
    while msg_count < bundle_count:
        try:
            source, data = await asyncio.wait_for(msg.get(), timeout=0.2)
            result.append(data)
            msg_count += 1
            print("MSG from {}: {}".format(source, data))
        except asyncio.TimeoutError:
            # if client is no longer connected break
            if client_disconect.is_set():
                break

    noto.end()
    client_task.cancel()
    return result

# Helper functions to test the websocket server
async def client_recv(ws_subprotocol, ws_port, queue, start_event, client_disconect):
    inURL = 'ws://localhost:{}'.format(ws_port)
    #await asyncio.sleep(0.3)
    print("CONNECTING CLIENT to", inURL)
    async with websockets.connect(inURL, subprotocols=[ws_subprotocol]) as ws:
        print("CLIENT CONNECTED to", inURL)
        # Signal server side to start sending packets
        start_event.set()
        while True:
            try:
                data = await asyncio.wait_for(ws.recv(), timeout=0.2)
                #data = await ws.recv()
                print("Cliente {}".format(data))
                await queue.put(('client', data))

            # Signal we're about to disconect from server
            except asyncio.TimeoutError:
                client_disconect.set()
                break
            except asyncio.CancelledError:
                client_disconect.set()
                break

            except Exception as e:
                print("Client Error, {}|{}".format(type(e),e ))
                await queue.put(('client', "END because of error: {}|{}".format(type(e),e )))

async def send_receive_loop(comm, conf, addr, payload):
    msg = asyncio.Queue()
    # spawn two workers in parallel, and have them send
    # data to our queue
    ev = asyncio.Event()
    client_disconect = asyncio.Event()
    client_task = asyncio.create_task(
            client_recv("Chordata",
                        conf.comm.websocket_port, msg, ev, client_disconect))
    # Wait for client to connect
    await ev.wait()
    for i in range(len(payload)):
        comm.transmit(addr[i], payload[i])
    comm.flush()
    result = list()
    while True:
        try:
            source, data = await asyncio.wait_for(msg.get(), timeout=0.2)

            result.append(data)
            print("MSG from {}: {}".format(source, data))
        except asyncio.TimeoutError:
            # if client is no longer connected break
            if client_disconect.is_set():
                break

    client_task.cancel()

    return result

async def send_messages_loop(comm, conf, protocol, payload):
    msg = asyncio.Queue()
    # spawn two workers in parallel, and have them send
    # data to our queue
    ev = asyncio.Event()
    client_disconect = asyncio.Event()
    client_task = asyncio.create_task(
            client_recv(protocol,
                        conf.comm.websocket_port, msg, ev, client_disconect))
    # Wait for client to connect
    await ev.wait()
    for p in payload:
        if protocol == "Chordata_comm":
            comm.info(p)
        elif protocol == "Chordata_error":
            comm.error(p)
    comm.flush()
    result = list()
    while True:
        try:
            source, data = await asyncio.wait_for(msg.get(), timeout=0.2)

            result.append(data)
            print("MSG from {}: {}".format(source, data))
        except asyncio.TimeoutError:
            # if client is no longer connected break
            if client_disconect.is_set():
                break

    client_task.cancel()

    return result
