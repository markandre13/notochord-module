import sysconfig
import configparser
import scons_compiledb
from os import popen, environ, cpu_count, path
import re
import json

print("Using python{}".format(sysconfig.get_config_var('LDVERSION')))
EnsurePythonVersion(3, 7)
EnsureSConsVersion(4, 2)

git_hash = popen('git rev-parse HEAD').read()

AddOption('--debug-build',
			dest='debug',
			action='store_true',
			help='compile with debug flags [ -g -O0 ]')

AddOption('--arm64',
			dest='arm64',
			action='store_true',
			help='cross compile for arm64')

AddOption('--armhf',
			dest='armhf',
			action='store_true',
			help='cross compile for arm64')

AddOption('--trace-i2c',
			dest='trace-i2c',
			action='store_true',
			help='pedantic trace of each i2c transaction')

AddOption('--trace-websocket',
		dest='trace-websocket',
		action='store_true',
		help='log low level websocket info')

AddOption('--verbose',
			dest='verbose',
			action='store_true',
			help='print compiler commands')

AddOption('--pedantic',
			dest='pedantic',
			action='store_true',
			help='print compiler commands and include tree')

AddOption('--ccflags',
			dest='ccflags',
			type='string',
			action='store',
			default="",
			help='Flags to pass to the C++ compiler')

AddOption('--cython',
			dest='cython',
			type=str,
			default="cython",
			help='Path for the Cython compiler')


if not GetOption('num_jobs') >1 and cpu_count():
	SetOption('num_jobs', cpu_count())
	
print ("Running with -j", GetOption('num_jobs'))

   
if GetOption('trace-i2c'):
	cppdefines = ['NOTOCHORD_TRACE_IO']
else:
	cppdefines = [] 

if GetOption('trace-websocket'):
	cppdefines += ['NOTOCHORD_TRACE_WEBSOCKET']

# -----------  SRC  -----------

env = Environment(
		CXX= 'g++',
		CC = 'gcc',
		CXXFLAGS = ["-std=c++14", "-pthread", "-Wno-psabi"],
		CCFLAGS = GetOption('ccflags').split(" "), 
		CPPPATH = ['#lib'] + Split(sysconfig.get_config_var('INCLUDEPY')),
  		CPPDEFINES = cppdefines,
		_BUILD_DIR = '#/build',
		_DIST_DIR = '#/dist',
		LIBS = ['cap']
		)

if GetOption('arm64') and GetOption('armhf'):
	prtin('Not possible to have both arm64 and armhf active at the same time')
	exit(1)

def set_multiarch(arch, pre_COMSTR):
	_basedir = Dir('#').srcnode().abspath
	json_path = path.join(_basedir, 
						'multiarch/inc/{}/multiarch_conf_vars.json'.format(arch))
	print("loading multiarch configuration at:", json_path )
	with open(json_path) as json_file:
		march_vars = json.load(json_file)

	print("Cross compiling for {}:python{}".format(march_vars["MULTIARCH"], march_vars["LDVERSION"]))

	env['CXX'] = "{}-g++".format(arch)
	env['CC'] = "{}-gcc".format(arch)
	env['CPPPATH'] = ['#lib', "#multiarch/inc/python{}".format(march_vars["LDVERSION"]) , "#multiarch/inc"]
	env['_BUILD_DIR'] = '#/multiarch/{}/build'.format(arch)
	env['_DIST_DIR'] =  '#/multiarch/{}/dist'.format(arch)
	return march_vars

pre_COMSTR = ""
if GetOption('arm64'):
	arch="aarch64-linux-gnu"
	pre_COMSTR = "[arm64] "
	env["CPPDEFINES"] += ['_M_X64'] # this makes oscpack treat integers correctly
	march_vars = set_multiarch(arch, pre_COMSTR)


if GetOption('armhf'):
	arch="arm-linux-gnueabihf"
	pre_COMSTR = "[armhf] "
	march_vars = set_multiarch(arch, pre_COMSTR)

if GetOption('debug'):
	env['CCFLAGS'] += ['-g', '-O0']
	env['LINKFLAGS'] = ['-g']
else:
	env['CCFLAGS'] += ['-O3']

if not GetOption('verbose') and not GetOption('pedantic'):
	env['SHCXXCOMSTR'] = pre_COMSTR + "Compiling SRC shared object: $TARGET",
	env['SHCCCOMSTR'] = pre_COMSTR + "Compiling C SRC shared object: $TARGET",
	env['CXXCOMSTR'] = pre_COMSTR + "Compiling shared library: $TARGET",
	env['SHLINKCOMSTR'] = pre_COMSTR + "Linking shared library $TARGET",
elif GetOption('pedantic'):
	env['CCFLAGS'] += ['-H']

 
if GetOption('trace-i2c'):
	env['CPPDEFINES'] += ['NOTOCHORD_TRACE_IO']

scons_compiledb.enable(env)

_src = SConscript(['src/SConscript'], exports = ['env', 'git_hash'])

# -----------  LIBS  -----------

_lib = SConscript(['lib/SConscript'], exports = ['env', 'pre_COMSTR'])


chordlib = env.SharedLibrary('{}/notochord/chordata'.format(env['_DIST_DIR']),  _src + _lib )

# -----------  CYTHON  -----------
def scan_pxd(node, env, _path):
	_pxd = re.sub(r"\.pyx$", ".pxd", str(node)) 
	return [path.basename(_pxd)]

pxd_scanner = Scanner(scan_pxd)

cy_action = '{} $SOURCE -3 --cplus -o $TARGET $CYFLAGS -I$CYINCL -X $CYDIRECTS'.format(GetOption('cython'))
print("Using cy action:", cy_action)
cython_builder =  builder = Builder(
		action=Action(cy_action, '$CYCOMSTR'),
	   suffix='.cpp', src_suffix='.pyx', source_scanner=pxd_scanner)


keys_propagate = ["PATH", "LC_ALL", "LANG"] #get these keys from current environ

cython_env = Environment(BUILDERS = {'Cython' : cython_builder}, 
	CXX= env["CXX"],
	CXXFLAGS = ["-std=c++14"],
	ENV = {key: val for key,val in environ.items() if key in keys_propagate },
	CCFLAGS = sysconfig.get_config_var('CFLAGS') + " " + " ".join(env['CCFLAGS']),
	LINKFLAGS = sysconfig.get_config_var('LDFLAGS') + " -Wl,-rpath='$$ORIGIN'", # attention! dual $ signs in $$ORIGIN
	CPPPATH = ["#/src"] + env['CPPPATH'],
	CYINCL = "src",
	CYDIRECTS="language_level=3",
	SHLIBPREFIX= '',
	SHLIBSUFFIX= sysconfig.get_config_var('EXT_SUFFIX'),
	LIBS = ['chordata', 'cap'], LIBPATH = ['{}/notochord'.format(env['_DIST_DIR'])],
	)

if GetOption('arm64'):
	cython_env['CCFLAGS'] = march_vars['CFLAGS'] + " " + " ".join(env['CCFLAGS']),
	cython_env['LINKFLAGS'] = march_vars['LDFLAGS'] + " -Wl,-rpath='$$ORIGIN'"
	cython_env['SHLIBSUFFIX'] = march_vars['EXT_SUFFIX']

if GetOption('armhf'):
	cython_env['CCFLAGS'] = march_vars['CFLAGS'] + " " + " ".join(env['CCFLAGS']),
	cython_env['LINKFLAGS'] = march_vars['LDFLAGS'] + " -Wl,-rpath='$$ORIGIN'"
	cython_env['SHLIBSUFFIX'] = march_vars['EXT_SUFFIX']
 

if not GetOption('verbose') and not GetOption('pedantic'):
	cython_env['CYCOMSTR'] = pre_COMSTR + 'Generating cpp source with Cython: $TARGET ',
	cython_env['SHLINKCOMSTR'] = pre_COMSTR + "Linking Cython extension: $TARGET",
	cython_env['SHCXXCOMSTR'] = pre_COMSTR + "Compiling Cython-generated source $SOURCE to object: $TARGET",

if GetOption('debug'):
	cython_env['CYFLAGS'] = '--gdb'

cython_exts = SConscript(['pyx/notochord/SConscript'], exports = ['cython_env', 'env'])

Requires(cython_exts, chordlib)
# Requires(chordlib)

# Copy required files to dist

env.Install(env['_DIST_DIR'], "LICENSE")
env.Install(env['_DIST_DIR'], "README.md")
env.Install(env['_DIST_DIR'], "setup.cfg")
env.Install("{}/notochord".format(env['_DIST_DIR']), "Chordata.xml")
env.Install("{}/notochord".format(env['_DIST_DIR']), "Chordata.xsd")

# Create compile DB and includes file to use in IDEs and text editor for code completion

env.CompileDb()
