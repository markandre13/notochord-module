# -- START OF COPYRIGHT & LICENSE NOTICES -- 
#
# Chordata Motion's Notochord (python module)
# -- Core implementation of Chordata Motion's Open Source motion capture framework
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2021 Bruno Laurencich
# Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
#
# This file is part of Chordata Motion's Notochord (python module).
#
# Chordata Motion's Notochord (python module) is free software: you can 
# redistribute it and/or modify it under the terms of the 
# GNU General Public License as published by the Free Software Foundation, 
# either version 3 of the License, or (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata Motion's Notochord (python module).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

from libcpp.memory cimport shared_ptr, static_pointer_cast
from .node cimport (Link, link_ptr, Node,
					_Hierarchy, Hierarchy,
					_Mux_Channel,
					_Node, _Node_ptr,					
					EEPROM_info,
					uint64_t, 
					peek_destruction)
from .node import Mux_Channel
from .mock_node cimport (_Mock_Mux, Mock_Mux, mock_mux_ptr,
                         Mock_Branch, _Mock_Branch, mock_branch_ptr,
					     Mock_KCeptor, _Mock_KCeptor, mock_kceptor_ptr,)

from .io cimport I2C
from .fusion._kalman cimport Kalman, kalman_ptr
from .math._types cimport _Matrix3, Matrix3, matrix3_ptr, create_Matrix3_ptr, vector3_ptr
from libcpp cimport bool, nullptr
from libcpp.string cimport string

from . import io
from . import error
from .utils import get_version_tuple_from_int, get_int_from_version_tuple
from enum import IntEnum
from collections.abc import Iterable
from datetime import datetime
from time import time

# ----------------- Branch ----------------- 
cdef class Mock_Branch(Node):
	def __cinit__(self, Mock_Mux mux):
		self._parent = None
		self._i2c = None
		if mux:
			self._parent = mux
			self._i2c = self._parent._i2c

	@staticmethod
	cdef Mock_Branch create_branch(Mock_Mux mux, mock_branch_ptr b_ptr):
		cdef Mock_Branch new_branch = Mock_Branch.__new__(Mock_Branch, mux)
		new_branch.c_ptr = static_pointer_cast[_Node, _Mock_Branch](b_ptr)
		return new_branch
	
	def bang(self):
		self.c_ptr.get().bang()

# ----------------- Mux ----------------- 

cdef class Mock_Mux(Node):
	def __cinit__(self, Node parent, uint8_t address, str label, I2C i2c = None):
		cdef link_ptr _parent = self.set_hierarchy(parent)
		cdef link_ptr mux_shared 
		self._i2c = Node.set_i2c(i2c)
		mux_shared = _Mock_Mux.Create(_parent, address, label.encode('utf-8'), self._i2c.c_ptr)
		self.c_ptr = static_pointer_cast[_Node, Link](mux_shared)
		self.branches = {}

	def get_branch(self, mch):
		if not isinstance(mch, Mux_Channel):
			raise TypeError("A Mux_Channel type is required to get a Branch")

		cdef link_ptr mux_link = static_pointer_cast[Link, _Node ](self.c_ptr)
		cdef mock_branch_ptr b_ptr
		if mch not in self.branches.keys():
			b_ptr = static_pointer_cast[_Mock_Branch, Link](mux_link.get().get_child(<_Mux_Channel> mch.value -1))
			new_branch = Mock_Branch.create_branch(self, b_ptr)
			self.branches[mch] = new_branch
		
		return self.branches[mch]

	def bang_branch(self, mch):
		if not isinstance(mch, Mux_Channel):
			raise TypeError("A Mux_Channel type is required to bang a Branch")
		static_pointer_cast[_Mock_Mux, _Node](self.c_ptr).get().bang_branch(<_Mux_Channel>mch.value)

	@property
	def state(self):
		return <uint8_t>static_pointer_cast[_Mock_Mux, _Node](self.c_ptr).get().get_state()



# ----------------- KCeptor ----------------- 

cdef class Mock_KCeptor(Node):
	def __cinit__(self, Node parent, uint8_t address, str label, I2C i2c = None):
		cdef link_ptr _parent = self.set_hierarchy(parent)
		cdef link_ptr kc_shared
		self._i2c = Node.set_i2c(i2c)

		kc_shared = _Mock_KCeptor.Create(_parent, address, label.encode('utf-8'), self._i2c.c_ptr)
		self.c_ptr = static_pointer_cast[_Node, Link](kc_shared)

	cdef _Mock_KCeptor* kc_ptr(self):
		return static_pointer_cast[_Mock_KCeptor, _Node](self.c_ptr).get()

	def get_hz(self):
		return self.kc_ptr()._get_hz()

	# --------- Properties -----------

	@property
	def accel(self):
		return self.kc_ptr()._get_accel_scale()
		
	@property
	def gyro(self):
		return self.kc_ptr()._get_gyro_scale()
		
	@property
	def mag(self):
		return self.kc_ptr()._get_mag_scale()

	@property
	def m_offset(self):
		cdef vector3_ptr offset = self.kc_ptr().get_m_offset()
		vec = Vector.create(offset)

		return vec

	@m_offset.setter
	def m_offset(self, other):
		if not isinstance(other, Iterable) or len(other) != 3:
			raise TypeError("An iterable of length 3 should be used to set the value of a KCeptor.offset")

		self.kc_ptr().set_m_offset(other[0], other[1], other[2])
	
	@property
	def m_matrix(self):
		if not self._m_matrix:
			self._m_matrix = Matrix3.create(self.kc_ptr().get_m_matrix())
			
		return self._m_matrix

	@m_matrix.setter
	def m_matrix(self, Matrix3 other):
		self.kc_ptr().set_m_matrix(other.sh_ptr)

	# --------- Methods -----------
	
	def get_debug_buffer(self, clear_buf=False):
		py_buffer = []
		for element in self.kc_ptr().debug_buffer:
			py_buffer.append(element)
		
		if clear_buf:
			self.kc_ptr().debug_buffer.clear()
		return py_buffer
	
	def apply_mag_calib(self, int x, int y, int z):
		cdef int16_t values[3]
		self.kc_ptr().apply_mag_calib(values, x,y,z)
		return (values[0], values[1], values[2])

	def set_EEPROM_attributes(self, int version, int timestamp):
		self.kc_ptr().set_EEPROM_attributes(version, timestamp)
	
	def setup(self):
		return self.kc_ptr().setup()

