# -- START OF COPYRIGHT & LICENSE NOTICES --
#
# Chordata Motion's Notochord (python module)
# -- Core implementation of Chordata Motion's Open Source motion capture framework
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2021 Bruno Laurencich
# Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
#
# This file is part of Chordata Motion's Notochord (python module).
#
# Chordata Motion's Notochord (python module) is free software: you can
# redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata Motion's Notochord (python module).
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES --

from . import defaults

HELP = "Display this help menu";

VERSION = "Display program's version and exit";

WAIT = "Disable confirmation to start the program. By default Notochord will wait for a confirmation from the user.";

RAW = "Transmit the raw lectures fron the K-Ceptors";

SCAN = "Scan the i2c port to create the Armature, instead of reading the configuration file";

CALIB = "Sensor calibration procedure. Notochord will perform a --scan, and calibrate the found K-Ceptor. It is adviced to plug just one KC to the Hub with this flag!!";

KC_VER = "The revision of the K-Ceptor hardware used.";

XML_G = "\nCONFIGURATION FILES:";

XML_FILENAME = "The path to the configuration file. Default [{}]".format(defaults.CHORDATA_CONF);

XML_SCHEMA = "The path to the xsd schema for the configuration file. Default [{}]".format(defaults.CHORDATA_CONF_SCHEMA);

IP = "The client's ip.\n";

PORT = "The client's port.\n";

COMM_G = "\nOUTPUT OPTIONS:";

LOG = "Where to redirect the information messages";

ERROR = "Where to redirect the errors";

TRANSMIT = "Where to redirect the actual payload (mostly Quaternion data)";

RUNNER = "Used when the notochord is initialized by a daemon who expects communications by OSC in localhost:50419";

I2C = "The user space i2c adapter, default: {}".format(defaults.CHORDATA_I2C_DEFAULT_ADAPTER_)

FILENAME = "The file where to put the log.";

VERBOSE = "Verbose output";

SEND_RATE = "Maximum send rate for transmision";

OSC_G = "\nOSC ADDRESSES";

OSC_BASE = "Base OSC address, default:\n {}".format(defaults.CHORDATA_DEF_OSC_ADDR);

OSC_ERR = "Errors OSC subaddress, default:\n {}".format(defaults.CHORDATA_DEF_ERROR_SADDR);

OSC_COMM = "Info OSC subaddress, default:\n {}".format(defaults.CHORDATA_DEF_MSG_SADDR);

NOTE = "OUTPUT OPTIONS can be repetead assigning more than one output. Valid options are `stdout`, `stderr`, `file`, `osc`, `python`";

DESCRIPTION = "Notochord Configuration Options"

CREDITS = "Information about people contributing to this program, libraries, etc"

MADGWICK = "Use Madgwick's sensor fusion algorithm (legacy)"

LIVE_MAG_CORRECTION = "Use Madgwick's sensor fusion algorithm (legacy)"

ODR = "Read sensors at this frequency (default=50Hz, max=100Hz)"

NO_BUNDLES = "Don't use COPP bundles (send plain OSC messages)"

WEBSOCKET_PROTOCOL = "The protocol name to use in the websocket server."

WEBSOCKET_PORT = "The port to use for the webscoket server."