# -- START OF COPYRIGHT & LICENSE NOTICES --
#
# Chordata Motion's Notochord (python module)
# -- Core implementation of Chordata Motion's Open Source motion capture framework
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2021 Bruno Laurencich
# Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
#
# This file is part of Chordata Motion's Notochord (python module).
#
# Chordata Motion's Notochord (python module) is free software: you can
# redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata Motion's Notochord (python module).
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES --

from libc.stdint cimport (uint32_t)
from libcpp.string cimport string
from .math._types cimport _Quaternion
from libc.stdint cimport (uint64_t)

cdef extern from "types.h" namespace "Chordata":
	cdef enum Output_Redirect:
		NONE   = 0
		STDOUT = 1
		STDERR = 1<<1
		FILE   = 1<<2
		OSC    = 1<<3
		PYTHON = 1<<4
		WEBSOCKET = 1<<5

	cdef enum _Mux_Channel:
		OFF  = 0 #< 0
		CH_1 = 1 #< 0x1
		CH_2 = 2 #< 0x2
		CH_3 = 3 #< 0x4
		CH_4 = 4 #< 0x8
		CH_5 = 5 #< 0x10
		CH_6 = 6 #< 0x20

	cdef cppclass EEPROM_info:
		uint32_t validation
		uint32_t version
		uint32_t timestamp

	cdef enum _Gyro_Scale_Options:
		gyro_245,
		gyro_500,
		gyro_2000

	cdef enum _Accel_Scale_Options:
		accel_2,
		accel_16,
		accel_4,
		accel_8

	cdef enum _Mag_Scale_Options:
		mag_4,
		mag_8,
		mag_12,
		mag_16

	cdef enum _Output_Data_Rate:
		OUTPUT_RATE_50,
		OUTPUT_RATE_119,
		OUTPUT_RATE_238

	cdef enum _Kc_Values:
		PlusPlus
		Two