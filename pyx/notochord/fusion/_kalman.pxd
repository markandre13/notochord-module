# -- START OF COPYRIGHT & LICENSE NOTICES -- 
#
# Chordata Motion's Notochord (python module)
# -- Core implementation of Chordata Motion's Open Source motion capture framework
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2021 Bruno Laurencich
# Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
#
# This file is part of Chordata Motion's Notochord (python module).
#
# Chordata Motion's Notochord (python module) is free software: you can 
# redistribute it and/or modify it under the terms of the 
# GNU General Public License as published by the Free Software Foundation, 
# either version 3 of the License, or (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata Motion's Notochord (python module).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

from libcpp cimport bool
from libcpp.memory cimport shared_ptr, static_pointer_cast
from libc.stdint cimport (uint8_t, uint16_t, uint32_t, uint64_t,
                          int8_t, int16_t, int32_t, int64_t)

from ..error cimport raise_py_error

from ..math._types cimport _Vector3, vector3_ptr

from ._types cimport (AccelSensor, AccelSensorView, GyroSensor, GyroSensorView, 
					  MagSensor, MagSensorView, MagCalibration, MagCalibrationView,
					  SV_9DOF_GBY_KALMAN, StateVectorView,
					  MagneticBuffer, MagneticBufferView, fquaternion)

cdef extern from "kalman.h" namespace 'Chordata':

	cdef cppclass Fusion_Kalman_Base:
		ctypedef int16_t xyz_array[3]
		
		Fusion_Kalman_Base(uint16_t sensorFs, uint8_t oversampleRatio, bool enable_mag_correct) except +raise_py_error

		MagneticBuffer thisMagBuffer
		uint32_t get_loopcounter()
		uint8_t get_icounter()

		void set_sensors(int16_t, int16_t, int16_t,
						int16_t, int16_t, int16_t,
						int16_t, int16_t, int16_t)

		void set_gyro(int16_t, int16_t, int16_t)
		void set_accel(int16_t, int16_t, int16_t)
		void set_mag(int16_t, int16_t, int16_t)

		const int16_t *get_avg_calib_measurement() const 
		
		float get_gyro_res()
		float get_accel_res()
		float get_mag_res()
		void set_sensor_res(float, float, float)

		bool process_and_run()

		bool set_mag_run_calib(int16_t, int16_t, int16_t)
		void get_mag_offset_for_EEPROM(vector3_ptr magOffset) const
		void get_mag_matrix_for_EEPROM(float (&magMatrix)[3][3]) const
		void set_mag_offset_from_EEPROM(const int16_t *magOffset)
		void set_mag_matrix_from_EEPROM(const float (&magMatrix)[3][3])



		# void get_mag_offset_for_EEPROM(FSsf::int16 *magOffset) const override;
		# # void get_mag_matrix_for_EEPROM(float (&magMatrix)[3][3]) const override;

		# void set_mag_offset_from_EEPROM(const FSsf::int16 *magOffset) override;
		# void set_mag_matrix_from_EEPROM(const float (&magMatrix)[3][3]) override;

		AccelSensor *get_accel_sensor()
		GyroSensor *get_gyro_sensor()
		MagSensor *get_mag_sensor()
		MagCalibration *get_mag_calib()
		SV_9DOF_GBY_KALMAN *get_sv()
	
		const fquaternion *get_quat()
		const MagneticBuffer *get_mag_calib_buf()
		const xyz_array *get_gyro_buf()
		const xyz_array *get_accel_buf()
		const xyz_array *get_mag_buf()
		float get_m_covariance()


	cdef cppclass Fusion_Kalman_Raw(Fusion_Kalman_Base):
		Fusion_Kalman_Raw(uint16_t sensorFs, uint8_t oversampleRatio, bool enable_mag_correct) except +raise_py_error

		void set_gyro(int16_t, int16_t, int16_t)
		void set_accel(int16_t, int16_t, int16_t)
		void set_mag(int16_t, int16_t, int16_t)

		void get_mag_offset_for_EEPROM(vector3_ptr magOffset) const
		void get_mag_matrix_for_EEPROM(float (&magMatrix)[3][3]) const
		void set_mag_offset_from_EEPROM(const int16_t *magOffset)
		void set_mag_matrix_from_EEPROM(const float (&magMatrix)[3][3])


	cdef cppclass Fusion_Kalman_LSM9DS1(Fusion_Kalman_Base):
		Fusion_Kalman_LSM9DS1(uint16_t sensorFs, uint8_t oversampleRatio, bool enable_mag_correct) except +raise_py_error

		void set_gyro(int16_t, int16_t, int16_t)
		void set_accel(int16_t, int16_t, int16_t)
		void set_mag(int16_t, int16_t, int16_t)

		void get_mag_offset_for_EEPROM(vector3_ptr magOffset) const
		void get_mag_matrix_for_EEPROM(float (&magMatrix)[3][3]) const
		void set_mag_offset_from_EEPROM(const int16_t *magOffset)
		void set_mag_matrix_from_EEPROM(const float (&magMatrix)[3][3])

ctypedef shared_ptr[Fusion_Kalman_Base] kalman_ptr

cdef class Kalman:
	cdef kalman_ptr c_ptr
	cdef uint8_t mag_buf_size_x
	cdef uint8_t mag_buf_size_y

	cdef AccelSensorView accel_sensor
	cdef GyroSensorView gyro_sensor
	cdef MagSensorView mag_sensor
	cdef MagCalibrationView mag_cal
	cdef MagneticBufferView mag_buf
	cdef StateVectorView sv

	@staticmethod
	cdef Kalman create(kalman_ptr c_ptr)
