from libc.stdint cimport (uint8_t, uint16_t, uint32_t, uint64_t,
						  int8_t, int16_t, int32_t, int64_t)

# Warning! these DEFs have to be known at compile time so cannot be imported by cython
DEF MAX_OVERSAMPLE_RATIO = 8
DEF FSsf_MAGBUFFSIZEX = 14					
DEF FSsf_MAGBUFFSIZEY = (2 * FSsf_MAGBUFFSIZEX)

cdef class AccelSensorView:
	@staticmethod
	cdef AccelSensorView create(AccelSensor *c_ptr):
		'''Create a AccelSensorView from a C pointer'''
		cdef AccelSensorView new_accel = AccelSensorView.__new__(AccelSensorView)
		new_accel.c_ptr = c_ptr
		return new_accel

	def __cinit__(self):
		pass
	
	property fGsAvg:
		def __get__(self):
			return tuple(self.c_ptr.fGsAvg[i] for i in range(3))

	property fgPerCount:
		def __get__(self):
			return self.c_ptr.fgPerCount

	property iGsBuffer:
		def __get__(self):
			for i in range(MAX_OVERSAMPLE_RATIO):
				yield (self.c_ptr.iGsBuffer[i][0], self.c_ptr.iGsBuffer[i][1], self.c_ptr.iGsBuffer[i][2])

	property iGs:
		def __get__(self):
			return tuple(self.c_ptr.iGs[i] for i in range(3))

	property iGsAvg:
		def __get__(self):
			return tuple(self.c_ptr.iGsAvg[i] for i in range(3))

	property iCountsPerg:
		def __get__(self):
			return self.c_ptr.iCountsPerg

	property iWhoAmI:
		def __get__(self):
			return self.c_ptr.iWhoAmI

	property fQvGQa:
		def __get__(self):
			return self.c_ptr.fQvGQa

cdef class GyroSensorView:
	@staticmethod
	cdef GyroSensorView create(GyroSensor *c_ptr):
		'''Create a GyroSensorView from a C pointer'''
		cdef GyroSensorView new_gyro = GyroSensorView.__new__(GyroSensorView)
		new_gyro.c_ptr = c_ptr
		return new_gyro

	def __cinit__(self):
		pass

	property fDegPerSecPerCount:
		def __get__(self):
			return self.c_ptr.fDegPerSecPerCount

	property iCountsPerDegPerSec:
		def __get__(self):
			return self.c_ptr.iCountsPerDegPerSec

	property iYs:
		def __get__(self):
			return tuple(self.c_ptr.iYs[i] for i in range(3))

	property iWhoAmI:
		def __get__(self):
			return self.c_ptr.iWhoAmI

	property iYsBuffer:
		def __get__(self):
			for i in range(MAX_OVERSAMPLE_RATIO):
				yield (self.c_ptr.iYsBuffer[i][0], self.c_ptr.iYsBuffer[i][1], self.c_ptr.iYsBuffer[i][2])

cdef class MagSensorView:
	@staticmethod
	cdef MagSensorView create(MagSensor *c_ptr):
		'''Create a MagSensorView from a C pointer'''
		cdef MagSensorView new_mag = MagSensorView.__new__(MagSensorView)
		new_mag.c_ptr = c_ptr
		return new_mag

	def __cinit__(self):
		pass

	property fBsAvg:
		def __get__(self):
			return tuple(self.c_ptr.fBsAvg[i] for i in range(3))

	property fBcAvg:
		def __get__(self):
			return tuple(self.c_ptr.fBcAvg[i] for i in range(3))

	property fuTPerCount:
		def __get__(self):
			return self.c_ptr.fuTPerCount

	property fCountsPeruT:
		def __get__(self):
			return self.c_ptr.fCountsPeruT

	property iBsBuffer:
		def __get__(self):
			for i in range(MAX_OVERSAMPLE_RATIO):
				yield (self.c_ptr.iBsBuffer[i][0], self.c_ptr.iBsBuffer[i][1], self.c_ptr.iBsBuffer[i][2])

	property iBs:
		def __get__(self):
			return tuple(self.c_ptr.iBs[i] for i in range(3))

	property iBsAvg:
		def __get__(self):
			return tuple(self.c_ptr.iBsAvg[i] for i in range(3))

	property iBcAvg:
		def __get__(self):
			return tuple(self.c_ptr.iBcAvg[i] for i in range(3))

	property iCountsPeruT:
		def __get__(self):
			return self.c_ptr.iCountsPeruT

	property iWhoAmI:
		def __get__(self):
			return self.c_ptr.iWhoAmI

	property fQvBQd:
		def __get__(self):
			return self.c_ptr.fQvBQd

cdef class MagCalibrationView:
	@staticmethod
	cdef MagCalibrationView create(MagCalibration *c_ptr):
		'''Create a MagCalibrationView from a C pointer'''
		cdef MagCalibrationView new_mag_cal = MagCalibrationView.__new__(MagCalibrationView)
		new_mag_cal.c_ptr = c_ptr
		return new_mag_cal

	def __cinit__(self):
		pass

	property fV:
		def __get__(self):
			return tuple(self.c_ptr.fV[i] for i in range(3))

	property finvW:
		def __get__(self):
			result = []
			for i in range(3):
				result.append(tuple(self.c_ptr.finvW[i][j] for j in range(3)))
			return tuple(result)

	property fB:
		def __get__(self):
			return self.c_ptr.fB

	property fFitErrorpc:
		def __get__(self):
			return self.c_ptr.fFitErrorpc

	property ftrV:
		def __get__(self):
			return tuple(self.c_ptr.ftrV[i] for i in range(3))

	property ftrinvW:
		def __get__(self):
			result = []
			for i in range(3):
				result.append(tuple(self.c_ptr.ftrinvW[i][j] for j in range(3)))
			return tuple(result)

	property ftrB:
		def __get__(self):
			return self.c_ptr.ftrB

	property ftrFitErrorpc:
		def __get__(self):
			return self.c_ptr.ftrFitErrorpc

	property fA:
		def __get__(self):
			result = []
			for i in range(3):
				result.append(tuple(self.c_ptr.fA[i][j] for j in range(3)))
			return tuple(result)

	property finvA:
		def __get__(self):
			result = []
			for i in range(3):
				result.append(tuple(self.c_ptr.finvA[i][j] for j in range(3)))
			return tuple(result)

	property fmatA:
		def __get__(self):
			result = []
			for i in range(10):
				result.append(tuple(self.c_ptr.fmatA[i][j] for j in range(10)))
			return tuple(result)

	property fmatB:
		def __get__(self):
			result = []
			for i in range(10):
				result.append(tuple(self.c_ptr.fmatB[i][j] for j in range(10)))
			return tuple(result)

	property fvecA:
		def __get__(self):
			return tuple(self.c_ptr.fvecA[i] for i in range(10))

	property fvecB:
		def __get__(self):
			return tuple(self.c_ptr.fvecB[i] for i in range(4))

	property iCalInProgress:
		def __get__(self):
			return self.c_ptr.iCalInProgress

	property iMagCalHasRun:
		def __get__(self):
			return self.c_ptr.iMagCalHasRun

	property iValidMagCal:
		def __get__(self):
			return self.c_ptr.iValidMagCal


cdef class MagneticBufferView:
	@staticmethod
	cdef MagneticBufferView create(MagneticBuffer *c_ptr):
		'''Create a MagneticBufferView from a C pointer'''
		cdef MagneticBufferView new_mag_buf = MagneticBufferView.__new__(MagneticBufferView)
		new_mag_buf.c_ptr = c_ptr
		return new_mag_buf
	
	def __cinit__(self):
		pass
	
	property iBs:
		def __get__(self):
			cdef const int16_t[:, :, ::1] buf = self.c_ptr.iBs

			for j in range(FSsf_MAGBUFFSIZEX):
				for k in range(FSsf_MAGBUFFSIZEY):
					if self.c_ptr.index[j][k] != -1:
						yield (buf[0, j, k], buf[1, j, k], buf[2, j, k])

	property index:
		def __get__(self):
			return tuple(tuple(self.c_ptr.index[i][j] for j in range(FSsf_MAGBUFFSIZEY)) for i in range(FSsf_MAGBUFFSIZEX))

	property tanarray:
		def __get__(self):
			return tuple(self.c_ptr.tanarray[i] for i in range(FSsf_MAGBUFFSIZEX - 1))
	
	@property
	def iMagBufferCount(self):
		return self.c_ptr.iMagBufferCount


cdef class StateVectorView:
	@staticmethod
	cdef StateVectorView create(SV_9DOF_GBY_KALMAN *c_ptr):
		cdef StateVectorView new_sv = StateVectorView.__new__(StateVectorView)
		new_sv.c_ptr = c_ptr
		return new_sv

	def __cinit__(self):
		pass

	# start: elements common to all motion state vectors
	property fPhiPl:
		def __get__(self):
			return self.c_ptr.fPhiPl

	property fThePl:
		def __get__(self):
			return self.c_ptr.fThePl

	property fPsiPl:
		def __get__(self):
			return self.c_ptr.fPsiPl

	property fRhoPl:
		def __get__(self):
			return self.c_ptr.fRhoPl

	property fChiPl:
		def __get__(self):
			return self.c_ptr.fChiPl

	property fRPl:
		def __get__(self):
			return tuple(tuple(self.c_ptr.fRPl[i][j] for j in range(3)) for i in range(3))

	@property
	def quaternion(self):
		return (self.c_ptr.fqPl.q0, self.c_ptr.fqPl.q1, self.c_ptr.fqPl.q2, self.c_ptr.fqPl.q3)

	property fRVecPl:
		def __get__(self):
			return (self.c_ptr.fRVecPl[0], self.c_ptr.fRVecPl[1], self.c_ptr.fRVecPl[2])

	property fOmega:
		def __get__(self):
			return (self.c_ptr.fOmega[0], self.c_ptr.fOmega[1], self.c_ptr.fOmega[2])

	property systick:
		def __get__(self):
			return self.c_ptr.systick

	# end: elements common to all motion state vectors

	property fQw10x10:
		def __get__(self):
			return tuple(tuple(self.c_ptr.fQw10x10[i][j] for j in range(10)) for i in range(10))
	
	property fK10x7:
		def __get__(self):
			return tuple(tuple(self.c_ptr.fK10x7[i][j] for j in range(7)) for i in range(10))
	
	property fQwCT10x7:
		def __get__(self):
			return tuple(tuple(self.c_ptr.fQwCT10x7[i][j] for j in range(7)) for i in range(10))
	
	property fZErr:
		def __get__(self):
			return tuple(self.c_ptr.fZErr[i] for i in range(7))

	property fQv7x1:
		def __get__(self):
			return tuple(self.c_ptr.fQv7x1[i] for i in range(7))
	
	property fDeltaPl:
		def __get__(self):
			return self.c_ptr.fDeltaPl
	
	property fsinDeltaPl:
		def __get__(self):
			return self.c_ptr.fsinDeltaPl
	
	property fcosDeltaPl:
		def __get__(self):
			return self.c_ptr.fcosDeltaPl
	
	property fqgErrPl:
		def __get__(self):
			return tuple(self.c_ptr.fqgErrPl[i] for i in range(3))
	
	property fqmErrPl:
		def __get__(self):
			return tuple(self.c_ptr.fqmErrPl[i] for i in range(3))

	property fbErrPl:
		def __get__(self):
			return tuple(self.c_ptr.fbErrPl[i] for i in range(3))

	property fDeltaErrPl:
		def __get__(self):
			return self.c_ptr.fDeltaErrPl

	property fAccGl:
		def __get__(self):
			return tuple(self.c_ptr.fAccGl[i] for i in range(3))

	property fVelGl:
		def __get__(self):
			return tuple(self.c_ptr.fVelGl[i] for i in range(3))

	property fDisGl:
		def __get__(self):
			return tuple(self.c_ptr.fDisGl[i] for i in range(3))
	
	property fGyrodeltat:
		def __get__(self):
			return self.c_ptr.fGyrodeltat

	property fKalmandeltat:
		def __get__(self):
			return self.c_ptr.fKalmandeltat

	property fgKalmandeltat:
		def __get__(self):
			return self.c_ptr.fgKalmandeltat

	property fAlphaOver2:
		def __get__(self):
			return self.c_ptr.fAlphaOver2

	property fAlphaOver2Sq:
		def __get__(self):
			return self.c_ptr.fAlphaOver2Sq

	property fAlphaOver2SqQvYQwb:
		def __get__(self):
			return self.c_ptr.fAlphaOver2SqQvYQwb

	property fAlphaOver2Qwb:
		def __get__(self):
			return self.c_ptr.fAlphaOver2Qwb

	property iFirstAccelMagLock:
		def __get__(self):
			return self.c_ptr.iFirstAccelMagLock

	property resetflag:
		def __get__(self):
			return self.c_ptr.resetflag	