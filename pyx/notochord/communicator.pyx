# -- START OF COPYRIGHT & LICENSE NOTICES -- 
#
# Chordata Motion's Notochord (python module)
# -- Core implementation of Chordata Motion's Open Source motion capture framework
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2021 Bruno Laurencich
# Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
#
# This file is part of Chordata Motion's Notochord (python module).
#
# Chordata Motion's Notochord (python module) is free software: you can 
# redistribute it and/or modify it under the terms of the 
# GNU General Public License as published by the Free Software Foundation, 
# either version 3 of the License, or (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata Motion's Notochord (python module).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

# distutils: language = c++

from .core cimport get_runtime
from .math._types cimport Math_type, Quaternion, _Quaternion, Vector, _Vector3, Matrix3
from .node cimport Node, KCeptor
from . cimport communicator as comm
from libcpp.string cimport string
from libcpp.vector cimport vector
from libcpp.memory cimport make_shared

from .types import Output_Sink, Log_Levels

def flush():
	comm.flush_loggers()

def dump_transmit():
	print(comm.get_trasmit_packet_hex().decode('utf-8', errors='replace'))

def info(str fmt, *args):
	"""Log strings using notochord's internal communicator dispatcher"""
	cdef string _to_log = fmt.format(*args).encode('utf-8')
	comm._info(_to_log.c_str())

def transmit(str _subaddr, Math_type payload):
	"""Transmit Math_types using notochord's internal communicator dispatcher"""
	cdef string subaddr = _subaddr.encode('utf-8')
	if isinstance(payload, Quaternion):
		comm._transmit(&subaddr, (<Quaternion>payload).sh_ptr.get())
	elif isinstance(payload, Vector):
		comm._transmit(&subaddr, (<Vector>payload).sh_ptr.get())
	elif isinstance(payload, Matrix3):
		raise NotImplementedError("The notochord communicator dispatcher currently doesn't\
handle Matrix3 transmission. If you have a use case for this please contact the Chordata team\
at https://forum.chordata.cc")

def transmit_node(Node n, Math_type payload):
	"""Transmit Math_types using notochord's internal communicator dispatcher"""
	if isinstance(payload, Quaternion):
		comm._transmit(n.c_ptr.get(), (<Quaternion>payload).sh_ptr.get())
	elif isinstance(payload, Vector):
		comm._transmit(n.c_ptr.get(), (<Vector>payload).sh_ptr.get())
	elif isinstance(payload, Matrix3):
		raise NotImplementedError("The notochord communicator dispatcher currently doesn't\
handle Matrix3 transmission. If you have a use case for this please contact the Chordata team\
at https://forum.chordata.cc")

def transmit_raw(KCeptor kc):
	comm._transmit(kc.kc_ptr())

def error(str fmt, *args):
	"""Log strings using notochord's internal communicator dispatcher"""
	cdef string _to_log = fmt.format(*args).encode('utf-8')
	comm._error(_to_log.c_str())
	
def debug(str fmt, *args):
	"""Log strings using notochord's internal communicator dispatcher"""
	cdef string _to_log = fmt.format(*args).encode('utf-8')
	comm._debug(_to_log.c_str())

def trace(str fmt, *args):
	"""Log strings using notochord's internal communicator dispatcher"""
	cdef string _to_log = fmt.format(*args).encode('utf-8')
	comm._trace(_to_log.c_str())

def warn(str fmt, *args):
	"""Log strings using notochord's internal communicator dispatcher"""
	cdef string _to_log = fmt.format(*args).encode('utf-8')
	comm._warn(_to_log.c_str())

def critical(str fmt, *args):
	"""Log strings using notochord's internal communicator dispatcher"""
	cdef string _to_log = fmt.format(*args).encode('utf-8')
	comm._critical(_to_log.c_str())

def current_log_file():
	s = get_runtime().get_current_log_file()[0]
	return s.decode('utf-8')

def get_logger_messagge():
	cdef CyLogger logger = CyLogger()
	logger.c_ptr = comm.get_logger_msg()
	return logger

def get_logger_error():
	cdef CyLogger logger = CyLogger()
	logger.c_ptr = comm.get_logger_err()

	return logger

def get_logger_transmit():
	cdef CyLogger logger = CyLogger()
	logger.c_ptr = comm.get_logger_tra()

	return logger

def get_registries(clear = False):
	return {
		"trace": get_registry(clear, Log_Levels.Trace),
		"debug": get_registry(clear, Log_Levels.Debug),
		"info": get_registry(clear, Log_Levels.Info),
		"warn": get_registry(clear, Log_Levels.Warn),
		"error": get_registry(clear, Log_Levels.Error),
		"critical": get_registry(clear, Log_Levels.Critical),
	}

def get_registry(clear = False, which = Log_Levels.Info):
	cdef vector[string] reg = comm._get_registry(which)
	py_reg = []
	for element in reg:
		py_reg.append(element.decode('utf-8'))
	if(clear):
		comm._clear_registry(which)
	return py_reg

cdef class CyLogger:
	@property
	def sinks(self):
		return Output_Sink(self.c_ptr.get().get_sinks())	

cdef class CyBuffer:
	@staticmethod
	cdef CyBuffer create(Cython_Buffer* c_ptr):
		cdef CyBuffer new_b = CyBuffer.__new__(CyBuffer)
		new_b.c_ptr = c_ptr
		new_b._mutable = True
		return new_b

	def __cinit__(self):
		pass
	
