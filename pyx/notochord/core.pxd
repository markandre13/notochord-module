# -- START OF COPYRIGHT & LICENSE NOTICES -- 
#
# Chordata Motion's Notochord (python module)
# -- Core implementation of Chordata Motion's Open Source motion capture framework
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2021 Bruno Laurencich
# Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
#
# This file is part of Chordata Motion's Notochord (python module).
#
# Chordata Motion's Notochord (python module) is free software: you can 
# redistribute it and/or modify it under the terms of the 
# GNU General Public License as published by the Free Software Foundation, 
# either version 3 of the License, or (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata Motion's Notochord (python module).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

# distutils: language = c++

from libcpp.string cimport string
from libcpp cimport bool
from .config cimport Configuration_Data, Notochord_Config
from .error cimport raise_py_error
from .io cimport io_ptr
from .timer cimport timek_ptr, nodesch_ptr
from .math._types cimport _Quaternion, _Vector3, _Matrix3
from .armature cimport armature_ptr
from .calibration cimport Calibration_Manager

cdef extern from "core.h" namespace "Chordata":
	cdef cppclass Notochord_Runtime:
		void setup(const Configuration_Data*, bool) except +raise_py_error
		string *get_current_log_file()
		io_ptr get_i2c() except +raise_py_error
		timek_ptr get_timekeeper() except +raise_py_error
		timek_ptr get_timekeeper(const string) except +raise_py_error
		void add_index_timekeeper(timek_ptr) except +raise_py_error
		bool start(nodesch_ptr) except +raise_py_error
		bool end() except +raise_py_error
		void set_armature(armature_ptr) except +raise_py_error
		armature_ptr get_armature() except +raise_py_error
		Calibration_Manager& get_calibration_manager() except +raise_py_error
		const Configuration_Data *get_config() except +raise_py_error

	Notochord_Runtime *get_runtime()
	Notochord_Runtime *terminate_runtime(bool terminate_communicator)
	int get_status()
	void _print_int_info()

