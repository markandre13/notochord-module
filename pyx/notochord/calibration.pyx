from .core cimport get_runtime
from .armature cimport _Bone, bone_ptr, _Armature, armature_ptr
from .math._types cimport _Quaternion
from libcpp.vector cimport vector
from .calibration cimport _Sensor_Payload, _Calibration_Status
from libc.stdint cimport (uint64_t)
from pose_calib.core import Calibrator
from pose_calib.defaults import g_s, a_s, m_s
import pandas as pd
from cython.operator cimport dereference as deref
from os import path, makedirs
from datetime import datetime

from enum import IntEnum

class Calibration_Status(IntEnum):
	CALIB_IDLE  = 0,
	CALIBRATING = 1,
	STATIC      = 2,
	ARMS        = 3,
	TRUNK       = 4,
	LEFT_LEG    = 5,
	RIGHT_LEG   = 6

def change_status(status):
	if not isinstance(status, Calibration_Status):
		raise TypeError("Only `Calibration_Status Enum` is supported, received {}".format(type(status)))
	
	get_runtime().get_calibration_manager().set_status(status.value)

def get_data():
	cdef vector[_Sensor_Payload] buffer = get_runtime().get_calibration_manager().get_buffer()

	Time = []
	Node_label = []
	Q_w = []
	Q_x = []
	Q_y = []
	Q_z = []
	G_x = []
	G_y = []
	G_z = []
	A_x = []
	A_y = []
	A_z = []
	M_x = []
	M_y = []
	M_z = []
	
	for element in buffer:
		Time.append(element.timestamp)
		Node_label.append(element.label.decode('utf-8'))
		Q_w.append(element.q.w())
		Q_x.append(element.q.x())
		Q_y.append(element.q.y())
		Q_z.append(element.q.z())
		G_x.append(element.gyro.x())
		G_y.append(element.gyro.y())
		G_z.append(element.gyro.z())
		A_x.append(element.accel.x())
		A_y.append(element.accel.y())
		A_z.append(element.accel.z())
		M_x.append(element.mag.x())
		M_y.append(element.mag.y())
		M_z.append(element.mag.z())
	
	data = {
		"time(msec)": Time,
		"node_label": Node_label,
		"q_w": Q_w,
		"q_x": Q_x,
		"q_y": Q_y,
		"q_z": Q_z,
		"g_x": G_x,
		"g_y": G_y,
		"g_z": G_z,
		"a_x": A_x,
		"a_y": A_y,
		"a_z": A_z,
		"m_x": M_x,
		"m_y": M_y,
		"m_z": M_z,
	}

	df = pd.DataFrame(data)
	
	return df

def get_indexes():
	cdef unordered_map[string, uint64_t] indexes = get_runtime().get_calibration_manager().get_indexes()

	py_indexes = {}

	for it in indexes:
		py_indexes[it.first.decode('utf-8')] = it.second
	
	return py_indexes

def run_calibration(debug = False):
	df = get_data()
	indexes = get_indexes()

	# Check that the data is not empty
	if df.empty:
		raise ValueError("No data to calibrate")

	# # Check that the indexes are present in the "node_label" column
	# if not set(indexes.keys()).issubset(set(df['node_label'])):
	# 	raise ValueError("One or more index labels are not present in the data")

	# Check that the initial state indexes are unique
	initial_indexes = [v for k, v in indexes.items() if k.endswith('_i')]
	if len(set(initial_indexes)) != len(initial_indexes):
		raise ValueError("Duplicate initial state indexes found")

	# Check that the final state indexes are unique and not zero
	final_indexes = [v for k, v in indexes.items() if k.endswith('_s')]
	if len(set(final_indexes)) != len(final_indexes):
		raise ValueError("Duplicate final state indexes found")
	if any(i == 0 for i in final_indexes):
		raise ValueError("One or more final state indexes are zero")
	
	size = df.shape[0]
	cycles = get_runtime().get_calibration_manager().get_cycle()
	n_nodes = df['node_label'].nunique() # TODO: Ideally we would get the value from ODRmodifiers
	if size != cycles * n_nodes:
		raise ValueError("The number of data points {} is not equal to the number of cycles {} * number of nodes {} = {}".format(size, cycles, n_nodes, cycles * n_nodes))

	now_str = datetime.now().strftime("%Y-%m-%dT%H-%M-%S")

	log_path = path.dirname(get_runtime().get_config().comm.filepath.decode('utf-8'))
	# Add a subfolder named "calibration_log", if it doesn't exist create it
	if not path.exists(log_path + "/calibration_log"):
		makedirs(log_path + "/calibration_log")
	log_path = log_path + "/calibration_log" + "/calib_{}.log".format(now_str) 

	# Run the calibration
	calib_1 = Calibrator(
		df,
		g_s, a_s, m_s,
		indexes,
		log=log_path
	)

	calib_1.run()

	try:
		apply_calibration(calib_1.results['heading_rot'], calib_1.results['post_rot'])
	except Exception as e:
		print("Error applying calibration", str(e))

	if debug:
		calib_1.save_calib_quats('Chordata_calib', location="out")

		calib_1.apply('Chordata_calib', location="out")

		to_plot_nodes = ['r-lowerarm','neck', 'r-upperleg', 'l-upperleg']
		calib_1.plot('world', to_plot_nodes)

		calib_1.show_plots()

	return calib_1.results

def apply_calibration(pre_quat_dict, post_quat_dict):
	cdef unordered_map[string, bone_ptr] bones = get_runtime().get_armature().get().get_bones()
	cdef _Quaternion c_q

	for bone in bones:
		if bone.first.decode('utf-8') in pre_quat_dict:
			q = pre_quat_dict[bone.first.decode('utf-8')]
			c_q = _Quaternion(q.w, q.x, q.y, q.z)
			bone.second.get().set_pre_quaternion(c_q)
		if bone.first.decode('utf-8') in post_quat_dict:
			q = post_quat_dict[bone.first.decode('utf-8')]
			c_q = _Quaternion(q.w, q.x, q.y, q.z)
			bone.second.get().set_post_quaternion(c_q)










	
	
