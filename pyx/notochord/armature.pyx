# -- START OF COPYRIGHT & LICENSE NOTICES -- 
#
# Chordata Motion's Notochord (python module)
# -- Core implementation of Chordata Motion's Open Source motion capture framework
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2021 Bruno Laurencich
# Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
#
# This file is part of Chordata Motion's Notochord (python module).
#
# Chordata Motion's Notochord (python module) is free software: you can 
# redistribute it and/or modify it under the terms of the 
# GNU General Public License as published by the Free Software Foundation, 
# either version 3 of the License, or (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata Motion's Notochord (python module).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

from libcpp.string cimport string
from .armature cimport _Bone, bone_ptr, _Armature, armature_ptr, Vector
from libcpp cimport nullptr
from libcpp cimport bool
from cython.operator cimport dereference as deref
from pygltflib import GLTF2
from pygltflib.validator import validate, summary
from os.path import join, realpath, dirname
import warnings

cdef class Bone():
    def __cinit__(self, Bone _parent, str label, Matrix4 _transform = None, uint8_t max_childs = 5):
        if _transform is None:
            self.local_transform = Matrix4()
        else:
            self.local_transform = _transform 
        
        self.global_transform = Matrix4()
            
        cdef _Bone* bone_pt
        cdef bone_ptr _null_parent = <bone_ptr>nullptr
        if _parent is None:
            bone_pt = new _Bone(_null_parent, label.encode('utf-8'), self.local_transform.sh_ptr, self.global_transform.sh_ptr, max_childs)
        else:
            bone_pt = new _Bone(_parent.c_ptr, label.encode('utf-8'), self.local_transform.sh_ptr, self.global_transform.sh_ptr, max_childs)
            
            
        cdef bone_ptr bone_shared = shared_ptr[_Bone](bone_pt)
        self.c_ptr = bone_shared
        self.parent = _parent
        if _parent is not None:
            self.parent.add_child(self)
    
    @property
    def label(self):
        return self.c_ptr.get().get_label().decode('utf-8')

    @property
    def local_transform(self):
        return self.local_transform
    
    @property
    def global_transform(self):
        return self.global_transform
    
    @property
    def local_translation(self):
        return Vector(self.local_transform[0, 3], self.local_transform[1, 3], self.local_transform[2, 3])
    
    @property
    def global_translation(self):
        return Vector(self.global_transform[0, 3], self.global_transform[1, 3], self.global_transform[2, 3])
    
    @property
    def local_rotation(self):      
        cdef _Quaternion q = self.c_ptr.get().get_local_rotation() 
        return Quaternion(q.w(), q.x(), q.y(), q.z())

    @property
    def global_rotation(self):
        cdef _Quaternion q = self.c_ptr.get().get_global_rotation() 
        return Quaternion(q.w(), q.x(), q.y(), q.z())

    @property
    def parent(self):
        return self.parent

    def add_child(self, Bone _child):
        self.c_ptr.get().add_child(_child.c_ptr) 

    def set_local_transform(self, Matrix4 _transform):
        self.c_ptr.get().set_local_transform(deref(_transform.sh_ptr.get()))
    
    def set_local_translation(self, Vector _translation):
        self.c_ptr.get().set_local_translation(deref(_translation.sh_ptr.get()))
    
    def translate_local(self, Vector _translation):
        self.c_ptr.get().translate_local(deref(_translation.sh_ptr.get()))

    def set_local_rotation(self, Quaternion _rotation):
        self.c_ptr.get().set_local_rotation(deref(_rotation.sh_ptr.get()))
    
    def rotate_local(self, Quaternion _rotation):
        self.c_ptr.get().rotate_local(deref(_rotation.sh_ptr.get()))    

    def set_global_transform(self, Matrix4 _transform):
        self.c_ptr.get().set_global_transform(deref(_transform.sh_ptr.get()))
    
    def set_global_translation(self, Vector _translation):
        self.c_ptr.get().set_global_translation(deref(_translation.sh_ptr.get()))
    
    def translate_global(self, Vector _translation):
        self.c_ptr.get().translate_global(deref(_translation.sh_ptr.get()))
    
    def set_global_rotation(self, Quaternion _rotation):
        self.c_ptr.get().set_global_rotation(deref(_rotation.sh_ptr.get()))
    
    def rotate_global(self, Quaternion _rotation):
        self.c_ptr.get().rotate_global(deref(_rotation.sh_ptr.get()))

    def set_pre(self, Quaternion _pre):
        self.c_ptr.get().set_pre_quaternion(deref(_pre.sh_ptr.get()))
    
    def set_post(self, Quaternion _post):
        self.c_ptr.get().set_post_quaternion(deref(_post.sh_ptr.get()))

    def update(self):
        self.c_ptr.get().update()
    
cdef class Armature():
    def __cinit__(self, str label, str gltf_path):
        self.bones = {}
        self.parse_gltf(gltf_path)

        cdef _Armature* armature_pt = new _Armature(label.encode('utf-8'), self.root.c_ptr)   
        cdef armature_ptr armature_shared = shared_ptr[_Armature](armature_pt)
        self.c_ptr = armature_shared
    
    @property
    def label(self):
        return self.c_ptr.get().get_label().decode('utf-8')

    @property
    def root(self):
        return self.root
    
    @property
    def bones(self):
        return self.bones

    def update(self):
        self.c_ptr.get().update()
    
    def parse_gltf(self, gltf_path):
        path = join(dirname(realpath(__file__)), gltf_path)
        self.gltf = GLTF2.load(path)

        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            validate(self.gltf)        
        summary(self.gltf)

        root_node = self.gltf.nodes[-1]

        self.root = Bone(None, root_node.name)

        if root_node.rotation is not None:
            rotation = Quaternion(root_node.rotation[3], root_node.rotation[0], root_node.rotation[1], root_node.rotation[2])
            self.root.set_local_rotation(rotation)
        if root_node.translation is not None:
            translation = Vector(root_node.translation[0], root_node.translation[1], root_node.translation[2])
            self.root.set_local_translation(translation)
            
        self.bones[root_node.name] = self.root

        self.build_hierarchy(self.root, root_node.children)

        self.root.update()

    
    def build_hierarchy(self, parent, children):
        for i in children:
            node = self.gltf.nodes[i]
            bone = Bone(parent, node.name)
            if node.rotation is not None:
                rotation = Quaternion(node.rotation[3], node.rotation[0], node.rotation[1], node.rotation[2])
                bone.set_local_rotation(rotation)
            if node.translation is not None:
                translation = Vector(node.translation[0], node.translation[1], node.translation[2])
                bone.set_local_translation(translation)
            self.bones[node.name] = bone

            if node.children:
                self.build_hierarchy(bone, node.children)        
            
            



    
    
        
