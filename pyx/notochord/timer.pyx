# -- START OF COPYRIGHT & LICENSE NOTICES -- 
#
# Chordata Motion's Notochord (python module)
# -- Core implementation of Chordata Motion's Open Source motion capture framework
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2021 Bruno Laurencich
# Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
#
# This file is part of Chordata Motion's Notochord (python module).
#
# Chordata Motion's Notochord (python module) is free software: you can 
# redistribute it and/or modify it under the terms of the 
# GNU General Public License as published by the Free Software Foundation, 
# either version 3 of the License, or (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata Motion's Notochord (python module).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 


from libcpp cimport bool, nullptr_t, nullptr
from libcpp.memory cimport shared_ptr
from libcpp.string cimport string
from libc.stdint cimport (uint8_t, uint16_t, uint32_t, uint64_t,
						  int8_t, int16_t, int32_t, int64_t)

from .core cimport get_runtime, Notochord_Runtime
from .timer cimport _Timekeeper, Timekeeper
from .node cimport Node, KCeptor, _Node_ptr, _Node, KCeptorPP, Mux

from .communicator import info, debug, trace
from .error import IO_Error
from .types import Mux_Channel
from libcpp.memory cimport make_shared
from time import sleep
from . import io
from .io cimport I2C

global_timekeeper = None

cdef class Timekeeper:
	def __cinit__(self, str _label):
		cdef Notochord_Runtime *n = get_runtime()
		cdef string label = _label.encode('utf-8')
		cdef timek_ptr _ptr = n.get_timekeeper(label)

		if _ptr == nullptr:
			_ptr = shared_ptr[_Timekeeper](new _Timekeeper(label))
			n.add_index_timekeeper(_ptr)

		self.c_ptr = _ptr

		if not self.c_ptr:
			from .error import Notochord_Error
			raise Notochord_Error("Error getting the timekeeper {}".format(_label))

	def reset(self):
		self.c_ptr.get().reset()

	@property
	def label(self):
		return self.c_ptr.get().get_label().decode('utf-8')

	@property
	def millis(self):
		return self.c_ptr.get().millis()

	@property
	def micros(self):
		return self.c_ptr.get().micros()

	@property
	def seconds(self):
		return self.c_ptr.get().seconds()

cdef class Node_Scheduler:
	def __cinit__(self):
		self.c_ptr = make_shared[_Node_Scheduler]()
	
	def get_ODR_modifiers(self):
		return self.c_ptr.get().get_ODR_modifiers()

	def length(self):
		return self.c_ptr.get().length()

	cpdef add_node(self, Node n):
		self.c_ptr.get().add_node(n.c_ptr)
	
	cpdef bang(self, uint64_t wait):
		return self.c_ptr.get().bang(wait)	

	def handlers(self):
		self.c_ptr.get().handlers()

	def scan(self, mux = None):
		sensors = []
		_i2c = Node.set_i2c(None)
		if _i2c.adapter == '/dev/null':
			return []
		mux_addr = 0x70
		info("== Started Hierarchy scanning ==")
		if mux == None:
			try:
				debug("Scanning Mux 0x{:2x}".format(mux_addr))
				mux = Mux(None, mux_addr, "test_mux")
			except:			
				mux_addr = 0x73
				debug("Scanning Mux 0x{:2x}".format(mux_addr))
				try:
					mux = Mux(None, mux_addr, "test_mux")
				except IO_Error:
					raise IO_Error("No Mux found at addresses {}, {}".format(0x70, 0x73))
				pass
		else:
			mux_addr = mux.address
			
		for i in range(1, 7):
			debug("Scan: opening branch {}".format(Mux_Channel(i).name))
			branch = mux.get_branch(Mux_Channel(i))
			mux.bang_branch(Mux_Channel(i))
			_parent = branch
			if mux_addr == 0x70:
				for j in range(64, 97):
					if KCeptorPP.is_kc_connected(_i2c.c_ptr, j):
						try:
							debug("Scan: KCeptor++ found, address: 0x{:2x}".format(j))
							kc = KCeptorPP(_parent, j, "kc_"+hex(j)+"branch"+str(i))

						except IO_Error:
							continue
						try:
							#kc.setup()
							#sleep(0.05)
							#kc.stop()
							self.add_node(kc)
							sensors.append(kc)
						except IO_Error:
							raise IO_Error("KCeptorPP found at address {} but cannot initialize".format(j))

						_parent = kc
			else:
				for j in range(0, 4):
					if KCeptor.is_kc_connected(_i2c.c_ptr, j):
						try:
							debug("Scan: KCeptor found, translation value: {}".format(j))
							kc = KCeptor(_parent, j, "kc_"+hex(j)+"branch"+str(i))

						except IO_Error:
							continue
						try:
							#kc.setup()
							#sleep(0.05)
							#kc.stop()
							self.add_node(kc)
							sensors.append(kc)
						except IO_Error:
							raise IO_Error("KCeptor found at address {} but cannot initialize".format(j))

						_parent = kc
						
			self.add_node(branch)
			sensors.append(branch)
		
		info("Scanning complete")
		return sensors
	
	@property
	def sensors(self):
		return self.sensors

	def stop_sensors(self):
		self.c_ptr.get().stop_sensors()

cdef class Timer:
	def __cinit__(self, Node_Scheduler actuator):
		self.c_ptr = make_shared[_Timer](actuator.c_ptr)

	def start(self):
		return self.c_ptr.get().start()

	def terminate(self):
		return self.c_ptr.get().terminate()

