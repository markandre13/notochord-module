# -- START OF COPYRIGHT & LICENSE NOTICES --
#
# Chordata Motion's Notochord (python module)
# -- Core implementation of Chordata Motion's Open Source motion capture framework
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2021 Bruno Laurencich
# Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
#
# This file is part of Chordata Motion's Notochord (python module).
#
# Chordata Motion's Notochord (python module) is free software: you can
# redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata Motion's Notochord (python module).
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES --

from enum import IntEnum, IntFlag

class Mux_Channel(IntEnum):
	OFF  = 0 #< 0
	CH_1 = 1 #< 0x1
	CH_2 = 2 #< 0x2
	CH_3 = 3 #< 0x4
	CH_4 = 4 #< 0x8
	CH_5 = 5 #< 0x10
	CH_6 = 6 #< 0x20

class Accel_Scale_Options(IntEnum):
	accel_2  = 0b00,
	accel_16 = 0b01,
	accel_4  = 0b10,
	accel_8  = 0b11

class Gyro_Scale_Options(IntEnum):
	gyro_245  = 0b00,
	gyro_500  = 0b01,
	gyro_2000 = 0b10 #	This used to be 0b11, because of the actual bits that need to
					 #	be configured in the LSM9DS1 register. But it caused compatibility
					 #	issues between cython and c++

class Mag_Scale_Options(IntEnum):
	mag_4  = 0b00,
	mag_8  = 0b01,
	mag_12 = 0b10,
	mag_16 = 0b11

class Output_Data_Rate(IntEnum):
	OUTPUT_RATE_50  = 0b00,
	OUTPUT_RATE_119 = 0b01,
	OUTPUT_RATE_238 = 0b10

class KC_STATUS(IntEnum):
	SETUP    = 0,
	GX_CALIB = 1,
	WAITING  = 2,
	M_CALIB  = 3,
	SAVING   = 4,
	ACTIVE   = 5


class Output_Sink(IntFlag):
	NONE = 0
	STDOUT = 1
	STDERR = 1<<1
	FILE = 1<<2
	OSC = 1<<3
	PYTHON = 1<<4
	WEBSOCKET = 1<<5

class Kc_Values(IntEnum):
	PlusPlus = 1
	Two = 2

class Log_Levels(IntEnum):
	Trace = 0
	Debug = 1
	Info = 2
	Warn = 3
	Error = 4
	Critical = 5