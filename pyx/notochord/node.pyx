# -- START OF COPYRIGHT & LICENSE NOTICES -- 
#
# Chordata Motion's Notochord (python module)
# -- Core implementation of Chordata Motion's Open Source motion capture framework
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2021 Bruno Laurencich
# Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
#
# This file is part of Chordata Motion's Notochord (python module).
#
# Chordata Motion's Notochord (python module) is free software: you can 
# redistribute it and/or modify it under the terms of the 
# GNU General Public License as published by the Free Software Foundation, 
# either version 3 of the License, or (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata Motion's Notochord (python module).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

from libcpp.memory cimport shared_ptr, static_pointer_cast
from .node cimport (Link, link_ptr, Node,
					_Hierarchy, Hierarchy,
					_Mux, Mux, mux_ptr, _Mux_Channel,
					_Node, _Node_ptr,
					Branch, _Branch, branch_ptr,
					KCeptor, _KCeptor, kceptor_ptr,
					KCeptorPP, _KCeptorPP,
					EEPROM_info,
					uint64_t, 
					peek_destruction)

#from .io cimport I2C
from .fusion._kalman cimport Kalman, kalman_ptr
from .math._types cimport _Matrix3, Matrix3, matrix3_ptr, create_Matrix3_ptr, vector3_ptr
from libcpp cimport bool, nullptr
from libcpp.string cimport string

from . import io
from . import error
from .utils import get_version_tuple_from_int, get_int_from_version_tuple
from .types import Mux_Channel, Accel_Scale_Options, Gyro_Scale_Options, Mag_Scale_Options, Output_Data_Rate, KC_STATUS
from enum import IntEnum
from collections.abc import Iterable
from datetime import datetime
from time import time
from . import defaults


# ----------------- Node ----------------- 
cdef class Hierarchy:
	def __cinit__(self, Node first_node):
		cdef link_ptr _first_node = static_pointer_cast[Link, _Node](first_node.c_ptr)
		self.c_ptr = new _Hierarchy(_first_node)
		self._index_built = False

	def __dealloc__(self):
		if self.c_ptr != NULL:
			del self.c_ptr

	def rebuild_index(self):
		self.c_ptr.build_index()
		self._index_built = True

	def get_node_info(self, str label):
		if not self._index_built:
			self.rebuild_index()
		
		cdef Link_Info info = self.c_ptr.get_link_info(label.encode("utf-8"))

		return {
			"label" : info.label.decode('utf-8'),
			"parent_label" : info.parent_label.decode('utf-8'),
			"children_label" : tuple(l.decode('utf-8') for l in info.children_label),
			"users" : info.shared_ptr_count
		} 


# ----------------- Node ----------------- 

cdef class Node:
	@property
	def label(self):
		return self.c_ptr.get().get_label().decode('utf-8')

	@property
	def address(self):
		return self.c_ptr.get().get_address()

	@property
	def parent(self):
		return self._parent

	@property
	def link_ptr(self):
		return <uint64_t>(<_Node*>(self.c_ptr.get()))

	@property
	def children(self):
		if isinstance(self, Mux):
			return tuple(self.get_branch(Mux_Channel(i+1)) for i in range(6))
		elif self._child:
			return (self._child,)
		else:
			return None

	@property
	def i2c(self):
		return self._i2c

	cdef link_ptr set_hierarchy(self, Node parent):
		cdef link_ptr _parent = <link_ptr>nullptr
		self._parent = parent
		if parent:
			parent._child = self
			_parent = static_pointer_cast[Link, _Node](self._parent.c_ptr)

		return _parent

	@staticmethod
	cdef I2C set_i2c(I2C _i2c):
		if not _i2c:
			if not io.i2c: # does the io module has an active I2C ob?
				_i2c = I2C() #add a Mock i2c
			else:
				_i2c = io.i2c # use the global (io module) one
		return _i2c

	def bang(self):
		return self.c_ptr.get().bang()
# ----------------- Branch ----------------- 

cdef class Branch(Node):
	def __cinit__(self, Mux mux):
		self._parent = None
		self._i2c = None
		if mux:
			self._parent = mux
			self._i2c = self._parent._i2c

	@staticmethod
	cdef Branch create_branch(Mux mux, branch_ptr b_ptr):
		cdef Branch new_branch = Branch.__new__(Branch, mux)
		new_branch.c_ptr = static_pointer_cast[_Node, _Branch](b_ptr)
		return new_branch

# ----------------- Mux ----------------- 

cdef class Mux(Node):
	def __cinit__(self, Node parent, uint8_t address, str label, I2C i2c = None):
		cdef link_ptr _parent = self.set_hierarchy(parent)
		cdef link_ptr mux_shared 
		self._i2c = Node.set_i2c(i2c)
		mux_shared = _Mux.Create(_parent, address, label.encode('utf-8'), self._i2c.c_ptr)
		self.c_ptr = static_pointer_cast[_Node, Link](mux_shared)
		self.branches = {}

	def get_branch(self, mch):
		if not isinstance(mch, Mux_Channel):
			raise TypeError("A Mux_Channel type is required to get a Branch")

		cdef link_ptr mux_link = static_pointer_cast[Link, _Node ](self.c_ptr)
		cdef branch_ptr b_ptr
		if mch not in self.branches.keys():
			b_ptr = static_pointer_cast[_Branch, Link](mux_link.get().get_child(<_Mux_Channel> mch.value -1))
			new_branch = Branch.create_branch(self, b_ptr)
			self.branches[mch] = new_branch
		
		return self.branches[mch]

	def bang_branch(self, mch):
		if not isinstance(mch, Mux_Channel):
			raise TypeError("A Mux_Channel type is required to bang a Branch")
		static_pointer_cast[_Mux, _Node](self.c_ptr).get().bang_branch(<_Mux_Channel>mch.value)

	@property
	def state(self):
		return <uint8_t>static_pointer_cast[_Mux, _Node](self.c_ptr).get().get_state()

# ----------------- KCeptor ----------------- 

cdef class KCeptor(KCeptor_Base):
	def __cinit__(self, Node parent, uint8_t address, str label, I2C i2c = None):
		cdef link_ptr _parent = self.set_hierarchy(parent)
		cdef link_ptr kc_shared
		self._i2c = Node.set_i2c(i2c)

		kc_shared = _KCeptor.Create(_parent, address, label.encode('utf-8'), self._i2c.c_ptr)
		self.c_ptr = static_pointer_cast[_Node, Link](kc_shared)

	cdef _KCeptor* kc_ptr(self):
		return static_pointer_cast[_KCeptor, _Node](self.c_ptr).get()

	# --------- Properties -----------

	@property
	def odr(self):
		return self.kc_ptr().get_odr()

	@property
	def kalman(self):
		if not self._kalman:
			self._kalman = Kalman.create(self.kc_ptr().get_kalman())
		return self._kalman

	@property
	def g_offset(self):
		cdef vector3_ptr offset = self.kc_ptr().get_g_offset()
		vec = Vector.create(offset)

		return vec

	@g_offset.setter
	def g_offset(self, other):
		if not isinstance(other, Iterable) or len(other) != 3:
			raise TypeError("An iterable of length 3 should be used to set the value of a KCeptor.offset")
		
		self.kc_ptr().set_g_offset(other[0], other[1], other[2])

	@property
	def a_offset(self):
		cdef vector3_ptr offset = self.kc_ptr().get_a_offset()
		vec = Vector.create(offset)

		return vec

	@a_offset.setter
	def a_offset(self, other):
		if not isinstance(other, Iterable) or len(other) != 3:
			raise TypeError("An iterable of length 3 should be used to set the value of a KCeptor.offset")

		self.kc_ptr().set_a_offset(other[0], other[1], other[2])

	@property
	def m_offset(self):
		cdef vector3_ptr offset = self.kc_ptr().get_m_offset()
		vec = Vector.create(offset)

		return vec

	@m_offset.setter
	def m_offset(self, other):
		if not isinstance(other, Iterable) or len(other) != 3:
			raise TypeError("An iterable of length 3 should be used to set the value of a KCeptor.offset")

		self.kc_ptr().set_m_offset(other[0], other[1], other[2])

	@property
	def m_matrix(self):
		if not self._m_matrix:
			self._m_matrix = Matrix3.create(self.kc_ptr().get_m_matrix())
			
		return self._m_matrix

	@m_matrix.setter
	def m_matrix(self, Matrix3 other):
		self.kc_ptr().set_m_matrix(other.sh_ptr)

	@property
	def accel(self):
		return Accel_Scale_Options(self.kc_ptr()._get_accel_scale())
		
	@property
	def gyro(self):
		return Gyro_Scale_Options(self.kc_ptr()._get_gyro_scale())
		
	@property
	def mag(self):
		return Mag_Scale_Options(self.kc_ptr()._get_mag_scale())
	
	@property
	def rate(self):
		return Output_Data_Rate(self.kc_ptr()._get_kcpp_rate())

	# --------- Methods -----------

	def setup(self):
		return self.kc_ptr().setup()

	def read_gyro(self, real_units = False):
		# cdef const LSM9DS1[IO_Base] *imu = self.kc_ptr().get_imu()
		cdef _KCeptor* kc = self.kc_ptr() 
		kc.read_gyro()
		if real_units:
			return (kc.get_g_read(X_AXIS), kc.get_g_read(Y_AXIS), kc.get_g_read(Z_AXIS))

		return (kc.get_g_raw_read(X_AXIS), kc.get_g_raw_read(Y_AXIS), kc.get_g_raw_read(Z_AXIS))

	def read_accel(self, real_units = False):
		cdef _KCeptor* kc = self.kc_ptr() 
		kc.read_accel()
		if real_units:
			return (kc.get_a_read(X_AXIS), kc.get_a_read(Y_AXIS), kc.get_a_read(Z_AXIS))

		return (kc.get_a_raw_read(X_AXIS), kc.get_a_raw_read(Y_AXIS), kc.get_a_raw_read(Z_AXIS))

	def read_mag(self, real_units = False):
		cdef _KCeptor* kc = self.kc_ptr() 
		kc.read_mag(False)
		if real_units:
			return (kc.get_m_read(X_AXIS), kc.get_m_read(Y_AXIS), kc.get_m_read(Z_AXIS))

		return (kc.get_m_raw_read(X_AXIS), kc.get_m_raw_read(Y_AXIS), kc.get_m_raw_read(Z_AXIS))

	def read_sensors(self, real_units = False):
		cdef _KCeptor* kc = self.kc_ptr()
		kc.read_gyro()
		kc.read_accel() 
		kc.read_mag(True)
		if real_units:
			return (kc.get_g_read(X_AXIS), kc.get_g_read(Y_AXIS), kc.get_g_read(Z_AXIS),
					kc.get_a_read(X_AXIS), kc.get_a_read(Y_AXIS), kc.get_a_read(Z_AXIS),
					kc.get_m_read(X_AXIS), kc.get_m_read(Y_AXIS), kc.get_m_read(Z_AXIS))

		return (kc.get_g_raw_read(X_AXIS), kc.get_g_raw_read(Y_AXIS), kc.get_g_raw_read(Z_AXIS),
				kc.get_a_raw_read(X_AXIS), kc.get_a_raw_read(Y_AXIS), kc.get_a_raw_read(Z_AXIS),
				kc.get_m_raw_read(X_AXIS), kc.get_m_raw_read(Y_AXIS), kc.get_m_raw_read(Z_AXIS))

	def calibrate_ag(self):
		return self.kc_ptr().calibrate_xg()

	def calibrate_m(self):
		return self.kc_ptr().calibrate_m()
	
	def stop(self):
		self.kc_ptr().stop()

	@staticmethod
	cdef bool is_kc_connected(io_ptr _io, uint8_t _address):
		return _KCeptor.is_kc_connected(_io, _address)

	# --------- EEPROM -----------
	
	def read_calib_from_eeprom(self):
		self.kc_ptr().read_calib_from_EEPROM()

	def write_calib_to_eeprom(self):
		cdef EEPROM_info info;
		info.validation = defaults.NOTOCHORD_CRC32
		info.version = get_int_from_version_tuple((1, 11, 21))
		info.timestamp = int(time())
		self.kc_ptr().write_calib_to_EEPROM(&info)

	@property
	def eeprom(self):
		cdef EEPROM_info info;
		try:
			info = self.kc_ptr().get_info_from_EEPROM()
		except error.IO_Error:
			return {
				"status": "EEPROM not found, KCeptor: {}, 0x{:02x}".format(self.label, self.address),
				"validation": 0,
				"version" : (0,0,0),
				"timestamp" : datetime.fromtimestamp(0)
			}
		
		if info.validation == defaults.NOTOCHORD_CRC32:
			status = "OK"
		else:
			status = "NO VALID: 0x{:8X}".format(defaults.NOTOCHORD_CRC32)
		
		return {
			"status": status,
			"validation": info.validation,
			"version" : get_version_tuple_from_int(info.version),
			"timestamp" : datetime.fromtimestamp(info.timestamp)
		}



# ----------------- KCeptorPP ----------------- 

cdef class KCeptorPP(KCeptor_Base):
	def __cinit__(self, Node parent, uint8_t address, str label, I2C i2c = None):
		cdef link_ptr _parent = self.set_hierarchy(parent)
		cdef link_ptr kc_shared
		self._i2c = Node.set_i2c(i2c)

		kc_shared = _KCeptorPP.Create(_parent, address, label.encode('utf-8'), self._i2c.c_ptr)
		self.c_ptr = static_pointer_cast[_Node, Link](kc_shared)

	cdef _KCeptorPP* kc_ptr(self):
		return static_pointer_cast[_KCeptorPP, _Node](self.c_ptr).get()


	def setup(self):
		self.kc_ptr().setup()

	def read(self, calc = True):
		self.kc_ptr().read_sensors_DMA(calc)

	cpdef change_addr(self, uint8_t addr):
		self.kc_ptr().change_i2c_addr(addr)
	
	def stop(self):
		self.kc_ptr().stop()

	def save_data(self):
		self.kc_ptr().save_data()

	def read_data(self):
		self.kc_ptr().read_data()

	@staticmethod
	cdef bool is_kc_connected(io_ptr _io, uint8_t _address):
		try:
			_KCeptorPP.is_kc_connected(_io, _address)
			return True
		except error.IO_Error:
			return False

	@property
	def m_matrix(self):
		if not self._m_matrix:
			self._m_matrix = Matrix3.create(self.kc_ptr().get_m_matrix())
			
		return self._m_matrix

	@m_matrix.setter
	def m_matrix(self, Matrix3 other):
		self.kc_ptr().set_m_matrix(other.sh_ptr)

	def set_state(self, state):
		self.kc_ptr().set_state(state.value)
	# --------- Properties -----------

