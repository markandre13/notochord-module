# -- START OF COPYRIGHT & LICENSE NOTICES -- 
#
# Chordata Motion's Notochord (python module)
# -- Core implementation of Chordata Motion's Open Source motion capture framework
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2021 Bruno Laurencich
# Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
#
# This file is part of Chordata Motion's Notochord (python module).
#
# Chordata Motion's Notochord (python module) is free software: you can 
# redistribute it and/or modify it under the terms of the 
# GNU General Public License as published by the Free Software Foundation, 
# either version 3 of the License, or (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata Motion's Notochord (python module).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

from .io cimport I2C, I2C_io, Mock_I2C_io, io_ptr, mock_ptr
from libcpp.string cimport string
from libcpp.memory cimport shared_ptr, static_pointer_cast, dynamic_pointer_cast

i2c = None

cdef class I2C:
	def __cinit__(self):
		cdef shared_ptr[Mock_I2C_io] stub_ptr = shared_ptr[Mock_I2C_io](new Mock_I2C_io()) 
		self.c_ptr = static_pointer_cast[IO_Base, Mock_I2C_io](stub_ptr)

	@property
	def adapter(self):
		cdef IO_Base * _i2c = <IO_Base *>self.c_ptr.get() 
		return _i2c.get_adapter_str().decode('utf-8')

	def write_simple_byte(self, uint8_t address, uint8_t data):
		self.c_ptr.get().write_byte(address, data)

	def write_byte(self, uint8_t address, uint8_t sub_address, uint8_t data):
		self.c_ptr.get().write_byte(address, sub_address, data)

	def read_simple_byte(self, uint8_t address):
		return  self.c_ptr.get().read_byte(address)

	def read_byte(self, uint8_t address, uint8_t sub_address):
		return  self.c_ptr.get().read_byte(address, sub_address)

	def read_bytes(self, uint8_t address, uint8_t sub_address, uint8_t count):
		return  self.c_ptr.get().read_bytes(address, sub_address, count)

	@property
	def _mock_reads(self):
		cdef mock_ptr mock = dynamic_pointer_cast[Mock_I2C_io, IO_Base](self.c_ptr)
		if mock:
			return mock.get().reads
		else:
			return []

	@property
	def _mock_writes(self):
		cdef mock_ptr mock = dynamic_pointer_cast[Mock_I2C_io, IO_Base](self.c_ptr)
		if mock:
			return mock.get().writes
		else:
			return []

	def _mock_reset(self):
		cdef mock_ptr mock = dynamic_pointer_cast[Mock_I2C_io, IO_Base](self.c_ptr)
		if mock:
			mock.get().reset()

