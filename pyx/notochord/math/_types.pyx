# -- START OF COPYRIGHT & LICENSE NOTICES -- 
#
# Chordata Motion's Notochord (python module)
# -- Core implementation of Chordata Motion's Open Source motion capture framework
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2021 Bruno Laurencich
# Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
#
# This file is part of Chordata Motion's Notochord (python module).
#
# Chordata Motion's Notochord (python module) is free software: you can 
# redistribute it and/or modify it under the terms of the 
# GNU General Public License as published by the Free Software Foundation, 
# either version 3 of the License, or (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata Motion's Notochord (python module).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

from collections.abc import Iterable
from ._types cimport (Math_type, _Quaternion, Quaternion, _Vector3, Vector, vector3_ptr, 
						_Matrix3, Matrix3, create_Matrix3_ptr, matrix3_ptr)
from cpython.object cimport Py_EQ, Py_NE
from libcpp.memory cimport shared_ptr, make_shared

from notochord.error import Inmutable_Error

from cython.operator cimport dereference


cdef class Math_type:
	def __cinit__(self):
		self._mutable = True

	@property 
	def mutable(self):
		return self._mutable

	cdef check_mutable(self):
		if not self._mutable: raise Inmutable_Error("Trying to set a value in an inmutable Math Type, use .copy() to get a mutable one")

cdef class Quaternion:	
	@staticmethod
	cdef Quaternion create(quat_ptr q_ptr):
		cdef Quaternion new_q = Quaternion.__new__(Quaternion, __no_c_ptr__ = True)
		new_q.sh_ptr = q_ptr
		new_q._mutable = False
		return new_q

	def __cinit__(self, *args, **kwargs):
		if "__no_c_ptr__" in kwargs.keys() and kwargs["__no_c_ptr__"]:
			pass
		else:
			self.build_from_args(*args)
		self.n = 0

	def build_from_args(self, *args):
		if len(args) == 0:
			self.sh_ptr = make_shared[_Quaternion](1, 0, 0, 0)
		
		elif len(args) == 1:
			if isinstance(args[0], Iterable):
				for i,arg in enumerate(args[0]):
					if not isinstance(arg,(int, float)):
						raise ValueError("A not numeric value passed to Quaternion constructor in position {}, got{}".format(i, type(arg)))

				if len(args[0]) == 4:
					self.sh_ptr = make_shared[_Quaternion](float(args[0][0]), float(args[0][1]), float(args[0][2]), float(args[0][3]))
								
				else:
					raise ValueError("A sequence of incorrect length passed to QUaternion constructor, got={}, len=".format(type(args[0]), len(args[0])))
	
			else:
				raise ValueError("Not numeric or sequence value passed to Quaternion constructor, got={}".format(type(args[0])))

		elif len(args) == 4:
			for i,arg in enumerate(args):
				if not isinstance(arg,(int, float)):
					raise ValueError("A not numeric value passed to Quaternion constructor in position {}, got{}".format(i, type(arg)))
			
			self.sh_ptr = make_shared[_Quaternion](float(args[0]), float(args[1]), float(args[2]), float(args[3]))

		else:
			raise ValueError("Expecting 1 or 4 arguments on Quaternion constructor, got={}".format(len(args)))
		
		self.n = 0				
		
	def __dealloc__(self):
		pass

	def __repr__(self):
		return ("Quaternion(w={:.5f}, x={:.5f}, y={:.5f}, z={:.5f})".format(self.sh_ptr.get().w(), 
			self.sh_ptr.get().x(), self.sh_ptr.get().y(), self.sh_ptr.get().z()))

	def __iter__(self):
		self.n = 0
		return self

	def __next__(self):
		self.n += 1
		if self.n == 1:
			return self.sh_ptr.get().w()
		elif self.n == 2:
			return self.sh_ptr.get().x()
		elif self.n == 3:
			return self.sh_ptr.get().y()
		elif self.n == 4:
			return self.sh_ptr.get().z()
		else:
			raise StopIteration

	def __getitem__(self, key):
		cdef _Quaternion *q = self.sh_ptr.get() 

		if key > 3 or key < 0:
			raise IndexError("Quaternion value accesing out of bounds, range=(0,3), got={}".format(key))

		return q.get(key)
	
	def __mul__(self, other):			
		return self.mul(other)

	cpdef mul(self, Quaternion other):
		cdef _Quaternion result = self.sh_ptr.get()[0] * other.sh_ptr.get()[0] 	
		py_result = Quaternion(result.w(),result.x(),result.y(),result.z())

		return py_result

	def __setitem__(self, key, value):
		self.check_mutable()
		cdef _Quaternion *q = self.sh_ptr.get()

		if key > 3 or key < 0:
			raise IndexError("Quaternion value setting out of bounds, range=(0,3), got={}".format(key))
		
		q.set_value(value, key)
		
	@property
	def w(self):
		return self.sh_ptr.get().w()

	@property
	def x(self):
		return self.sh_ptr.get().x()

	@property
	def y(self):
		return self.sh_ptr.get().y()

	@property
	def z(self):
		return self.sh_ptr.get().z()

	@w.setter
	def w(self, val):
		self.check_mutable()
		cdef _Quaternion *q = self.sh_ptr.get()
		q.set_value(val, 0) 

	@x.setter
	def x(self, val):
		self.check_mutable()
		cdef _Quaternion *q = self.sh_ptr.get()
		q.set_value(val, 1) 

	@y.setter
	def y(self, val):
		self.check_mutable()
		cdef _Quaternion *q = self.sh_ptr.get()
		q.set_value(val, 2)

	@z.setter
	def z(self, val):
		self.check_mutable()
		cdef _Quaternion *q = self.sh_ptr.get()
		q.set_value(val, 3)  

	def __hash__(self):
		return id(self)

	def __richcmp__(self, other, int op):
		if type(other) is not type(self):
			TypeError("Only equality comparsions between Quaternions are supported")

		if op == Py_EQ:
			return tuple(self) == tuple(other)

		if op == Py_NE:
			return tuple(self) != tuple(other)

		raise TypeError("Only equality comparsions between Quaternions are supported")
	
	cpdef conjugate(self):
		cdef _Quaternion *q = self.sh_ptr.get()
		cdef _Quaternion q_conj = q.conjugate()
		
		py_conj = Quaternion(q_conj.w(),q_conj.x(),q_conj.y(),q_conj.z())
		return py_conj

	cpdef inverse(self):
		cdef _Quaternion *q = self.sh_ptr.get()
		cdef _Quaternion q_inv = q.inverse()
		py_inv = Quaternion(q_inv.w(),q_inv.x(),q_inv.y(),q_inv.z())
		return py_inv
	
	def normalize(self):
		self.check_mutable()
		cdef _Quaternion *q = self.sh_ptr.get()
		q.normalize()
	
	cpdef normalized(self):
		cdef _Quaternion *q = self.sh_ptr.get()
		cdef _Quaternion q_normalized = q.normalized()
		py_normalized = Quaternion(q_normalized.w(),q_normalized.x(),q_normalized.y(),q_normalized.z())
		return py_normalized

	def isApprox(self, Quaternion other, float precision):
		cdef _Quaternion *other_ptr = other.sh_ptr.get()
		return self.sh_ptr.get().isApprox(dereference(other_ptr), precision)

	def norm(self):
		return self.sh_ptr.get().norm()
	
	def dot(self, Quaternion other):
		return self.sh_ptr.get().dot(dereference(other.sh_ptr.get()))
	
	cpdef slerp(self, float t, Quaternion other):
		cdef _Quaternion *q = self.sh_ptr.get()
		cdef _Quaternion *other_ptr = other.sh_ptr.get()
		cdef _Quaternion q_interp = q.slerp(t, dereference(other_ptr))
		py_interp = Quaternion(q_interp.w(),q_interp.x(),q_interp.y(),q_interp.z())
		return py_interp
	
	cpdef toRotationMatrix(self):
		cdef _Matrix3 mat = self.sh_ptr.get().toRotationMatrix()
		return Matrix3(
			mat.get(0,0), mat.get(0,1), mat.get(0,2),
			mat.get(1,0), mat.get(1,1), mat.get(1,2),
			mat.get(2,0), mat.get(2,1), mat.get(2,2),
		)
	
	def copy(self):
		return Quaternion(self.w, self.x, self.y, self.z)

cdef class Vector:	
	@staticmethod
	cdef Vector create(vector3_ptr v_ptr):
		cdef Vector new_v = Vector.__new__(Vector, __no_c_ptr__ = True)
		new_v.sh_ptr = v_ptr
		new_v._mutable = False
		return new_v

	def __cinit__(self, *args, **kwargs):
		if "__no_c_ptr__" in kwargs.keys() and kwargs["__no_c_ptr__"]:
			pass
		else:
			self.build_from_args(*args)
		self.n = 0

	def build_from_args(self, *args):
		if len(args) == 0:
			self.sh_ptr = make_shared[_Vector3](0, 0, 0)
		
		elif len(args) == 1:
			if isinstance(args[0], Iterable):
				for i,arg in enumerate(args[0]):
					if not isinstance(arg,(int, float)):
						raise ValueError("A not numeric value passed to Vector constructor in position {}, got{}".format(i, type(arg)))

				if len(args[0]) == 3:
					self.sh_ptr = make_shared[_Vector3](float(args[0][0]), float(args[0][1]), float(args[0][2]))
								
				else:
					raise ValueError("A sequence of incorrect length passed to Vector constructor, got={}, len=".format(type(args[0]), len(args[0])))
	
			else:
				raise ValueError("Not numeric or sequence value passed to Vector constructor, got={}".format(type(args[0])))

		elif len(args) == 3:
			for i,arg in enumerate(args):
				if not isinstance(arg,(int, float)):
					raise ValueError("A not numeric value passed to Vector constructor in position {}, got{}".format(i, type(arg)))
			
			self.sh_ptr = make_shared[_Vector3](float(args[0]), float(args[1]), float(args[2]))

		else:
			raise ValueError("Expecting 1 or 3 arguments on Vector constructor, got={}".format(len(args)))
			
	def __dealloc__(self):
		pass
	
	def __repr__(self):
		return ("Vector(x={:.5f}, y={:.5f}, z={:.5f})".format( self.sh_ptr.get().x(), 
						self.sh_ptr.get().y(), self.sh_ptr.get().z()))

	def __iter__(self):
		self.n = 0
		return self

	def __next__(self):
		self.n += 1
		if self.n == 1:
			return self.sh_ptr.get().x()
		elif self.n == 2:
			return self.sh_ptr.get().y()
		elif self.n == 3:
			return self.sh_ptr.get().z()
		else:
			raise StopIteration

	def __getitem__(self, key):
		cdef _Vector3 * v = self.sh_ptr.get() 

		if key > 2 or key < 0:
			raise IndexError("Vector value accesing out of bounds, range=(0,2), got={}".format(key))

		return v.get(key)

	def __setitem__(self, key, value):
		self.check_mutable()
		cdef _Vector3 * v = self.sh_ptr.get() 

		if key > 2 or key < 0:
			raise IndexError("Vector value setting out of bounds, range=(0,2), got={}".format(key))

		v.set_value(value, key)

	def __len__(self):
		return 3

	@property
	def x(self):
		return self.sh_ptr.get().x()

	@property
	def y(self):
		return self.sh_ptr.get().y()

	@property
	def z(self):
		return self.sh_ptr.get().z()

	@x.setter
	def x(self, val):
		self.check_mutable()
		cdef _Vector3 * v = self.sh_ptr.get()
		v.set_value(val, 0)  

	@y.setter
	def y(self, val):
		self.check_mutable()
		cdef _Vector3 * v = self.sh_ptr.get()
		v.set_value(val, 1)

	@z.setter
	def z(self, val):
		self.check_mutable()
		cdef _Vector3 * v = self.sh_ptr.get()
		v.set_value(val, 2)

	def __hash__(self):
		return id(self)

	def __richcmp__(self, other, int op):
		if type(other) is not type(self):
			TypeError("Only equality comparsions between Vectors are supported")

		if op == Py_EQ:
			return tuple(self) == tuple(other)

		if op == Py_NE:
			return tuple(self) != tuple(other)

		raise TypeError("Only equality comparsions between Vectors are supported")
	
	def __mul__(self, other):	
		if isinstance(other, Vector):
			return other.mul(self)
		elif isinstance(other, float):
			return self.mul(other)

		raise TypeError("Only multiplication between Vectors and scalars are supported")
	
	def mul(self, float other):
		cdef _Vector3 result = self.sh_ptr.get()[0] * other 	

		return Vector(result.x(), result.y(), result.z())

	cpdef cross(self, Vector other):
		cdef _Vector3 *ptr = self.sh_ptr.get()
		cdef _Vector3 result = ptr.cross(other.sh_ptr.get()[0])

		return Vector(result.x(), result.y(), result.z())
	
	def dot(self, Vector other):
		return self.sh_ptr.get().dot(dereference(other.sh_ptr.get()))
	
	def norm(self):
		return self.sh_ptr.get().norm()
	
	def normalized(self):
		cdef _Vector3 vec = self.sh_ptr.get().normalized()

		return Vector(vec.x(), vec.y(), vec.z())	
	
	def copy(self):
		return Vector(self.x, self.y, self.z)

cdef class Matrix3:
	@staticmethod
	cdef Matrix3 create(matrix3_ptr m_ptr):
		cdef Matrix3 new_m = Matrix3.__new__(Matrix3, __no_c_ptr__ = True)
		new_m.sh_ptr = m_ptr
		new_m._mutable = False
		return new_m

	def __cinit__(self, *args, **kwargs):
		if "__no_c_ptr__" in kwargs.keys() and kwargs["__no_c_ptr__"]:
			pass
		else:
			self.build_from_args(*args)
		self.n = 0
		

	def build_from_args(self, *args):
		if len(args) == 0:
			self.sh_ptr = create_Matrix3_ptr() # identity

		elif len(args) == 1:
			if isinstance(args[0],(int, float)):
				self.sh_ptr = create_Matrix3_ptr( 	args[0], args[0],  args[0],
													args[0], args[0],  args[0],
													args[0], args[0],  args[0])
			
			elif isinstance(args[0], Iterable):
				for i,arg in enumerate(args[0]):
					if not isinstance(arg,(int, float)):
						raise ValueError("A not numeric value passed to Matrix3 constructor in position {}, got={}".format(i, type(arg)))
						
				if len(args[0]) == 9:
					self.sh_ptr = create_Matrix3_ptr( 	args[0][0], args[0][1], args[0][2],
														args[0][3], args[0][4],  args[0][5],
														args[0][6], args[0][7],  args[0][8])

				else:
					raise ValueError("A sequence of incorrect length passed to Matrix3 constructor, got={}, len=".format(type(args[0]), len(args[0])))


			else:
				raise ValueError("Not numeric or sequence value passed to Matrix3 constructor, got={}".format(type(args[0])))

		elif len(args) == 9:
			for i,arg in enumerate(args):
				if not isinstance(arg,(int, float)):
					raise ValueError("A not numeric value passed to Matrix3 constructor in position {}, got={}".format(i, type(arg)))

			self.sh_ptr = create_Matrix3_ptr( 	args[0], args[1],  args[2],
												args[3], args[4],  args[5],
												args[6], args[7],  args[8])
		else:
			raise ValueError("Expecing 1 or 9 arguments on Matrix3 constructor, got={}".format(len(args)))
		
		# end build_from_args

	def __dealloc__(self):
		pass

	def __getitem__(self, key):
		cdef _Matrix3 * m = self.sh_ptr.get() 
		try:
			row,col = key
		except (TypeError, ValueError):
			raise TypeError("Indexes for row & col should be provided to acces Matrix3 elements")

		if row > 2 or col > 2:
			raise IndexError("Matrix3 value accesing out of bounds, max=(2, 2), got={}".format(key))
		elif row < 0 or col < 0:
			raise IndexError("Matrix3 doesn't support negative indexing, got={}".format(key))

		return m.get(row,col)

	def __setitem__(self, key, value):
		self.check_mutable()
		cdef _Matrix3 * m = self.sh_ptr.get() 
		
		try:
			row,col = key
		except (TypeError, ValueError):
			raise TypeError("Indexes for row & col should be provided to set Matrix3 elements")

		if row > 2 or col > 2:
			raise IndexError("Matrix3 value setting out of bounds, max=(2, 2), got={}".format(key))
		elif row < 0 or col < 0:
			raise IndexError("Matrix3 doesn't support negative indexing, got={}".format(key))

		m.set_value(value, row, col)

	def __repr__(self):
		t = tuple(self)
		return ("Matrix3({},\n        {},\n        {})".format(t[0], t[1], t[2]))

	def __iter__(self):
		self.n = 0
		return self

	def __next__(self):
		cdef _Matrix3 *m = self.sh_ptr.get()  
		if self.n < 3:
			row = m.get(self.n, 0), m.get(self.n, 1), m.get(self.n, 2)
			self.n += 1
			return row
		else:
			raise StopIteration

	def __hash__(self):
		return id(self)

	def __richcmp__(self, other, int op):
		if type(other) is not type(self):
			TypeError("Only equality comparsions between Matrix3 are supported")

		if op == Py_EQ:
			return tuple(self) == tuple(other)

		if op == Py_NE:
			return tuple(self) != tuple(other)

		raise TypeError("Only equality comparsions between Matrix3 are supported")
	
	def __mul__(self, other):	
		if isinstance(self, Matrix3) and isinstance(other, Matrix3):
			return self.mul_mat(other)
		elif isinstance(self, Matrix3):
			return self.mul_scalar(other)
		elif isinstance(other, Matrix3):
			return other.mul_scalar(self)
		
		raise TypeError("Only multiplications between Matrix3 and scalars are supported")
	
	def mul_scalar(self, float val):
		cdef _Matrix3 mat = self.sh_ptr.get()[0] * val 
		
		return Matrix3(
			mat.get(0,0), mat.get(0,1), mat.get(0,2),
			mat.get(1,0), mat.get(1,1), mat.get(1,2),
			mat.get(2,0), mat.get(2,1), mat.get(2,2),
		)
	
	def mul_mat(self, Matrix3 other):
		cdef _Matrix3 mat = self.sh_ptr.get()[0] * other.sh_ptr.get()[0] 
		
		return Matrix3(
			mat.get(0,0), mat.get(0,1), mat.get(0,2),
			mat.get(1,0), mat.get(1,1), mat.get(1,2),
			mat.get(2,0), mat.get(2,1), mat.get(2,2),
		)

	def inverse(self):
		cdef _Matrix3 mat = self.sh_ptr.get().inverse()

		return Matrix3(
			mat.get(0,0), mat.get(0,1), mat.get(0,2),
			mat.get(1,0), mat.get(1,1), mat.get(1,2),
			mat.get(2,0), mat.get(2,1), mat.get(2,2),
		)	
	
	def norm(self):
		return self.sh_ptr.get().norm()
	
	def determinant(self):
		return self.sh_ptr.get().determinant()

	def copy(self):
		return Matrix3(
			self[0,0], self[0,1], self[0,2],
			self[1,0], self[1,1], self[1,2],
			self[2,0], self[2,1], self[2,2]
		)	

cdef class Matrix4:
	@staticmethod
	cdef Matrix4 create(matrix4_ptr m_ptr):
		cdef Matrix4 new_m = Matrix4.__new__(Matrix4, __no_c_ptr__ = True)
		new_m.sh_ptr = m_ptr
		new_m._mutable = False
		return new_m

	def __cinit__(self, *args, **kwargs):
		if "__no_c_ptr__" in kwargs.keys() and kwargs["__no_c_ptr__"]:
			pass
		else:
			self.build_from_args(*args)
		self.n = 0
		

	def build_from_args(self, *args):
		if len(args) == 0:
			self.sh_ptr = create_Matrix4_ptr() # identity

		elif len(args) == 1:
			if isinstance(args[0],(int, float)):
				self.sh_ptr = create_Matrix4_ptr( 	args[0], args[0],  args[0], args[0],
													args[0], args[0],  args[0], args[0],
													args[0], args[0],  args[0], args[0],
													args[0], args[0],  args[0], args[0])
			
			elif isinstance(args[0], Iterable):
				for i,arg in enumerate(args[0]):
					if not isinstance(arg,(int, float)):
						raise ValueError("A not numeric value passed to Matrix4 constructor in position {}, got={}".format(i, type(arg)))
						
				if len(args[0]) == 16:
					self.sh_ptr = create_Matrix4_ptr( 	args[0][ 0], args[0][ 1], args[0][ 2], args[0][ 3],
														args[0][ 4], args[0][ 5], args[0][ 6], args[0][ 7],
														args[0][ 8], args[0][ 9], args[0][10], args[0][11],
														args[0][12], args[0][13], args[0][14], args[0][15])

				else:
					raise ValueError("A sequence of incorrect length passed to Matrix4 constructor, got={}, len=".format(type(args[0]), len(args[0])))


			else:
				raise ValueError("Not numeric or sequence value passed to Matrix4 constructor, got={}".format(type(args[0])))

		elif len(args) == 16:
			for i,arg in enumerate(args):
				if not isinstance(arg,(int, float)):
					raise ValueError("A not numeric value passed to Matrix4 constructor in position {}, got={}".format(i, type(arg)))

			self.sh_ptr = create_Matrix4_ptr( 	args[ 0], args[ 1], args[ 2], args[ 3],
												args[ 4], args[ 5], args[ 6], args[ 7],
												args[ 8], args[ 9], args[10], args[11],
												args[12], args[13], args[14], args[15])
		else:
			raise ValueError("Expecing 1 or 16 arguments on Matrix4 constructor, got={}".format(len(args)))
		
		# end build_from_args

	def __dealloc__(self):
		pass

	def __getitem__(self, key):
		cdef _Matrix4 * m = self.sh_ptr.get() 
		try:
			row,col = key
		except (TypeError, ValueError):
			raise TypeError("Indexes for row & col should be provided to acces Matrix4 elements")

		if row > 3 or col > 3:
			raise IndexError("Matrix4 value accesing out of bounds, max=(3, 3), got={}".format(key))
		elif row < 0 or col < 0:
			raise IndexError("Matrix4 doesn't support negative indexing, got={}".format(key))

		return m.get(row,col)

	def __setitem__(self, key, value):
		self.check_mutable()
		cdef _Matrix4 * m = self.sh_ptr.get() 
		
		try:
			row,col = key
		except (TypeError, ValueError):
			raise TypeError("Indexes for row & col should be provided to set Matrix4 elements")

		if row > 3 or col > 3:
			raise IndexError("Matrix4 value setting out of bounds, max=(3, 3), got={}".format(key))
		elif row < 0 or col < 0:
			raise IndexError("Matrix4 doesn't support negative indexing, got={}".format(key))

		m.set_value(value, row, col)

	def __repr__(self):
		t = tuple(self)
		return ("Matrix4({},\n        {},\n        {},\n        {})".format(t[0], t[1], t[2], t[3]))

	def __iter__(self):
		self.n = 0
		return self

	def __next__(self):
		cdef _Matrix4 *m = self.sh_ptr.get()  
		if self.n < 4:
			row = m.get(self.n, 0), m.get(self.n, 1), m.get(self.n, 2), m.get(self.n, 3)
			self.n += 1
			return row
		else:
			raise StopIteration

	def __hash__(self):
		return id(self)

	def __richcmp__(self, other, int op):
		if type(other) is not type(self):
			TypeError("Only equality comparsions between Matrix4 are supported")

		if op == Py_EQ:
			return tuple(self) == tuple(other)

		if op == Py_NE:
			return tuple(self) != tuple(other)

		raise TypeError("Only equality comparsions between Matrix4 are supported")
	
	def __mul__(self, other):	
		if isinstance(self, Matrix4) and isinstance(other, Matrix4):
			return self.mul_mat(other)
		elif isinstance(self, Matrix4):
			return self.mul_scalar(other)
		elif isinstance(other, Matrix4):
			return other.mul_scalar(self)
		
		raise TypeError("Only multiplications between Matrix4 and scalars are supported")
	
	def mul_scalar(self, float val):
		cdef _Matrix4 mat = self.sh_ptr.get()[0] * val 
		
		return Matrix4(
			mat.get(0,0), mat.get(0,1), mat.get(0,2), mat.get(0,3),
			mat.get(1,0), mat.get(1,1), mat.get(1,2), mat.get(1,3),
			mat.get(2,0), mat.get(2,1), mat.get(2,2), mat.get(2,3),
			mat.get(3,0), mat.get(3,1), mat.get(3,2), mat.get(3,3),
		)
	
	def mul_mat(self, Matrix4 other):
		cdef _Matrix4 mat = self.sh_ptr.get()[0] * other.sh_ptr.get()[0] 
		
		return Matrix4(
			mat.get(0,0), mat.get(0,1), mat.get(0,2), mat.get(0,3),
			mat.get(1,0), mat.get(1,1), mat.get(1,2), mat.get(1,3),
			mat.get(2,0), mat.get(2,1), mat.get(2,2), mat.get(2,3),
			mat.get(3,0), mat.get(3,1), mat.get(3,2), mat.get(3,3),
		)

	def inverse(self):
		cdef _Matrix4 mat = self.sh_ptr.get().inverse()

		return Matrix4(
			mat.get(0,0), mat.get(0,1), mat.get(0,2), mat.get(0,3),
			mat.get(1,0), mat.get(1,1), mat.get(1,2), mat.get(1,3),
			mat.get(2,0), mat.get(2,1), mat.get(2,2), mat.get(2,3),
			mat.get(3,0), mat.get(3,1), mat.get(3,2), mat.get(3,3),
		)	
	
	def norm(self):
		return self.sh_ptr.get().norm()
	
	def determinant(self):
		return self.sh_ptr.get().determinant()

	def copy(self):
		return Matrix4(
			self[0,0], self[0,1], self[0,2], self[0,3],
			self[1,0], self[1,1], self[1,2], self[1,3],
			self[2,0], self[2,1], self[2,2], self[2,3],
			self[3,0], self[3,1], self[3,2], self[3,3]
		)	