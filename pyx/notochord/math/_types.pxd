# -- START OF COPYRIGHT & LICENSE NOTICES -- 
#
# Chordata Motion's Notochord (python module)
# -- Core implementation of Chordata Motion's Open Source motion capture framework
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2021 Bruno Laurencich
# Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
#
# This file is part of Chordata Motion's Notochord (python module).
#
# Chordata Motion's Notochord (python module) is free software: you can 
# redistribute it and/or modify it under the terms of the 
# GNU General Public License as published by the Free Software Foundation, 
# either version 3 of the License, or (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata Motion's Notochord (python module).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

# distutils: language = c++

from libcpp cimport bool
from libcpp.string cimport string
from libcpp.memory cimport shared_ptr, weak_ptr
from libc.stdint cimport (int16_t)
# from .config cimport Configuration_Data, Notochord_Config
# from .error cimport raise_py_error
# from .io cimport io_ptr
# from .timer cimport timek_ptr

cdef extern from "math_types.h" namespace "Chordata":

	cdef cppclass _Vector3:
		_Vector3()
		_Vector3(float _x, float _y, float _z)
		float get "operator()" (int)
		void set_value(float, int)
		float x()
		float y()
		float z() 
		const _Vector3 cross(const _Vector3 &other)
		const float dot(const _Vector3 &other)
		const float norm()
		const _Vector3 normalized()
		const _Vector3 operator*(float)

	cdef cppclass _Vector3i:
		int16_t get "operator()" (int)
		float x()
		float y()
		float z() 

	cdef cppclass _Matrix3:
		_Matrix3()
		float get "operator()" (int, int)
		void set_value(float, int, int)		
		const _Matrix3 inverse()
		const float norm()
		const float determinant()
		const _Matrix3 operator*(float)
		const _Matrix3 operator*(_Matrix3 &other)
	
	cdef cppclass _Matrix4:
		_Matrix4()
		float get "operator()" (int, int)
		void set_value(float, int, int)		
		const _Matrix4 inverse()
		const float norm()
		const float determinant()
		const _Matrix4 operator*(float)
		const _Matrix4 operator*(_Matrix4 &other)
	
	cdef cppclass _Quaternion:
		_Quaternion()
		_Quaternion(float _w, float _x, float _y, float _z)
		_Quaternion(_Quaternion &other)
		float get "operator[]" (int)
		void set_value(float, int)
		float w()
		float x()
		float y()
		float z() 
		const _Quaternion conjugate()
		const _Quaternion inverse()
		const _Quaternion normalized()
		const bool isApprox(_Quaternion &other, float precision)
		const float norm()
		const float dot(const _Quaternion &other)
		const _Quaternion slerp(float t, const _Quaternion &other)
		void normalize()
		const _Quaternion operator*(const _Quaternion &other)
		const _Matrix3 toRotationMatrix()

	ctypedef shared_ptr[_Quaternion] quat_ptr
	ctypedef shared_ptr[_Vector3] vector3_ptr
	ctypedef shared_ptr[_Matrix3] matrix3_ptr
	ctypedef shared_ptr[_Matrix4] matrix4_ptr

	matrix3_ptr create_Matrix3_ptr(float _00, float _01, float _02,
								   float _10, float _11, float _12,
								   float _20, float _21, float _22)
	
	matrix3_ptr create_Matrix3_ptr()

	matrix4_ptr create_Matrix4_ptr(float _00, float _01, float _02, float _03,
								   float _10, float _11, float _12, float _13,
								   float _20, float _21, float _22, float _23,
								   float _30, float _31, float _32, float _33)
	
	matrix4_ptr create_Matrix4_ptr()


cdef class Math_type:
	cdef bool _mutable
	cdef check_mutable(self)

cdef class Quaternion(Math_type):
	cdef quat_ptr sh_ptr
	cdef int n
	cpdef conjugate(self)
	cpdef inverse(self)
	cpdef normalized(self)
	cpdef slerp(self, float, Quaternion)
	cpdef mul(self, Quaternion)
	cpdef toRotationMatrix(self)

	@staticmethod
	cdef Quaternion create(quat_ptr q_ptr)

cdef class Vector(Math_type):
	cdef vector3_ptr sh_ptr
	cdef int n
	cpdef cross(self, Vector)

	@staticmethod
	cdef Vector create(vector3_ptr m_ptr)

cdef class Matrix3(Math_type):
	cdef matrix3_ptr sh_ptr
	cdef int n

	@staticmethod
	cdef Matrix3 create(matrix3_ptr m_ptr)

cdef class Matrix4(Math_type):
	cdef matrix4_ptr sh_ptr
	cdef int n

	@staticmethod
	cdef Matrix4 create(matrix4_ptr m_ptr)
