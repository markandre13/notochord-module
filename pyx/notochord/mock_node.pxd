# -- START OF COPYRIGHT & LICENSE NOTICES -- 
#
# Chordata Motion's Notochord (python module)
# -- Core implementation of Chordata Motion's Open Source motion capture framework
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2021 Bruno Laurencich
# Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
#
# This file is part of Chordata Motion's Notochord (python module).
#
# Chordata Motion's Notochord (python module) is free software: you can 
# redistribute it and/or modify it under the terms of the 
# GNU General Public License as published by the Free Software Foundation, 
# either version 3 of the License, or (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata Motion's Notochord (python module).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 



from libcpp.string cimport string
from libcpp.vector cimport vector
from libcpp.memory cimport shared_ptr, weak_ptr
from libcpp cimport bool, nullptr
from libc.stdint cimport (uint8_t, uint16_t, uint32_t, uint64_t,
							int8_t, int16_t, int32_t, int64_t)
from libc.stddef cimport size_t

from .math._types cimport _Vector3, vector3_ptr, Vector, _Matrix3, Matrix3, matrix3_ptr
from .io cimport IO_Base, I2C, I2C_io, io_ptr
from .error cimport raise_py_error
from .utils cimport _Offset_Calibrator
from .fusion._kalman cimport Kalman, Fusion_Kalman_Base
from .node cimport link_ptr, io_ptr, Node, _Node, _Mux_Channel


cdef extern from "mock_phy_nodes.h" namespace "Chordata":
    # // -------- MUX ------- //
	cdef cppclass _Mock_Mux(_Node):
		@staticmethod
		link_ptr Create(link_ptr _parent,
						uint8_t _address, string _label,
						io_ptr _io ) except +raise_py_error

		void bang_branch(_Mux_Channel mch) except +raise_py_error
		_Mux_Channel get_state() const 

	# // -------- BRANCH ------- //
	cdef cppclass _Mock_Branch(_Node):
		void bang()

	# // -------- KCEPTOR ------- //
	cdef cppclass _Mock_KCeptor(_Node):
		vector[float] debug_buffer
		@staticmethod
		link_ptr Create(link_ptr _parent, 
						uint8_t _address, string _label,
						io_ptr _io) except +raise_py_error
		

		void apply_mag_calib(int16_t *a, int16_t x, int16_t y, int16_t z)
		bool setup()
		void set_EEPROM_attributes(uint32_t version, uint32_t timestamp)

		# /*-------------- Getters --------------*/
		const vector3_ptr get_g_offset()
		const vector3_ptr get_a_offset() 
		const vector3_ptr get_m_offset() 
		const matrix3_ptr get_m_matrix()

		float _get_hz()
		uint8_t _get_accel_scale()
		uint16_t _get_gyro_scale()
		uint8_t _get_mag_scale()

		# /*-------------- Setters --------------*/
		void set_g_offset(int16_t,int16_t,int16_t)
		void set_a_offset(int16_t,int16_t,int16_t)
		void set_m_offset(int16_t,int16_t,int16_t)
		void set_m_matrix(matrix3_ptr)

ctypedef shared_ptr[_Mock_Mux] mock_mux_ptr
ctypedef shared_ptr[_Mock_Branch] mock_branch_ptr
ctypedef shared_ptr[_Mock_KCeptor] mock_kceptor_ptr

cdef class Mock_Branch(Node):
	@staticmethod
	cdef Mock_Branch create_branch(Mock_Mux mux, mock_branch_ptr b_ptr)

cdef class Mock_Mux(Node):
	cdef dict branches

cdef class Mock_KCeptor(Node):
	cdef _Mock_KCeptor* kc_ptr(self)
