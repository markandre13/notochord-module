"""A setuptools based setup module.

See:
https://packaging.python.org/guides/distributing-packages-using-setuptools/
https://github.com/pypa/sampleproject
"""


from setuptools import setup, find_packages , Extension
import pathlib
import re

here = pathlib.Path(__file__).parent.resolve()

version ='1.0.2'
# One line description
description = 'Chordata Motion\'s core application as a python module'
# Long description from the README file
long_description = (here / 'README.md').read_text(encoding='utf-8')


class CompiledExtension(Extension):
    """from: https://stackoverflow.com/questions/42585210/extending-setuptools-extension-to-use-cmake-in-setup-py"""
    def __init__(self, name, source):
        # don't invoke the original build_ext for this special extension
        super().__init__(name, sources=[source])

def get_compiled_extensions():
    extensions = []
    p = here.joinpath('notochord')
    files = [x for x in p.iterdir() if x.is_file()]
    for f in files:
        res = re.match(r'([\w_-]*)\.cpython.*\.so', f.name)
        if res:
            print("Found compiled extension:", res.group(1), f)
            extensions.append(CompiledExtension("notochord.{}".format(res.group(1)), str(f)))

    return extensions

setup(
    name='notochord',  # Required
    version=version,  # Required
    description=description,  # Optional
    long_description=long_description,  # Optional
    long_description_content_type='text/markdown',  # Optional 
    url='https://gitlab.com/chordata/notochord-module',  # Optional
    author='Bruno Laurencich',  # Optional
    author_email='bruno@chordata.cc',  # Optional
    # ext_modules= get_compiled_extensions(),
    classifiers=[  # Optional
        'Development Status :: 5 - Production/Stable',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Programming Language :: Python :: 3.7',
        'Operating System :: POSIX :: Linux'
    ],
    keywords='motion capture, mocap, sensors',  # Optional

    packages=find_packages(),  # Required
    include_package_data=True, #to include the compiled .so
    
    python_requires='>=3.7',
    platform=['Operating System :: POSIX :: Linux'],

    entry_points={  # Optional
        'console_scripts': [
            'notochord=notochord.__main__:main',
        ],
    },

    project_urls={  # Optional
        'Source': 'https://gitlab.com/chordata/notochord-module',
        'Issue tracker': 'https://gitlab.com/chordata/notochord-module/-/issues',
        'Project page': 'https://chordata.cc',
        'Documents': 'https://docs.chordata.cc',
        'Forum': 'https://forum.chordata.cc',
    },
)
