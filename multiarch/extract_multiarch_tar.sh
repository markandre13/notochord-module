#!/bin/bash
set -eu

AARCH64=aarch64-linux-gnu 
TAR_AARCH64=multiarch_files_$AARCH64.tar

ARMHF=arm-linux-gnueabihf
TAR_ARMHF=multiarch_files_$ARMHF.tar

if [ ! $# -eq 1 ]; then
	echo Must provide architecture name. Aborting.
	exit 1
fi

if [ ! -f $(dirname "$0")/$TAR_AARCH64 ] && [ ! -f $(dirname "$0")/$TAR_ARMHF ]; then
    echo No $TAR_AARCH64 nor $TAR_ARMHF found in 'multiarch' folder. Aborting
    exit 1
fi

echo Creating and cleaning multiarch include directory at: $(dirname "$0")/inc 
mkdir -p $(dirname "$0")/inc
rm -rf $(dirname "$0")/inc/*

if [ $1 == "arm64" ] && [ -f $(dirname "$0")/$TAR_AARCH64 ]; then
	echo unpacking compilation files from: $(dirname "$0")/$TAR_AARCH64
	tar xvf $(dirname "$0")/$TAR_AARCH64 -C $(dirname "$0")/inc
fi

if [ $1 == "armhf" ] && [ -f $(dirname "$0")/$TAR_ARMHF ]; then
	echo unpacking compilation files from: $(dirname "$0")/$TAR_ARMHF
	tar xvf $(dirname "$0")/$TAR_ARMHF -C $(dirname "$0")/inc
fi