
/*--------- FOR FUTURE IMPLEMENTATIONS --------*/
// This output format will only work for Eigen::Vector3f
// If the need of transmitting Eigen::Matrix3f arises a template 
// specialization should be done within the class definition using 
// something like the example below from:
// https://stackoverflow.com/questions/22384901/template-specialization-within-class-definition

// ------- init example--------
// template <bool C, typename T = void>
// using only_if = typename std::enable_if <C, T>::type;

// template <typename A, typename B>
// using eq = typename std::is_same <A, B>::type;

// template <class V> class A {
// public:
//     template <typename D = int, only_if <!eq <V, bool>{}, D> = 0>
//     A() { std::cout<<"Generic"<<std::endl; };

//     template <typename D = int, only_if <eq <V, bool>{}, D> = 0>
//     A() { std::cout<<"bool"<<std::endl; }
// };
// ------- end example -------

// for template arguments:
// Eigen::Matrix<float, 3, 1>
// Eigen::Matrix<float, 3, 3>


#define VEC_FMT_BASE( P ) "{}, {:" P "f}, {:" P "f}, {:" P "f}"
#define VEC_FMT VEC_FMT_BASE(MATH_FMT_NUM_PRECISION)

void format_to(const std::string& head, spdlog::memory_buf_t& buf) const{
	fmt::format_to(buf, VEC_FMT, head,
	this->operator[](0), this->operator[](1), this->operator[](2));
}

inline void set_value(Scalar value, Index rowId, Index colId){
	this->operator()(rowId, colId) = value;
}

inline void set_value(Scalar value, Index id){
	this->operator()(id) = value;
}