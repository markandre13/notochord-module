

#define QUAT_FMT_BASE( P ) "{}, {:" P "f}, {:" P "f}, {:" P "f}, {:" P "f}"
#define QUAT_FMT QUAT_FMT_BASE(MATH_FMT_NUM_PRECISION)

void format_to(const std::string& head, spdlog::memory_buf_t& buf) const{
	fmt::format_to(buf, QUAT_FMT, head, 
	this->w(), this->x(), this->y(), this->z());
}

inline float operator[](int i){
	const auto vec_repr = this->coeffs();
	return vec_repr[ (i + 4 - 1)%4 ]; //coeffs returns a vector representation in the form (x,y,z,w)
}

inline void set_value(Scalar value, int i){
	// See this project's test/cpp_scratchpad/test_eigen.cpp
	// for some notes about Eigen::Quaternionf::Coefficients
	Coefficients& vec_repr = this->coeffs();
	vec_repr( ((i + 4 - 1)%4), 0 ) = value; 
}