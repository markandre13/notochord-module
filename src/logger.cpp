// -- START OF COPYRIGHT & LICENSE NOTICES -- 
//
// Chordata Motion's Notochord (python module)
// -- Core implementation of Chordata Motion's Open Source motion capture framework
//
// http://chordata.cc
// contact@chordata.cc
//
//
// Copyright 2018-2021 Bruno Laurencich
// Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
//
// This file is part of Chordata Motion's Notochord (python module).
//
// Chordata Motion's Notochord (python module) is free software: you can 
// redistribute it and/or modify it under the terms of the 
// GNU General Public License as published by the Free Software Foundation, 
// either version 3 of the License, or (at your option) any later version.
//
// Chordata client is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Chordata Motion's Notochord (python module).  
// If not, see <https://www.gnu.org/licenses/>.
//
//
// -- END OF COPYRIGHT & LICENSE NOTICES -- 

#include "logger.h"
#include "osc.h"
#include <memory>

namespace Chordata
{
    void Communicator::Logger::clear_sinks() {
        logger_ref->sinks().clear();
        osc_sink = std::make_shared<Chordata::Communicator::OscOut_Null>();
        ws_sink = std::make_shared<Chordata::Communicator::OscOut_Null>();
        has_log_sinks = false;
        has_osc_sink = false;
        has_ws_sink = false;
    }

    void Communicator::Logger::add_sink(spdlog::sink_ptr new_sink, const Output_Redirect redir) {

        if (redir == Chordata::NONE) return;

        for (auto &sink : logger_ref->sinks()){
            if (sink == new_sink){
                throw std::logic_error("Sink already present in the logger");
            }
        }

        if (is_transmit_logger && redir == Chordata::OSC){
            if(has_ws_sink) throw std::logic_error("Cannot set both UDP and WEBSOCKET at the same time");
            osc_sink = std::dynamic_pointer_cast<Chordata::Communicator::OscOutBase>(new_sink);
            has_osc_sink = true;
        }
        else if(is_transmit_logger && redir == Chordata::WEBSOCKET){
            if(has_osc_sink) throw std::logic_error("Cannot set both UDP and WEBSOCKET at the same time");
            ws_sink = std::dynamic_pointer_cast<Chordata::Communicator::OscOutBase>(new_sink);
            has_ws_sink = true;
        } else {
            logger_ref->sinks().push_back(new_sink);
            has_log_sinks = true;
        }

    }

    std::string Communicator::Logger::get_trasmit_packet_hex() const {
        if (!osc_sink) return std::string("(no osc sink)");
			return osc_sink->get_current_packet_hex();
    }

} // namespace Chordata

