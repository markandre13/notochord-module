// -- START OF COPYRIGHT & LICENSE NOTICES -- 
//
// Chordata Motion's Notochord (python module)
// -- Core implementation of Chordata Motion's Open Source motion capture framework
//
// http://chordata.cc
// contact@chordata.cc
//
//
// Copyright 2018-2021 Bruno Laurencich
// Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
//
// This file is part of Chordata Motion's Notochord (python module).
//
// Chordata Motion's Notochord (python module) is free software: you can 
// redistribute it and/or modify it under the terms of the 
// GNU General Public License as published by the Free Software Foundation, 
// either version 3 of the License, or (at your option) any later version.
//
// Chordata client is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Chordata Motion's Notochord (python module).  
// If not, see <https://www.gnu.org/licenses/>.
//
//
// -- END OF COPYRIGHT & LICENSE NOTICES -- 

#ifndef __NOTOCHORD_UTILS
#define __NOTOCHORD_UTILS

#include "chord_error.h"
#include "io.h"

#include <deque>
#include <cmath>
#include <cstdint>
#include <chrono>

#define CHORDATA_SLEEP(t) std::this_thread::sleep_for(std::chrono::milliseconds(t))


namespace Chordata{

	// This function exists because of conflicts when importing "fmt/chrono.h" in other files.
	void trace_EEPROM_info(uint32_t version, uint32_t t);

	using scatter_line = std::deque<int16_t>;

	struct Line {
	private:	
	  double _slope, _yInt;
	public:
	  double get_y(double x) {
	    return _slope*x + _yInt;
	  }

	  double slope() const{
	  	return _slope;
	  }

	  // Construct line from points
	  bool fit_points(const scatter_line& pts);
	};

	class _Offset_Calibrator; 
	using Offset_Calibrator = _Offset_Calibrator; //avoid name clash in cython

	class _Offset_Calibrator {
		uint8_t max_readings;
		uint8_t rounds;
		float max_slope;
		scatter_line bias_readings[3];
	
	public:
		uint16_t samples;
		int16_t result[3]; 

		_Offset_Calibrator(float _max_slope):
		max_readings(30),
		rounds(4),//The first readings tend to be wrong, so leave them behind
		max_slope(_max_slope),
		samples(0)
		{}

		float add_reading(int16_t x, int16_t y, int16_t z);
	};

	using time_point = std::chrono::time_point<std::chrono::high_resolution_clock>;
	using clock = std::chrono::high_resolution_clock;
	using micros = std::chrono::microseconds;
	using millis = std::chrono::milliseconds;
	using seconds = std::chrono::seconds;

    // using thread_sleep = std::this_thread::sleep_for;

	using micro_duration = std::chrono::duration<long, std::ratio<1, 1000000>> ;

	class _Timekeeper{
	public:
		// typedef std::unordered_map<std::string, time_point> time_table;	
		
		explicit _Timekeeper(const std::string& _label);

		void reset(){ 
			lap = clock::now();
			
		}

		void reset_start(){
			start = clock::now();	// WARNING: Check if this is correct! ()
		}

		const std::string& get_label() const {
			return label;
		}
		
		// inline void initBetaCounter(uint16_t millis) {
		// 	counter_beta = clock::now();
		// 	beta_millis = millis;
		// }

		// float getBetaCounter() const{
		// 	return std::chrono::duration_cast<millis>(clock::now() - counter_beta ).count() / 
		// 			beta_millis;
		// }


		// inline uint64_t lap_micros() const{
		// 	return std::chrono::duration_cast<Chordata::micros>(clock::now() - lap ).count();
		// }

		inline uint64_t highres_now() const{
			return std::chrono::duration_cast<Chordata::micros>(clock::now().time_since_epoch()).count();
		}

		inline uint64_t micros() const{
			return std::chrono::duration_cast<Chordata::micros>(clock::now() - lap ).count();
		}

		inline uint64_t millis() const{
			return std::chrono::duration_cast<Chordata::millis>(clock::now() - lap ).count();
		}

		inline double seconds() const{
			uint64_t count = std::chrono::duration_cast<Chordata::millis>(clock::now() - lap).count();
			return (double) count / 1000.0 ;
		}

		inline uint64_t total_micros() const{
			return std::chrono::duration_cast<Chordata::micros>(clock::now() - start ).count();
		}

		inline uint64_t total_millis() const{
			return std::chrono::duration_cast<Chordata::millis>(clock::now() - start ).count();
		}

		inline double total_seconds() const{
			uint64_t count = std::chrono::duration_cast<Chordata::millis>(clock::now() - start).count();
			return (double)count / 1000.0;
		}

		time_point get_start() const {return start;};

		time_point get_lap_start() const {return lap;};

		inline const std::time_t initial_timestamp() const{
			return std::chrono::system_clock::to_time_t(start);
		}

		inline const char *initial_time() const{
			const std::time_t time = std::chrono::system_clock::to_time_t(start);
			return std::ctime(&time);
		}

		inline const char *now() const{
			const std::time_t time = std::chrono::system_clock::to_time_t(clock::now());
			return std::ctime(&time);
		}

		static const char* time_str(const std::time_t& time){
			return std::ctime(&time);	
		}		

	private:
		time_point start;
		time_point lap;
		std::string label;
		// time_table last_transmisions;
		// time_point counter_beta;
		// float beta_millis;


	}; using Timekeeper = _Timekeeper;

	#define EEPROM_RW_SLEEP_MILLIS 4
	#define EEPROM_RW_SLEEP CHORDATA_SLEEP(EEPROM_RW_SLEEP_MILLIS)
	// #define EEPROM_RW_SLEEP (void)0

	namespace EEPROM {
		using Matrix33 = std::array<std::array<float, 3>, 3>;
		using Matrix9 = std::array<float, 9>;
		using Vector3 = std::array<int16_t, 3>;

		void write_calib(I2C_io* io, uint16_t address, Vector3 vals, const int offset = 0);

		void write_calib(I2C_io* io, uint16_t address, Matrix9 vals, const int offset = 0);

		void write_calib(I2C_io* io, uint16_t address, Matrix33 vals, const int offset = 0);

		Vector3 read_calib(I2C_io* io, uint16_t address, const int offset = 0);

		Matrix9 read_matrix(I2C_io* io, uint16_t address, int offset);

		void write_info(I2C_io* io, uint16_t address, uint32_t val, const int offset = 0);

		uint32_t read_info(I2C_io* io, uint16_t address,  const int offset = 0);

	};

	void throw_for_test(const char* msg);	//This function is to be used on the test suite only

}


#endif