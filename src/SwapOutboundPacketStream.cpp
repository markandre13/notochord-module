#include "SwapOutboundPacketStream.h"

namespace osc{
    SwapOutboundPacketStream::SwapOutboundPacketStream( char *buffer, std::size_t capacity ):
        OutboundPacketStream( buffer, capacity )
    {}

    SwapOutboundPacketStream::~SwapOutboundPacketStream(){}
    
    SwapOutboundPacketStream& SwapOutboundPacketStream::Swap( SwapOutboundPacketStream& rhs ){
        // swap the packet data
        std::swap( data_, rhs.data_ );
        std::swap( end_, rhs.end_ );

        // swap the type tags
        std::swap( typeTagsCurrent_, rhs.typeTagsCurrent_ );
        std::swap( messageCursor_, rhs.messageCursor_ );
        std::swap( argumentCurrent_, rhs.argumentCurrent_ );

        return *this;
    }
}