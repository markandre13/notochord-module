#ifndef __NOTOCHORD_CALIBRATOR__
#define __NOTOCHORD_CALIBRATOR__

#include <string>
#include <vector>
#include "math_types.h"

namespace Chordata{
    class _KCeptor_Base;
    
    /*----------  SENSOR PAYLOAD  ----------*/
	struct _Sensor_Payload{
        _Sensor_Payload() {};
        _Sensor_Payload(uint64_t timestamp, std::string label, Quaternion q,
                        Vector3i gyro, Vector3i accel, Vector3i mag);
		uint64_t timestamp;
		std::string label;
		Quaternion q;
		Vector3i gyro, accel, mag;		
	}; using Sensor_Payload = _Sensor_Payload;

	enum _Calibration_Status {
        CALIB_IDLE  = 0,
		CALIB_WAIT  = 1,
		STATIC      = 2,
		ARMS        = 3,
		TRUNK       = 4,
		LEFT_LEG    = 5,
		RIGHT_LEG   = 6
	}; using Calibration_Status = _Calibration_Status;

    class Calibration_Manager{
    private:
        std::vector<Sensor_Payload> buffer;
        std::unordered_map<std::string, uint64_t> indexes;
        Calibration_Status status, new_status;
        bool status_changed;
        int cycle;

        void set_i_index(Calibration_Status status, int index);
        void set_s_index(Calibration_Status status, int index);
    public:
        Calibration_Manager();
        ~Calibration_Manager(); 

        void set_status(int i_status);
        void handle_status();

        void add_payload(uint64_t timestamp, _KCeptor_Base* node, Quaternion q);

        const std::vector<Sensor_Payload>& get_buffer();
        const std::unordered_map<std::string, uint64_t>& get_indexes();

        void clear_buffer();

        int get_cycle() { return cycle; };
    };
};

#endif