// -- START OF COPYRIGHT & LICENSE NOTICES -- 
//
// Chordata Motion's Notochord (python module)
// -- Core implementation of Chordata Motion's Open Source motion capture framework
//
// http://chordata.cc
// contact@chordata.cc
//
//
// Copyright 2018-2021 Bruno Laurencich
// Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
//
// This file is part of Chordata Motion's Notochord (python module).
//
// Chordata Motion's Notochord (python module) is free software: you can 
// redistribute it and/or modify it under the terms of the 
// GNU General Public License as published by the Free Software Foundation, 
// either version 3 of the License, or (at your option) any later version.
//
// Chordata client is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Chordata Motion's Notochord (python module).  
// If not, see <https://www.gnu.org/licenses/>.
//
//
// -- END OF COPYRIGHT & LICENSE NOTICES -- 

#ifndef __CHORDATA_KALMAN_BASE__
#define __CHORDATA_KALMAN_BASE__ 


#include "def.h"
#include "math_types.h"
// #include "Chordata_communicator.h"


namespace FSsf{
	extern "C" {
	  	#include "FS_sensor_fusion/build.h"
		#include "FS_sensor_fusion/types.h"
		#include "FS_sensor_fusion/fusion.h"
		#include "FS_sensor_fusion/magnetic.h"
		#include "FS_sensor_fusion/orientation.h"

	}

	namespace CoordSystem{
		const FSsf::int16 _NED = NED;
		const FSsf::int16 _ANDROID = ANDROID;
		const FSsf::int16 _WIN8 = WIN8;
	};
}

namespace Chordata{

	struct SensorResolution{
		float gRes; //in DPS/tick 
		float aRes; //in G/tick
		float mRes;	//in uT/tick
		
		void convert_GAUSS_to_uT(){
			mRes = 100*mRes;
		}
	};


	class Fusion_Kalman_Base {
	protected:
		FSsf::AccelSensor thisAccel;
		FSsf::GyroSensor thisGyro;
		FSsf::MagSensor thisMag;
		FSsf::MagCalibration thisMagCal;
		FSsf::uint8 calibSetFlag;

	private:
		FSsf::SV_9DOF_GBY_KALMAN thisSV;
		FSsf::MagneticBuffer thisMagBuffer;
		FSsf::FilterRate thisFilterRate;

		FSsf::uint32 loopcounter;

		FSsf::int16 skip_mag_calibration;

		FSsf::int8 iCounter;
		static const FSsf::fquaternion rotate_match_coord_sys;

		bool enable_mag_correct;

	public:
		typedef FSsf::int16 xyz_array[3];
		
		Fusion_Kalman_Base(FSsf::uint16, FSsf::uint8, bool);
		virtual ~Fusion_Kalman_Base() {};

		FSsf::uint32 get_loopcounter(){return loopcounter;}	
		FSsf::uint8 get_icounter(){return iCounter;}		

		float get_gyro_res();
		float get_accel_res();
		float get_mag_res();
		
		void set_sensor_res(float, float, float);

		void process_readings();
		bool process_and_run();
		bool run_mag_calib();
		bool set_mag_run_calib(FSsf::int16, FSsf::int16, FSsf::int16);
		void run_filter();

		virtual void set_gyro(FSsf::int16, FSsf::int16, FSsf::int16) = 0;
		virtual void set_accel(FSsf::int16, FSsf::int16, FSsf::int16) = 0;
		virtual void set_mag(FSsf::int16, FSsf::int16, FSsf::int16)	= 0;

		void set_sensors(	FSsf::int16, FSsf::int16, FSsf::int16,
							FSsf::int16, FSsf::int16, FSsf::int16,
							FSsf::int16, FSsf::int16, FSsf::int16);

		virtual void get_mag_offset_for_EEPROM(vector3_ptr magOffset) const = 0;
		virtual void get_mag_matrix_for_EEPROM(float (&magMatrix)[3][3]) const = 0;

		virtual void set_mag_offset_from_EEPROM(const FSsf::int16 *magOffset) = 0;
		virtual void set_mag_matrix_from_EEPROM(const float (&magMatrix)[3][3]) = 0;


		const FSsf::fquaternion *get_quat(){
			static 	FSsf::fquaternion result;
			
			FSsf::qAeqBxC(&result, &rotate_match_coord_sys, &thisSV.fqPl );
			
			return &result;
		}

		// TODO: make these getters const

		FSsf::MagneticBuffer *get_mag_calib_buf() {
			return &thisMagBuffer;
		}

		FSsf::AccelSensor *get_accel_sensor() {
			return &thisAccel;
		}

		FSsf::GyroSensor *get_gyro_sensor() {
			return &thisGyro;
		}

		FSsf::MagSensor *get_mag_sensor() {
			return &thisMag;
		}

		FSsf::MagCalibration *get_mag_calib() {
			return &thisMagCal;
		}

		FSsf::SV_9DOF_GBY_KALMAN *get_sv() {
			return &thisSV;
		}

		const xyz_array *get_gyro_buf() const{
			return thisGyro.iYsBuffer;
		}

		const xyz_array *get_accel_buf() const{
			return thisAccel.iGsBuffer;
		}

		const xyz_array *get_mag_buf() const{
			return thisMag.iBsBuffer;
		}

		float get_a_covariance() const {
			return thisAccel.fQvGQa;
		};

		float get_m_covariance() const {
			return thisMag.fQvBQd;
		}
		// void get_mag_offset_for_EEPROM(std::array<int16_t, 3>&, std::array<std::array<float, 3>, 3>&);
		// void set_mag_cal_from_EEPROM(uint32_t, const float*, const std::array<std::array<float, 3>, 3>&);
		// void set_mag_cal_from_EEPROM_v010( const float*, const std::array<std::array<float, 3>, 3>&);



		// template <typename Node>
		// void transmit_mag_pointcloud(const Node *n){
		// 	int cc = 0;
		// 	static int j, k;

		// 	for (j = 0; j < MAGBUFFSIZEX; j++){
		// 		for (k = 0; k < MAGBUFFSIZEY; k++){
		// 			if (thisMagBuffer.index[j][k] != -1){
		// 				cc++;
		// 				Chordata::Communicator::transmit( n ,
		// 					thisMagBuffer.iBs[0][j][k],
		// 					thisMagBuffer.iBs[1][j][k],
		// 					thisMagBuffer.iBs[2][j][k]);
						

		// 			}
		// 		}
		// 	}
		// 	Chordata::Communicator::debug("Magnetic buffer length: {} ", cc);

		// 	Chordata::Communicator::transmit_pointc(); 
		// }


	};
}



#endif