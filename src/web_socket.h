// -- START OF COPYRIGHT & LICENSE NOTICES --
//
// Chordata Motion's Notochord (python module)
// -- Core implementation of Chordata Motion's Open Source motion capture framework
//
// http://chordata.cc
// contact@chordata.cc
//
//
// Copyright 2022-2023 Joan Prim Armengol
//
// This file is part of Chordata Motion's Notochord (python module).
//
// Chordata Motion's Notochord (python module) is free software: you can
// redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
//
// Chordata client is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Chordata Motion's Notochord (python module).
// If not, see <https://www.gnu.org/licenses/>.
//
//
// -- END OF COPYRIGHT & LICENSE NOTICES --
#ifndef __NOTOCHORD_WEBSOCKET__
#define __NOTOCHORD_WEBSOCKET__


#include <iostream>
#include <string>
#include <unordered_map>
#include "libwebsockets/include/libwebsockets.h"
#include "config.h"
#include "types.h"

namespace Chordata {
	struct vhost_protocol_data {
		lws_ring* ring_buffer;
		struct client_data* clients;
		bool binary;
	};
	struct client_data {
		struct client_data* pss_list;
		lws* wsi;
		uint32_t tail;
	};
	struct msg_data {
		unsigned char* data;
		size_t size;
	};
	

	class WebSocket {
		struct lws_context* context;

		bool interrupt;
		std::thread worker;
		std::vector<std::string> protocol_names;
		std::unordered_map<std::string, struct lws_protocols*> pprotocols;
		std::vector<struct lws_protocols> protocols;
		public:
		WebSocket(const Configuration_Data& conf);

		~WebSocket() {
			this->stop();
			if(worker.joinable()) worker.join();
			lws_context_destroy(context);
			for(auto p : protocols) {
				if(p.user) {
					delete (vhost_protocol_data*)p.user;
				}
			}
		}

		void stop();
		void loop();

		//void send(std::string& msg);
		void send(const char* data, size_t size);
		void send(const char* data, size_t size, const std::string& protocol);
	};
}

#endif
