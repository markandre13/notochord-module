// -- START OF COPYRIGHT & LICENSE NOTICES -- 
//
// Chordata Motion's Notochord (python module)
// -- Core implementation of Chordata Motion's Open Source motion capture framework
//
// http://chordata.cc
// contact@chordata.cc
//
//
// Copyright 2018-2021 Bruno Laurencich
// Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
//
// This file is part of Chordata Motion's Notochord (python module).
//
// Chordata Motion's Notochord (python module) is free software: you can 
// redistribute it and/or modify it under the terms of the 
// GNU General Public License as published by the Free Software Foundation, 
// either version 3 of the License, or (at your option) any later version.
//
// Chordata client is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Chordata Motion's Notochord (python module).  
// If not, see <https://www.gnu.org/licenses/>.
//
//
// -- END OF COPYRIGHT & LICENSE NOTICES -- 

#include "fmt/chrono.h"
#include "communicator.h"
#include "utils.h"
#include "timer.h"

namespace comm = Chordata::Communicator;
// This function exists because of conflicts when importing "fmt/chrono.h" in other files.
void Chordata::trace_EEPROM_info(uint32_t version, uint32_t t) {
	comm::_trace("EEPROM calib info found. Version: {}, {}, {} | Date: {:%d-%m-%Y %H:%M:%S}",
				_GET_MAJOR_FROM_INT(version), _GET_MINOR_FROM_INT(version), _GET_PATCH_FROM_INT(version),
				fmt::localtime(t));
}

bool Chordata::Line::fit_points(const scatter_line& pts) {
	int nPoints = pts.size();
	if( nPoints < 2 ) {
	  // Fail: infinitely many lines passing through this single point
	  return false;
	}
	double sumX=0, sumY=0, sumXY=0, sumX2=0;
	for(int i=0; i<nPoints; i++) {
	  sumX += i;
	  sumY += pts[i];
	  sumXY += pts[i] * i;
	  sumX2 += i * i;
	}
	double xMean = sumX / nPoints;
	double yMean = sumY / nPoints;
	double denominator = sumX2 - sumX * xMean;
	// You can tune the eps (1e-7) below for your specific task
	if( std::fabs(denominator) < 1e-7 ) {
	  // Fail: it seems a vertical line
	  return false;
	}
	_slope = (sumXY - sumX * yMean) / denominator;
	_yInt = yMean - _slope * xMean;
	return true;
}

float Chordata::_Offset_Calibrator::add_reading(int16_t x, int16_t y, int16_t z){
	uint16_t ii, jj;

	Chordata::Line line;

	bias_readings[0].push_back(x); 
	bias_readings[1].push_back(y); 
	bias_readings[2].push_back(z);
	for (ii = 0; ii < 3; ++ii){
		while (bias_readings[ii].size() > max_readings) bias_readings[ii].pop_front(); 
	}

	for (ii = 0; ii < 3; ++ii){
		if (bias_readings[ii].size() < rounds){
			return max_slope + 1; // wait until there's a minimal buffer to fit the line
		}

		line.fit_points(bias_readings[ii]);
		if (std::fabs(line.slope()) > max_slope ) return line.slope(); 
	}

	for (ii = 0; ii < 3; ++ii){
		double accumulator = 0;
		for (jj = 0; jj < bias_readings[ii].size(); jj++){
			accumulator += bias_readings[ii][jj];
		}
		result[ii] = static_cast<int16_t>(std::round(accumulator / bias_readings[ii].size()));
	}

	return 0.0f;
}

void Chordata::throw_for_test(const char* msg){
	//This function is to be used on the test suite only
	throw Chordata::Idiot_Error(msg);
}


// ---------------- EEPROM ------------------

using EE_Vec3 = Chordata::EEPROM::Vector3;
using EE_Mat9 = Chordata::EEPROM::Matrix9;
using EE_Mat33 = Chordata::EEPROM::Matrix33;

void Chordata::EEPROM::write_calib(I2C_io* io, uint16_t address, EE_Vec3 vals, int offset){
	for (int i = 0; i < 6; i+=2)
	{	
		int16_t val = vals[i/2];
		
		int8_t msb = (val & 0xff00) >> 8;
		io->write_byte( address, offset+i+0 , msb  );
		EEPROM_RW_SLEEP;

		int8_t lsb = (val & 0x00ff);
		io->write_byte( address, offset+i+1 , lsb );
		EEPROM_RW_SLEEP;
	}
}


void Chordata::EEPROM::write_calib(I2C_io* io, uint16_t address, EE_Mat9 vals, int offset){
	static_assert((sizeof(float) == 4), "The sizeof float is not 4!\n\
	This might cause problems when storing floating point data on the EEPROM.\n\
	Please report this issue.");
			
	for (int i = 0; i < (9*4); i+=4)
	{	
		
		float fval = vals[i/4];
		uint32_t val = *((int*)&fval);
		
		int8_t byte = 	(val & 0xff000000) >> 24; //MSB
		io->write_byte( address, offset+i+0 , byte  );
		EEPROM_RW_SLEEP;
		byte = 			(val & 0x00ff0000) >> 16;
		io->write_byte( address, offset+i+1 , byte  );
		EEPROM_RW_SLEEP;
		byte = 			(val & 0x0000ff00) >> 8;
		io->write_byte( address, offset+i+2 , byte  );
		EEPROM_RW_SLEEP;
		byte = (val & 0x000000ff); //LSB
		io->write_byte( address, offset+i+3 , byte );
		EEPROM_RW_SLEEP;
	}
}

void Chordata::EEPROM::write_calib(I2C_io* io, uint16_t address, EE_Mat33 vals, int offset){
	EE_Mat9 M;
	for (int i = 0; i < 9; ++i){
		int row = i / 3;
		int column = i % 3;
		M[i] = vals[row][column]; 
	}

	write_calib(io, address, M, offset);
}

EE_Mat9 Chordata::EEPROM::read_matrix(I2C_io* io, uint16_t address, int offset){
	EE_Mat9 ret;

	for (int i = 0; i < (9*4); i+=4){	
		
		uint32_t val = 0x00000000;
		int8_t byte;
		byte = io->read_byte( address, offset+i+0);
		val |= (static_cast<uint32_t>(byte) << 24) & 0xff000000; 
		// EEPROM_RW_SLEEP;

		byte = io->read_byte( address, offset+i+1);
		val |= (static_cast<uint32_t>(byte) << 16) & 0x00ff0000; 
		// EEPROM_RW_SLEEP;

		byte = io->read_byte( address, offset+i+2);
		val |= (static_cast<uint32_t>(byte) << 8) & 0x0000ff00;
		// EEPROM_RW_SLEEP;

		byte = io->read_byte( address, offset+i+3);
		val |= static_cast<uint32_t>(byte) & 0x000000ff;
		// EEPROM_RW_SLEEP;

		ret[i/4] = *((float*)&val);
	}

	return ret;
}

EE_Vec3 Chordata::EEPROM::read_calib(I2C_io* io, uint16_t address, int offset){
	EE_Vec3 results;
	for (int i = 0; i < 6; i+=2)
	{
		int8_t msb = io->read_byte( address, offset+i+0 ) ;
		// EEPROM_RW_SLEEP;

		int8_t lsb =  io->read_byte( address, offset+i+1 )  ;
		// EEPROM_RW_SLEEP;

		results[i/2] = (msb << 8) | (lsb & 0x00ff);

		// printf("SLOT %d [ MSB:%2x LSB:%2x - RESULT:%4x / %d ]\n", i , msb, lsb, results[i/2], results[i/2]);
	}

	return results;
}

void Chordata::EEPROM::write_info(I2C_io* io, uint16_t address, uint32_t val, const int offset){
	static const std::size_t N = 4;
	uint8_t chunks[N] = {
		(uint8_t)((val & 0xff000000UL) >> 24),
    	(uint8_t)((val & 0x00ff0000UL) >> 16),
    	(uint8_t)((val & 0x0000ff00UL) >>  8),
    	(uint8_t) (val & 0x000000ffUL)    
    };

    for (std::size_t i = 0; i < N; ++i){
    	io->write_byte( address, offset+i , chunks[i]  );
    	EEPROM_RW_SLEEP;
    }
}

uint32_t Chordata::EEPROM::read_info(I2C_io* io, uint16_t address, const int offset){
	static const std::size_t N = 4;
	uint8_t chunks[N]; 

    for (std::size_t i = 0; i < N; ++i){
    	chunks[i] = io->read_byte( address, offset+i );
    	// EEPROM_RW_SLEEP;
    }

    uint32_t result = 0x00000000UL;
    result |= ( chunks[0] << 24 ) 	& 0xff000000UL;
    result |= ( chunks[1] << 16 ) 	& 0x00ff0000UL;
    result |= ( chunks[2] << 8 ) 	& 0x0000ff00UL;
    result |=   chunks[3] 			& 0x000000ffUL;

    return result;
}
