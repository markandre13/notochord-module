// -- START OF COPYRIGHT & LICENSE NOTICES -- 
//
// Chordata Motion's Notochord (python module)
// -- Core implementation of Chordata Motion's Open Source motion capture framework
//
// http://chordata.cc
// contact@chordata.cc
//
//
// Copyright 2018-2021 Bruno Laurencich
// Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
//
// This file is part of Chordata Motion's Notochord (python module).
//
// Chordata Motion's Notochord (python module) is free software: you can 
// redistribute it and/or modify it under the terms of the 
// GNU General Public License as published by the Free Software Foundation, 
// either version 3 of the License, or (at your option) any later version.
//
// Chordata client is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Chordata Motion's Notochord (python module).  
// If not, see <https://www.gnu.org/licenses/>.
//
//
// -- END OF COPYRIGHT & LICENSE NOTICES -- 

#ifndef __MOCK_NOTOCHORD_NODE__
#define __MOCK_NOTOCHORD_NODE__

#include <string>
#include "phy_nodes.h"

namespace Chordata {	

// ====================================================== //
// ======================= Branch ======================= //
// ====================================================== //
	class _Mock_Mux;
	using Mock_Mux = _Mock_Mux;
	using mock_mux_ptr = std::shared_ptr<_Mock_Mux>;

	class _Mock_Branch: public _Branch {
		friend Mock_Mux;
		_Mock_Mux *thisMux;

		void switch_branch() override;
		
		_Mock_Branch(mock_mux_ptr _mux,
					 uint8_t _address, std::string _label,
					 io_ptr _io, Mux_Channel _channel
					);
	public:
		~_Mock_Branch(){}

		void bang() override;

	}; using Mock_Branch = _Mock_Branch;

	using mock_branch_ptr = std::shared_ptr<_Mock_Branch>;

// ====================================================== //
// ========================= Mux ======================== //
// ====================================================== //

	class _Mock_Mux: public _Mux {
	private:
		friend _Mock_Branch;
		_Mock_Branch off_branch;
		
		_Mock_Mux(link_ptr _parent,
				uint8_t _address, std::string _label,
				io_ptr _io);
	
	public:
		~_Mock_Mux(){}

		static link_ptr Create(link_ptr _parent,
									uint8_t _address, std::string _label,
									io_ptr _i2c );

	};


// ====================================================== //
// ======================= KCeptor ====================== //
// ====================================================== //

	class _Mock_KCeptor: public _KCeptor {

		_Mock_KCeptor(link_ptr _parent,
					uint8_t _address, std::string _label,
					io_ptr _io);

	public:
		std::vector<float> debug_buffer;

		static link_ptr Create(link_ptr _parent,
								uint8_t _address, std::string _label,
								io_ptr _io );

		~_Mock_KCeptor();

		void bang() override;
		bool setup() override;
		void apply_mag_calib(int16_t *a, int16_t x, int16_t y, int16_t z);
		void set_EEPROM_attributes(uint32_t version, uint32_t timestamp);

		/*-------------- Getters --------------*/
        float _get_hz();
		uint8_t _get_accel_scale() { return imu.settings.accel.scale; };
		uint16_t _get_gyro_scale() { return imu.settings.gyro.scale; };
		uint8_t _get_mag_scale()   { return imu.settings.mag.scale; };

		/*-------------- Setters --------------*/

	private:
		uint32_t EEPROM_version;
		std::time_t EEPROM_timestamp;
	}; 	using Mock_KCeptor = _Mock_KCeptor; //avoid name clash in cython

	using mock_kceptor_ptr = std::shared_ptr<Chordata::_Mock_KCeptor>;
}
#endif
