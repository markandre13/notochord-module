// -- START OF COPYRIGHT & LICENSE NOTICES -- 
//
// Chordata Motion's Notochord (python module)
// -- Core implementation of Chordata Motion's Open Source motion capture framework
//
// http://chordata.cc
// contact@chordata.cc
//
//
// Copyright 2018-2021 Bruno Laurencich
// Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
//
// This file is part of Chordata Motion's Notochord (python module).
//
// Chordata Motion's Notochord (python module) is free software: you can 
// redistribute it and/or modify it under the terms of the 
// GNU General Public License as published by the Free Software Foundation, 
// either version 3 of the License, or (at your option) any later version.
//
// Chordata client is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Chordata Motion's Notochord (python module).  
// If not, see <https://www.gnu.org/licenses/>.
//
//
// -- END OF COPYRIGHT & LICENSE NOTICES -- 

#include "def.h"
#include "io.h"
#include "chord_error.h"

#include "i2c-dev.h" 

#include <sys/ioctl.h> 
#include <fcntl.h>
#include <cstdint>

#ifdef NOTOCHORD_TRACE_IO
#include "communicator.h"
namespace comm = Chordata::Communicator;
#define TRACE_READ(addr, ssbaddr, data, num, total)\
	comm::_trace("<< i2c R << a:{:#04x}, s:{:#04x}, d:{:#04x} | n:{}/{}", \
								addr, ssbaddr, data, num, total)
#define TRACE_WRITE(addr, ssbaddr, data)\
	comm::_trace(">> i2c W >> a:{:#04x}, s:{:#04x}, d:{:#04x}",\
								addr, ssbaddr, data)
#else
#define TRACE_READ(addr, ssbaddr, data, num, total)
#define TRACE_WRITE(addr, ssbaddr, data)
#endif
 
using namespace std;
using namespace Chordata;
using Chordata::IO_Error;

I2C_Device::I2C_Device(const string& adapter):
	i2c_adapter(adapter)
	{	
		if (!init())
			throw IO_Error("The i2c adapter is not available on this system");
	};

bool I2C_Device::init(){
	if ( ( i2c_file = open(i2c_adapter.c_str(), O_RDWR) ) < 0)
		return false;

	return true;
}

// ====================================================== //
// =================== i2c Read/Write =================== //
// ====================================================== //

std::vector<char> I2C_io::i2c_rdwr(uint8_t address, char sub_address, uint8_t count){
		char command[] = { sub_address };
		std::vector<char> dest;
		dest.resize(count);

		struct i2c_msg messages[] = {
			{ address, 0, sizeof(command), command },
			{ address, I2C_M_RD, count, dest.data() },
		};

		struct i2c_rdwr_ioctl_data ioctl_data = { messages, 2 };
		int result = ioctl(get_adapter_i2c_file(), I2C_RDWR, &ioctl_data);
		if (result != 2){
			throw IO_Error("Cannot perform i2c RDWR to slave device");
		}
		return dest;

	}

void I2C_io::i2c_write_byte(uint8_t address, uint8_t data){
	if (ioctl(get_adapter_i2c_file(), I2C_SLAVE, address) < 0){
		throw IO_Error("Slave device not found");
	} 

	if ( i2c_smbus_write_byte(get_adapter_i2c_file(), data) < 0){
		throw IO_Error("Cannot write to slave device");
	} 
	TRACE_WRITE(address, 0, data);
}

void I2C_io::i2c_write_byte(uint8_t address, uint8_t sub_address, uint8_t data)
{	
	if (ioctl(get_adapter_i2c_file(), I2C_SLAVE, address) < 0){
		throw IO_Error("Slave device not found");
	} 

	if ( i2c_smbus_write_byte_data(get_adapter_i2c_file(), sub_address, data) < 0){
		throw IO_Error("Cannot set subAdress to slave device");
	} 
	TRACE_WRITE(address, sub_address, data);
 }

void I2C_io::i2c_write_bytes(uint8_t address, char *data, uint8_t count){	
    struct i2c_msg messages[] = {
      { address, 0, count, data },
    };

    struct i2c_rdwr_ioctl_data ioctl_data = { messages, 1 };
    int result = ioctl(get_adapter_i2c_file(), I2C_RDWR, &ioctl_data);
    if (result != 1){
		std::cout << result << std::endl;
		throw IO_Error("Cannot perform i2c muy byte WRITE to slave device");
    }
}

 int32_t I2C_io::i2c_read_byte(uint8_t address)
{	
	if (ioctl(I2C_io::get_adapter_i2c_file(), I2C_SLAVE, address) < 0){
		throw IO_Error("Slave device not found");
	}

	int32_t data; // `data` will store the register data	 
	if ( ( data = i2c_smbus_read_byte(get_adapter_i2c_file()) ) < 0 ){
		throw IO_Error("Can't read from the slave device");
	}

	TRACE_READ(address, 0, data, 1, 1);
	return data;          // Return data read from slave register
}

int32_t I2C_io::i2c_read_byte(uint8_t address, uint8_t sub_address)
{	

	if (ioctl(I2C_io::get_adapter_i2c_file(), I2C_SLAVE, address) < 0){
		throw IO_Error("Slave device not found");
	}

	int32_t data; // `data` will store the register data	 
	if ( ( data = i2c_smbus_read_byte_data(get_adapter_i2c_file(), sub_address) ) < 0 ){
		throw IO_Error("Can't read from the slave device");
	}

	TRACE_READ(address, sub_address, data, 1, 1);
	return data;          // Return data read from slave register
}

void I2C_io::i2c_read_bytes(uint8_t address, uint8_t sub_address, uint8_t * dest, uint8_t count)
{  	
	if (ioctl(get_adapter_i2c_file(), I2C_SLAVE, address) < 0){
		throw IO_Error("Slave device not found");
	}

	// i2c_smbus_read_i2c_block_data(int file, __u8 command, __u8 length, __u8 *values)
	if ( i2c_smbus_read_i2c_block_data(get_adapter_i2c_file(), sub_address, count, dest) < 0 ){
		throw IO_Error("Can't read from the slave device");
	}
#ifdef NOTOCHORD_TRACE_IO
	for (size_t i = 0; i < count; i++){	TRACE_READ(address, sub_address, dest[i], i, count);}
#endif
}

// ====================================================== //
// ====================== MOCK i2c ====================== //
// ====================================================== //

void Mock_I2C_io::write_byte(uint8_t address, uint8_t data){
	writes.push_back({address, 0, {data}});
}

void Mock_I2C_io::write_byte(uint8_t address, uint8_t sub_address, uint8_t data){
	writes.push_back({address, sub_address, {data}});

}

int32_t Mock_I2C_io::read_byte(uint8_t address){
	reads.push_back({address, 0, 1});
	return 0;
	};

int32_t Mock_I2C_io::read_byte(uint8_t address, uint8_t sub_address)  {
	reads.push_back({address, sub_address, 1});
	return 0;
	};

void Mock_I2C_io::read_bytes(uint8_t address, uint8_t sub_address, uint8_t * dest, uint8_t count)   {
	for (size_t i = 0; i < count; i++){
		dest[i] = 0;
	}
	reads.push_back({address, sub_address, count});
}

std::vector<uint8_t> Mock_I2C_io::read_bytes(uint8_t address, uint8_t sub_address, uint8_t count)  {
	std::vector<uint8_t> dest;
	for (size_t i = 0; i < count; i++){
		dest.push_back(0);
	}
	reads.push_back({address, sub_address, count});
	return dest;
}

std::vector<char> Mock_I2C_io::i2c_rdwr(uint8_t address, char sub_address, uint8_t count){
	std::vector<char> dest;
	for (size_t i = 0; i < count; i++){
		dest.push_back(0);
	}
	reads.push_back({address, (uint8_t)sub_address, count});
	return dest;
}

void Mock_I2C_io::reset(){
	writes.clear();
	reads.clear();
}