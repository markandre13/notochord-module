// -- START OF COPYRIGHT & LICENSE NOTICES -- 
//
// Chordata Motion's Notochord (python module)
// -- Core implementation of Chordata Motion's Open Source motion capture framework
//
// http://chordata.cc
// contact@chordata.cc
//
//
// Copyright 2018-2021 Bruno Laurencich
// Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
//
// This file is part of Chordata Motion's Notochord (python module).
//
// Chordata Motion's Notochord (python module) is free software: you can 
// redistribute it and/or modify it under the terms of the 
// GNU General Public License as published by the Free Software Foundation, 
// either version 3 of the License, or (at your option) any later version.
//
// Chordata client is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Chordata Motion's Notochord (python module).  
// If not, see <https://www.gnu.org/licenses/>.
//
//
// -- END OF COPYRIGHT & LICENSE NOTICES -- 

#include "core.h"
#include "logger.h"
#include "communicator.h"
#include "chord_error.h"
#include "timer.h"
#include "io.h"
#include "fmt/core.h"
#include <mutex>
#include <iostream>
#include <memory>
#include <thread>
#include "fusion_worker.h"

#include "osc.h"
#include <cstdint>

using namespace Chordata;

// init static members
Notochord_Runtime* Notochord_Runtime::_instance{nullptr};
std::mutex Notochord_Runtime::mutex_;
Calibration_Manager Notochord_Runtime::calib_manager;

void w_thread_run(Chordata::Notochord_Worker* w){
	//std::cout << "THREAD run " << std::this_thread::get_id() << std::endl;
	
	w->handler();
}

Notochord_Runtime::Notochord_Runtime():
	_comm_instance(new Communicator_Runtime()),
	_i2c_instance(nullptr),
	global_timekeeper(nullptr),
	th_f_worker(std::thread(w_thread_run, &fusion_worker)),
	th_a_worker(std::thread(w_thread_run, &armature_worker)),
	pending_kc(0)
	{
	}

Notochord_Runtime::~Notochord_Runtime()
	{
		//std::cout << "RUNTIME DSTCTOR, STOPPING" << std::endl;
		
		fusion_worker.stop();
		armature_worker.stop();

		//std::cout << "JOINING THREAD" << std::endl;

		if (th_f_worker.joinable())
			th_f_worker.join();

		if (th_a_worker.joinable())
			th_a_worker.join();
	}

Notochord_Runtime *
Notochord_Runtime::init(){
	if (_instance == nullptr){
        std::lock_guard<std::mutex> lock(mutex_);
        if (_instance == nullptr){
            _instance = new Notochord_Runtime();
			_instance->get_timekeeper();	// Initialize timekeeper to start counting from start
		}
    }
    return _instance;
}

bool Notochord_Runtime::is_active(){
	return (_instance != nullptr);
}

Notochord_Status Notochord_Runtime::status(){
	if (is_active()) {
		if (_instance->timer != nullptr && _instance->timer->is_running()) {
			return Notochord_Status::RUNNING;
		} else {
			return Notochord_Status::STOPPED;
		}
	}

	return Notochord_Status::IDLE;
}

const Chordata::Configuration_Data *Notochord_Runtime::get_config(){
	return &(Notochord_Runtime::init()->config);
}

Notochord_Runtime *
Notochord_Runtime::terminate(bool terminate_communicator){
	if(terminate_communicator) {
		Communicator_Runtime::terminate();
	}
	delete _instance;
	_instance = nullptr;
	return nullptr;	
}

void Notochord_Runtime::setup(const Chordata::Configuration_Data *_config, bool reconfigure_communicator=true){
	// Configure Conmunicator
	if(reconfigure_communicator)
		_comm_instance->configure(*(_config));
	// Configure i2c
	if ( _config->comm.adapter.length() == 0 ||  _config->comm.adapter.compare("/dev/null") == 0){
		_i2c_instance = io_ptr(new Mock_I2C_io(_config->comm.adapter));
	} else {
		_i2c_instance = io_ptr(new I2C_io(_config->comm.adapter));
	}

	config = *_config; 
}

// void Notochord_Runtime::configure_communicator_runtime(const Chordata::Configuration_Data *_config){
// 	_comm_instance->configure(*(_config));
// }

logger_ptr Notochord_Runtime::get_logger(Logger_type r){
	if (r == TRA_LOG) return _comm_instance->t_logger;
	if (r == MSG_LOG) return _comm_instance->msg_logger;
	if (r == ERR_LOG) return _comm_instance->err_logger;
	return _comm_instance->t_logger; // this statment is here to avoid complaiments from the compiler
}

const std::string *Notochord_Runtime::get_current_log_file() const{
	return &(_comm_instance->current_file_path);
}

void Notochord_Runtime::flush_loggers(){
	_comm_instance->flush_loggers();	
}

Cython_Buffer Notochord_Runtime::_get_buffer(){
	return _comm_instance->_get_buffer();
}

std::vector<std::string>& Notochord_Runtime::_get_registry(uint32_t level){
	return _comm_instance->_get_registry(level);
}

void Notochord_Runtime::_clear_registry(uint32_t level){
	return _comm_instance->_clear_registry(level);
}
// void Notochord_Runtime::configure_i2c(const Chordata::Configuration_Data* _config){
// 	if ( _config->comm.adapter.length() == 0 ||  _config->comm.adapter.compare("/dev/null") == 0){
// 		_i2c_instance = io_ptr(new Mock_I2C_io(_config->comm.adapter));
// 	} else {
// 		_i2c_instance = io_ptr(new I2C_io(_config->comm.adapter));
// 	}
// }

io_ptr Notochord_Runtime::get_i2c(){
	if (!_i2c_instance) throw IO_Error("The i2c adapter should be configured before getting it from the Notochord_Runtime");
	return _i2c_instance;
}

void Notochord_Runtime::add_index_timekeeper(timek_ptr timek){
	
	auto search = timekeeper_index.find(timek->get_label());
	if (search != timekeeper_index.end()){
		throw Notochord_Error(fmt::format("A timekeeper with label {} was already created", timek->get_label()));
	}
	timekeeper_index[timek->get_label()] = timek;

	if (timek->get_label() == "global"){
		global_timekeeper = timek;
	}
}

timek_ptr Notochord_Runtime::get_timekeeper(const std::string &label){
	try	{
		auto tk = timekeeper_index.at(label);
		return tk;
	} catch(const std::exception& e){
		return nullptr;
	}
}

timek_ptr Notochord_Runtime::get_timekeeper()
{
  if (!global_timekeeper){
    add_index_timekeeper(std::make_shared<_Timekeeper>("global"));
  }
  return global_timekeeper;
}

bool Notochord_Runtime::start(scheduler_ptr scheduler) {

	if (!_comm_instance->configured) {
		throw Notochord_Error("Notochord module has not been configured yet!");
	}

	timer = std::make_unique<_Timer>(scheduler);
	return timer->start();
}

bool Notochord_Runtime::end() {
	if (timer != nullptr) {
		timer->terminate();
		return true;
	}
	return false;
}

void Notochord_Runtime::handlers(){
	timer->handlers();
}

std::string read_user_input() {
    std::string line;
    std::getline(std::cin, line);
    std::cout << "User Input: " << line << std::endl;

    return line;
}

bool Notochord_Runtime::get_user_input() {
	if (!future.valid()) {
		future = std::async(std::launch::async, read_user_input);
	}

	return (future.wait_for(std::chrono::seconds(0)) == std::future_status::ready);
}

void Notochord_Runtime::set_armature(armature_ptr _armature){
	armature = _armature;
}

uint64_t Chordata::runtime_millis(){ return get_runtime()->get_timekeeper()->total_millis();};


void Chordata::_print_int_info(){
	std::cout << "Size of char: " << sizeof(char) << " byte" << std::endl;
  std::cout << "Size of int: " << sizeof(int) << " bytes" << std::endl;
  std::cout << "Size of float: " << sizeof(float) << " bytes" << std::endl;
  std::cout << "Size of double: " << sizeof(double) << " bytes" << std::endl;

  std::cout << "-----------------------------------------------" << std::endl;

  std::cout << "Size of int16_t: " << sizeof(int16_t) << " bytes" << std::endl;
  std::cout << "Size of uint16_t: " << sizeof(uint16_t) << " bytes" << std::endl;
  std::cout << "-----------------------------------------------" << std::endl;

  std::cout << "Size of int32_t: " << sizeof(int32_t) << " bytes" << std::endl;
  std::cout << "Size of uint32_t: " << sizeof(uint32_t) << " bytes" << std::endl;

  std::cout << "Size of osc::int32: " << sizeof(osc::int32) << " bytes" << std::endl;
  std::cout << "Size of osc::uint32: " << sizeof(osc::uint32) << " bytes" << std::endl;
  std::cout << "-----------------------------------------------" << std::endl;

  std::cout << "Size of int64_t: " << sizeof(int64_t) << " bytes" << std::endl;
  std::cout << "Size of uint64_t: " << sizeof(uint64_t) << " bytes" << std::endl;

  std::cout << "Size of osc::int64: " << sizeof(osc::int64) << " bytes" << std::endl;
  std::cout << "Size of osc::uint64: " << sizeof(osc::uint64) << " bytes" << std::endl;

  #if defined(__x86_64__)
  std::cout << "__x86_64__ defined" << std::endl;

  #endif

  #if defined(_M_X64)
  std::cout << "_M_X64 defined" << std::endl;
  #endif
}