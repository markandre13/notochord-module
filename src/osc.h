// -- START OF COPYRIGHT & LICENSE NOTICES --
//
// Chordata Motion's Notochord (python module)
// -- Core implementation of Chordata Motion's Open Source motion capture framework
//
// http://chordata.cc
// contact@chordata.cc
//
//
// Copyright 2018-2021 Bruno Laurencich
// Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
//
// This file is part of Chordata Motion's Notochord (python module).
//
// Chordata Motion's Notochord (python module) is free software: you can
// redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
//
// Chordata client is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Chordata Motion's Notochord (python module).
// If not, see <https://www.gnu.org/licenses/>.
//
//
// -- END OF COPYRIGHT & LICENSE NOTICES --

#ifndef __NOTOCHORD_OSC__
#define __NOTOCHORD_OSC__

#include <deque>
#include <cstdint>

#include "config.h"
#include "math_types.h"
#include "phy_nodes.h"
#include "armature.h"

#include "spdlog/sinks/base_sink.h"

#include "SwapOutboundPacketStream.h"
#include "ip/UdpSocket.h"
#include "osc/OscTypes.h"
#include "ip/NetworkingUtils.h"
#include "web_socket.h"

#include "fmt/core.h"


//See OSC BUFFER NOTES.md
#define PAYLOAD_BUF_SIZE  _CHORDATA_PAYLOAD_BUFFER_SIZE
#define COMM_BUF_SIZE  _CHORDATA_COMM_BUFFER_SIZE
#define PAYLOAD_BUNDLE_SIZE (PAYLOAD_BUF_SIZE * _CHORDATA_PAYLOAD_RESERVE_SLOTS)\
								+ 16 + (4 * _CHORDATA_PAYLOAD_RESERVE_SLOTS)

namespace Chordata{
	namespace Communicator{
		template< int capacity, int padding = 0 >
		struct OSC_Packet {
			OSC_Packet():
			buffer(),
	        p(buffer+padding, capacity)
			{}

			char buffer[capacity+padding];
		    osc::SwapOutboundPacketStream p;

			const char* data() const {
				return &(buffer[padding]);
			}

			size_t size() const {
				return p.Size()-padding;
			}

		};

		template<int capacity>
		using OSC_Websocket_Packet = OSC_Packet<capacity, LWS_PRE>;

		class OscOutBase: public spdlog::sinks::base_sink <std::mutex>{
		    public:
		        explicit OscOutBase(const Chordata::Configuration_Data& conf ):
			        base_address(conf.osc.base_address),
			        msg_address(conf.osc.base_address + conf.osc.msg_address),
			        error_address(conf.osc.base_address + conf.osc.error_address),
			        q_packets(_CHORDATA_PAYLOAD_RESERVE_SLOTS),
			        i_q_pkt(q_packets.begin()),
					raw_packets(_CHORDATA_PAYLOAD_RESERVE_SLOTS),
			        i_raw_pkt(raw_packets.begin()),
			        log_packets(_CHORDATA_PAYLOAD_RESERVE_SLOTS),
			        i_log_pkt(log_packets.begin()),
					error_packets(_CHORDATA_PAYLOAD_RESERVE_SLOTS),
					e_log_pkt(error_packets.begin()),
			        quat_bundle(),
			        raw_bundle(),
			        pos_bundle(),
					is_running(true)
			        {}

		        void sink_it_(const spdlog::details::log_msg& msg) override;

		        void flush_() override {};


		    	virtual void transmit(const std::string& subtarget, const Quaternion& q) = 0;
				virtual void transmit(const std::string& subtarget, const Vector3& v) = 0;

				virtual void transmit(const _Node& n, const Quaternion& q) = 0;
				virtual void transmit(const _Node& n, const Vector3& v) = 0;
				virtual void transmit(const _KCeptor_Base& kc) = 0; // For RAW transmit

				virtual void transmit(const _Armature& arm) = 0;

				virtual void thread_handler() = 0;

				std::string get_current_packet_hex();


		    protected:
		    	OscOutBase(){};

				std::mutex _p_mutex;
				std::condition_variable c;
				volatile bool keep_running, flag_set, is_running;
				std::thread *osc_thread;

		    	std::string base_address, msg_address, error_address;
		    	std::deque<OSC_Packet<_CHORDATA_PAYLOAD_BUFFER_SIZE>> q_packets;
		    	std::deque<OSC_Packet<_CHORDATA_PAYLOAD_BUFFER_SIZE>>::iterator i_q_pkt;

				std::deque<OSC_Packet<_CHORDATA_PAYLOAD_BUFFER_SIZE>> raw_packets;
		    	std::deque<OSC_Packet<_CHORDATA_PAYLOAD_BUFFER_SIZE>>::iterator i_raw_pkt;
		    	std::deque<OSC_Packet<COMM_BUF_SIZE>> log_packets;
		    	std::deque<OSC_Packet<COMM_BUF_SIZE>>::iterator i_log_pkt;
		    	std::deque<OSC_Packet<COMM_BUF_SIZE>> error_packets;
		    	std::deque<OSC_Packet<COMM_BUF_SIZE>>::iterator e_log_pkt;

		    	OSC_Packet<PAYLOAD_BUNDLE_SIZE> quat_bundle, raw_bundle, pos_bundle;
				OSC_Packet<PAYLOAD_BUNDLE_SIZE> temp_quat_bundle, temp_raw_bundle, temp_pos_bundle;
		    	virtual void append_message(const spdlog::details::log_msg& msg);

		    	void append_to_bundle(const std::string& subtarget, const Chordata::Quaternion& q);
				void append_to_bundle(const std::string& subtarget, const Chordata::Vector3& v);

				void append_to_bundle(const std::string& subtarget, const _KCeptor_Base& kc);
		    	void append_message(const std::string& subtarget, const Chordata::Quaternion& q);
		    	void append_message(const std::string& subtarget, const Chordata::Vector3& v);

				void append_message(const std::string& subtarget, const _KCeptor_Base& kc);
	    		void append_message_bundle(const std::string& subtarget, const Chordata::Quaternion& q);
	    		void append_message_bundle(const std::string& subtarget, const Chordata::Vector3& v);

				void append_message_bundle(const std::string& subtarget, const _KCeptor_Base& kc);
		};

		template<bool use_bundles>
		class OscOutUdp : public OscOutBase {
		private:
	    	unsigned long ip;
	    	uint16_t port;

		protected:
			UdpSocket transmitSocket;
		    //void append_message(const spdlog::details::log_msg& msg) override;
		public:
			explicit OscOutUdp(const Chordata::Configuration_Data& conf) :
				OscOutBase(conf),
		        ip( GetHostByName(conf.comm.ip.c_str()) ),
		        port( conf.comm.port ),
			    transmitSocket()
			    {
			    	transmitSocket.SetEnableBroadcast(true);
		        	transmitSocket.Connect(IpEndpointName( ip, port ));

					// Set the thread to run thread_handler
					keep_running = true;
					flag_set = false;
					osc_thread = new std::thread(&OscOutBase::thread_handler, this);
			    }

			~OscOutUdp(){ 
				int wait_attempts = 1;
				while (is_running){
					keep_running = false;
					CHORDATA_SLEEP(_CHORDATA_THREAD_DELAY_MS * (wait_attempts++));						
					c.notify_all();
				}
				if (osc_thread != nullptr && osc_thread->joinable())
					osc_thread->join();		
				delete osc_thread;
				osc_thread = nullptr;					
			}



			void flush_() override;

		    template <typename Packet_list, typename  Iter>
		    void flush_packets(Packet_list&, Iter&);

	        void transmit(const std::string& subtarget, const Quaternion& q) override;
			void transmit(const std::string& subtarget, const Vector3& v) override;

			void transmit(const _Node& n, const Quaternion& q) override;
			void transmit(const _Node& n, const Vector3& v) override;
			void transmit(const _KCeptor_Base& kc) override;

			void transmit(const _Armature& arm) override;

			void thread_handler() override;

		};

		template<bool use_bundles>
		class OscOutWebsocket : public OscOutBase {
		public:
			explicit OscOutWebsocket(const Chordata::Configuration_Data& conf) :
				OscOutBase(conf),
				socket(conf) {
					// Set the thread to run thread_handler
					keep_running = true;
					flag_set = false;
					osc_thread = new std::thread(&OscOutBase::thread_handler, this);
				}
			~OscOutWebsocket() {
				socket.stop();
				
				int wait_attempts = 1;
				while (is_running){
					keep_running = false;
					CHORDATA_SLEEP(100 * (wait_attempts++));						
					c.notify_all();
				}
				if (osc_thread != nullptr && osc_thread->joinable())
					osc_thread->join();		
				delete osc_thread;
				osc_thread = nullptr;
			}
	        void flush_() override {};


	    	void transmit(const std::string& subtarget, const Quaternion& q) override;
			void transmit(const std::string& subtarget, const Vector3& v) override;

			void transmit(const _Node& n, const Quaternion& q) override;
			void transmit(const _Node& n, const Vector3& v) override;
			void transmit(const _KCeptor_Base& kc) override; // For RAW transmit

			void transmit(const _Armature& arm) override;

			void thread_handler() override;

			template <typename Packet_list, typename  Iter>
			void flush_packets(Packet_list& packets, Iter& iter, const std::string& p);

		private:
			WebSocket socket;

		};

		class OscOut_Null: public OscOutBase{
		public:
			OscOut_Null(): OscOutBase(){};
			void sink_it_(const spdlog::details::log_msg& msg) override {};
			void flush_() override {};
			void transmit(const std::string& subtarget, const Quaternion& q) override {};
			void transmit(const std::string& subtarget, const Vector3& v) override {};

			void transmit(const _Node& n, const Quaternion& q) override {};
			void transmit(const _Node& n, const Vector3& v) override {};
			void transmit(const _KCeptor_Base& kc) override {};

			void transmit(const _Armature& arm) override {};

		    void append_message(const spdlog::details::log_msg& msg) override {};

			void thread_handler() override {};
		};
	}
}




#endif
