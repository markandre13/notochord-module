// -- START OF COPYRIGHT & LICENSE NOTICES -- 
//
// Chordata Motion's Notochord (python module)
// -- Core implementation of Chordata Motion's Open Source motion capture framework
//
// http://chordata.cc
// contact@chordata.cc
//
//
// Copyright 2018-2021 Bruno Laurencich
// Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
//
// This file is part of Chordata Motion's Notochord (python module).
//
// Chordata Motion's Notochord (python module) is free software: you can 
// redistribute it and/or modify it under the terms of the 
// GNU General Public License as published by the Free Software Foundation, 
// either version 3 of the License, or (at your option) any later version.
//
// Chordata client is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Chordata Motion's Notochord (python module).  
// If not, see <https://www.gnu.org/licenses/>.
//
//
// -- END OF COPYRIGHT & LICENSE NOTICES -- 

#include "kalman.h"
#include "communicator.h"

#include <stdexcept>

using FSsf::int32; 
using FSsf::int16;
using FSsf::int8;

#define CALIB_SET_OFFSET 1
#define CALIB_SET_MATRIX 1 << 1

/*----------  Kalman Raw  ----------*/


void Chordata::Fusion_Kalman_Raw::set_gyro( FSsf::int16 x, FSsf::int16 y, FSsf::int16 z ){

	thisGyro.iYs[CHX] = x;
	thisGyro.iYs[CHY] = y;
	thisGyro.iYs[CHZ] = z;
}

void Chordata::Fusion_Kalman_Raw::set_accel( FSsf::int16 x, FSsf::int16 y, FSsf::int16 z ){

	thisAccel.iGs[CHX] = x;
	thisAccel.iGs[CHY] = y;
	thisAccel.iGs[CHZ] = z;
}

void Chordata::Fusion_Kalman_Raw::set_mag( FSsf::int16 x, FSsf::int16 y, FSsf::int16 z ){
	thisMag.iBs[CHX] = x;
	thisMag.iBs[CHY] = y;
	thisMag.iBs[CHZ] = z;
}

void Chordata::Fusion_Kalman_Raw::
get_mag_offset_for_EEPROM(vector3_ptr magOffset) const{
	(*magOffset)[0] = -(thisMagCal.fV[0] * thisMag.fCountsPeruT);
	(*magOffset)[1] = -(thisMagCal.fV[1] * thisMag.fCountsPeruT);
	(*magOffset)[2] = -(thisMagCal.fV[2] * thisMag.fCountsPeruT);
}



void Chordata::Fusion_Kalman_Raw::
get_mag_matrix_for_EEPROM(float (&magMatrix)[3][3]) const{
	for (int i=0;i<3;i++){
		for (int j=0;j<3;j++){
			magMatrix[i][j] = thisMagCal.finvW[i][j];

		}
	}

}


void Chordata::Fusion_Kalman_Raw::
set_mag_offset_from_EEPROM(const FSsf::int16 *_magOffset){
	if (thisMag.fCountsPeruT == -1){
		throw std::logic_error("The sensor sensitivities should be initialized\
 in the Kalman_Filter before setting the magnetometer calibration offset");
	}

	thisMagCal.fV[0] = - (_magOffset[0] / thisMag.fCountsPeruT);
	thisMagCal.fV[1] = - (_magOffset[1] / thisMag.fCountsPeruT); 
	thisMagCal.fV[2] = - (_magOffset[2] / thisMag.fCountsPeruT);

	calibSetFlag |= CALIB_SET_OFFSET;
	if (calibSetFlag == (CALIB_SET_OFFSET | CALIB_SET_MATRIX)){
		thisMagCal.iValidMagCal = true;
	}

}

void Chordata::Fusion_Kalman_Raw::
set_mag_matrix_from_EEPROM(const float (&magMatrix)[3][3]){

	for (int i=0;i<3;i++){
		for (int j=0;j<3;j++){
			thisMagCal.finvW[i][j] = magMatrix[i][j];

		}
	}

	calibSetFlag |= CALIB_SET_MATRIX;
	if (calibSetFlag == (CALIB_SET_OFFSET | CALIB_SET_MATRIX)){
		thisMagCal.iValidMagCal = true;
	}
}



/*----------  Kalman LSM9DS1  ----------*/

Chordata::Fusion_Kalman_LSM9DS1::Fusion_Kalman_LSM9DS1(FSsf::uint16 sensorFs, FSsf::uint8 oversampleRatio, bool enable_mag_correct):
	Fusion_Kalman_Base(sensorFs, oversampleRatio, enable_mag_correct)
	{
		get_runtime()->get_logger()->warn("Warning! Fusion_Kalman_LSM9DS1 is deprecated, use Fusion_Kalman_Raw instead");
	}


void Chordata::Fusion_Kalman_LSM9DS1::set_gyro( FSsf::int16 x, FSsf::int16 y, FSsf::int16 z ){

	thisGyro.iYs[CHX] = y;
	thisGyro.iYs[CHY] = x;
	thisGyro.iYs[CHZ] = z;
}

void Chordata::Fusion_Kalman_LSM9DS1::set_accel( FSsf::int16 x, FSsf::int16 y, FSsf::int16 z ){

	thisAccel.iGs[CHX] = -y;
	thisAccel.iGs[CHY] = -x;
	thisAccel.iGs[CHZ] = -z;
}

void Chordata::Fusion_Kalman_LSM9DS1::set_mag( FSsf::int16 x, FSsf::int16 y, FSsf::int16 z ){
	thisMag.iBs[CHX] = y;
	thisMag.iBs[CHY] = -x;
	thisMag.iBs[CHZ] = z;
}



void Chordata::Fusion_Kalman_LSM9DS1::
get_mag_offset_for_EEPROM(vector3_ptr magOffset) const{
	// magOffset[0] = (thisMagCal.fV[1] * thisMag.fCountsPeruT);
	// magOffset[1] = -(thisMagCal.fV[0] * thisMag.fCountsPeruT);
	// magOffset[2] = -(thisMagCal.fV[2] * thisMag.fCountsPeruT);

	(*magOffset)[0] = (thisMagCal.fV[0] * thisMag.fCountsPeruT);
	(*magOffset)[1] = (thisMagCal.fV[1] * thisMag.fCountsPeruT);
	(*magOffset)[2] = (thisMagCal.fV[2] * thisMag.fCountsPeruT);
}


void Chordata::Fusion_Kalman_LSM9DS1::
get_mag_matrix_for_EEPROM(float (&magMatrix)[3][3]) const{
	const auto& Mk = thisMagCal.finvW;
	auto& Me = magMatrix;

	for (int i=0;i<3;i++){
		for (int j=0;j<3;j++){
			magMatrix[i][j] = thisMagCal.finvW[i][j];

		}
	}

	// Me[0][0] = Mk[1][1];	Me[0][1] = Mk[1][0]; 	Me[0][2] = Mk[1][2]; 
	// Me[1][0] = Mk[0][1]; 	Me[1][1] = Mk[0][0];	Me[1][2] = Mk[0][2];
	// Me[2][0] = Mk[2][1]; 	Me[2][1] = Mk[2][0]; 	Me[2][2] = Mk[2][2]; 

}


void Chordata::Fusion_Kalman_LSM9DS1::
set_mag_offset_from_EEPROM(const FSsf::int16 *_magOffset){
	if (thisMag.fCountsPeruT == -1){
		throw std::logic_error("The sensor sensitivities should be initialized\
in the Kalman_Filter before setting the magnetometer calibration offset");
	}

	// thisMagCal.fV[1] = (_magOffset[0] / thisMag.fCountsPeruT);
	// thisMagCal.fV[0] = - (_magOffset[1] / thisMag.fCountsPeruT); 
	// thisMagCal.fV[2] = - (_magOffset[2] / thisMag.fCountsPeruT);

	thisMagCal.fV[0] = (_magOffset[0] / thisMag.fCountsPeruT);
	thisMagCal.fV[1] = (_magOffset[1] / thisMag.fCountsPeruT); 
	thisMagCal.fV[2] = (_magOffset[2] / thisMag.fCountsPeruT);


	calibSetFlag |= CALIB_SET_OFFSET;
	if (calibSetFlag == CALIB_SET_OFFSET | CALIB_SET_MATRIX){
		thisMagCal.iValidMagCal = true;
	}

}

void Chordata::Fusion_Kalman_LSM9DS1::
set_mag_matrix_from_EEPROM(const float (&magMatrix)[3][3]){
	auto& Mk = thisMagCal.finvW;
	const auto& Me = magMatrix;

	// // Reference rearrengment from OCTAVE tests
	// //amod = 	[a(2,2), -a(2,1), a(2,3);
    // // 			 a(1,2), -a(1,1), a(1,3);
    // // 			 a(3,2), -a(3,1), a(3,3)];
    // // NOTE: the minus sign in the second column is ignored here, already taken into accoun in the magOffset

	// Mk[1][1] = Me[0][0];	Mk[1][0] = Me[0][1]; 	Mk[1][2] = Me[0][2]; 
	// Mk[0][1] = Me[1][0]; 	Mk[0][0] = Me[1][1];	Mk[0][2] = Me[1][2];
	// Mk[2][1] = Me[2][0]; 	Mk[2][0] = Me[2][1]; 	Mk[2][2] = Me[2][2]; 

	for (int i=0;i<3;i++){
		for (int j=0;j<3;j++){
			thisMagCal.finvW[i][j] = magMatrix[i][j];

		}
	}

	calibSetFlag |= CALIB_SET_MATRIX;
	if (calibSetFlag == CALIB_SET_OFFSET | CALIB_SET_MATRIX){
		thisMagCal.iValidMagCal = true;
	}
}


