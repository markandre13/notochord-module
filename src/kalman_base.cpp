// -- START OF COPYRIGHT & LICENSE NOTICES -- 
//
// Chordata Motion's Notochord (python module)
// -- Core implementation of Chordata Motion's Open Source motion capture framework
//
// http://chordata.cc
// contact@chordata.cc
//
//
// Copyright 2018-2021 Bruno Laurencich
// Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
//
// This file is part of Chordata Motion's Notochord (python module).
//
// Chordata Motion's Notochord (python module) is free software: you can 
// redistribute it and/or modify it under the terms of the 
// GNU General Public License as published by the Free Software Foundation, 
// either version 3 of the License, or (at your option) any later version.
//
// Chordata client is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Chordata Motion's Notochord (python module).  
// If not, see <https://www.gnu.org/licenses/>.
//
//
// -- END OF COPYRIGHT & LICENSE NOTICES -- 

#include <cmath>
#include <stdexcept>

#include "kalman.h"
#include "utils.h"
#include "chord_error.h"

using FSsf::int32; 
using FSsf::int16;
using FSsf::int8;

const FSsf::fquaternion Chordata::Fusion_Kalman_Base::rotate_match_coord_sys = 
					{0,0,1,0}; // Rotate 180deg in Y

Chordata::Fusion_Kalman_Base::Fusion_Kalman_Base(FSsf::uint16 sensorFs, FSsf::uint8 oversampleRatio, bool enable_mag_correct = false):
	thisAccel{},
	thisGyro{},
	thisMag{},
	thisMagCal{},
	calibSetFlag(0),
	thisSV{},
	thisMagBuffer{},
	thisFilterRate{},
	loopcounter(0),
	iCounter(0),
	skip_mag_calibration(sensorFs/oversampleRatio),
	enable_mag_correct(enable_mag_correct)
	{
		thisSV.resetflag = true;
		thisFilterRate.sensorFs = sensorFs;
		if (oversampleRatio > MAX_OVERSAMPLE_RATIO){
			//Note: this limitation is caused by the static allocation of some arrays in 
			//FS_sensor_fusion/types.h with the constant MAX_OVERSAMPLE_RATIO
			throw Chordata::Value_Error("The oversample ratio cannot be bigger than 8");
		}

		if (oversampleRatio < 1) {
			throw Chordata::Value_Error("The oversample ratio cannot be smaller than 1");
		}

		thisFilterRate.oversampleRatio = oversampleRatio;

		FSsf::fInit_9DOF_GBY_KALMAN(&thisSV, &thisAccel,  &thisMag, &thisMagCal, &thisFilterRate );
		FSsf::fInitMagCalibration(&thisMagCal, &thisMagBuffer);

		//initiate sensitivities to -1 to flag them as invalid
		thisAccel.fgPerCount = -1;
		thisMag.fCountsPeruT = -1;
		thisMag.fuTPerCount = -1;
		thisGyro.fDegPerSecPerCount = -1;

		// initialize magnetometer data structure
		// zero the calibrated measurement since this is used for indexing the magnetic buffer even before first calibration
		for (int i = CHX; i <= CHZ; i++)
			thisMag.iBcAvg[i]= 0;

	}

void Chordata::Fusion_Kalman_Base::set_sensor_res(float gRes, float aRes, float mRes){
	//Set the sensors scale resolutions
	thisAccel.fgPerCount = aRes;

	// convert GAUSS to uT
	mRes = 100*mRes;
	thisMag.fCountsPeruT = 1 / mRes;
	thisMag.fuTPerCount = mRes;

	thisGyro.fDegPerSecPerCount = gRes;
}

float Chordata::Fusion_Kalman_Base::get_gyro_res(){
	return thisGyro.fDegPerSecPerCount;
}

float Chordata::Fusion_Kalman_Base::get_accel_res(){
	return thisAccel.fgPerCount;
}

float Chordata::Fusion_Kalman_Base::get_mag_res(){
	return thisMag.fuTPerCount / 100;
}

void Chordata::Fusion_Kalman_Base::set_sensors( FSsf::int16 gx, FSsf::int16 gy, FSsf::int16 gz,
												FSsf::int16 ax, FSsf::int16 ay, FSsf::int16 az,
												FSsf::int16 mx, FSsf::int16 my, FSsf::int16 mz ){
	set_gyro(gx,gy,gz);
	set_accel(ax,ay,az);
	set_mag(mx,my,mz);
}


void Chordata::Fusion_Kalman_Base::process_readings(){
	FSsf::int32 iSum[3];					// array of sums
	FSsf::int32 itmp;					// scratch
	FSsf::int8 i, j, k, l;				// counter

	/*----------  GYRO  ----------*/

	// store in a buffer for later gyro integration by sensor fusion algorithm
	for (i = CHX; i <= CHZ; i++)
			thisGyro.iYsBuffer[iCounter][i] = thisGyro.iYs[i];

	/*----------  ACEL  ----------*/
	
	// store measurement in a buffer for later end of block processing
	for (i = CHX; i <= CHZ; i++)
		thisAccel.iGsBuffer[iCounter][i] = thisAccel.iGs[i];

	// every thisFilterRate.oversampleRatio passes calculate the block averaged measurement
	if (iCounter == (thisFilterRate.oversampleRatio - 1))
	{	
		// calculate the block averaged measurement in counts and g
		for (i = CHX; i <= CHZ; i++)
		{
			iSum[i] = 0;
			for (j = 0; j < thisFilterRate.oversampleRatio; j++)
				iSum[i] += (int32)thisAccel.iGsBuffer[j][i];
			// compute the average with nearest integer rounding
			if (iSum[i] >= 0)
				thisAccel.iGsAvg[i] = (int16)((iSum[i] + (thisFilterRate.oversampleRatio >> 1)) / thisFilterRate.oversampleRatio);
			else
				thisAccel.iGsAvg[i] = (int16)((iSum[i] - (thisFilterRate.oversampleRatio >> 1)) / thisFilterRate.oversampleRatio);
			// convert from integer counts to float g
			thisAccel.fGsAvg[i] = (float)thisAccel.iGsAvg[i] * thisAccel.fgPerCount;
		}
	} // end of test for end of thisFilterRate.oversampleRatio block

	/*----------  MAG  ----------*/

	// store in a buffer for later end of block processing
	for (i = CHX; i <= CHZ; i++)
		thisMag.iBsBuffer[iCounter][i] = thisMag.iBs[i];

	// update magnetic buffer with iBs avoiding a write to the shared structure while a calibration is in progress.
	if (!thisMagCal.iCalInProgress)
		FSsf::iUpdateMagnetometerBuffer(&thisMagBuffer, &thisMag, loopcounter);

	// every thisFilterRate.oversampleRatio passes calculate the block averaged and calibrated measurement using an anti-glitch filter
	// that rejects the measurement furthest from the mean. magnetometer sensors are sensitive
	// to occasional current pulses from power supply traces and so on and this is a simple method to remove these.
	if (iCounter == (thisFilterRate.oversampleRatio - 1))
	{
		// calculate the channel means using all measurements
		for (i = CHX; i <= CHZ; i++)
		{
			// accumulate channel sums
			iSum[i] = 0;
			for (j = 0; j < thisFilterRate.oversampleRatio; j++)
				iSum[i] += (int32)thisMag.iBsBuffer[j][i];
		}
		// store axis k in buffer measurement l furthest from its mean
		itmp = 0;
		for (i = CHX; i <= CHZ; i++)
		{
			for (j = 0; j < thisFilterRate.oversampleRatio; j++)
			{
				if (std::abs((int32)thisMag.iBsBuffer[j][i] * thisFilterRate.oversampleRatio - iSum[i]) >= itmp)
				{
					k = i;
					l = j;
					itmp = std::abs((int32)thisMag.iBsBuffer[j][i] * thisFilterRate.oversampleRatio - iSum[i]);
				}
			}
		}

		// re-calculate the block averaged measurement ignoring channel k in measurement l
		if (thisFilterRate.oversampleRatio == 1)
		{
			// use the one available measurement for averaging in this case
			for (i = CHX; i <= CHZ; i++)
			{
				thisMag.iBsAvg[i] = thisMag.iBsBuffer[0][i];
			}
		} // end of compute averages for thisFilterRate.oversampleRatio = 1
		else
		{
			// sum all measurements ignoring channel k in measurement l
			for (i = CHX; i <= CHZ; i++)
			{
				iSum[i] = 0;
				for (j = 0; j < thisFilterRate.oversampleRatio; j++)
				{
					if (!((i == k) && (j == l)))
						iSum[i] += (int32)thisMag.iBsBuffer[j][i];
				}
			}
			// compute the average with nearest integer rounding
			for (i = CHX; i <= CHZ; i++)
			{
				if (i != k)
				{
					// thisFilterRate.oversampleRatio measurements were used
					if (iSum[i] >= 0)
						thisMag.iBsAvg[i] = (int16)((iSum[i] + (thisFilterRate.oversampleRatio >> 1)) / thisFilterRate.oversampleRatio);
					else
						thisMag.iBsAvg[i] = (int16)((iSum[i] - (thisFilterRate.oversampleRatio >> 1)) / thisFilterRate.oversampleRatio);
				}
				else
				{
					// thisFilterRate.oversampleRatio - 1 measurements were used
					if (iSum[i] >= 0)
						thisMag.iBsAvg[i] = (int16)((iSum[i] + ((thisFilterRate.oversampleRatio - 1) >> 1)) / (thisFilterRate.oversampleRatio - 1));
					else
						thisMag.iBsAvg[i] = (int16)((iSum[i] - ((thisFilterRate.oversampleRatio - 1) >> 1)) / (thisFilterRate.oversampleRatio - 1));
				}
			}
		} // end of compute averages for thisFilterRate.oversampleRatio = 1

		// convert the averages to float
		for (i = CHX; i <= CHZ; i++)
			thisMag.fBsAvg[i] = (float)thisMag.iBsAvg[i] * thisMag.fuTPerCount;

		// remove hard and soft iron terms from fBsAvg (uT) to get calibrated data fBcAvg (uT), iBc (counts)
		FSsf::fInvertMagCal(&thisMag, &thisMagCal);
		
	} // end of test for end of thisFilterRate.oversampleRatio block

	// increment the loopcounter (used for time stamping magnetic data)
	loopcounter++;
}

bool Chordata::Fusion_Kalman_Base::process_and_run(){
	process_readings();
	
	// every thisFilterRate.oversampleRatio passes zero the decimation counter and run the sensor fusion
	if (iCounter++ == (thisFilterRate.oversampleRatio - 1))
	{
		iCounter = 0;
		run_filter();
		if (enable_mag_correct){
			return run_mag_calib(); 
		}
		return true;
	} 
	return false;
}

void Chordata::Fusion_Kalman_Base::run_filter(){
	FSsf::fRun_9DOF_GBY_KALMAN(&thisSV, &thisAccel, &thisMag, &thisGyro,
		&thisMagCal, &thisFilterRate);
}

bool Chordata::Fusion_Kalman_Base::set_mag_run_calib( int16 x, int16 y, int16 z ){
	set_mag(x, y, z);
	enable_mag_correct = true;
	return process_and_run();
}


bool Chordata::Fusion_Kalman_Base::run_mag_calib(){
	static FSsf::int8 counter = 0;
	bool initiatemagcal = false;

	if ( !thisMagCal.iCalInProgress )
	{
		counter = 0;
		// do the first 4 element calibration immediately there are a minimum of MINMEASUREMENTS4CAL
		initiatemagcal = (!thisMagCal.iMagCalHasRun && (thisMagBuffer.iMagBufferCount >= MINMEASUREMENTS4CAL));

		// otherwise initiate a calibration at intervals depending on the number of measurements available
		initiatemagcal |= ((thisMagBuffer.iMagBufferCount >= MINMEASUREMENTS4CAL) && 
				(thisMagBuffer.iMagBufferCount < MINMEASUREMENTS7CAL) &&
				!(loopcounter % INTERVAL4CAL));
		initiatemagcal |= ((thisMagBuffer.iMagBufferCount >= MINMEASUREMENTS7CAL) &&
				(thisMagBuffer.iMagBufferCount < MINMEASUREMENTS10CAL) &&
				!(loopcounter % INTERVAL7CAL));
		initiatemagcal |= ((thisMagBuffer.iMagBufferCount >= MINMEASUREMENTS10CAL) &&
				!(loopcounter % INTERVAL10CAL));


		// initiate the magnetic calibration if any of the conditions are met
		if (initiatemagcal)
		{
			FSsf::fRunMagCalibration(&thisMagCal, &thisMagBuffer, &thisMag, &thisFilterRate );
		}

	} // end of test that no calibration is already in progress

	return initiatemagcal;
}

