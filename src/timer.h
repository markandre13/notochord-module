// -- START OF COPYRIGHT & LICENSE NOTICES -- 
//
// Chordata Motion's Notochord (python module)
// -- Core implementation of Chordata Motion's Open Source motion capture framework
//
// http://chordata.cc
// contact@chordata.cc
//
//
// Copyright 2018-2021 Bruno Laurencich
// Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
//
// This file is part of Chordata Motion's Notochord (python module).
//
// Chordata Motion's Notochord (python module) is free software: you can 
// redistribute it and/or modify it under the terms of the 
// GNU General Public License as published by the Free Software Foundation, 
// either version 3 of the License, or (at your option) any later version.
//
// Chordata client is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Chordata Motion's Notochord (python module).  
// If not, see <https://www.gnu.org/licenses/>.
//
//
// -- END OF COPYRIGHT & LICENSE NOTICES -- 

#ifndef __NOTOCHORD_TIME__
#define __NOTOCHORD_TIME__

#include <thread>
#include <chrono>
#include <string>
#include "core.h"
#include "chord_error.h"
#include "phy_nodes.h"
#include "mock_phy_nodes.h"
#include <queue>


namespace Chordata{
	 // avoid name clash in cython

	class _Node_Scheduler {
	public:
	using scheduler_citer = std::vector<std::shared_ptr<Chordata::_Node>>::const_iterator;
	using scheduler_iter = std::vector<std::shared_ptr<Chordata::_Node>>::iterator;

	private:
		std::queue<_Node*> consume_buffer;
		std::vector<std::shared_ptr<_Node>> nodelist;
		scheduler_iter current;
		
		const uint8_t odr; 
		int ODRModifiers;
		uint64_t target_wait;

	    int64_t lap_target_duration;
		uint64_t odr_counter; // This counter keeps track of the number of bangs until we reach 1 second
		scheduler_citer first_sensor;

		bool initialized;
		bool setup_finished;

		std::vector<void (*)()> _handlers;
		timek_ptr scheduler_timek;


		_Node_Scheduler& step();

		void do_search(); // Trigger the search
	
	public:
		_Node_Scheduler():			
			consume_buffer(),
			current(nodelist.end()-1),
			odr(Chordata::get_config()->odr()),
			ODRModifiers(0),			
			lap_target_duration(1000000 / Chordata::get_config()->odr()),
			odr_counter(Chordata::get_config()->odr()),
			initialized(false),
			setup_finished(false)
			{
				// Perhaps timer should have a unique identifier (in case we want multiple timers with different timing)
				if (get_runtime()->get_timekeeper("scheduler") == nullptr) {
					get_runtime()->add_index_timekeeper(std::make_shared<_Timekeeper>("scheduler"));
				}
				scheduler_timek = get_runtime()->get_timekeeper("scheduler");
			};		
		
		void add_node(std::shared_ptr<_Node> n);
		int get_ODR_modifiers() const { return ODRModifiers; };
		bool empty() const { return nodelist.empty(); }
		std::vector<_Node*>::size_type length() const { return nodelist.size(); };
		uint64_t get_global_round_micros() { return global_round_micros; };
		uint64_t calculate_target_wait();
		void add_handler(void (*f)());
		void handlers();

		uint64_t bang(uint64_t);	

		timek_ptr get_timekeeper() { return scheduler_timek; };	

		void stop_sensors();
	}; using Node_Scheduler = _Node_Scheduler; // avoid name clash in cython

	class _Timer {
	private:
		scheduler_ptr scheduler;
		std::thread *t;
		std::atomic_bool stop, running;
		std::exception_ptr t_excp;
		void start_timer();

	public:

		_Timer(scheduler_ptr _scheduler):
			scheduler(_scheduler),
			t(nullptr),
			stop(false),
			running(false)
			{}
		~_Timer();


		bool start();
		void terminate();
		bool is_running() { return running; };
		void handlers(); //TODO: Perhaps we should return the scheduler to avoid boilerplate code

	}; using Timer = _Timer; // avoid name clash in cython
}

#endif