// -- START OF COPYRIGHT & LICENSE NOTICES -- 
//
// Chordata Motion's Notochord (python module)
// -- Core implementation of Chordata Motion's Open Source motion capture framework
//
// http://chordata.cc
// contact@chordata.cc
//
//
// Copyright 2018-2021 Bruno Laurencich
// Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
//
// This file is part of Chordata Motion's Notochord (python module).
//
// Chordata Motion's Notochord (python module) is free software: you can 
// redistribute it and/or modify it under the terms of the 
// GNU General Public License as published by the Free Software Foundation, 
// either version 3 of the License, or (at your option) any later version.
//
// Chordata client is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Chordata Motion's Notochord (python module).  
// If not, see <https://www.gnu.org/licenses/>.
//
//
// -- END OF COPYRIGHT & LICENSE NOTICES -- 

#ifndef __CYTHON_SINK__
#define __CYTHON_SINK__


#include "spdlog/sinks/base_sink.h"
#include "fmt/core.h"
#include <iostream>

namespace Chordata{
	class Cython_Buffer{
		private:
			std::vector<std::vector<std::string>> log_registry;
		
		public:
			Cython_Buffer(){
				log_registry.resize(spdlog::level::level_enum::n_levels);
			};
			void add_registry(std::string&& msg, spdlog::level::level_enum level) {
				if (log_registry[level].capacity() == 0)
					log_registry[level].reserve(_CHORDATA_CYTHON_SINK_BUFFER_SIZE);
				log_registry[level].push_back(std::move(msg));
			}

			void clear_buffer(uint32_t level) {
				log_registry[level].clear();
			}

			std::vector<std::string>& get_registry(uint32_t level) {
				return log_registry[level];
			}

	};

	namespace Communicator{
        template<typename Mutex>
		class CythonSink: public spdlog::sinks::base_sink <Mutex>{
			public:
				Cython_Buffer *buffer = NULL;

				CythonSink(){};
				CythonSink(Cython_Buffer *buffer) {
					this->buffer = buffer;
				};

		    protected:
		        void sink_it_(const spdlog::details::log_msg& msg) override {
                    spdlog::memory_buf_t formatted;   
					spdlog::sinks::base_sink<Mutex>::formatter_->format(msg, formatted);

					if(buffer != NULL) {
						buffer->add_registry(fmt::to_string(formatted), msg.level);
					}
                }
		        void flush_() override {}		    
		};	

	}
}


#include "spdlog/details/null_mutex.h"
#include <mutex>
using cython_sink_mt = Chordata::Communicator::CythonSink<std::mutex>;
using cython_sink_st = Chordata::Communicator::CythonSink<spdlog::details::null_mutex>;


#endif