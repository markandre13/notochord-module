// -- START OF COPYRIGHT & LICENSE NOTICES --
//
// Chordata Motion's Notochord (python module)
// -- Core implementation of Chordata Motion's Open Source motion capture framework
//
// http://chordata.cc
// contact@chordata.cc
//
//
// Copyright 2018-2021 Bruno Laurencich
// Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
//
// This file is part of Chordata Motion's Notochord (python module).
//
// Chordata Motion's Notochord (python module) is free software: you can
// redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
//
// Chordata client is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Chordata Motion's Notochord (python module).
// If not, see <https://www.gnu.org/licenses/>.
//
//
// -- END OF COPYRIGHT & LICENSE NOTICES --

#ifndef __NOTOCHORD_CONFIG__
#define __NOTOCHORD_CONFIG__

#include <string>

#include "def.h"
#include "types.h"
// #include "communicator.h"

namespace Chordata {
	struct Configuration_Data
		{
			Configuration_Data();

			Configuration_Data& operator=(Configuration_Data
			 other);

			const int hardware_concurrency;
			const std::string version;
			const uint32_t version_number;

			bool wait;
			bool raw;
			bool calib;
			bool scan;
			bool use_armature;
			int odr() const { return _int_ODR[sensor.output_rate]; };
			_Kc_Values kc_revision;

			struct Conf_XML{
				Conf_XML();
				std::string filename;
				std::string schema;
			} xml;

			struct Conf_comm{
				Conf_comm();
				std::string adapter;
				std::string ip;
				uint16_t port;
				Output_Redirect msg;
				Output_Redirect error;
				Output_Redirect transmit;
				std::string filepath;
				char log_level;
				float send_rate;
				bool use_bundles;
				uint16_t websocket_port;
			} comm;

			struct Conf_osc{
				Conf_osc();
				std::string base_address;
				std::string error_address;
				std::string msg_address;
			} osc;

			struct Conf_sensor{
				Conf_sensor();
				Gyro_Scale_Options gyro_scale;
				Accel_Scale_Options accel_scale;
				Mag_Scale_Options mag_scale;
				Output_Data_Rate output_rate;
				bool madgwick;
				bool live_mag_correction;
			} sensor;


		};
}


#endif