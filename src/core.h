// -- START OF COPYRIGHT & LICENSE NOTICES -- 
//
// Chordata Motion's Notochord (python module)
// -- Core implementation of Chordata Motion's Open Source motion capture framework
//
// http://chordata.cc
// contact@chordata.cc
//
//
// Copyright 2018-2021 Bruno Laurencich
// Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
//
// This file is part of Chordata Motion's Notochord (python module).
//
// Chordata Motion's Notochord (python module) is free software: you can 
// redistribute it and/or modify it under the terms of the 
// GNU General Public License as published by the Free Software Foundation, 
// either version 3 of the License, or (at your option) any later version.
//
// Chordata client is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Chordata Motion's Notochord (python module).  
// If not, see <https://www.gnu.org/licenses/>.
//
//
// -- END OF COPYRIGHT & LICENSE NOTICES -- 

#ifndef __NOTOCHORD_CORE__
#define __NOTOCHORD_CORE__


#include "types.h"
#include "config.h"
#include <mutex>
#include <string>
#include <unordered_map>
#include "cython_sink.h"
#include <thread>
#include <future>
#include "fusion_worker.h"
#include "calibration_manager.h"

namespace Chordata{

	class Notochord_Runtime{
	private:
	    static Notochord_Runtime *_instance;
	    static std::mutex mutex_;
		static Calibration_Manager calib_manager;
	   	Notochord_Runtime();
   	    ~Notochord_Runtime();
   	    comm_runtime_ptr _comm_instance;
		io_ptr _i2c_instance;
		timek_ptr global_timekeeper;
		timek_index timekeeper_index;
		Configuration_Data config;
		timer_ptr timer;
		Notochord_Worker fusion_worker, armature_worker;
		std::thread th_f_worker, th_a_worker;
		armature_ptr armature;

		// void configure_communicator_runtime(const Chordata::Configuration_Data*);
		// void configure_i2c(const Chordata::Configuration_Data*);

	public : 
		std::future<std::string> future; // User for User Input
		int pending_kc;
		Notochord_Runtime(Notochord_Runtime &other) = delete;
		void operator=(const Notochord_Runtime &) = delete;

		static Notochord_Runtime *init();
		static bool is_active();
		static Notochord_Status status();
		static Notochord_Runtime *terminate(bool terminate_communicator = true);
		
		/*----------  Configuration interface  ----------*/
		static const Configuration_Data *get_config();
		void setup(const Configuration_Data*, bool reconfigure_communicator);

		/*----------  Communicator interface  ----------*/
		logger_ptr get_logger(Logger_type r = MSG_LOG);
		const std::string *get_current_log_file() const;
		void flush_loggers();

		std::vector<std::string>& _get_registry(uint32_t level);
		void _clear_registry(uint32_t level);
		Cython_Buffer _get_buffer();

		Calibration_Manager& get_calibration_manager() { return calib_manager; };

		/*----------  i2c interface  ----------*/
		io_ptr get_i2c();

		/*----------  timers interface  ----------*/
		void add_index_timekeeper(timek_ptr timek);
		timek_ptr get_timekeeper(const std::string& label);
		timek_ptr get_timekeeper();

		bool start(scheduler_ptr scheduler);
		bool end();

		void handlers();

		/*-------- User Input --------*/
		bool get_user_input();

		/*-------- Get Fusion Worker --------*/
		Notochord_Worker* get_fusion_worker() { return &fusion_worker; };
		Notochord_Worker* get_armature_worker() { return &armature_worker; };

		/*-------- Armature interface --------*/
		void set_armature(armature_ptr _armature);
		armature_ptr get_armature() { return armature; };

	};

	/*----------  Global functions  ----------*/
	
	inline Notochord_Runtime *get_runtime(){ return Notochord_Runtime::init(); }
	inline Notochord_Runtime *terminate_runtime(bool terminate_communicator = true){ return Notochord_Runtime::terminate(terminate_communicator); };
	inline const Configuration_Data *get_config() { return Notochord_Runtime::get_config(); };
	inline Notochord_Status get_status() { return Notochord_Runtime::status(); };
	inline bool check_user_input() {return get_runtime()->get_user_input(); };
	uint64_t runtime_millis();
	void _print_int_info();

}

#endif