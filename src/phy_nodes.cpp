// -- START OF COPYRIGHT & LICENSE NOTICES -- 
//
// Chordata Motion's Notochord (python module)
// -- Core implementation of Chordata Motion's Open Source motion capture framework
//
// http://chordata.cc
// contact@chordata.cc
//
//
// Copyright 2018-2021 Bruno Laurencich
// Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
//
// This file is part of Chordata Motion's Notochord (python module).
//
// Chordata Motion's Notochord (python module) is free software: you can 
// redistribute it and/or modify it under the terms of the 
// GNU General Public License as published by the Free Software Foundation, 
// either version 3 of the License, or (at your option) any later version.
//
// Chordata client is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Chordata Motion's Notochord (python module).  
// If not, see <https://www.gnu.org/licenses/>.
//
//
// -- END OF COPYRIGHT & LICENSE NOTICES -- 

#include "phy_nodes.h"
#include "io.h"
#include "chord_error.h"
#include "communicator.h"
#include "fusion_worker.h"

#include "utils.h"

#include "Python.h"

// #include <iostream>
using Chordata::link_ptr;
using Chordata::mux_ptr;
using Chordata::branch_ptr;
using Chordata::kceptor_ptr;
namespace comm = Chordata::Communicator;
namespace EEPROM = Chordata::EEPROM;

#define _CHORDATA_DEF_G_RES SENSITIVITY_GYROSCOPE_2000
#define _CHORDATA_DEF_A_RES SENSITIVITY_ACCELEROMETER_8
#define _CHORDATA_DEF_M_RES SENSITIVITY_MAGNETOMETER_4


// #define NOTOCHORD_TRACE_FLASH_RW //uncoment to enable OFFSET & MATRIX dump

// ====================================================== //
// ========================= _Mux ======================= //
// ====================================================== //

int Chordata::_KCeptor_Base::PENDING_KC = 0;

Chordata::_Mux::_Mux( link_ptr _parent,
            uint8_t _address, std::string _label,
            io_ptr _io
        ):
    _Node(_parent, _address, _label, _io),
    state(OFF),
    off_branch(nullptr, _address, "off_branch", _io, OFF)
    {
        comm::_trace("(+Node) Created Mux:{}, addr: {:#04x} | parent: {}", _label, 
                                                _address, (_parent)? _parent->get_label() : "None");

    };

// -------- Factory ------- //

link_ptr Chordata::_Mux::Create(link_ptr _parent,
                                    uint8_t _address, std::string _label,
                                    io_ptr _io )
{

    _Mux* this_link_raw = new _Mux(_parent, _address, _label, _io);
    mux_ptr this_link = std::shared_ptr<_Mux>(this_link_raw);
    
    this_link->off_branch.parent = this_link;;
    this_link->off_branch.thisMux = this_link.get();
    this_link->off_branch.bang();

    for (std::size_t i = 0; i < this_link->max_children; i++){
        Branch *new_branch_raw = new Branch(this_link, _address, 
                                             fmt::format("{}.CH_{}", _label, i+1), 
                                            _io, static_cast<Mux_Channel>( i+1 ) );

        link_ptr new_branch = std::shared_ptr<Branch>(new_branch_raw);
        this_link->add_child(new_branch);

    }  
        
    if (_parent){
        _parent->add_child(this_link);
    }
    return this_link;
} 

// -------- Methods ------- //

void Chordata::_Mux::bang_branch(Mux_Channel ch){
    if (ch == OFF){
        off_branch.bang();
    } else {
        branch_ptr branch = std::dynamic_pointer_cast<Branch>(get_child((uint8_t)ch - 1));
        branch->bang();
    }
}

// ====================================================== //
// ======================= Branch ======================= //
// ====================================================== //

Chordata::_Branch::_Branch(	mux_ptr _mux,
                            uint8_t _address, std::string _label, 
                            io_ptr _io, Mux_Channel _channel):
    _Node(_mux, _address, _label, _io, 1),
    // I2C_Node(_i2c, _address, _label, __Mux ),
    thisMux(_mux.get()),
    channel(_channel),
    channel_value( 1 << (((uint8_t) _channel) - 1))
    {
        if (channel_value != 0){
            comm::_trace("(+Node) Created Branch:{} with CH:{}, mux value: {:#04x}", _label, (uint8_t)_channel, channel_value);
        }
    };


void Chordata::Branch::bang(){

    for (int i = 0; i < 5; ++i){
        try{
            this->switch_branch();
            thisMux->state = channel;
            return;
        } catch (const Chordata::IO_Error& e){
            comm::_error("error switching branch {}. Retrying.. ({})", get_label(), i);
            CHORDATA_SLEEP(20);
        }
    }

    comm::_error("Too many errors when switching branch {}.", get_label());
    throw IO_Error("Can't write to _Mux");

}

void Chordata::Branch::switch_branch(){
    io->write_byte(address, channel_value );
}

// ====================================================== //
// ===================== KCeptor Base==================== //
// ====================================================== //

Chordata::_KCeptor_Base::_KCeptor_Base(	link_ptr _parent,
            uint8_t _address, std::string _label,
            io_ptr _io):
    _Node(_parent, _address, _label, _io, 1),
    setup_done(false),
    odr(Chordata::get_config()->odr()),
    g_offset(new _Vector3(0,0,0)),a_offset(new _Vector3(0,0,0)),m_offset(new _Vector3(0,0,0)),
    m_matrix(new _Matrix3()),
    g_calibrator(nullptr), a_calibrator(nullptr),
    kalman(new Fusion_Kalman_Raw(odr, _KALMAN_OVERSAMPLE_RATIO, false))
    {
        verify_imu_settings();
        kalman->set_sensor_res(gRes, aRes, mRes);

        m_matrix->setIdentity();
        
        comm::_debug("(+Node) Created KCeptor:{}, addr: {:#04x} | parent: {}", _label, 
                                        _address, (_parent)? _parent->get_label() : "(none)");
        status = KC_STATUS::SETUP;
        get_runtime()->pending_kc += 1;
    }

Chordata::_KCeptor_Base::~_KCeptor_Base(){
    reset_xg_calib();
}

#define OFFSET_DEBUG_MSG(W, V, S)\
				comm::_debug("{:>10} correction vector =[{:^8} {:^8} {:^8}] {}", W, V->x(),V->y(),V->z(), S)

#define MATRIX_DEBUG_MSG(M, S)\
				comm::_debug("{10:>10} Matrix = {9}\n"\
								"\t\t\t|{0:^12} {1:^12} {2:^12}|\n"\
								"\t\t\t|{3:^12} {4:^12} {5:^12}|\n"\
								"\t\t\t|{6:^12} {7:^12} {8:^12}|",\
								 (*M)(0,0), (*M)(0,1), (*M)(0,2),\
								 (*M)(1,0), (*M)(1,1), (*M)(1,2),\
								 (*M)(2,0), (*M)(2,1), (*M)(2,2), S, "Mag")

uint64_t Chordata::global_round_micros;

//#include <memory>

bool Chordata::_KCeptor_Base::check_status() {
    static int counter;
    switch (status) {
        case KC_STATUS::SETUP:
            setup(); 
            counter = 0;
            break;
        case KC_STATUS::GX_CALIB:
        {
            if (counter == 0)
                comm::_info("Collecting Accelerometer and Gyroscope samples, don't move the sensor...");
            double slope = calibrate_xg();
            
            if (slope == 0.0) {
                OFFSET_DEBUG_MSG("Gyro",  g_offset, "");
                OFFSET_DEBUG_MSG("Accel", a_offset, "");
                comm::_info("Accelerometer and Gyroscope's calibration done");
                comm::_info("Press a key when ready to perform the Magnetometer calibration procedure\n~>");
                // sleep(3);
                status = KC_STATUS::WAITING;
            } else if (counter++ > 10){
                comm::_info("Too much variation (slope = {}). Is the sensor steady?\nRetrying...", slope);
                counter = 1;
            }             
        } break;
        case KC_STATUS::WAITING:
        {
            return false;
        } break;
        case KC_STATUS::M_CALIB:
        {            
            calibrate_m();
            if (counter++ > 20) {
                OFFSET_DEBUG_MSG("Mag",   m_offset, "NONE");
                counter = 0;
            }

            return false;
        } break;
        case KC_STATUS::SAVING:
        {
            OFFSET_DEBUG_MSG("Mag",   m_offset, "NONE");
            MATRIX_DEBUG_MSG(m_matrix, "NONE");                
            comm::_info("Calibration Done!");
            status = KC_STATUS::ACTIVE; 
            save_data();
            break;
        }
    }
    return true;
}

void Chordata::_KCeptor_Base::bang() {
    if (!check_status())
        return;

    try
    {
        read_sensors();

        Fusion_Task *ft = new Fusion_Task(this, global_round_micros,
                                          g_raw_read[X_AXIS], g_raw_read[Y_AXIS], g_raw_read[Z_AXIS],
                                          a_raw_read[X_AXIS], a_raw_read[Y_AXIS], a_raw_read[Z_AXIS],
                                          my, -mx, mz);

        auto t = std::unique_ptr<Fusion_Task>(ft);

		get_runtime()->get_fusion_worker()->enqueue(std::move(t));
    }
    catch(const Chordata::IO_Error& e)
    {
        comm::_error("Error reading {}", get_label());
		setup_done = false;
        status = KC_STATUS::SETUP;
        get_runtime()->pending_kc += 1;
    }   

}

bool Chordata::_KCeptor_Base::verify_imu_settings() {
    // TODO: Calculate resolution by dividing sensitivity by (2^15)
    switch (Chordata::get_config()->sensor.accel_scale) {
        case accel_2:
            aRes = SENSITIVITY_ACCELEROMETER_2; break;
        case accel_4:
            aRes = SENSITIVITY_ACCELEROMETER_4; break;
        case accel_8:
            aRes = SENSITIVITY_ACCELEROMETER_8; break;
        case accel_16:
            aRes = SENSITIVITY_ACCELEROMETER_16; break;
        default:
            // Invalid value 
            comm::_error("Wrong accelerometer value K_Ceptor: {}, got: {}", get_label(), Chordata::get_config()->sensor.accel_scale);
    }
    switch (Chordata::get_config()->sensor.gyro_scale) {
        case gyro_245:
            gRes = SENSITIVITY_GYROSCOPE_245; break;
        case gyro_500:
            gRes = SENSITIVITY_GYROSCOPE_500; break;
        case gyro_2000:
            gRes = SENSITIVITY_GYROSCOPE_2000; break;
        default:
            // Invalid value 
            comm::_error("Wrong gyroscope value K_Ceptor: {}, got: {}", get_label(), Chordata::get_config()->sensor.gyro_scale);
    }
    switch (Chordata::get_config()->sensor.mag_scale) {
        case mag_4:
            mRes = SENSITIVITY_MAGNETOMETER_4; break;
        case mag_8:
            mRes = SENSITIVITY_MAGNETOMETER_8; break;
        case mag_12:
            mRes = SENSITIVITY_MAGNETOMETER_12; break;
        case mag_16:
            mRes = SENSITIVITY_MAGNETOMETER_16; break;
        default:
            // Invalid value, set to defaults (check config.cpp 69)
            aRes = _CHORDATA_DEF_A_RES;
            gRes = _CHORDATA_DEF_G_RES;
            mRes = _CHORDATA_DEF_M_RES;
            comm::_error("Wrong magnetometer value K_Ceptor: {}, got: {}", get_label(), Chordata::get_config()->sensor.mag_scale);
            return false;
    }

    return true;
}

/*-------------- Setters --------------*/
void Chordata::_KCeptor_Base::set_g_offset(int16_t x,int16_t y,int16_t z){
    (*g_offset)[X_AXIS] = x;
    (*g_offset)[Y_AXIS] = y;
    (*g_offset)[Z_AXIS] = z;
}
void Chordata::_KCeptor_Base::set_a_offset(int16_t x,int16_t y,int16_t z){
    (*a_offset)[X_AXIS] = x;
    (*a_offset)[Y_AXIS] = y;
    (*a_offset)[Z_AXIS] = z;
}
void Chordata::_KCeptor_Base::set_m_offset(int16_t x,int16_t y,int16_t z){
    (*m_offset)[X_AXIS] = x;
    (*m_offset)[Y_AXIS] = y;
    (*m_offset)[Z_AXIS] = z;
}
void Chordata::_KCeptor_Base::set_m_matrix(matrix3_ptr other){
    Matrix3 &m =  *other;
    *m_matrix = m;
}

/*-------------- Calibration --------------*/
float Chordata::_KCeptor_Base::calibrate_m(int16_t _gx, int16_t _gy, int16_t _gz,
                                        int16_t _ax, int16_t _ay, int16_t _az,
                                        int16_t _mx, int16_t _my, int16_t _mz){

    comm::_trace("Mag calibration, readings: x={:6d} y={:6d} z{:6d}", _my, -_mx, _mz);
    kalman->set_gyro(_gx, _gy, _gz);
    kalman->set_accel(_ax, _ay, _az);
    bool calib_result = kalman->set_mag_run_calib(_mx, _my, _mz);

    if (calib_result){
        kalman->get_mag_offset_for_EEPROM(m_offset);
        float temp_matrix[3][3];
        kalman->get_mag_matrix_for_EEPROM(temp_matrix);
        for (int i=0;i<3;i++){
            for (int j=0;j<3;j++){
                (*m_matrix)(i,j) = temp_matrix[i][j];
            }
        }
        
        return kalman->get_m_covariance();
    } else {
        return -1;
    }
}

float Chordata::_KCeptor_Base::calibrate_m(){
    read_sensors(false);

    // pass the imu readings in the same order than the original LSM9DS1 readings
    // the accel and gyro values with the calibration offset applied
    // the mag values without any calibration
    return _KCeptor_Base::calibrate_m(  g_raw_read[X_AXIS],g_raw_read[Y_AXIS],g_raw_read[Z_AXIS],
                                        a_raw_read[X_AXIS],a_raw_read[Y_AXIS],a_raw_read[Z_AXIS],
                                        m_raw_read[X_AXIS],m_raw_read[Y_AXIS],m_raw_read[Z_AXIS]);
}

float Chordata::_KCeptor_Base::calibrate_xg(){
    static float g_slope, a_slope;
    if (g_calibrator == nullptr) {
        g_calibrator = new Chordata::Offset_Calibrator(_MAX_AG_CALIB_SLOPE);
        g_slope = 1;
    }
    
    if (a_calibrator == nullptr) {
        a_calibrator = new Chordata::Offset_Calibrator(_MAX_AG_CALIB_SLOPE);
        a_slope = 1;
    }

    read_sensors(false);
    comm::_trace("AG calibration, readings: gx={:6d} gy={:6d} gz{:6d} | ax={:6d} ay={:6d} az{:6d}",
                     g_raw_read[X_AXIS],g_raw_read[Y_AXIS],g_raw_read[Z_AXIS], 
                     a_raw_read[X_AXIS],a_raw_read[Y_AXIS],a_raw_read[Z_AXIS]);
    

    if (g_slope != 0.0){
        g_slope = g_calibrator->add_reading(g_raw_read[X_AXIS],g_raw_read[Y_AXIS],g_raw_read[Z_AXIS]);
    }

    if (a_slope != 0.0){ //Assumes sensor facing up!
        a_slope = a_calibrator->add_reading(a_raw_read[X_AXIS],a_raw_read[Y_AXIS],
                                                a_raw_read[Z_AXIS] + (int16_t)(1/kalman->get_accel_res()));
    }

    if (g_slope + a_slope == 0){
        for (size_t i = 0; i < 3; i++){
            (*g_offset)[i] = g_calibrator->result[i];
            (*a_offset)[i] = a_calibrator->result[i];
        }
    }

    return g_slope + a_slope;
}

void Chordata::_KCeptor_Base::apply_mag_calib(int16_t *a){
    a[X_AXIS] =  my - (*m_offset)[X_AXIS];
    a[Y_AXIS] = -mx - (*m_offset)[Y_AXIS];
    a[Z_AXIS] =  mz - (*m_offset)[Z_AXIS];

    float b[3] = {(float)a[0], (float)a[1], (float)a[2]};
    float c[3] = {0,0,0};
    for (int i=0;i<3;i++){
        for (int j=0;j<3;j++){
            c[i]+=( (*m_matrix)(i,j)*b[j]);
        }
    }
    a[X_AXIS] = (int16_t)c[X_AXIS];
    a[Y_AXIS] = (int16_t)c[Y_AXIS];
    a[Z_AXIS] = (int16_t)c[Z_AXIS];
}

void Chordata::_KCeptor_Base::reset_xg_calib(){
    if (g_calibrator != nullptr)
        delete g_calibrator;
    if (a_calibrator != nullptr)
        delete a_calibrator; 
    g_calibrator = nullptr;
    a_calibrator = nullptr;
}

bool Chordata::_KCeptor_Base::set_state(int _status) {
    if (status == KC_STATUS::WAITING || status == KC_STATUS::M_CALIB) {
        status = KC_STATUS(_status);
        return true;
    }

    comm::_error("Can't change current status, the sensor is bussy...");
    return false;
}


// ====================================================== //
// ======================= KCeptor ====================== //
// ====================================================== //


Chordata::_KCeptor::_KCeptor(	link_ptr _parent,
            uint8_t _address, std::string _label,
            io_ptr _io):
            _KCeptor_Base(_parent, _address, _label, _io),
            imu(_io, _address, odr)
            {                
                // Sensor settings are verified in parent's constructor
                imu.settings.accel.scale = _int_accel_scale[Chordata::get_config()->sensor.accel_scale];
                imu.settings.gyro.scale = _int_gyro_scale[Chordata::get_config()->sensor.gyro_scale];
                imu.settings.mag.scale = _int_mag_scale[Chordata::get_config()->sensor.mag_scale];                    
                
                comm::_trace("Imu settings scales initialized with values accelerometer: {}, gyroscope: {}, magnetometer: {}",
                    imu.settings.accel.scale, 
                    imu.settings.gyro.scale,
                    imu.settings.mag.scale);
            }



// -------- Factory ------- //
link_ptr Chordata::_KCeptor::Create(link_ptr _parent,
                                    uint8_t _address, std::string _label,
                                    io_ptr _io ){

    return _KCeptor_Base::Create<_KCeptor>(_parent, _address, _label, _io);
} 


// -------- Methods ------- //

/*-------------- IMU Control --------------*/

bool Chordata::_KCeptor::setup(){
    try{
        comm::_trace("Setting up KCeptor:{} | XOR:0x{:x}, XG_ADDR:0x{:x}, M_ADDR:0x{:x}", get_label(), imu.xor_addr, imu.xg_addr, imu.m_addr);
        uint16_t setup_result = imu.begin();
        if (setup_result != (WHO_AM_I_AG_RSP << 8 | WHO_AM_I_M_RSP)){
            comm::_error("Wrong setup K_Ceptor:{}, response:0x{:x}", get_label(), setup_result);
            return false;
        }

        get_info_from_EEPROM();
        read_calib_from_EEPROM();
        FSsf::int16 aux[] = {(FSsf::int16)m_offset->y(), (FSsf::int16)-m_offset->x(), (FSsf::int16)m_offset->z()};
        kalman->set_mag_offset_from_EEPROM(aux);
        float aux_mat[3][3] = {{(*m_matrix)(0,0), (*m_matrix)(0,1), (*m_matrix)(0,2)}, 
                               {(*m_matrix)(1,0), (*m_matrix)(1,1), (*m_matrix)(1,2)}, 
                               {(*m_matrix)(2,0), (*m_matrix)(2,1), (*m_matrix)(2,2)}};
        kalman->set_mag_matrix_from_EEPROM(aux_mat);
        
        OFFSET_DEBUG_MSG("Gyro",  g_offset, "");
        OFFSET_DEBUG_MSG("Accel", a_offset, "");
        OFFSET_DEBUG_MSG("Mag",   m_offset, "");
        MATRIX_DEBUG_MSG(m_matrix, "");

        comm::_info("Setup KCeptor:{:.<12} OK! (0x{:x})", get_label(), setup_result);
    } catch (const Chordata::IO_Error& e){
        comm::_error("Can't find K_Ceptor:{:.<12}", get_label());
        return false;
    }
    setup_done = true;
    if (get_config()->calib) {
        (*g_offset) = _Vector3(0,0,0);
        (*a_offset) = _Vector3(0,0,0);
        (*m_offset) = _Vector3(0,0,0);
        m_matrix = create_Matrix3_ptr();
        status = KC_STATUS::GX_CALIB;
    } else {
        status = KC_STATUS::ACTIVE;
    }
    get_runtime()->pending_kc -= 1;
    return true;
}

void Chordata::_KCeptor::read_gyro(){
    imu.readGyro();    
    gx = imu.gx;
    gy = imu.gy;
    gz = imu.gz;

    g_raw_read[X_AXIS] = imu.gy - (*g_offset)[X_AXIS];
    g_raw_read[Y_AXIS] = imu.gx - (*g_offset)[Y_AXIS];
    g_raw_read[Z_AXIS] = imu.gz - (*g_offset)[Z_AXIS];
}

void Chordata::_KCeptor::read_accel(){
    imu.readAccel();
    ax = imu.ax;
    ay = imu.ay;
    az = imu.az;

    a_raw_read[X_AXIS] = -(imu.ay + (*a_offset)[X_AXIS]);
    a_raw_read[Y_AXIS] = -(imu.ax + (*a_offset)[Y_AXIS]);
    a_raw_read[Z_AXIS] = -(imu.az + (*a_offset)[Z_AXIS]);
}

void Chordata::_KCeptor::read_mag(bool calc){
    imu.readMag();
    mx = imu.mx;
    my = imu.my;
    mz = imu.mz;

    if (calc) {
        apply_mag_calib(m_raw_read);
    } else {
        m_raw_read[X_AXIS] = imu.my;
        m_raw_read[Y_AXIS] = - imu.mx;
        m_raw_read[Z_AXIS] = imu.mz;
    }
}

void Chordata::_KCeptor::read_sensors(bool calc) {
    read_gyro();
    read_accel();
    read_mag(calc);
}

/*-------------- EEPROM Control / private manage--------------*/
#define CALC_THIS_EEPROM_ADDR (this->get_address() ^ KCR2_EEPROM_ADDR)

Chordata::EEPROM_info Chordata::_KCeptor::get_info_from_EEPROM(){
    uint8_t eeprom_addr = CALC_THIS_EEPROM_ADDR;
    uint32_t validation = EEPROM::read_info((I2C_io*)io.get(), eeprom_addr, _CHORDATA_EEPROM_VAL_CHKSUM);
    uint32_t version = EEPROM::read_info((I2C_io*)io.get(), eeprom_addr, _CHORDATA_EEPROM_VERSION);
    uint32_t timestamp = EEPROM::read_info((I2C_io*)io.get(), eeprom_addr, _CHORDATA_EEPROM_TIMESTAMP);
    
    trace_EEPROM_info(version, timestamp);
    if (validation != _NOTOCHORD_CRC32) {
        comm::_error("Error validating EPPROM_info checksum. Got: {}, expected: {}.", validation, _NOTOCHORD_CRC32);
        throw IO_Error("Wrong CHECKSUM while reading EPPROM's calibration");
    }

    return {validation, version, timestamp};
}

void Chordata::_KCeptor::read_calib_from_EEPROM(){
    Chordata::EEPROM_info info = get_info_from_EEPROM();

    read_g_offset_from_EEPROM();
    read_a_offset_from_EEPROM();
    read_m_offset_from_EEPROM();
    read_m_matrix_from_EEPROM();
}

void Chordata::_KCeptor::read_g_offset_from_EEPROM(){
    uint8_t eeprom_addr = CALC_THIS_EEPROM_ADDR;
    EEPROM::Vector3 vec = EEPROM::read_calib((I2C_io*)io.get(), eeprom_addr, _CHORDATA_EEPROM_GYRO_SPACE);
    (*g_offset)[X_AXIS] = vec[X_AXIS];
    (*g_offset)[Y_AXIS] = vec[Y_AXIS];
    (*g_offset)[Z_AXIS] = vec[Z_AXIS];
}

void Chordata::_KCeptor::read_a_offset_from_EEPROM(){
    uint8_t eeprom_addr = CALC_THIS_EEPROM_ADDR;
    EEPROM::Vector3 vec = EEPROM::read_calib((I2C_io*)io.get(), eeprom_addr, _CHORDATA_EEPROM_ACCEL_SPACE);
    (*a_offset)[X_AXIS] = vec[X_AXIS];
    (*a_offset)[Y_AXIS] = vec[Y_AXIS];
    (*a_offset)[Z_AXIS] = vec[Z_AXIS];
}

void Chordata::_KCeptor::read_m_offset_from_EEPROM(){
    uint8_t eeprom_addr = CALC_THIS_EEPROM_ADDR;
    EEPROM::Vector3 vec = EEPROM::read_calib((I2C_io*)io.get(), eeprom_addr, _CHORDATA_EEPROM_MAG_SPACE);
    (*m_offset)[X_AXIS] = vec[X_AXIS];
    (*m_offset)[Y_AXIS] = vec[Y_AXIS];
    (*m_offset)[Z_AXIS] = vec[Z_AXIS];
}

void Chordata::_KCeptor::read_m_matrix_from_EEPROM(){
    uint8_t eeprom_addr = CALC_THIS_EEPROM_ADDR;
    EEPROM::Matrix9 M = EEPROM::read_matrix((I2C_io*)io.get(), eeprom_addr, _CHORDATA_EEPROM_MMAG_SPACE);
    	
    for (int i = 0; i < 9; ++i){
		int row = i / 3;
		int column = i % 3;
		(*m_matrix)(row,column) = M[i]; 
	}

}

#define INVALID_CHKSUM (~_NOTOCHORD_CRC32)

void Chordata::_KCeptor::write_calib_to_EEPROM(const EEPROM_info *info) const{
    uint8_t eeprom_addr = CALC_THIS_EEPROM_ADDR;
    comm::_trace("EEPROM write info INVALID_CHKSUM (0x{:x}), addr: 0x{:x}, ee_offset: 0x{:x}", 
                INVALID_CHKSUM, eeprom_addr, _CHORDATA_EEPROM_VAL_CHKSUM);
    EEPROM::write_info((I2C_io*)io.get(), eeprom_addr, INVALID_CHKSUM, _CHORDATA_EEPROM_VAL_CHKSUM);
    comm::_trace("EEPROM write info TIMESTAMP (0x{:x}), addr: 0x{:x}, ee_offset: 0x{:x}", 
                info->timestamp, eeprom_addr, _CHORDATA_EEPROM_TIMESTAMP);
    EEPROM::write_info((I2C_io*)io.get(), eeprom_addr, info->timestamp, _CHORDATA_EEPROM_TIMESTAMP);

    write_g_offset_to_EEPROM();
    write_a_offset_to_EEPROM();
    write_m_offset_to_EEPROM();
    write_m_matrix_to_EEPROM();

    comm::_trace("EEPROM write info VERSION (0x{:x}), addr: 0x{:x}, ee_offset: 0x{:x}", 
                info->version, eeprom_addr, _CHORDATA_EEPROM_VERSION);
    EEPROM::write_info((I2C_io*)io.get(), eeprom_addr, info->version, _CHORDATA_EEPROM_VERSION);

    comm::_trace("EEPROM write info OK_CHKSUM (0x{:x}), addr: 0x{:x}, ee_offset: 0x{:x}", 
                info->validation, eeprom_addr, _CHORDATA_EEPROM_VAL_CHKSUM);
    EEPROM::write_info((I2C_io*)io.get(), eeprom_addr, info->validation, _CHORDATA_EEPROM_VAL_CHKSUM);
    
}

void Chordata::_KCeptor::write_g_offset_to_EEPROM() const{
    uint8_t eeprom_addr = CALC_THIS_EEPROM_ADDR;
    EEPROM::Vector3 vec = { (int16_t)(*g_offset)[X_AXIS], (int16_t)(*g_offset)[Y_AXIS], (int16_t)(*g_offset)[Z_AXIS]};
    EEPROM::write_calib((I2C_io*)io.get(), eeprom_addr, vec, _CHORDATA_EEPROM_GYRO_SPACE);
}
void Chordata::_KCeptor::write_a_offset_to_EEPROM() const{
    uint8_t eeprom_addr = CALC_THIS_EEPROM_ADDR;
    EEPROM::Vector3 vec = { (int16_t)(*a_offset)[X_AXIS], (int16_t)(*a_offset)[Y_AXIS], (int16_t)(*a_offset)[Z_AXIS]};
    EEPROM::write_calib((I2C_io*)io.get(), eeprom_addr, vec, _CHORDATA_EEPROM_ACCEL_SPACE);
}
void Chordata::_KCeptor::write_m_offset_to_EEPROM() const{
    uint8_t eeprom_addr = CALC_THIS_EEPROM_ADDR;
    EEPROM::Vector3 vec = { (int16_t)(*m_offset)[X_AXIS], (int16_t)(*m_offset)[Y_AXIS], (int16_t)(*m_offset)[Z_AXIS]};
    EEPROM::write_calib((I2C_io*)io.get(), eeprom_addr, vec, _CHORDATA_EEPROM_MAG_SPACE);

}
void Chordata::_KCeptor::write_m_matrix_to_EEPROM() const{
    uint8_t eeprom_addr = CALC_THIS_EEPROM_ADDR;
    EEPROM::Matrix9 M;
    for (int i = 0; i < 9; ++i){
		int row = i / 3;
		int column = i % 3;
		M[i] =(*m_matrix)(row,column); 
	}
    EEPROM::write_calib((I2C_io*)io.get(), eeprom_addr, M, _CHORDATA_EEPROM_MMAG_SPACE);
}

void Chordata::_KCeptor::save_data() {
    EEPROM_info current_info = get_info_from_EEPROM();
    EEPROM_info info;
    info.timestamp = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now().time_since_epoch()).count();
    info.version = _CHORDATA_EEPROM_CURRENT_VERSION;
    info.validation = _NOTOCHORD_CRC32;
    write_calib_to_EEPROM(&info);

    comm::_info("Saved calibration data to KCeptor '{}'", get_label());
}

/*-------------- Utility functions ----------------*/
bool Chordata::_KCeptor::is_kc_connected(io_ptr _io, uint8_t addr) {
    LSM9DS1<IO_Base> imu = LSM9DS1<IO_Base>(_io, addr, 50);
    
    uint16_t setup_result = imu.begin();
    if (setup_result != (WHO_AM_I_AG_RSP << 8 | WHO_AM_I_M_RSP)){
        comm::_error("Wrong address K_Ceptor:{}, response:0x{:x}", addr, setup_result);
        throw IO_Error("Error inisializing K_Ceptor");
        return false;
    }
    return true;
}

// ====================================================== //
// ====================== KCeptorPP ===================== //
// ====================================================== //

#define ODR_G_MASK 0xe0    // 11100000
#define ODR_G_VAL_50 0x40
#define SCALE_G_VAL_2000 0x18

#define SCALE_G_MASK 0x18  // 00011000
#define SCALE_XL_MASK 0x18 // 00011000
#define SCALE_M_MASK 0x60  // 01100000

const unsigned char hex_gyro_scale[4]  = {0x00, 0x08, 0x18, 0x18};     // For gyroscope SCALE
const unsigned char hex_accel_scale[4] = {0x00, 0x08, 0x10, 0x18};     // For accelerometer SCALE
const unsigned char hex_mag_scale[4]   = {0x00, 0x20, 0x40, 0x60};     // For magnetometer SCALE

const unsigned char hex_rate[3] = {0x40, 0x60, 0x80};         // For accelerometer RATE

Chordata::_KCeptorPP::_KCeptorPP(	link_ptr _parent,
            uint8_t _address, std::string _label,
            io_ptr _io):
            _KCeptor_Base(_parent, _address, _label, _io)
            {}

bool Chordata::_KCeptorPP::setup(){
    // --- Stop IMU ---
    io->write_byte(address, (uint8_t)KCpp_i2c_mode::INIT_IMU, 0);
    CHORDATA_SLEEP(100);
    
    // --- Write conf values ---
    io->write_byte(address, (uint8_t)KCpp_i2c_mode::G_SCALE, (uint8_t)get_config()->sensor.gyro_scale);
    io->write_byte(address, (uint8_t)KCpp_i2c_mode::A_SCALE, (uint8_t)get_config()->sensor.accel_scale);
    io->write_byte(address, (uint8_t)KCpp_i2c_mode::M_SCALE, (uint8_t)get_config()->sensor.mag_scale);
    io->write_byte(address, (uint8_t)KCpp_i2c_mode::RATE,    (uint8_t)get_config()->sensor.output_rate);

    // --- Init IMU ---
    io->write_byte(address, (uint8_t)KCpp_i2c_mode::INIT_IMU, 1);
    CHORDATA_SLEEP(100);

    // --- Fetch raw IMU registers to assert configuration done ---
    
    //---------------------------------------------------------------
    // The CTRL_REG1_G Gyro registry holds the (gyro)RATE and G_SCALE
    // see LSM9DS1 datasheet 7.12 (pag. 45)
    io->write_byte(address, (uint8_t)KCpp_i2c_mode::FETCH_AG_REG, CTRL_REG1_G);
    CHORDATA_SLEEP(20);
    uint8_t _CTRL_REG1_G = io->read_byte(address, (uint8_t)KCpp_i2c_mode::FETCH_AG_REG);

    if ((_CTRL_REG1_G & ODR_G_MASK) != hex_rate[get_config()->sensor.output_rate]){
        comm::_error("Error in KCPP setup: G_ODR value should be {:b}, got {:b}", hex_rate[get_config()->sensor.output_rate], (_CTRL_REG1_G & ODR_G_MASK));
        return false;
    }

    // Assert _CTRL_REG1_G SCALE
    if ((_CTRL_REG1_G & SCALE_G_MASK) != hex_gyro_scale[get_config()->sensor.gyro_scale]){
        comm::_error("Error in KCPP setup: G_SCALE value should be {:b}, got {:b}", hex_gyro_scale[get_config()->sensor.gyro_scale], (_CTRL_REG1_G & SCALE_G_MASK));
        return false;
    }

    //---------------------------------------------------------------
    // The CTRL_REG1_G Accel registry holds the (accel)RATE and A_SCALE
    // see LSM9DS1 datasheet 7.24 (pag. 52)
    io->write_byte(address, (uint8_t)KCpp_i2c_mode::FETCH_AG_REG, CTRL_REG6_XL);
    CHORDATA_SLEEP(20);
    uint8_t _CTRL_REG6_XL = io->read_byte(address, (uint8_t)KCpp_i2c_mode::FETCH_AG_REG);
  
    // Assert CTRL_REG6_XL SCALE
    if ((_CTRL_REG6_XL & SCALE_XL_MASK) != hex_accel_scale[get_config()->sensor.accel_scale]){
        comm::_error("Error in KCPP setup: XL_SCALE value should be {:b}, got {:b}", hex_accel_scale[get_config()->sensor.accel_scale], (_CTRL_REG6_XL & SCALE_XL_MASK));
        return false;
    }
    // Assert CTRL_REG6_XL RATE
    if ((_CTRL_REG6_XL & ODR_G_MASK) != hex_rate[get_config()->sensor.output_rate]){
        comm::_error("Error in KCPP setup: XL_ODR value should be {:b}, got {:b}", hex_rate[get_config()->sensor.output_rate], (_CTRL_REG6_XL & ODR_G_MASK));
        return false;
    }

    //---------------------------------------------------------------
    // The CTRL_REG2_M Mag registry holds the M_SCALE, rate is constant so we don't assert it
    // see LSM9DS1 datasheet 8.6 (pag. 64)
    io->write_byte(address, (uint8_t)KCpp_i2c_mode::FETCH_M_REG, CTRL_REG2_M);
    CHORDATA_SLEEP(20);
    uint8_t _CTRL_REG2_M = io->read_byte(address, (uint8_t)KCpp_i2c_mode::FETCH_M_REG);

    // Assert CTRL_REG2_M SCALE
    if ((_CTRL_REG2_M & SCALE_M_MASK) != hex_mag_scale[get_config()->sensor.mag_scale]){
        comm::_error("Error in KCPP setup: XL_SCALE value should be {:b}, got {:b}", hex_mag_scale[get_config()->sensor.mag_scale], (_CTRL_REG2_M & SCALE_M_MASK));
        return false;
    }
    setup_done = true;
    if (get_config()->calib) {
        (*g_offset) = _Vector3(0,0,0);
        (*a_offset) = _Vector3(0,0,0);
        (*m_offset) = _Vector3(0,0,0);
        m_matrix = create_Matrix3_ptr();
        status = KC_STATUS::GX_CALIB;
    } else {
        read_data();
        status = KC_STATUS::ACTIVE;
    }
    get_runtime()->pending_kc -= 1;

    comm::_info("Setup KCeptor++: {:.<12} OK!", get_label());

    return true;
}

void Chordata::_KCeptorPP::read_gyro(){}
void Chordata::_KCeptorPP::read_accel(){}
void Chordata::_KCeptorPP::read_mag(bool calc){}

void Chordata::_KCeptorPP::read_sensors_DMA(bool calc){
    //auto start = get_runtime()->get_timekeeper()->highres_now();
    std::vector<char> reading = io->i2c_rdwr(address, (char)KCpp_i2c_mode::READ_PAYLOAD_DMA, 18);
    //auto read_time = get_runtime()->get_timekeeper()->highres_now();

    //std::cout << "read time: " << read_time - start << std::endl;

    gx = (reading[ 1] << 8) | reading[ 0]; // Store x-axis values into gx
	gy = (reading[ 3] << 8) | reading[ 2]; // Store y-axis values into gy
	gz = (reading[ 5] << 8) | reading[ 4]; // Store z-axis values into gz

    ax = (reading[ 7] << 8) | reading[ 6]; // Store x-axis values into ax
	ay = (reading[ 9] << 8) | reading[ 8]; // Store y-axis values into ay
	az = (reading[11] << 8) | reading[10]; // Store z-axis values into az

    mx = (reading[13] << 8) | reading[12]; // Store x-axis values into mx
	my = (reading[15] << 8) | reading[14]; // Store y-axis values into my
	mz = (reading[17] << 8) | reading[16]; // Store z-axis values into mz
   
    g_raw_read[X_AXIS] = gy - (*g_offset)[X_AXIS];
    g_raw_read[Y_AXIS] = gx - (*g_offset)[Y_AXIS];
    g_raw_read[Z_AXIS] = gz - (*g_offset)[Z_AXIS];

    a_raw_read[X_AXIS] = -(ay + (*a_offset)[X_AXIS]);
    a_raw_read[Y_AXIS] = -(ax + (*a_offset)[Y_AXIS]);
    a_raw_read[Z_AXIS] = -(az + (*a_offset)[Z_AXIS]);

    if (calc) {
        apply_mag_calib(m_raw_read);
    } else {
        m_raw_read[X_AXIS] =   my;
        m_raw_read[Y_AXIS] = - mx;
        m_raw_read[Z_AXIS] =   mz;
    }

#ifdef PEDANTIC_TRACE
    comm::_trace("Offsets read from KCPP gyro: {}, {}, {}" , g_raw_read[0], g_raw_read[1], g_raw_read[2]);
    comm::_trace("Offsets read from KCPP accel: {}, {}, {}", a_raw_read[0], a_raw_read[1], a_raw_read[2]);
    comm::_trace("Offsets read from KCPP mag: {}, {}, {}"  , m_raw_read[0], m_raw_read[1], m_raw_read[2]);
#endif

}

void Chordata::_KCeptorPP::read_sensors(bool calc) {
    read_sensors_DMA(calc);
}

bool Chordata::_KCeptorPP::save_offsets(int16_t *data, int count){
    uint8_t size = count*2 + 1;

    char array[size];

    array[0] = (char)KCpp_i2c_mode::CALIB_OFFSET;

    int aux = 1;

    for (int i = 0; i < count; i++) {
        array[aux++] = (char)((data[i] & 0xff00) >> 8);
        array[aux++] = (char) (data[i] & 0x00ff);    
    }
    
    comm::_trace("Writing i2c command CALIB_OFFSET: {:#04x}", (int)data[0]);

    io->write_bytes(address, array, size);
    CHORDATA_SLEEP(200);

    std::vector<uint8_t> response = io->read_bytes(address, (char)KCpp_i2c_mode::CALIB_OFFSET, size-1);

    //TODO print with comm::pedantic 
#ifdef NOTOCHORD_TRACE_FLASH_RW
    std::cout << "OFFSET TO WRITE  ";
    for (int i = 0; i < size-1; i++) {
        std::cout <<" 0x" << std::hex << (int)array[i+1];
    } std::cout << std::endl;

    std::cout << "OFFSET FROM FLASH";
    for (int i = 0; i < size-1; i++) {
        std::cout <<" 0x" << std::hex << (int)response[i];
    }  std::cout << std::endl;
#endif
    // Assert respone == array
    for (int i = 0; i < size-1; i++) {
        if (response[i] != array[i+1]) {
            comm::_error("At index {}: expected value {:#04x}, but got {:#04x} instead", i, array[i+1], response[i]);
            throw IO_Error("Got wrong OFFSETS data from i2c.");
        }
    }
    int16_t val;
    for (int i = 0; i < size-1; i+=2) {
        val = response[i+1];
        val |= ((uint16_t)response[i] << 8) & 0xff00;

        if (val != data[i/2]) {
            comm::_error("At index {}: expected value 0x{:#04x}, but got 0x{:#04x} instead", i, data[i/2], val);
            throw IO_Error("Got wrong OFFSETS value from i2c.");
        }
    }

    return true;
}

bool Chordata::_KCeptorPP::save_matrix(int32_t *data, int count, KCpp_i2c_mode cmd){
    const uint8_t size = count*4 + 1;

    char array[size];

    array[0] = (char)cmd;

    int aux = 1;

    for (int i = 0; i < count; i++) {
        array[aux++] = (char) (data[i] & 0x000000ff);    
        array[aux++] = (char)((data[i] & 0x0000ff00) >> 8);
        array[aux++] = (char)((data[i] & 0x00ff0000) >> 16);
        array[aux++] = (char)((data[i] & 0xff000000) >> 24);
    }

    comm::_trace("Writing i2c command CALIB_MATRIX ({:#04x}): {:#04x}",cmd, (int)data[0]);

    io->write_bytes(address, array, size);    
    CHORDATA_SLEEP(200);

    std::vector<uint8_t> response = io->read_bytes(address, (char)cmd, size-1);

    //TODO print with comm::pedantic 
#ifdef NOTOCHORD_TRACE_FLASH_RW
    std::cout << "MATRIX ROW TO WRITE  ";
    for (int i = 0; i < size-1; i++) {
        std::cout <<" 0x" << std::hex << (int)array[i+1];
    } std::cout << std::endl;


    std::cout << "MATRIX ROW FROM FLASH";
    for (int i = 0; i < size-1; i++) {
        std::cout <<" 0x" << std::hex << (int)response[i];
    }  std::cout << std::endl;
#endif

    // Assert respone == array
    for (int i = 0; i < size-1-4; i++) {
        if (response[i] != array[i+1]) {
            comm::_error("At index {}: expected value {:#04x}, but got {:#04x} instead", i, array[i+1], response[i]);
            throw IO_Error("Got wrong MATRIX data from i2c.");
        }
    }
    int32_t val;
    for (int i = 0; i < size-1-4; i+=4) {
        val = response[i];
        val |= ((int32_t)response[i+1] <<  8) & 0x0000ff00;
        val |= ((int32_t)response[i+2] << 16) & 0x00ff0000;
        val |= ((int32_t)response[i+3] << 24) & 0xff000000;

        if (val != data[i/4]) {
            comm::_error("At index {}: expected value {:#04x}, but got {:#04x} instead", i, data[i/4], val);
            throw IO_Error("Got wrong MATRIX value from i2c.");
        }
    }

    return true;
}

void Chordata::_KCeptorPP::save_data(){

    int16_t offsets[9];

    int aux = 0; 

    for(int i = 0; i < 3; i++){
        offsets[i  ] = (int16_t)(*g_offset)[i];
        offsets[i+3] = (int16_t)(*a_offset)[i];
        offsets[i+6] = (int16_t)(*m_offset)[i];
    }

    int32_t matrix[9];

    aux = 0;

    for (int row = 0; row < 3; row++) {
        for (int col = 0; col < 3; col++) {
            union {
                float from;
                int32_t to;
            } pun = { .from = (*m_matrix)(row,col) };

            matrix[aux++] = pun.to;
        }
    }

    save_offsets(offsets, 9);
    save_matrix(matrix, 3, KCpp_i2c_mode::CALIB_MATRIX_ROW1);
    save_matrix(matrix+3, 3, KCpp_i2c_mode::CALIB_MATRIX_ROW2);
    save_matrix(matrix+6, 3, KCpp_i2c_mode::CALIB_MATRIX_ROW3);

    io->write_byte(address, (uint8_t)KCpp_i2c_mode::STORE_CALIB, 1);
    CHORDATA_SLEEP(100);

}

void Chordata::_KCeptorPP::read_data() {
    // Read offsets
    std::vector<uint8_t> response = io->read_bytes(address, (char)KCpp_i2c_mode::CALIB_OFFSET, 18);
    std::vector<int16_t> offsets;
    static const KCpp_i2c_mode matrix_i2c_cmds[] = {KCpp_i2c_mode::CALIB_MATRIX_ROW1, KCpp_i2c_mode::CALIB_MATRIX_ROW2, KCpp_i2c_mode::CALIB_MATRIX_ROW3};

    int16_t val;
    for (int i = 0; i < 18; i+=2) {
        val = response[i+1];
        val |= ((uint16_t)response[i] << 8) & 0xff00;

        offsets.push_back(val);
    }
    
    for(int i = 0; i < 3; i++){
        (*g_offset)[i] = offsets[i  ];
        (*a_offset)[i] = offsets[i+3];
        (*m_offset)[i] = offsets[i+6];
    }

    // Read matrix
    std::vector<float> matrix;

    for (int j = 0 ; j < 3; j++){
        response = io->read_bytes(address, (char)matrix_i2c_cmds[j], 3*4);
        int32_t m_val;
        for (int i = 0; i < 3*4; i+=4) {
            m_val = response[i];
            m_val |= ((uint32_t)response[i+1] <<  8) & 0x0000ff00;
            m_val |= ((uint32_t)response[i+2] << 16) & 0x00ff0000;
            m_val |= ((uint32_t)response[i+3] << 24) & 0xff000000;

            union {	//type punning float as int32_t
                float from;
                int32_t to;
            } pun = { .to = m_val }; 

            matrix.push_back(pun.from);
        }
        CHORDATA_SLEEP(50);
    }

    int i = 0;

    for(int row = 0; row < 3; row++) {
        for(int col = 0; col < 3; col++) {
            (*m_matrix)(row, col) = matrix[i++];
        }
    }

    FSsf::int16 aux[] = {(FSsf::int16)m_offset->x(), (FSsf::int16)m_offset->y(), (FSsf::int16)m_offset->z()};
    kalman->set_mag_offset_from_EEPROM(aux);
    
    float aux_mat[3][3] =  {{(*m_matrix)(0,0), (*m_matrix)(0,1), (*m_matrix)(0,2)}, 
                            {(*m_matrix)(1,0), (*m_matrix)(1,1), (*m_matrix)(1,2)}, 
                            {(*m_matrix)(2,0), (*m_matrix)(2,1), (*m_matrix)(2,2)}};
    kalman->set_mag_matrix_from_EEPROM(aux_mat);
    
    OFFSET_DEBUG_MSG("Gyro ",   g_offset, "NONE");
    OFFSET_DEBUG_MSG("Accel",   a_offset, "NONE");
    OFFSET_DEBUG_MSG("Mag  ",   m_offset, "NONE");
    MATRIX_DEBUG_MSG(m_matrix, "NONE");

}

void Chordata::_KCeptorPP::change_i2c_addr(uint8_t addr) {
    if (addr < KCPP_MIN_I2C_ADDR || addr > KCPP_MAX_I2C_ADDR) {
        comm::_error("Invalid i2c address {:#04x}. Must be between 0x40 and 0x5f.", addr);
        throw Value_Error("Invalid i2c address.");
    }
    io->write_byte(address, (uint8_t)KCpp_i2c_mode::SET_I2C_ADDR, addr);
    
    if (is_kc_connected(io, addr)) {
        address = addr;
        comm::_info("Successfully changed i2c address to {:#04x}", addr);
    } else {
        comm::_error("Failed to change i2c address to {:#04x}", addr);
        throw IO_Error("Failed to change i2c address.");
    }
}

void Chordata::_KCeptorPP::stop() {
    comm::_info("Stopping KC: {}, with address: {:#04x}", get_label(), address);
    io->write_byte(address, (uint8_t)KCpp_i2c_mode::INIT_IMU, 0);
    CHORDATA_SLEEP(100);
    status = KC_STATUS::SETUP;
    get_runtime()->pending_kc += 1;
}

// -------- Factory ------- //
link_ptr Chordata::_KCeptorPP::Create(link_ptr _parent,
                                    uint8_t _address, std::string _label,
                                    io_ptr _io ){

    return _KCeptor_Base::Create<_KCeptorPP>(_parent, _address, _label, _io);
} 

/*-------------- Utility functions ----------------*/
bool Chordata::_KCeptorPP::is_kc_connected(io_ptr io, uint8_t address) {
    // --- Stop IMU ---
    io->write_byte(address, (uint8_t)KCpp_i2c_mode::INIT_IMU, 0);
    CHORDATA_SLEEP(100);
    
    return true;
}