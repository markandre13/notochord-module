// -- START OF COPYRIGHT & LICENSE NOTICES -- 
//
// Chordata Motion's Notochord (python module)
// -- Core implementation of Chordata Motion's Open Source motion capture framework
//
// http://chordata.cc
// contact@chordata.cc
//
//
// Copyright 2018-2021 Bruno Laurencich
// Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
//
// This file is part of Chordata Motion's Notochord (python module).
//
// Chordata Motion's Notochord (python module) is free software: you can 
// redistribute it and/or modify it under the terms of the 
// GNU General Public License as published by the Free Software Foundation, 
// either version 3 of the License, or (at your option) any later version.
//
// Chordata client is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Chordata Motion's Notochord (python module).  
// If not, see <https://www.gnu.org/licenses/>.
//
//
// -- END OF COPYRIGHT & LICENSE NOTICES -- 

#ifndef __NOTOCHORD_ABSTRACTION__
#define __NOTOCHORD_ABSTRACTION__

#include "hierarchy.h"
#include "math_types.h"
#include "phy_nodes.h"
#include <unordered_map>

namespace Chordata {

    // ====================================================== //
    // ======================== Bone ======================= //
    // ====================================================== //

    class _Bone;
    using bone_ptr = std::shared_ptr<_Bone>;

    class _Bone: public Link {
    private:
        matrix4_ptr local_transform;    // Original transform
        matrix4_ptr global_transform;   // Derived transform

        bone_ptr parent;                // Parent bone
        std::vector<bone_ptr> children; // Child bones 

        Quaternion pre, post;           // Pre and post rotation for calibration  

    public:
        _Bone(bone_ptr _parent, std::string _label, matrix4_ptr _local_transform, matrix4_ptr _global_transform, std::size_t max = _CHORDATA_MUX_MAX_GATES);
        ~_Bone();

        // Flags
        bool auto_update;
        bool modified;
        bool dirty;

        void bang() override {};

        // ==================== Local Space ==================== //
        void set_local_transform(Matrix4 &_local_transform);
        void update_local_transform();

        // Translation
        void set_local_translation(Vector3 &_local_translation);
        void translate_local(Vector3 &_translation);

        // Rotation
        void set_local_rotation(Quaternion &_local_rotation);
        void rotate_local(Quaternion &_rotation);

        
        // ==================== Global Space ==================== //
        void set_global_transform(Matrix4 &_global_transform);
        void update_global_transform();

        // Translation
        void set_global_translation(Vector3 &_global_translation);
        void translate_global(Vector3 &_translation);

        // Rotation
        void set_global_rotation(Quaternion &_global_rotation, bool use_calibration = true);
        void rotate_global(Quaternion &_rotation);
        

        // ==================== Getters ==================== //
        Matrix4 get_local_transform();
        Matrix4 get_global_transform();

        // Translation
        Vector3 get_local_translation();
        Vector3 get_global_translation();

        // Rotation
        Quaternion get_local_rotation();
        Quaternion get_global_rotation();

        // ==================== Setters ==================== //
        void set_auto_update(bool _auto_update);
        void set_pre_quaternion(Quaternion &_pre);
        void set_post_quaternion(Quaternion &_post);

        // ==================== Hierarchy ==================== //
        void add_child(bone_ptr child);
        void remove_child(bone_ptr _child);        
        void update_children(bool update_local = false);
        void update();
        std::vector<bone_ptr> get_children() { return children; };       

    }; using Bone = _Bone;

    // ====================================================== //
    // ======================== Armature ==================== //
    // ====================================================== //

    class _Armature;
    using armature_ptr = std::shared_ptr<_Armature>;
    using bone_dict = std::unordered_map<std::string, bone_ptr>;

    class _Armature: public Hierarchy {
    private:
        std::string label;
        bone_ptr root;
        bone_dict bones;

        void build_bone_dict_helper(bone_dict &dict, bone_ptr bone);
    
    public:
        _Armature(std::string _label, bone_ptr _root_bone);
        ~_Armature();

        bone_dict build_bone_dict() ;

        // ==================== Getters ==================== //
        std::string get_label();
        bone_ptr get_root();
        const bone_dict& get_bones() const { return bones; };

        // ==================== Setters ==================== //
        // void set_label(std::string _label);
        // void set_root(bone_ptr _root_bone);
        void set_calibration_quaternions(Quaternion &_pre, Quaternion &_post);

        // ==================== Update ==================== //
        void update(bone_ptr _bone = nullptr);

        void process_bone(const _Node &n, Quaternion &q);

    }; using Armature = _Armature;

}

#endif
