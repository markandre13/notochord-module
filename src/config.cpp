// -- START OF COPYRIGHT & LICENSE NOTICES --
//
// Chordata Motion's Notochord (python module)
// -- Core implementation of Chordata Motion's Open Source motion capture framework
//
// http://chordata.cc
// contact@chordata.cc
//
//
// Copyright 2018-2021 Bruno Laurencich
// Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
//
// This file is part of Chordata Motion's Notochord (python module).
//
// Chordata Motion's Notochord (python module) is free software: you can
// redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
//
// Chordata client is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Chordata Motion's Notochord (python module).
// If not, see <https://www.gnu.org/licenses/>.
//
//
// -- END OF COPYRIGHT & LICENSE NOTICES --

#include <thread>
#include <string>
#include "def.h"
#include "types.h"
#include "config.h"

#include "fmt/core.h"

#ifndef _NOTOCHORD_GIT_HASH
#define _NOTOCHORD_GIT_HASH "dev"
#endif


#define	_NOTOCHORD_VERSION_STR fmt::format("{}.{}.{}{} #{}",\
							_NOTOCHORD_VERSION_MAJOR,\
							_NOTOCHORD_VERSION_MINOR,\
							_NOTOCHORD_VERSION_PATCH,\
							_NOTOCHORD_VERSION_POST,\
							_NOTOCHORD_GIT_HASH)

Chordata::Configuration_Data::Configuration_Data():
	hardware_concurrency(std::thread::hardware_concurrency()),
	wait(true),
	raw(false),
	calib(false),
	scan(false),
	use_armature(false),
	version(_NOTOCHORD_VERSION_STR),
	version_number(_NOTOCHORD_VERSION)
	//odr(_CHORDATA_DEF_ODR)
	{}

Chordata::Configuration_Data& Chordata::Configuration_Data::operator=(Chordata::Configuration_Data other){
	wait=other.wait;
	raw=other.raw;
	calib=other.calib;
	scan=other.scan;
	use_armature=other.use_armature;
	xml=other.xml;
	comm=other.comm;
	osc=other.osc;
	//odr=other.odr;
	sensor=other.sensor;
	return *this;
}

Chordata::Configuration_Data::Conf_XML::Conf_XML():
	filename(_CHORDATA_CONF),
	schema(_CHORDATA_CONF_SCHEMA)
	{}

Chordata::Configuration_Data::Conf_comm::Conf_comm():
	ip(_CHORDATA_TRANSMIT_ADDR),
	port(_CHORDATA_TRANSMIT_PORT),
	adapter(_CHORDATA_I2C_DEFAULT_ADAPTER_),
	filepath(_CHORDATA_FILEOUT_PATH),
	log_level(_CHORDATA_VERBOSITY),
	send_rate(_CHORDATA_SEND_RATE),
	use_bundles(true),
	msg(_CHORDATA_DEF_LOG_REDIR),
	error(_CHORDATA_DEF_ERR_REDIR),
	transmit(_CHORDATA_DEF_TRA_REDIR),
	websocket_port(_CHORDATA_DEF_WEBSOCKET_PORT)
	{}

Chordata::Configuration_Data::Conf_osc::Conf_osc():
	base_address(_CHORDATA_DEF_OSC_ADDR),
	error_address(_CHORDATA_DEF_ERROR_SADDR),
	msg_address(_CHORDATA_DEF_MSG_SADDR)
	{}

Chordata::Configuration_Data::Conf_sensor::Conf_sensor():
	gyro_scale(_CHORDATA_DEF_GYRO),
	accel_scale(_CHORDATA_DEF_ACCEL),
	mag_scale(_CHORDATA_DEF_MAG),
	output_rate(_CHORDATA_DEF_RATE),
	madgwick(false),
	live_mag_correction(false)
	{}