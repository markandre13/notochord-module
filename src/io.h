// -- START OF COPYRIGHT & LICENSE NOTICES -- 
//
// Chordata Motion's Notochord (python module)
// -- Core implementation of Chordata Motion's Open Source motion capture framework
//
// http://chordata.cc
// contact@chordata.cc
//
//
// Copyright 2018-2021 Bruno Laurencich
// Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
//
// This file is part of Chordata Motion's Notochord (python module).
//
// Chordata Motion's Notochord (python module) is free software: you can 
// redistribute it and/or modify it under the terms of the 
// GNU General Public License as published by the Free Software Foundation, 
// either version 3 of the License, or (at your option) any later version.
//
// Chordata client is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Chordata Motion's Notochord (python module).  
// If not, see <https://www.gnu.org/licenses/>.
//
//
// -- END OF COPYRIGHT & LICENSE NOTICES -- 


#ifndef __NOTOCHORD_IO__
#define __NOTOCHORD_IO__

#include "def.h"
#include "core.h"


#include <cstdint>
#include <stdio.h>
#include <string>
#include <vector>
#include <iostream>


namespace Chordata {
	class I2C_Device{
		int i2c_file;
		std::string i2c_adapter;

	public:
		bool ready() const { return i2c_file > 0; }

		explicit I2C_Device(const std::string& adapter);

		I2C_Device (const I2C_Device& ) = delete;
		I2C_Device& operator=(const I2C_Device&) = delete;
		I2C_Device (I2C_Device&& ) = default;
		I2C_Device& operator=(I2C_Device&&) = default;

		int get_i2c_file() const{
			return i2c_file;
		}

		std::string get_i2c_str() const{
			return i2c_adapter;
		}

		bool init();
	};

	class IO_Base {
	public: 
		virtual ~IO_Base(){};
		virtual void write_byte(uint8_t address, uint8_t data) = 0;
		virtual void write_byte(uint8_t address, uint8_t sub_address, uint8_t data) = 0;
		virtual void write_bytes(uint8_t address, char *data, uint8_t count) = 0;
		virtual int32_t read_byte(uint8_t address) = 0;
		virtual int32_t read_byte(uint8_t address, uint8_t sub_address) = 0;
		virtual void read_bytes(uint8_t address, uint8_t sub_address, uint8_t * dest, uint8_t count) = 0;
		virtual std::vector<uint8_t> read_bytes(uint8_t address, uint8_t sub_address, uint8_t count) = 0;
		virtual std::vector<char> i2c_rdwr(uint8_t address, char sub_address, uint8_t count)  = 0;


		virtual std::string get_adapter_str() const{
			return "/dev/null";
		}
		
	};


	class I2C_io: public IO_Base {
		I2C_Device *adapter;
	
		explicit I2C_io(I2C_Device *_adapter):
		adapter(_adapter)
		{};

		explicit I2C_io(const char *_adapter):
			adapter(new I2C_Device(_adapter))
		{};

		explicit I2C_io(const std::string& _adapter):
		adapter(new I2C_Device(_adapter))
		{};

		// i2c_write_byte() -- Write a byte out of I2C directly into the device (no register set)
		// Input:
		//	- address = The 7-bit I2C address of the slave device.
		//	- data = Byte to be written to the register.
		void i2c_write_byte(uint8_t address, uint8_t data);
		
		void i2c_write_bytes(uint8_t address, char *data, uint8_t count);

		// i2c_write_byte() -- Write a byte out of I2C to a register in the device
		// Input:
		//	- address = The 7-bit I2C address of the slave device.
		//	- sub_address = The register to be written to.
		//	- data = Byte to be written to the register.
		void i2c_write_byte(uint8_t address, uint8_t sub_address, uint8_t data);

		// i2c_read_byte() -- Read a single byte from a register over I2C.
		// Input:
		//	- address = The 7-bit I2C address of the slave device.
		// Output:
		//	- The byte read from the requested address. (returned as a 32bit signed in order to match the SMBus return type)
		int32_t i2c_read_byte(uint8_t address);

		// i2c_read_byte() -- Read a single byte from a register over I2C.
		// Input:
		//	- address = The 7-bit I2C address of the slave device.
		//	- sub_address = The register to be read from.
		// Output:
		//	- The byte read from the requested address. (returned as a 32bit signed in order to match the SMBus return type)
		int32_t i2c_read_byte(uint8_t address, uint8_t sub_address);
		
		// i2c_read_bytes() -- Read a series of bytes, starting at a register via I2C
		// Input:
		//	- address = The 7-bit I2C address of the slave device.
		//	- sub_address = The register to begin reading.
		// 	- * dest = Pointer to an array where we'll store the readings.
		//	- count = Number of registers to be read.
		// Output: No value is returned by the function, but the registers read are
		// 		all stored in the *dest array given.
		void i2c_read_bytes(uint8_t address, uint8_t sub_address, uint8_t * dest, uint8_t count);

	public:
		friend Notochord_Runtime;
		bool ready() const { return adapter->ready(); }

		I2C_io(I2C_io &other) = delete;
		void operator=(const I2C_io &) = delete;

		~I2C_io() override {
			delete adapter;
		};

		std::string get_adapter_str() const override {
			return adapter->get_i2c_str();
		}	

		inline int get_adapter_i2c_file() const{
			return adapter->get_i2c_file();
		}		

		void write_bytes(uint8_t address, char *data, uint8_t count) override {
			i2c_write_bytes(address, data, count);
		};

		void write_byte(uint8_t address, uint8_t data) override {
			i2c_write_byte(address, data);
		}
				
		void write_byte(uint8_t address, uint8_t sub_address, uint8_t data) override {
			i2c_write_byte(address, sub_address, data);
		}
		
		int32_t read_byte(uint8_t address) override {
			return i2c_read_byte(address);
		}		
		
		int32_t read_byte(uint8_t address, uint8_t sub_address) override{
			return i2c_read_byte(address, sub_address);
		}

		void read_bytes(uint8_t address, uint8_t sub_address, uint8_t * dest, uint8_t count) override{
			i2c_read_bytes(address, sub_address, dest, count);
		}

		std::vector<uint8_t> read_bytes(uint8_t address, uint8_t sub_address, uint8_t count){
			std::vector<uint8_t> dest;
			dest.resize(count);

			i2c_read_bytes(address, sub_address, dest.data(), count);

			return dest;
		}

		// i2c_rdwr() -- Perform a combined r/w operation (one STOP only)
		// Input:
		//	- address = The 7-bit I2C address of the slave device.
		//	- sub_address = The register to begin reading.
		//	- count = Number of registers to be read.
		std::vector<char> i2c_rdwr(uint8_t address, char sub_address, uint8_t count) override;
	
	};

	struct I2C_read{
		uint8_t address;
		uint8_t sub_address;
		uint8_t count;
	};

	struct I2C_write{
		uint8_t address;
		uint8_t sub_address;
		std::vector<uint8_t> payload;
	};

	class Mock_I2C_io: public IO_Base {
	public:
		std::vector<I2C_read> reads;
		std::vector<I2C_write> writes;

		Mock_I2C_io(){};
		
		explicit Mock_I2C_io(I2C_Device *_adapter){};

		explicit Mock_I2C_io(const char *_adapter){};

		explicit Mock_I2C_io(const std::string& _adapter){};
	
		void write_byte(uint8_t address, uint8_t data) override;
		void write_byte(uint8_t address, uint8_t sub_address, uint8_t data) override;
		void write_bytes(uint8_t address, char *data, uint8_t count) override {};
		int32_t read_byte(uint8_t address) override;
		int32_t read_byte(uint8_t address, uint8_t sub_address) override;
		void read_bytes(uint8_t address, uint8_t sub_address, uint8_t * dest, uint8_t count) override;
		std::vector<uint8_t> read_bytes(uint8_t address, uint8_t sub_address, uint8_t count) override;
		std::vector<char> i2c_rdwr(uint8_t address, char sub_address, uint8_t count) override;
		void reset();

	};

}

#endif