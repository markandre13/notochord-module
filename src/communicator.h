// -- START OF COPYRIGHT & LICENSE NOTICES -- 
//
// Chordata Motion's Notochord (python module)
// -- Core implementation of Chordata Motion's Open Source motion capture framework
//
// http://chordata.cc
// contact@chordata.cc
//
//
// Copyright 2018-2021 Bruno Laurencich
// Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
//
// This file is part of Chordata Motion's Notochord (python module).
//
// Chordata Motion's Notochord (python module) is free software: you can 
// redistribute it and/or modify it under the terms of the 
// GNU General Public License as published by the Free Software Foundation, 
// either version 3 of the License, or (at your option) any later version.
//
// Chordata client is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Chordata Motion's Notochord (python module).  
// If not, see <https://www.gnu.org/licenses/>.
//
//
// -- END OF COPYRIGHT & LICENSE NOTICES -- 

#ifndef __NOTOCHORD_COMMUNICATOR__
#define __NOTOCHORD_COMMUNICATOR__

#include <string>
#include <stdexcept>


#include "spdlog/spdlog.h"
#include "fmt/core.h"

#include "types.h"
#include "core.h"
#include "config.h"
#include "osc.h"
#include "logger.h"

#include <vector>

// Uncoment to compile with a higher level of trace outputs
// #define PEDANTIC_TRACE

namespace Chordata{

	class Communicator_Runtime{
	private:
		static comm_runtime_ptr _comm_instance;
		Communicator_Runtime();
		logger_ptr msg_logger, err_logger, t_logger;
		std::string current_file_path;
		uint16_t configured;
		Cython_Buffer cy_buffer; 
		
		void set_log_level(const Chordata::Configuration_Data&);

	public:
		friend Notochord_Runtime;
		
		static comm_runtime_ptr init();
		static bool is_active();
		static comm_runtime_ptr terminate();
		
		Communicator_Runtime(Communicator_Runtime &other) = delete;
		void operator=(const Communicator_Runtime &) = delete;
		~Communicator_Runtime();

		void configure(const Chordata::Configuration_Data&);

		void flush_msg();
		void flush_err();
		void flush_t();
		void flush_loggers();

		std::vector<std::string>& _get_registry(uint32_t level) {
			return cy_buffer.get_registry(level);
		};
		void _clear_registry(uint32_t level) {
			cy_buffer.clear_buffer(level);
		};
		Cython_Buffer _get_buffer();	
	};

	inline comm_runtime_ptr get_communicator(){ return Communicator_Runtime::init(); }
	inline comm_runtime_ptr terminate_communicator(){ return Communicator_Runtime::terminate(); };

	namespace Communicator{
		
		/*======================================================
		=                   Public utilities                   =
		======================================================*/

		inline logger_ptr get_logger(Logger_type r = MSG_LOG){
			return Chordata::get_runtime()->get_logger(r);
		}

		inline logger_ptr get_logger_msg(){return get_logger(MSG_LOG);}
		inline logger_ptr get_logger_err(){return get_logger(ERR_LOG);}
		inline logger_ptr get_logger_tra(){return get_logger(TRA_LOG);}

		inline void flush_loggers(){ get_runtime()->flush_loggers(); }	

		std::string get_trasmit_packet_hex();

		inline Cython_Buffer _get_buffer() {
			return Chordata::get_runtime()->_get_buffer();
		}

		inline std::vector<std::string>& _get_registry(uint32_t level) {
			return Chordata::get_runtime()->_get_registry(level);
		}

		inline void _clear_registry(uint32_t level) {
			return Chordata::get_runtime()->_clear_registry(level);
		}

		/*======================================================
		=            Communication public interface            =
		========================================================
		= Undescored functions are used as an interface to Cython,
		= Non underscored alias or overloads are provided for C++ use
		 */
		
		template <typename... Args>
		inline void _info(const char* fmt, const Args&... args){
			get_logger_msg()->info(fmt, args...);
		}

		template<typename... Args>
		constexpr auto info = _info<const Args&...>;

		template <typename Payload>
		inline void transmit(const std::string& subtarget, const Payload& payload){
			get_logger_tra()->transmit(subtarget, payload);
		}

		template <typename Payload>
		inline void _transmit(const std::string* subtarget, const Payload* payload){
			transmit(*subtarget, *payload);
		}

		template <typename Payload>
		inline void _transmit(const _Node &n, const Payload &payload){
			get_logger_tra()->transmit(n, payload);
		}

		template <typename Payload>
		inline void _transmit(const _Node *n, const Payload* payload){
			_transmit(*n, *payload);
		}

		inline void _transmit(const _KCeptor_Base &kc) {
			get_logger_tra()->transmit(kc);
		}

		inline void _transmit(const _KCeptor_Base *kc) {
			_transmit(*kc);
		}

		inline void _transmit(const _Armature &arm) {
			get_logger_tra()->transmit(arm);
		}

		inline void _transmit(const _Armature *arm) {
			_transmit(*arm);
		}

		template <typename... Args>
		inline void _error(const char* fmt, const Args&... args){
			get_logger_err()->error(fmt, args...);
		} 

		template<typename... Args>
		constexpr auto error = _error<const Args&...>;

		template <typename... Args>
		inline void _debug(const char* fmt, const Args&... args){
			get_logger_msg()->debug(fmt, args...);
		} 

		template<typename... Args>
		constexpr auto debug = _debug<const Args&...>;

		template <typename... Args>
		inline void _trace(const char* fmt, const Args&... args){
			get_logger_msg()->trace(fmt, args...);
		} 

		template<typename... Args>
		constexpr auto trace = _trace<const Args&...>;

		template <typename... Args>
		inline void _warn(const char* fmt, const Args&... args){
			get_logger_msg()->warn(fmt, args...);
		} 

		template<typename... Args>
		constexpr auto warn = _warn<const Args&...>;

		template <typename... Args>
		inline void _critical(const char* fmt, const Args&... args){
			get_logger_msg()->critical(fmt, args...);
		} 

		template<typename... Args>
		constexpr auto critical = _critical<const Args&...>;

	} // end namespace Communicator
} // end namespace Chordata

#endif
