// -- START OF COPYRIGHT & LICENSE NOTICES --
//
// Chordata Motion's Notochord (python module)
// -- Core implementation of Chordata Motion's Open Source motion capture framework
//
// http://chordata.cc
// contact@chordata.cc
//
//
// Copyright 2018-2021 Bruno Laurencich
// Copyright 2022 Bruno Laurencich, Ouassim Aouattah Akandouch
//
// This file is part of Chordata Motion's Notochord (python module).
//
// Chordata Motion's Notochord (python module) is free software: you can
// redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
//
// Chordata client is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Chordata Motion's Notochord (python module).
// If not, see <https://www.gnu.org/licenses/>.
//
//
// -- END OF COPYRIGHT & LICENSE NOTICES --

#include "communicator.h"
#include "config.h"
#include "osc.h"
#include "logger.h"
#include "math_types.h"

#include <memory>
#include <vector>
#include <iostream>
#include "spdlog/spdlog.h"
#include "spdlog/sinks/null_sink.h"
#include "spdlog/sinks/stdout_color_sinks.h"
#include "spdlog/sinks/daily_file_sink.h"
#include "fmt/format.h"

using namespace Chordata;

comm_runtime_ptr Communicator_Runtime::_comm_instance{nullptr};

// #TODO MOVE
Output_Redirect Communicator::Logger::get_sinks() const {
	Output_Redirect redir = Chordata::NONE;
	for (const auto sink : logger_ref->sinks()){
		if (dynamic_cast<spdlog::sinks::stdout_color_sink_mt*>(sink.get())){
			redir |= Chordata::STDOUT;
		} else if (dynamic_cast<spdlog::sinks::stderr_color_sink_mt*>(sink.get())){
			redir |= Chordata::STDERR;
		} else if (dynamic_cast<spdlog::sinks::daily_file_sink_mt*>(sink.get())){
			redir |= Chordata::FILE;
		} else if (dynamic_cast<Chordata::Communicator::OscOutUdp<false>*>(sink.get())){
			redir |= Chordata::OSC;
		} else if (dynamic_cast<Chordata::Communicator::OscOutUdp<true>*>(sink.get())){
			redir |= Chordata::OSC;
		} else if (dynamic_cast<cython_sink_mt*>(sink.get())){
			redir |= Chordata::PYTHON;
		} else if (dynamic_cast<Chordata::Communicator::OscOutWebsocket<false>*>(sink.get())){
			redir |= Chordata::WEBSOCKET;
		} else if (dynamic_cast<Chordata::Communicator::OscOutWebsocket<true>*>(sink.get())){
			redir |= Chordata::WEBSOCKET;
		}
	}

	if ( has_osc_sink ){
		redir |= Chordata::OSC;
	}
	if ( has_ws_sink ){
		redir |= Chordata::WEBSOCKET;
	}

	return redir;
}


/*======================================================
=                 Communicator_Runtime                 =
======================================================*/
comm_runtime_ptr Communicator_Runtime::init() {
	if(_comm_instance == nullptr) {
		_comm_instance = std::shared_ptr<Communicator_Runtime>(new Communicator_Runtime());
	}
	return _comm_instance;
}

bool Communicator_Runtime::is_active() {
	return _comm_instance != nullptr;
}

comm_runtime_ptr Communicator_Runtime::terminate() {
	if(_comm_instance != nullptr) {
		delete _comm_instance.get();
		_comm_instance = nullptr;
	}
	return _comm_instance;
}

Communicator_Runtime::Communicator_Runtime():
	current_file_path(""),
	configured(0)
	{
	auto null_sink = std::make_shared<spdlog::sinks::null_sink_st>();

	msg_logger =
		std::make_shared<Chordata::Communicator::Logger>("Communication logger", null_sink);

	err_logger =
		std::make_shared<Chordata::Communicator::Logger>("Error logger", null_sink);

	t_logger =
		std::make_shared<Chordata::Communicator::Logger>("Transmit logger", null_sink, true);

}

Communicator_Runtime::~Communicator_Runtime(){
	spdlog::shutdown();
}

class millis_flag : public spdlog::custom_flag_formatter
{
public:
	void format(const spdlog::details::log_msg &, const std::tm &, spdlog::memory_buf_t &dest) override
	{

		std::string some_txt = fmt::format("{:>8d}",Chordata::runtime_millis());
		dest.append(some_txt.data(), some_txt.data() + some_txt.size());
	}

	std::unique_ptr<custom_flag_formatter> clone() const override
	{
		return spdlog::details::make_unique<millis_flag>();
	}
};

void Communicator_Runtime::configure(const Chordata::Configuration_Data& config){
	auto stdout_sink = std::make_shared<spdlog::sinks::stdout_color_sink_mt>();
	auto stderr_sink = std::make_shared<spdlog::sinks::stderr_color_sink_mt>();
	auto file_sink = std::make_shared<spdlog::sinks::daily_file_sink_mt>(config.comm.filepath, 0, 0 );
	current_file_path = file_sink->filename();

	osc_sink_ptr osc_sink, ws_sink;
	if (!config.comm.use_bundles){
		osc_sink = std::make_shared< Chordata::Communicator::OscOutUdp<false> >(config);
		ws_sink = std::make_shared<Chordata::Communicator::OscOutWebsocket<false>>(config);
	}
	else {
		osc_sink = std::make_shared< Chordata::Communicator::OscOutUdp<true> >(config);
		ws_sink = std::make_shared<Chordata::Communicator::OscOutWebsocket<true>>(config);
	}


	auto cython_sink = std::make_shared<cython_sink_mt>(&cy_buffer);

	auto configure_logger = //start lambda
		[&config, stdout_sink, stderr_sink, file_sink, osc_sink, cython_sink, ws_sink]
		(logger_ptr which, const Output_Redirect redir)
		{
			which->clear_sinks();

			for (unsigned i = 0, mask; i<= OUTPUT_REDIRECT_N; ++i ){
				mask = 1 << (i-1);
				switch (redir & mask){
					case Chordata::NONE:
					break;
					case Chordata::STDOUT:
						which->add_sink(stdout_sink, Chordata::STDOUT);
					break;
					case Chordata::STDERR:
						which->add_sink(stderr_sink, Chordata::STDERR);
					break;
					case Chordata::FILE:
						which->add_sink(file_sink, Chordata::FILE);
					break;
					case Chordata::OSC:
						which->add_sink(osc_sink, Chordata::OSC);
					break;
					case Chordata::PYTHON:
						which->add_sink(cython_sink, Chordata::PYTHON);
					break;
					case Chordata::WEBSOCKET:
						which->add_sink(ws_sink, Chordata::WEBSOCKET);
					break;
				}
			}
		};// end lambda configure_logger


	configure_logger(msg_logger, config.comm.msg);
	configure_logger(err_logger, config.comm.error);
	configure_logger(t_logger, config.comm.transmit);

	set_log_level(config);

	auto formatter = std::make_unique<spdlog::pattern_formatter>();
    formatter->add_flag<millis_flag>('*').set_pattern(std::move(_CHORDATA_LOGGING_PATTERN));

	err_logger->set_formatter(std::move(formatter->clone()));
	t_logger->set_formatter(std::move(formatter->clone()));
	msg_logger->set_formatter(std::move(formatter));

	configured ++;
}

void Communicator_Runtime::set_log_level(const Chordata::Configuration_Data& conf){
	spdlog::level::level_enum log_level, err_level;

	if (conf.comm.log_level >= 4) {
		log_level = spdlog::level::critical;
	} else if (conf.comm.log_level == 3) {
		log_level = spdlog::level::warn;
	} else if (conf.comm.log_level == 2){
		log_level = spdlog::level::trace;
	} else if (conf.comm.log_level == 1){
		log_level = spdlog::level::debug;
	} else {
		log_level = spdlog::level::info;
	}

	err_level = (conf.comm.log_level == 0)?
		spdlog::level::err : spdlog::level::warn;

	msg_logger->set_level(log_level);
	err_logger->set_level(err_level);

}

void Communicator_Runtime::flush_msg(){
	msg_logger->flush();
}

void Communicator_Runtime::flush_err(){
	err_logger->flush();
}

void Communicator_Runtime::flush_t(){
	t_logger->flush_osc();
	t_logger->flush_websocket();
	t_logger->flush();
}

void Communicator_Runtime::flush_loggers(){
	flush_msg();
	flush_err();
	flush_t();
}

Cython_Buffer Communicator_Runtime::_get_buffer() {
	return cy_buffer;
}

/*======================================================
=                   Public utilities                   =
======================================================*/


std::string Communicator::get_trasmit_packet_hex(){return get_logger_tra()->get_trasmit_packet_hex();}

