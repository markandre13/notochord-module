// -- START OF COPYRIGHT & LICENSE NOTICES --
//
// Chordata Motion's Notochord (python module)
// -- Core implementation of Chordata Motion's Open Source motion capture framework
//
// http://chordata.cc
// contact@chordata.cc
//
//
// Copyright 2022-2023 Joan Prim Armengol
//
// This file is part of Chordata Motion's Notochord (python module).
//
// Chordata Motion's Notochord (python module) is free software: you can
// redistribute it and/or modify it under the terms of the
// GNU General Public License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
//
// Chordata client is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Chordata Motion's Notochord (python module).
// If not, see <https://www.gnu.org/licenses/>.
//
//
// -- END OF COPYRIGHT & LICENSE NOTICES --
#include "web_socket.h"
#include "types.h"

void ws_worker_run(Chordata::WebSocket* ws) {
	ws->loop();
}

namespace Chordata {
	static void destroy_message(void* msg) {
		struct Chordata::msg_data* _msg = (struct Chordata::msg_data*)msg;
		delete _msg->data;
		_msg->size = 0;
	}

	static void free_element_from_ring(struct vhost_protocol_data* vhd) {
		uint32_t oldest_tail = lws_ring_get_oldest_tail(vhd->ring_buffer);
		lws_start_foreach_ll_safe(struct client_data*, pss, vhd->clients, pss_list) {
			if(pss->tail == oldest_tail) {
				// Free up one element
				lws_ring_consume_and_update_oldest_tail(vhd->ring_buffer, struct client_data,
					&pss->tail, 1, vhd->clients, tail, pss_list);
			}
		} lws_end_foreach_ll_safe(pss);
	}

	static int
		callback_minimal(struct lws *wsi, enum lws_callback_reasons reason,
				void *user, void *in, size_t len)
		{
			auto pss = (struct client_data*)user;
			lws_protocols* p = (lws_protocols*)lws_get_protocol(wsi);
			struct vhost_protocol_data* vhd;
			if (p) {
				vhd = (struct vhost_protocol_data*)p->user;
			}
			switch (reason) {
				case LWS_CALLBACK_PROTOCOL_INIT:
					{
						vhd->ring_buffer = lws_ring_create(sizeof(struct msg_data), 128, &destroy_message);
						vhd->clients = nullptr;
						break;
					}

				case LWS_CALLBACK_PROTOCOL_DESTROY:
					{
						lws_ring_destroy(vhd->ring_buffer);

						break;
					}

				case LWS_CALLBACK_ESTABLISHED:
					{
						lws_ll_fwd_insert(pss, pss_list, vhd->clients);
						pss->wsi = wsi;
						pss->tail = lws_ring_get_oldest_tail(vhd->ring_buffer);
						if(lws_ring_get_count_waiting_elements(vhd->ring_buffer, &pss->tail)) {
							lws_callback_on_writable(pss->wsi);
						}
						break;
					}

				case LWS_CALLBACK_CLOSED:
					{
						lws_ll_fwd_remove(struct client_data, pss_list, pss, vhd->clients);
						break;
					}

				case LWS_CALLBACK_SERVER_WRITEABLE:
					{
						int m;
						struct msg_data* msg = nullptr; 
						if(lws_ring_get_count_waiting_elements(vhd->ring_buffer, &pss->tail))
						{
							//std::lock_guard<std::mutex> lock(msgs_mutex);
							//pkt = msgs.front();
							msg = (struct msg_data*)lws_ring_get_element(vhd->ring_buffer, &pss->tail);
						}
						else break;
						auto protocol = vhd->binary ? LWS_WRITE_BINARY : LWS_WRITE_TEXT;
						m = lws_write(wsi, (unsigned char*)msg->data+LWS_PRE, msg->size, protocol);
						//lwsl_notice("Message sent %s", pkt.data()+LWS_PRE);
						if (m < msg->size) {
							lwsl_err("ERROR %d writing to ws\n", m);
						}
						{
							//std::lock_guard<std::mutex> lock(msgs_mutex);
							lws_ring_consume_and_update_oldest_tail(vhd->ring_buffer, struct client_data, 
									&pss->tail, 1, vhd->clients, tail, pss_list);
							//msgs.pop_front();
						}
						if(lws_ring_get_count_waiting_elements(vhd->ring_buffer, &pss->tail))
							lws_callback_on_writable(wsi);

						break;
					}
				default:
					break;
			}

			return 0;
		}
}

Chordata::WebSocket::WebSocket(const Configuration_Data& conf) : interrupt(false) {
	std::string tra_address, msg_address, error_address;
	// websockets library from Python doesn't allow '/' in the subprotocol name. Couldn't
	// find any info if it's a forbidden character in subprotocol name or the author
	// decided to ban it. Some testing or further investigation to know if it's
	// allowed to use the configuration addresses as websocket subprotocol name.
	if(conf.osc.base_address[0] == '/') {
		tra_address = conf.osc.base_address.substr(1);
	}
	protocol_names.emplace_back(std::move(tra_address));
	if(conf.osc.base_address[0] == '/') {
		msg_address = conf.osc.base_address.substr(1);
	}
	if(conf.osc.msg_address[0] == '/') {
		msg_address += '_';
		msg_address += conf.osc.msg_address.substr(1);
	}
	protocol_names.emplace_back(std::move(msg_address));
	if(conf.osc.base_address[0] == '/') {
		error_address = conf.osc.base_address.substr(1);
	}
	if(conf.osc.error_address[0] == '/') {
		error_address += '_';
		error_address += conf.osc.error_address.substr(1);
	}
	protocol_names.emplace_back(std::move(error_address));

	auto t = new vhost_protocol_data;
	t->binary = true;
	auto m = new vhost_protocol_data;
	m->binary = false;
	auto e = new vhost_protocol_data;
	e->binary = false;

	protocols = {
		{protocol_names[0].c_str(),
			Chordata::callback_minimal,
			sizeof(struct client_data),
			1024,
			0, t, 0},
		{protocol_names[1].c_str(),
			Chordata::callback_minimal,
			sizeof(struct client_data),
			1024,
			0, m, 0},
		{protocol_names[2].c_str(),
			Chordata::callback_minimal,
			sizeof(struct client_data),
			1024,
			0, e, 0},
		LWS_PROTOCOL_LIST_TERM
	};

	pprotocols.emplace(conf.osc.base_address, &protocols[0]);
	pprotocols.emplace(conf.osc.base_address+conf.osc.msg_address, &protocols[1]);
	pprotocols.emplace(conf.osc.base_address+conf.osc.error_address, &protocols[2]);

	struct lws_context_creation_info info;
	// A custom function can be passed to handle logging
	// https://libwebsockets.org/lws-api-doc-v4.3-stable/html/group__log.html#ga36dda7d78375812fb9fbbcbc7d4ed52b
#ifdef NOTOCHORD_TRACE_WEBSOCKET
	// Enable loggin for LWS
	lws_set_log_level(LLL_ERR|LLL_WARN|LLL_NOTICE, NULL);
#else
	// Disable logging for LWS
	lws_set_log_level(0, NULL);
#endif

	memset(&info, 0, sizeof info); /* otherwise uninitialized garbage */
	info.port = conf.comm.websocket_port;
	info.protocols = protocols.data();
	info.user = this;
	info.options =LWS_SERVER_OPTION_VALIDATE_UTF8 |
		LWS_SERVER_OPTION_HTTP_HEADERS_SECURITY_BEST_PRACTICES_ENFORCE;

	context = lws_create_context(&info);

	worker = std::thread(ws_worker_run, this);
}

void Chordata::WebSocket::send(const char *data, size_t size) {
	std::string protocol = "/Chordata";
	this->send(data, size, protocol);
}

void Chordata::WebSocket::send(const char* data, size_t size, const std::string& protocol) {
	auto p = pprotocols.find(protocol);
	if(p != pprotocols.end()) {

		unsigned char* d = new unsigned char[size+LWS_PRE];
		memcpy(d+LWS_PRE, data, size);
		msg_data msg;
		msg.data = d;
		msg.size = size;
		struct vhost_protocol_data* vhd = (struct vhost_protocol_data*)p->second->user;
		if(lws_ring_get_count_free_elements(vhd->ring_buffer) == 0) {
			free_element_from_ring(vhd);
		}
		lws_ring_insert(vhd->ring_buffer, &msg, 1);
		lws_callback_on_writable_all_protocol(context, p->second);
		// p->second->send(d, size);
	}
}

void Chordata::WebSocket::stop() {
	interrupt = true;
	lws_cancel_service(context);
}

void Chordata::WebSocket::loop() {
	int n = 0;
	while (n >= 0 && !interrupt)
		n = lws_service(context, 0);
}

