OSC BUFFER NOTES
================

A buffer of 512 is ok for strings, supporting long ones such as:

```Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
```

A buffer of 128 should be ok for normal payload, example of long message:

```
000:2f43686f 72646174 612f7261 772f612d     /Cho rdat a/ra w/a-
016:7265616c 6c792d6c 6f6e672d 73756274     real ly-l ong- subt
032:61726765 74000000 2c696969 69696969     arge t... ,iii iiii
048:69690000 11aabbcc 11aabbcc 11aabbcc     ii.. .... .... ....
064:11aabbcc 11aabbcc 11aabbcc 11aabbcc     .... .... .... ....
080:11aabbcc 11aabbcc                       .... ....
```

The bundle header requires 16, and an additional 4 for each message
