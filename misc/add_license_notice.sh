#!/bin/bash
set -eu

#This script adds or updates the license notice in all source
#files of this repository

export SCRIPT_DIR=$(dirname "$0")

#Create py and C notices
cp $SCRIPT_DIR/_license_notice.txt $SCRIPT_DIR/_py_license_notice.txt
sed "s|^#|//|" $SCRIPT_DIR/_license_notice.txt > $SCRIPT_DIR/_c_license_notice.txt

SCRS=$(find $SCRIPT_DIR/../src -type f -name "*.h" \
		-o \
		-type f -name "*.cpp" \
		)

SCRATCH_CPP=$(find $SCRIPT_DIR/../test -type f -name "*.cpp" )


PYXS=$(find $SCRIPT_DIR/../pyx -type f -name "*.py" \
		-o -type f -name "*.pyx" \
		-o -type f -name "*.pxd" \
		-o -type f -name "*.pxi" \
		)

TEST=$(find $SCRIPT_DIR/../test -type f -name "*.py" \
		-not -path "*/copp_server/*" \
		-not -path "*/scratchpad/*" \
		-o -type f -name "*.pxd" \
		-o -type f -name "*.pxi" \
		)
		# -o -type f -name "*.cpp" \

C_FILES="$SCRS $SCRATCH_CPP"
PY_FILES="$PYXS $TEST"

# PY_FILES="misc/test__.py"
# NOTICE=$(cat $NOTICE_FILE)
# NOTICE=$(echo ${NOTICE} | tr '\n' "\\n")
TEST_HAS_LIC_NOTICE="^[#/]+ -- END OF COPYRIGHT & LICENSE NOTICES -- "

function update_notice {
	local FILE=$1
	local NOTICE_FILE=$2
	if [  ! -z "$(cat $FILE | grep -E "$TEST_HAS_LIC_NOTICE")" ]; then
		echo Removing old notice to $FILE
		sed -E -i "1,/$TEST_HAS_LIC_NOTICE/d" $FILE
		sed -i "0,/^.*/d" $FILE
	fi
	echo Adding notice to $FILE
	echo -e "\\n" > $FILE.tmp
	cat $FILE >> $FILE.tmp
	cat $NOTICE_FILE $FILE.tmp > $FILE
	rm $FILE.tmp

}

for FILE in $PY_FILES ; do
	update_notice $FILE $SCRIPT_DIR/_py_license_notice.txt
done

for FILE in $C_FILES ; do
	update_notice $FILE $SCRIPT_DIR/_c_license_notice.txt
done

rm $SCRIPT_DIR/_py_license_notice.txt
rm $SCRIPT_DIR/_c_license_notice.txt
